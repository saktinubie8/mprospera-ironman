package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;

import org.springframework.data.domain.Page;

import id.co.telkomsigma.btpns.mprospera.model.survey.Survey;

public interface SurveyManager {

    void save(Survey sentra);

    Page<Survey> findByCreatedDate(Date startDate, Date endDate, String officeId);

    void clearCache();

}