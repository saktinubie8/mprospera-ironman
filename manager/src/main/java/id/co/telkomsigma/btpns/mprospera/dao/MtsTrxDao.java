package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.prs.MtsTrx;


public interface MtsTrxDao extends JpaRepository<MtsTrx, Long> {

    @Query("SELECT a"
            + " FROM MtsTrx a "
            + "  WHERE DATEDIFF(DAY,a.approvedDate,:adate)=0"
            + "  AND a.approvedBy IN  :approveBy "    //if per Wisma
            + "   ORDER BY a.approvedDate")
    List<MtsTrx> findByApprovedDateByApprovedByOrderByApprovedDate(@Param("adate") Date aDate, @Param("approveBy") List<String> listUserMS);

}
