package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

import java.util.List;

public interface CustomerManager {

    void clearCache();

    List<Customer> getBySentraId(Sentra sentra);

    Customer findById(long parseLong);

    Customer getCustomerByCifId(String cifId);

    List<CustomerPRS> getCustomerPRSByPrsId(PRS prs);

    CustomerPRS getCustomerPRSById(Long customerId);

    CustomerPRS findTopCustomerPRSbyCustomer(Customer customer);

}