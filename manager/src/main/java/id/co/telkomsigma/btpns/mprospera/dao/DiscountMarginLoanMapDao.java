package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginLoanMap;

/**
 * Created by Dzulfiqar on 23/05/2017.
 */
public interface DiscountMarginLoanMapDao extends JpaRepository<DiscountMarginLoanMap, Long> {

    List<DiscountMarginLoanMap> findByDiscountMarginId(Long discountMarginId);

    DiscountMarginLoanMap findByLoanPrsIdAndDiscountMarginId(Long loanPrsId, Long discountMarginId);

}
