package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.dao.*;
import id.co.telkomsigma.btpns.mprospera.model.prs.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.telkomsigma.btpns.mprospera.manager.PRSManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

@Service("prsManager")
public class PRSManagerImpl implements PRSManager {

    protected Log log = LogFactory.getLog(this.getClass());

    @Autowired
    private PRSDao prsDao;

    @Autowired
    private PRSPhotoDao prsPhotoDao;

    @Autowired
    private CustomerPRSDao customerPrsDao;

    @Autowired
    private LoanPRSDao loanPrsDao;

    @Autowired
    private SavingPRSDao savingPrsDao;

    @Autowired
    private PRSParamDao prsParameterDao;

    @Autowired
    private ReportTrxDao reportTrxDao;

    @Autowired
    private ReportCollectionDao reportCollectionDao;

    @Autowired
    private MutationDao mutationDao;

    @Transactional
    @Override
    public PRS getPRSById(Long prsId) {
        // TODO Auto-generated method stub
        return prsDao.findByPrsId(prsId);
    }

    @Transactional
    @Override
    @Caching(evict = {
            @CacheEvict(value = "irn.prs.getPRSBySentra", key = "#prs.sentraId", beforeInvocation = true),
            @CacheEvict(value = "irn.saving.prs.findSavingRTBySentraId", allEntries = true, beforeInvocation = true)
    })
    public PRS savePRSEvent(PRS prs, List<CustomerPRS> customerPRSList, List<LoanPRS> loanPRSList, List<SavingPRS> savingPRSList) {
        // TODO Auto-generated method stub
        PRS prsFromDb = prsDao.findByPrsId(prs.getPrsId());
        if (prsFromDb != null) {
            prsFromDb.setLongitude(prs.getLongitude());
            prsFromDb.setLatitude(prs.getLatitude());
            prsFromDb.setPrsEndTime(prs.getPrsEndTime());
            prsFromDb.setPrsTime(prs.getPrsTime());
            prsFromDb.setPsCompanion(prs.getPsCompanion());
            prsFromDb.setCreatedBy(prs.getCreatedBy());
            prsFromDb.setSentraId(prs.getSentraId());
            prsFromDb.setStatus(prs.getStatus());
            prsFromDb.setUpdateBy(prs.getUpdateBy());
            prsFromDb.setUpdateDate(prs.getUpdateDate());
            prsFromDb.setActualMoney(prs.getActualMoney());
            prsFromDb.setBringMoney(prs.getBringMoney());
            prsFromDb.setPaymentMoney(prs.getPaymentMoney());
            prsFromDb.setWithdrawalAdhoc(prs.getWithdrawalAdhoc());
            prsDao.save(prsFromDb);
        }
        for (CustomerPRS customerPRS : customerPRSList) {
            CustomerPRS customerPrsFromDb = customerPrsDao.findOne(customerPRS.getCustomerPrsId());
            if (customerPrsFromDb != null) {
                customerPrsFromDb.setIsAttend(customerPRS.getIsAttend());
                customerPrsFromDb.setNotAttendReason(customerPRS.getNotAttendReason());
                customerPrsFromDb.setUpdatedBy(customerPRS.getUpdatedBy());
                customerPrsFromDb.setUpdatedDate(customerPRS.getUpdatedDate());
                customerPrsDao.save(customerPrsFromDb);
            }
        }
        for (LoanPRS loanPRS : loanPRSList) {
            LoanPRS loanPrsFromDb = loanPrsDao.findOne(loanPRS.getLoanPRSId());
            if (loanPrsFromDb != null) {
                loanPrsFromDb.setCanPendingDisbursement(loanPRS.getCanPendingDisbursement());
                loanPrsFromDb.setCurrentMargin(loanPRS.getCurrentMargin());
                loanPrsFromDb.setDisbursementAmount(loanPRS.getDisbursementAmount());
                loanPrsFromDb.setDisbursementFlag(loanPRS.getDisbursementFlag());
                loanPrsFromDb.setEarlyTerminationReason(loanPRS.getEarlyTerminationReason());
                loanPrsFromDb.setInstallmentAmount(loanPRS.getInstallmentAmount());
                loanPrsFromDb.setInstallmentPaymentAmount(loanPRS.getInstallmentPaymentAmount());
                loanPrsFromDb.setIsEarlyTermination(loanPRS.getIsEarlyTermination());
                loanPrsFromDb.setIsEarlyTerminationAdHoc(loanPRS.getIsEarlyTerminationAdHoc());
                loanPrsFromDb.setEarlyTerminationPlan(loanPRS.getEarlyTerminationPlan());
                loanPrsFromDb.setMarginAmount(loanPRS.getMarginAmount());
                loanPrsFromDb.setMarginDiscountDeviationAmount(loanPRS.getMarginDiscountDeviationAmount());
                loanPrsFromDb.setMarginDiscountDeviationPercentage(loanPRS.getMarginDiscountDeviationPercentage());
                loanPrsFromDb.setMarginDiscountDeviationFlag(loanPRS.getMarginDiscountDeviationFlag());
                loanPrsFromDb.setOutstandingAmount(loanPRS.getOutstandingAmount());
                loanPrsFromDb.setOutstandingCount(loanPRS.getOutstandingCount());
                loanPrsFromDb.setOverdueDays(loanPRS.getOverdueDays());
                loanPrsFromDb.setPlafonAmount(loanPRS.getPlafonAmount());
                loanPrsFromDb.setRemainingPrincipal(loanPRS.getRemainingPrincipal());
                loanPrsFromDb.setUpdatedBy(loanPRS.getUpdatedBy());
                loanPrsFromDb.setUpdatedDate(loanPRS.getUpdatedDate());
                loanPrsFromDb.setUseEmergencyFund(loanPRS.getUseEmergencyFund());
                loanPrsFromDb.setWowIbStatus(loanPRS.getWowIbStatus());
                loanPrsDao.save(loanPrsFromDb);
            }
        }
        for (SavingPRS savingPRS : savingPRSList) {
            SavingPRS savingPrsFromDb = savingPrsDao.findOne(savingPRS.getAccountId());
            if (savingPrsFromDb != null) {
                savingPrsFromDb.setClearBalance(savingPRS.getClearBalance());
                savingPrsFromDb.setDepositAmount(savingPRS.getDepositAmount());
                savingPrsFromDb.setHoldBalance(savingPRS.getHoldBalance());
                savingPrsFromDb.setIsCloseSavingAccount(savingPRS.getIsCloseSavingAccount());
                savingPrsFromDb.setNextWithdrawalAmountPlan(savingPRS.getNextWithdrawalAmountPlan());
                savingPrsFromDb.setUpdatedBy(savingPRS.getUpdatedBy());
                savingPrsFromDb.setUpdatedDate(savingPRS.getUpdatedDate());
                savingPrsFromDb.setWithdrawalAmount(savingPRS.getWithdrawalAmount());
                savingPrsFromDb.setWithdrawalAmountPlan(savingPRS.getWithdrawalAmountPlan());
                savingPrsDao.save(savingPrsFromDb);
            }
        }
        return prs;
    }

    @Override
    @Cacheable(value = "irn.prs.getPRSBySentra", unless = "#result == null")
    public PRS getPRSBySentra(Sentra sentra, Date createdDate) {
        return prsDao.findTopBySentraIdAndPrsDateOrderByPrsDateDesc(sentra, createdDate);
    }

    @Override
    public PRS getPRSBySentraId(Sentra sentra) {
        return prsDao.findTopBySentraIdOrderByPrsDateDesc(sentra);
    }

    @Override
    public PRS getPRSBySentraPrevious(Sentra sentra, Date prsDate) {
        // TODO Auto-generated method stub
        return prsDao.findTopBySentraIdAndPrsDateLessThanEqualOrderByPrsDateDesc(sentra, prsDate);
    }

    @Override
    public PRSPhoto getPrsPhotoByPrs(PRS prs) {
        // TODO Auto-generated method stub
        return prsPhotoDao.findByPrsId(prs);
    }

    @Override
    @CacheEvict(value = {"irn.prs.getPRSBySentra"},
            allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "irn.prs.getPRSBySentra", key = "#prs.sentraId", beforeInvocation = true)
    })
    public void savePRS(PRS prs) {
        // TODO Auto-generated method stub
        PRS prsFromDb = prsDao.findByPrsId(prs.getPrsId());
        if (prsFromDb != null) {
            prsFromDb.setLongitude(prs.getLongitude());
            prsFromDb.setLatitude(prs.getLatitude());
            prsFromDb.setPrsEndTime(prs.getPrsEndTime());
            prsFromDb.setPsCompanion(prs.getPsCompanion());
            prsFromDb.setCreatedBy(prs.getCreatedBy());
            prsFromDb.setSentraId(prs.getSentraId());
            prsFromDb.setStatus(prs.getStatus());
            prsFromDb.setUpdateBy(prs.getUpdateBy());
            prsFromDb.setUpdateDate(prs.getUpdateDate());
            prsFromDb.setActualMoney(prs.getActualMoney());
            prsFromDb.setBringMoney(prs.getBringMoney());
            prsFromDb.setPaymentMoney(prs.getPaymentMoney());
            prsFromDb.setWithdrawalAdhoc(prs.getWithdrawalAdhoc());
            prsDao.save(prsFromDb);
        }
        prsDao.save(prs);
    }

    @Override
    public void saveToReport(ReportTrxTable reportTrxTable) {
        reportTrxDao.save(reportTrxTable);
    }

    @Override
    public void saveToReportCollection(ReportCollection reportCollection) {
        reportCollectionDao.save(reportCollection);
    }

    @Override
    public void saveMutation(MutationTrxTabel mutationTrxTabel) {
        log.info("Accessing Dao Save Mutation");
        mutationDao.save(mutationTrxTabel);
    }

    @Override
    public MutationTrxTabel findMutationByPrsId(Long id) {
        return mutationDao.findByPrsId(id);
    }

    @Override
    public List<ReportTrxTable> getRptTrxByMSInCOT(Date aDate, String userMS) {
        return reportTrxDao.InCOT_findTopByApprovedByOrderByApprovedDateDesc(aDate, userMS);
    }

    @Override
    public List<ReportTrxTable> getRptTrxByMSOutCOT(Date aDate, String userMS) {
        return reportTrxDao.OutCOT_findTopByApprovedByOrderByApprovedDateDesc(aDate, userMS);
    }

    @Override
    public BigDecimal getInAmountBeforeCOT(Date aDate, String userMS) {
        return reportTrxDao.sumAmountTrxInBeforeCOT(aDate, userMS);
    }

    @Override
    public BigDecimal getInAmountAfterCOT(Date aDate, String userMS) {
        return reportTrxDao.sumAmountTrxInAfterCOT(aDate, userMS);
    }

    @Override
    public BigDecimal getOutAmountAfterCOT(Date aDate, String userMS) {
        return reportTrxDao.sumAmountTrxInAfterCOT(aDate, userMS);
    }

    @Override
    public BigDecimal getAmountBeforeCOT(Date aDate, List<String> listTypeTrx, String userMS) {
        return reportTrxDao.sumAmountTrxBeforeCOT(aDate, listTypeTrx, userMS);
    }

    @Override
    public List<LoanPRS> getSentraHasPrsByLoanPrs(Long sentraId) {
        // TODO Auto-generated method stub
        return loanPrsDao.getSentraHasPrsByLoanPrs(sentraId);
    }

    @Override
    public List<SavingPRS> getSentraHasPrsBySavingPrs(Long sentraId) {
        // TODO Auto-generated method stub
        return savingPrsDao.getSentraHasPrsBySavingPrs(sentraId);
    }

    @Override
    public void savePrsPhoto(PRSPhoto prsPhoto) {
        prsPhotoDao.save(prsPhoto);

    }

    @Override
    public List<PRS> findByPrsAndPrsDate(String officeCode, Date prsDate) {
        // TODO Auto-generated method stub
        return prsDao.findByPrsAndPrsDate(officeCode, prsDate);
    }

    @Override
    public List<ReportCollection> findReportByAppIdCollection(String appId) {
        return reportCollectionDao.findByAppId(appId);
    }

    @Override
    public List<PRS> getPRSByPSAndStatus(String usernamePS, Date prsDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(prsDate);
        cal.add(Calendar.DATE, 1);
        return prsDao.findByPSAndStatusAndPrsDate(usernamePS, prsDate, cal.getTime());
    }

}