package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.List;


import id.co.telkomsigma.btpns.mprospera.model.claim.InsuranceClaim;
import id.co.telkomsigma.btpns.mprospera.model.claim.InsuranceClaimPhoto;

public interface InsuranceClaimManager {

    InsuranceClaim saveInsuranceClaim(InsuranceClaim insuranceClaim);

    InsuranceClaim findById(long parseLong);

    InsuranceClaimPhoto saveInsuranceClaimPhoto(InsuranceClaimPhoto photo);

    InsuranceClaimPhoto findInsuranceClaimPhotoById(long parseLong);

    List<InsuranceClaim> findInsuranceClaimIsNotApprovedByLocationId(String locationId);

}