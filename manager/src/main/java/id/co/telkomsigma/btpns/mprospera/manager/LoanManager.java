package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.DisbursementPhoto;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApproval;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApprovalAndHistory;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApprovalHistory;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginLoanMap;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginOfficerMap;
import id.co.telkomsigma.btpns.mprospera.model.prs.NonPRSPhoto;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;

public interface LoanManager {

    void save(DiscountMarginApproval discountMarginApproval);

    void save(DiscountMarginLoanMap discountMarginLoanMap);

    void save(DiscountMarginOfficerMap discountMarginOfficerMap);

    void save(DiscountMarginApprovalHistory discountMarginApprovalHistory);

    Boolean isNonPrsCustomer(Customer customerId);

    Page<Loan> findByCreatedDate(Date startDate, Date endDate, String officeId);

    void clearCache();

    Loan getLoanByAppId(String appId);

    Loan findById(long parseLong);

    List<LoanPRS> getLoanPRSByPrsId(PRS prs, List<CustomerPRS> customerPRSList);

    List<LoanPRS> getLoanPRSByPrsIdAndCustomer(PRS prs, CustomerPRS customer);

    List<Loan> getLoanCairBySenta(Long sentraId, Date startDate, Date endDate);

    LoanPRS getLoanPRSById(Long loanId);

    List<LoanPRS> getLoanPRSByAppId(String appId);

    LoanNonPRS saveLoanNonPRS(LoanNonPRS loanNonPRS);

    DisbursementPhoto getDisbursementPhoto(LoanPRS loan);

    NonPRSPhoto getNonPRSPhoto(Long loan);

    void saveDisbursementPhoto(DisbursementPhoto disbursementPhoto);

    void saveNonPrsPhoto(NonPRSPhoto nonPrsPhoto);

    List<LoanNonPRS> findLoanNonPRSByCustomer(Customer customer);

    Loan findByLoanId(Long loanId);

    LoanPRS findTopByLoanId(Loan loan);

    LoanNonPRS findLoanNonPRSById(Long id);

    LoanNonPRS findLoanNonPRSByLoanId(Loan loan);

    LoanPRS findLastLoanPRS(Loan loan, Date createdDate);

    List<DiscountMarginApproval> getDiscountMarginApprovalByCustomerId(Long customerId);

    DiscountMarginApproval getDiscountMarginApprovalById(Long discountMarginId);

    DiscountMarginApproval findLastDiscountMarginApprovalByCustomer(Long customerId);

    List<DiscountMarginLoanMap> getDiscountMarginLoanMapByDiscountMarginId(Long discountMarginId);

    List<DiscountMarginApprovalHistory> getDiscountMarginApprovalHistoryByDiscountMarginId(Long discountMarginId);

    List<DiscountMarginApprovalAndHistory> getDiscountMarginApprovalAndHistory(String userId);

    DiscountMarginApprovalHistory findByDiscountMarginIdAndLevel(Long discountMarginId, String level);

    List<Loan> findByCustomer(Customer customer);

    DiscountMarginLoanMap getDiscountMarginLoanMapByLoanPrsIdAndDiscountMarginId(Long loanPrsId,
                                                                                 Long discountMarginId);

    List<LoanPRS> getLoanPRSListByAppIdAndDisbursementAmount(String appId);

    List<LoanPRS> findLoanPRSByPrsId(PRS prs);

    LoanPRS findTopByAppIdOrderByLoanPRSIdDesc(String appId);

    List<LoanPRS> findBySentraAndPrsDateAndDisbursmentFlag(Long sentraId, Date prsDate, Date prsDate2, char disbursementFlag);

    List<DiscountMarginApproval> findByCustomerIdAndStatusNotRejected(Long customerId);

}