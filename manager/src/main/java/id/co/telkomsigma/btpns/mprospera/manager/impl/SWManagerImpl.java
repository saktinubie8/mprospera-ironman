package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import id.co.telkomsigma.btpns.mprospera.dao.LoanProductDao;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.SWDao;
import id.co.telkomsigma.btpns.mprospera.dao.SwIdPhotoDao;
import id.co.telkomsigma.btpns.mprospera.dao.SwSurveyPhotoDao;
import id.co.telkomsigma.btpns.mprospera.manager.SWManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwIdPhoto;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwSurveyPhoto;

@Service("swManager")
public class SWManagerImpl implements SWManager {

    @Autowired
    SWDao swDao;

    @Autowired
    SwIdPhotoDao swIdPhotoDao;

    @Autowired
    SwSurveyPhotoDao swSurveyPhotoDao;

    @Autowired
    private LoanProductDao loanProductDao;

    @Override
    public int countAll() {
        // TODO Auto-generated method stub
        return swDao.countAll();
    }

    @Override
    public Page<SurveyWawancara> findAll(List<String> kelIdList) {
        // TODO Auto-generated method stub
        return swDao.findByAreaIdIn(kelIdList, new PageRequest(0, countAll()));
    }

    @Override
    public Page<SurveyWawancara> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        return swDao.findByCreatedDate(kelIdList, startDate, cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public void save(SurveyWawancara surveyWawancara) {
        // TODO Auto-generated method stub
        swDao.save(surveyWawancara);
    }

    @Override
    public void save(SwIdPhoto swIdPhoto) {
        // TODO Auto-generated method stub
        swIdPhotoDao.save(swIdPhoto);
    }

    @Override
    public void save(SwSurveyPhoto SwSurveyPhoto) {
        // TODO Auto-generated method stub
        swSurveyPhotoDao.save(SwSurveyPhoto);
    }

    @Override
    public SwIdPhoto getSwIdPhoto(String swId) {
        // TODO Auto-generated method stub
        return swIdPhotoDao.getSwIdPhoto(Long.parseLong(swId));
    }

    @Override
    public SwSurveyPhoto getSwSurveyPhoto(String swId) {
        // TODO Auto-generated method stub
        return swSurveyPhotoDao.getSwSurveyPhoto(Long.parseLong(swId));
    }

    @Override
    public SurveyWawancara getSWById(String swId) {
        // TODO Auto-generated method stub
        return swDao.findSwBySwId(Long.parseLong(swId));
    }

    @Override
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    public LoanProduct findByProductId(String productId) {
        // TODO Auto-generated method stub
        return loanProductDao.findByProductId(Long.parseLong(productId));
    }

    @Override
    public SurveyWawancara findByCustomerId(Long customerId) {
        // TODO Auto-generated method stub
        return swDao.findByCustomerId(customerId);
    }

}