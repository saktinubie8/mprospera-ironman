package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.CustomerDao;
import id.co.telkomsigma.btpns.mprospera.dao.CustomerPRSDao;
import id.co.telkomsigma.btpns.mprospera.manager.CustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

@Service("customerManager")
public class CustomerManagerImpl implements CustomerManager {

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private CustomerPRSDao customerPRSDao;

    @Override
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    public Customer findById(long parseLong) {
        // TODO Auto-generated method stub
        return customerDao.findOne(parseLong);
    }

    @Override
    public Customer getCustomerByCifId(String cifId) {
        // TODO Auto-generated method stub
        return customerDao.findByCustomerCifNumber(cifId);
    }

    @Override
    public List<CustomerPRS> getCustomerPRSByPrsId(PRS prs) {
        // TODO Auto-generated method stub
        return customerPRSDao.findByPrsId(prs);
    }

    @Override
    public CustomerPRS getCustomerPRSById(Long customerId) {
        // TODO Auto-generated method stub
        return customerPRSDao.findOne(customerId);
    }

    @Override
    @Cacheable(value = "irn.customer.getBySentraId", unless = "#result == null")
    public List<Customer> getBySentraId(Sentra sentra) {
        // TODO Auto-generated method stub
        return customerDao.findBySentraId(sentra.getSentraId());
    }

    @Override
    public CustomerPRS findTopCustomerPRSbyCustomer(Customer customer) {
        // TODO Auto-generated method stub
        return customerPRSDao.findTopByCustomerIdOrderByPrsIdDesc(customer);
    }

}