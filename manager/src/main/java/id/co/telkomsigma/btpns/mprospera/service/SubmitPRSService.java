package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApproval;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginLoanMap;
import id.co.telkomsigma.btpns.mprospera.model.prs.EarlyTerminationPlan;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;
import id.co.telkomsigma.btpns.mprospera.request.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service("submitPRSService")
public class SubmitPRSService extends GenericService {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ParameterService parameterService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private SavingService savingService;

    @Value("${allow.submit:1}")
    private String allowSubmit;

    @Autowired
    private PRSService prsService;

    public void submitManualKFO(SubmitPRSRequest request, PRS prs, List<CustomerPRS> customerPRSForUpdate, List<LoanPRS> loanPRSForUpdate, List<SavingPRS> savingPRSForUpdate) {
        List<DiscountMarginReq> discountMarginReqs = request.getDiscountMarginPlan();
        if (null != request.getPsCompanion()) {
            // set PsIdCompanion
            if (!request.getPsCompanion().equals("")) {
                prs.setPsCompanion(request.getPsCompanion());
            } else {
                prs.setPsCompanion("");
            }
        }
        prs.setLatitude(request.getLatitude());
        prs.setLongitude(request.getLongitude());
        if (request.getBringMoney() != null && !"".equals(request.getBringMoney()))
            prs.setBringMoney(new BigDecimal(request.getBringMoney()));
        if (request.getActualMoney() != null && !"".equals(request.getActualMoney()))
            prs.setActualMoney(new BigDecimal(request.getActualMoney()));
        if (request.getPaymentMoney() != null && !"".equals(request.getPaymentMoney()))
            prs.setPaymentMoney(new BigDecimal(request.getPaymentMoney()));
        if (request.getWithdrawalAdhoc() != null && !"".equals(request.getWithdrawalAdhoc()))
            prs.setWithdrawalAdhoc(new BigDecimal(request.getWithdrawalAdhoc()));
        // mapping Customer PRS
        List<CustomerPRSRequest> customerPRSList = request.getCustomerList();
        for (CustomerPRSRequest customerPRSRequest : customerPRSList) {
            CustomerPRS custPRS = new CustomerPRS();
            // search customer PRS by customer id
            custPRS = customerService.getCustomerPRSById(Long.parseLong(customerPRSRequest.getCustomerPrsId()));
            custPRS.setPrsId(prs);
            custPRS.setCustomerPrsId(Long.parseLong(customerPRSRequest.getCustomerPrsId()));
            if (customerPRSRequest.getIsAttend().equals("true")) {
                custPRS.setIsAttend(true);
                if (customerPRSRequest.getNotAttendReason() != null) {
                    PRSParameter prsParamAttend = new PRSParameter();
                    if (!customerPRSRequest.getNotAttendReason().equals("")) {
                        prsParamAttend = parameterService.findByParameterId(Long.parseLong(customerPRSRequest
                                .getNotAttendReason()));
                        if (prsParamAttend != null)
                            custPRS.setNotAttendReason(prsParamAttend.getParameterId());
                    }
                }
            } else {
                custPRS.setIsAttend(false);
                if (customerPRSRequest.getNotAttendReason() != null) {
                    PRSParameter prsParamAttend = new PRSParameter();
                    if (!customerPRSRequest.getNotAttendReason().equals("")) {
                        prsParamAttend = parameterService.findByParameterId(Long.parseLong(customerPRSRequest
                                .getNotAttendReason()));
                        if (prsParamAttend != null)
                            custPRS.setNotAttendReason(prsParamAttend.getParameterId());
                    }
                }
            }
            if (customerPRSRequest.getIsEarlyTerminationPlan() != null) {
                if (customerPRSRequest.getIsEarlyTerminationPlan().equals("true")) {
                    DiscountMarginReq discountMarginReq = customerPRSRequest.getDiscountMarginReq();
                    for (LoanPRSRequest loan : discountMarginReq.getLoans()) {
                        LoanPRS loanPRS = loanService.getLoanPRSById(Long.parseLong(loan.getLoanPrsId()));
                        loanPRS.setEarlyTerminationPlan(true);
                    }
                    custPRS.setEarlyTerminationPlan(true);
                    EarlyTerminationPlan earlyTerminationPlan = new EarlyTerminationPlan();
                    earlyTerminationPlan.setCustomerPrsId(custPRS.getCustomerPrsId());
                } else {
                    custPRS.setEarlyTerminationPlan(false);
                }
            }
            // mapping LOAN PRS
            List<LoanPRSRequest> loanPRSList = customerPRSRequest.getLoanList();
            if (!loanPRSList.isEmpty() || loanPRSList != null) {
                for (LoanPRSRequest loanPRSRequest : loanPRSList) {
                    LoanPRS loanPRS = new LoanPRS();
                    loanPRS = loanService.getLoanPRSById(Long.parseLong(loanPRSRequest.getLoanPrsId()));
                    loanPRS.setPrsId(prs);
                    loanPRS.setCustomerId(custPRS);
                    loanPRS.setLoanPRSId(Long.parseLong(loanPRSRequest.getLoanPrsId()));
                    loanPRS.setAppId(loanPRSRequest.getAppId());
                    loanPRS.setDisbursementFlag(loanPRSRequest.getDisbursementFlag().charAt(0));
                    if (loanPRS.getRemainingPrincipal() == null) {
                        loanPRS.setRemainingPrincipal(loanPRS.getRemainingPrincipal().ZERO);
                    }
                    if (loanPRS.getInstallmentPaymentAmount() == null) {
                        loanPRS.setInstallmentPaymentAmount(loanPRS.getInstallmentPaymentAmount().ZERO);
                    }
                    loanPRS.setRemainingPrincipalAfterSubmit(loanPRS.getRemainingPrincipal().subtract(loanPRS.getInstallmentPaymentAmount()));
                    loanPRS.setInstallmentPaymentAmount(new BigDecimal(loanPRSRequest.getInstallmentPaymentAmount()));
                    if (loanPRSRequest.getUseEmergencyFund().equals("true"))
                        loanPRS.setUseEmergencyFund(true);
                    if (loanPRSRequest.getIsEarlyTerminationCanceled() != null) {
                        if (loanPRSRequest.getIsEarlyTerminationCanceled().equals("true")) {
                            loanPRS.setIsEarlyTermination(false);
                            loanPRS.setIsEarlyTerminationAdHoc(false);
                        } else {
                            if (loanPRSRequest.getIsEarlyTermination() != null) {
                                if (loanPRSRequest.getIsEarlyTermination().equals("true")) {
                                    loanPRS.setIsEarlyTermination(true);
                                    // earlyTerminationReason ambil dari parameter PRS
                                    if (loanPRSRequest.getEarlyTerminationReason() != null) {
                                        if (!loanPRSRequest.getEarlyTerminationReason().equals("")) {
                                            loanPRS.setEarlyTerminationReason(loanPRSRequest.getEarlyTerminationReason());
                                        }
                                    }
                                } else
                                    loanPRS.setIsEarlyTermination(false);
                            }
                            if (loanPRSRequest.getIsEarlyTerminationAdHoc() != null) {
                                if (loanPRSRequest.getIsEarlyTerminationAdHoc().equals("true")) {
                                    loanPRS.setIsEarlyTerminationAdHoc(true);
                                    // earlyTerminationReason ambil dari parameter PRS
                                    if (loanPRSRequest.getEarlyTerminationReason() != null) {
                                        if (!loanPRSRequest.getEarlyTerminationReason().equals("")) {
                                            loanPRS.setEarlyTerminationReason(loanPRSRequest.getEarlyTerminationReason());
                                        }
                                    }
                                } else {
                                    loanPRS.setIsEarlyTerminationAdHoc(false);
                                }
                            }
                        }
                    } else {
                        loanPRS.setIsEarlyTermination(false);
                        loanPRS.setIsEarlyTerminationAdHoc(false);
                    }
                    if (loanPRSRequest.getMarginDiscountDeviationFlag().equals("true")) {
                        loanPRS.setMarginDiscountDeviationFlag(true);
                        loanPRS.setMarginDiscountDeviationPercentage(new BigDecimal(loanPRSRequest
                                .getMarginDiscountDeviationPercentage()));
                        loanPRS.setMarginDiscountDeviationAmount(new BigDecimal(loanPRSRequest
                                .getMarginDiscountDeviationAmount()));
                        loanPRS.setInstallmentPaymentAmount(loanPRS.getInstallmentPaymentAmount().add(loanPRS.getMarginDiscountDeviationAmount()));
                    } else {
                        loanPRS.setMarginDiscountDeviationFlag(false);
                    }
                    loanPRS.setUpdatedBy(request.getUsername());
                    loanPRS.setUpdatedDate(new Date());
                    if (loanPRSRequest.getWowIbStatus().equals("true"))
                        loanPRS.setWowIbStatus(true);
                    loanPRSForUpdate.add(loanPRS);
                }
            }
            // mapping saving
            List<SavingPRSRequest> savingPRSList = customerPRSRequest.getSavingList();
            for (SavingPRSRequest savingPRSRequest : savingPRSList) {
                SavingPRS savingPRS = new SavingPRS();
                savingPRS = savingService.getSavingPRSById(Long.parseLong(savingPRSRequest.getAccountId()));
                savingPRS.setPrsId(prs);
                savingPRS.setCustomerId(custPRS);
                savingPRS.setAccountId(Long.parseLong(savingPRSRequest.getAccountId()));
                savingPRS.setAccountNumber(savingPRSRequest.getAccountNumber());
                savingPRS.setWithdrawalAmount(new BigDecimal(savingPRSRequest.getWithdrawalAmount()));
                if (savingPRSRequest.getNextWithdrawalAmountPlan() != null) {
                    savingPRS.setNextWithdrawalAmountPlan(new BigDecimal(savingPRSRequest
                            .getNextWithdrawalAmountPlan()));
                }
                if (savingPRSRequest.getIsCloseSavingAccount() != null) {
                    if (savingPRSRequest.getIsCloseSavingAccount().equals("true"))
                        savingPRS.setIsCloseSavingAccount(true);
                }
                savingPRS.setClearBalance(new BigDecimal(savingPRSRequest.getClearBalance()));
                savingPRS.setDepositAmount(new BigDecimal(savingPRSRequest.getDepositAmount()));
                savingPRS.setUpdatedBy(request.getUsername());
                savingPRS.setUpdatedDate(new Date());
                savingPRSForUpdate.add(savingPRS);
            }
            custPRS.setUpdatedBy(request.getUsername());
            custPRS.setUpdatedDate(new Date());
            customerPRSForUpdate.add(custPRS);
        }
        // mapping discount Margin
        if (discountMarginReqs != null) {
            if (!discountMarginReqs.isEmpty()) {
                for (DiscountMarginReq discountMarginReq : discountMarginReqs) {
                    DiscountMarginApproval discountMarginApproval = new DiscountMarginApproval();
                    discountMarginApproval.setAlasan(discountMarginReq.getAlasan());
                    discountMarginApproval.setCustomerId(discountMarginReq.getCustomerId());
                    discountMarginApproval.setTotal(discountMarginReq.getTotal());
                    discountMarginApproval.setStatus(WebGuiConstant.STATUS_WAITING_FOR_APPROVAL);
                    discountMarginApproval.setCreatedBy(request.getUsername());
                    loanService.save(discountMarginApproval);
                    for (LoanPRSRequest loan : discountMarginReq.getLoans()) {
                        DiscountMarginLoanMap discountMarginLoanMap = new DiscountMarginLoanMap();
                        discountMarginLoanMap.setDiscountMarginId(discountMarginApproval.getDiscountMarginId());
                        discountMarginLoanMap.setLoanPrsId(Long.parseLong(loan.getLoanPrsId()));
                        loanService.save(discountMarginLoanMap);
                    }
                }
            }
        }
        //save PRS
        prs.setUpdateBy(request.getUsername());
        prs.setUpdateDate(new Date());
        prs.setSentraId(prs.getSentraId());
        prs.setPrsDate(new Date());
        prs.setPrsTime(request.getPrsStartTime());
        prs.setPrsEndTime(request.getPrsEndTime());
        prs.setReport(request.getReport());
        prs.setCreatedBy(request.getUsername());
        log.info("[**] allow submit " + allowSubmit);
        if ("1".equals(allowSubmit)) {
            prs.setStatus(request.getStatus());
        }
        prs.setUpdateDate(new Date());
        log.info("UPDATING PRS TABLE");
        prsService.savePRS(prs);
        log.info("UPDATING SUCCESS");
    }

}