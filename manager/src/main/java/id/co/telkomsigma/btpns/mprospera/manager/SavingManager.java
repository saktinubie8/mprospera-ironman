package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.Saving;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;

import java.util.Date;
import java.util.List;

public interface SavingManager {

    List<SavingPRS> getSavingByPRSId(PRS prsId, CustomerPRS customerId);

    List<SavingPRS> getSavingByPRSIdAndCustomerId(PRS prsId, List<CustomerPRS> customerPRSList);

    SavingNonPRS saveSavingNonPRS(SavingNonPRS savingNonPRS);

    SavingPRS getSavingPRSById(Long savingId);

    void clearCache();

    List<SavingPRS> findSavingRTBySentraId(Long sentraId, Date prsDate);

    List<SavingNonPRS> findSavingNonPRSByCustomer(Customer customer);

    SavingNonPRS findSavingNonPRSById(Long id);

    Boolean isNonPrsCustomerSaving(Customer customerId);

    List<Saving> findSavingByCustomerId(Customer customerId);

    Saving findByAccountNumber(String accountNumber);

    List<SavingPRS> findSavingPRSByPrsId(PRS prsId);

}