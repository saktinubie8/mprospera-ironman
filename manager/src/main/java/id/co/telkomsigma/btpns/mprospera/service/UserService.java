package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.model.user.UserAndSentra;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
@Service("userService")
public class UserService extends GenericService implements UserDetailsService {

    @Autowired
    private UserManager userManager;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        User user = userManager.getUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User '" + username + "' not found.");
        }
        return (UserDetails) user;
    }

    public User findUserByUsername(String username) {
        return userManager.getUserByUsername(username);
    }


    public List<User> loadUserByLocationId(final String locId) throws UsernameNotFoundException {
        return userManager.getUserByLocId(locId);
    }

    public List<UserAndSentra> getUserAndSentraByLocationId(final String locId) throws UsernameNotFoundException {
        return userManager.getUserAndSentraByLocationId(locId);
    }

}