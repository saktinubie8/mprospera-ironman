package id.co.telkomsigma.btpns.mprospera.manager;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.prs.*;


import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

public interface PRSManager {

    PRS getPRSById(Long prsId);

    PRS getPRSBySentra(Sentra sentra, Date prsDate);

    PRS getPRSBySentraId(Sentra sentra);

    PRS getPRSBySentraPrevious(Sentra sentra, Date prsDate);

    PRSPhoto getPrsPhotoByPrs(PRS prs);

    PRS savePRSEvent(PRS prs, List<CustomerPRS> customerPRSList, List<LoanPRS> loanPRSList, List<SavingPRS> savingPRSList);

    void savePRS(PRS prs);

    //	BEGIN temporary out-of-convention: refer not to one table PRS but to another table ReportTrx
    //
    //
    void saveToReport(ReportTrxTable reportTrxTable);    //used in PRS-Approval

    void saveToReportCollection(ReportCollection reportCollection);

    void saveMutation(MutationTrxTabel mutationTrxTabel);

    MutationTrxTabel findMutationByPrsId(Long id);

    //
    //	to support Mutation PS MS not from PRS NonPRS but from ReportTrx
    //
    List<ReportTrxTable> getRptTrxByMSInCOT(Date aDate, String userMS);

    List<ReportTrxTable> getRptTrxByMSOutCOT(Date aDate, String userMS);

    BigDecimal getInAmountBeforeCOT(Date aDate, String userMS);

    BigDecimal getInAmountAfterCOT(Date aDate, String userMS);

    BigDecimal getOutAmountAfterCOT(Date aDate, String userMS);

    BigDecimal getAmountBeforeCOT(Date aDate, List<String> listTypeTrx, String userMS);

    //
    //	END   temporary out-of-convention: refer not to one table PRS but to another table ReportTrx

    void clearCache();

    List<LoanPRS> getSentraHasPrsByLoanPrs(Long sentraId);

    List<SavingPRS> getSentraHasPrsBySavingPrs(Long sentraId);

    void savePrsPhoto(PRSPhoto prsPhoto);

    List<PRS> findByPrsAndPrsDate(String officeCode, Date prsDate);

    List<ReportCollection> findReportByAppIdCollection(String appId);

    List<PRS> getPRSByPSAndStatus(String usernamePS, Date prsDate);

}