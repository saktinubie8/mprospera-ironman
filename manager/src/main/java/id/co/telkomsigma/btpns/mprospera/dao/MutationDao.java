package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.prs.MutationTrxTabel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MutationDao extends JpaRepository<MutationTrxTabel, Long> {

    MutationTrxTabel findByPrsId(Long id);

}
