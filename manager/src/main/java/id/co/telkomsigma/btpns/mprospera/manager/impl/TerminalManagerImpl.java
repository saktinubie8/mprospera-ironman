package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.TerminalActivityDao;
import id.co.telkomsigma.btpns.mprospera.dao.TerminalDao;
import id.co.telkomsigma.btpns.mprospera.manager.TerminalManager;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by Dzulfiqar on 11/11/15.
 */
@Service("terminalManager")
public class TerminalManagerImpl implements TerminalManager {

    @Autowired
    TerminalDao terminalDao;

    @Autowired
    TerminalActivityDao terminalActivityDao;

    public Terminal getTerminalByImei(String imei) {
        return terminalDao.findByImei(imei);
    }

    @Override
    public void clearCache() {
        // TODO Auto-generated method stub
    }

}