package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRSPhoto;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PRSPhotoDao extends JpaRepository<PRSPhoto, Long> {

    PRSPhoto findByPrsId(PRS prs);

}
