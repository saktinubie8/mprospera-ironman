package id.co.telkomsigma.btpns.mprospera.service;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.manager.SentraManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("sentraService")
public class SentraService extends GenericService {

    @Autowired
    private SentraManager sentraManager;

    @Autowired
    private UserManager userManager;

    public void save(Sentra sentra) {
        sentraManager.save(sentra);
    }

    public Sentra findBySentraId(String sentraId) {
        if (sentraId == null || "".equals(sentraId))
            return null;
        return sentraManager.findBySentraId(Long.parseLong(sentraId));
    }

    public List<Sentra> getSentraByWismaId(String wismaId) {
        return sentraManager.getSentraByWismaId(wismaId);
    }

    public Page<Sentra> getSentraByWismaIdPaging(String wismaId, String page) {
        int pageNumber = Integer.parseInt(page) - 1;
        if (page == null || page.equals("")) {
            return sentraManager.getSentraByWismaIdNoPaging(wismaId);
        } else {
            return sentraManager.getSentraByWismaIdPaging(wismaId, new PageRequest(pageNumber, 10));
        }
    }

    public Group getGroupByGroupId(String groupId) {
        if (groupId == null || "".equals(groupId))
            return null;
        return sentraManager.getGroupByGroupId(Long.parseLong(groupId));
    }

    public List<Sentra> findAll() {
        // TODO Auto-generated method stub
        return sentraManager.findAll();
    }

    public Sentra findByProsperaId(String prosperaId) {
        return sentraManager.findByProsperaId(prosperaId);
    }

}