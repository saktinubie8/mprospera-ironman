package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApproval;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Dzulfiqar on 23/05/2017.
 */
public interface DiscountMarginDao extends JpaRepository<DiscountMarginApproval, Long> {

    List<DiscountMarginApproval> findByCustomerId(Long customerId);

    DiscountMarginApproval findByDiscountMarginId(Long discountMarginId);

    DiscountMarginApproval findTopByCustomerIdOrderByDiscountMarginIdDesc(Long customerId);

    List<DiscountMarginApproval> findByCustomerIdAndStatusNot(Long customerId, String status);

}
