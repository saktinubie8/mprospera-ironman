package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.SurveyDetailDao;
import id.co.telkomsigma.btpns.mprospera.dao.SurveyDao;
import id.co.telkomsigma.btpns.mprospera.manager.SurveyManager;
import id.co.telkomsigma.btpns.mprospera.model.survey.Survey;

@Service("surveyManager")
public class SurveyManagerImpl implements SurveyManager {

    @Autowired
    private SurveyDao surveyDao;

    @Autowired
    private SurveyDetailDao surveyDetailDao;

    @Override
    public void save(Survey sentra) {
        surveyDao.save(sentra);
    }

    @Override
    public Page<Survey> findByCreatedDate(Date startDate, Date endDate, String officeId) {
        return surveyDao.findByCreatedDate(startDate, endDate, officeId, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public void clearCache() {
    }

}