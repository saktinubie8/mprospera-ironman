package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.dao.InsuranceClaimDao;
import id.co.telkomsigma.btpns.mprospera.dao.InsuranceClaimPhotoDao;
import id.co.telkomsigma.btpns.mprospera.manager.InsuranceClaimManager;
import id.co.telkomsigma.btpns.mprospera.model.claim.InsuranceClaim;
import id.co.telkomsigma.btpns.mprospera.model.claim.InsuranceClaimPhoto;

@Service
public class InsuranceClaimManagerImpl implements InsuranceClaimManager {

    @Autowired
    private InsuranceClaimDao insuranceClaimDao;

    @Autowired
    private InsuranceClaimPhotoDao insuranceClaimPhotoDao;

    @Override
    public InsuranceClaim saveInsuranceClaim(InsuranceClaim insuranceClaim) {
        return insuranceClaimDao.save(insuranceClaim);
    }

    @Override
    public InsuranceClaim findById(long parseLong) {
        // TODO Auto-generated method stub
        return insuranceClaimDao.findOne(parseLong);
    }

    @Override
    public InsuranceClaimPhoto saveInsuranceClaimPhoto(InsuranceClaimPhoto photo) {
        return insuranceClaimPhotoDao.save(photo);
    }

    @Override
    public InsuranceClaimPhoto findInsuranceClaimPhotoById(long id) {
        return insuranceClaimPhotoDao.findByInsuranceClaim_id(id);
    }

    @Override
    public List<InsuranceClaim> findInsuranceClaimIsNotApprovedByLocationId(String locationId) {
        // TODO Auto-generated method stub
        List<String> statusList = new ArrayList<>();
        statusList.add(WebGuiConstant.STATUS_DRAFT);
        statusList.add(WebGuiConstant.STATUS_REJECTED);
        return insuranceClaimDao.findByStatusInAndLocationId(statusList, locationId);
    }

}