package id.co.telkomsigma.btpns.mprospera.dao;


import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface GroupDao extends JpaRepository<Group, Long> {

    @Query("SELECT g FROM Group g WHERE g.isDeleted = false and g.id = :groupId")
    Group getGroupByGroupId(@Param("groupId") Long groupId);

}
