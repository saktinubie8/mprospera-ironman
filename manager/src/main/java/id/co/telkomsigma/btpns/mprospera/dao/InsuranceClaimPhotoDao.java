package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.claim.InsuranceClaimPhoto;

import org.springframework.data.jpa.repository.JpaRepository;

public interface InsuranceClaimPhotoDao extends JpaRepository<InsuranceClaimPhoto, Long> {

    InsuranceClaimPhoto findByInsuranceClaim_id(long id);

}