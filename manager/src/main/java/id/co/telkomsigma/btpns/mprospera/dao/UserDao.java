package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.model.user.UserAndSentra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
public interface UserDao extends JpaRepository<User, String> {

    /**
     * Get single user by username
     *
     * @param username
     * @return single User object
     */
    User findByUsername(String username);

    List<User> findByOfficeCode(String locId);

    @Query(" SELECT new id.co.telkomsigma.btpns.mprospera.model.user.UserAndSentra(u.username, u.name) FROM User u"
            + " WHERE u.officeCode=:locId AND u.roleUser='1' AND NOT EXISTS  "
            + " (select b.assignedTo from Sentra b WHERE u.username=b.assignedTo)")
    List<UserAndSentra> findUserAndSentraByLocationId(@Param("locId") String locId);

    @Query("SELECT u.sessionKey FROM User u WHERE username = :username")
    String findSessionKeyByUsername(@Param("username") String username);

}