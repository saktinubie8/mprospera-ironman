package id.co.telkomsigma.btpns.mprospera.service;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.manager.CustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("customerService")
public class CustomerService extends GenericService {

    @Autowired
    @Qualifier("customerManager")
    private CustomerManager customerManager;

    public List<Customer> getBySentraId(Sentra sentra) {
        return customerManager.getBySentraId(sentra);
    }

    public Customer findById(String customerId) {
        if (customerId == null || "".equals(customerId))
            return null;
        return customerManager.findById(Long.parseLong(customerId));
    }

    public Customer getCustomerByCifId(String cifId) {
        return customerManager.getCustomerByCifId(cifId);
    }

    public List<CustomerPRS> getCustomerPRSByPrsId(PRS prs) {
        return customerManager.getCustomerPRSByPrsId(prs);
    }

    public CustomerPRS getCustomerPRSById(Long customerId) {
        return customerManager.getCustomerPRSById(customerId);
    }

    public CustomerPRS findTopCustomerPRSbyCustomer(Customer customer) {
        // TODO Auto-generated method stub
        return customerManager.findTopCustomerPRSbyCustomer(customer);
    }

}