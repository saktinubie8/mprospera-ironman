package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;
import java.util.List;

public interface SentraManager {

    void save(Sentra sentra);

    Sentra findBySentraId(Long sentraId);

    Sentra findByProsperaId(String prosperaId);

    Page<Sentra> findByCreatedDate(String username, String loc, Date startDate, Date endDate);

    List<Sentra> getSentraByWismaId(String sentraId);

    Page<Sentra> getSentraByWismaIdPaging(String sentraId, PageRequest pageRequest);

    Page<Sentra> getSentraByWismaIdNoPaging(String sentraId);

    Group getGroupByGroupId(Long groupId);

    void clearCache();

    List<Sentra> findAll();

}