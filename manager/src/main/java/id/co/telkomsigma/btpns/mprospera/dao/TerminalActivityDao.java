package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
public interface TerminalActivityDao extends JpaRepository<TerminalActivity, String> {

    List<TerminalActivity> findByUsernameAndActivityTypeOrderByCreatedDateAsc(String username, int activityType);

    @Query("SELECT DISTINCT a FROM TerminalActivity a INNER JOIN a.terminal t WHERE t.terminalId = :terminalId AND a.createdDate >= :startDate AND a.createdDate <= :endDate  ORDER BY a.createdDate DESC")
    Page<TerminalActivity> getTerminalActivity(@Param("terminalId") Long terminalId,
                                               @Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable);

    @Query("SELECT DISTINCT a FROM TerminalActivity a INNER JOIN a.terminal t WHERE t.terminalId = :terminalId ORDER BY a.createdDate DESC")
    Page<TerminalActivity> getTerminalActivityWithoutRange(@Param("terminalId") Long terminalId, Pageable pageable);

    Integer countByReffNo(String rrn);

}