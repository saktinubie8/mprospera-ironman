package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.prs.NonPRSPhoto;

import org.springframework.data.jpa.repository.JpaRepository;

public interface NonPRSPhotoDao extends JpaRepository<NonPRSPhoto, Long> {

    NonPRSPhoto findByNonPrsId(Long loan);

}
