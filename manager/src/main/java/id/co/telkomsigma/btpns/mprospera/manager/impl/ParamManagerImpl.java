package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.PRSParamDao;
import id.co.telkomsigma.btpns.mprospera.dao.ParamDao;
import id.co.telkomsigma.btpns.mprospera.manager.ParamManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


@Service("paramManager")
public class ParamManagerImpl implements ParamManager {

    @Autowired
    private ParamDao paramDao;

    @Autowired
    private PRSParamDao prsParamDao;

    @Override
    @Cacheable(value = "irn.param.paramByParamName", unless = "#result == null", key = "#paramName")
    public SystemParameter getParamByParamName(String paramName) {
        SystemParameter param = paramDao.findByParamName(paramName);
        if (param != null)
            param.getCreatedBy().setOfficeCode(null);
        return param;
    }

    @Override
    @CacheEvict(value = {"irn.param.paramByParamName", "irn.prs.param.getAllPrsParam",
            "irn.prs.param.findByParameterId", "irn.prs.param.getFileDayaByFileName"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {

    }

    @Override
    @Cacheable(value = "irn.prs.param.getAllPrsParam", unless = "#result == null")
    public List<PRSParameter> getAllPrsParam(String paramGroup) {
        // TODO Auto-generated method stub
        return prsParamDao.findByParamGroupOrderByParameterId(paramGroup);
    }

    @Override
    @Cacheable(value = "irn.prs.param.findByParameterId", unless = "#result == null")
    public PRSParameter findByParameterId(Long parameterId) {
        // TODO Auto-generated method stub
        return prsParamDao.findByParameterId(parameterId);
    }

    @Override
    @Cacheable(value = "irn.prs.param.getFileDayaByFileName", unless = "#result == null")
    public PRSParameter getFileDayaByFileName(String fileName) {
        return prsParamDao.findByParamDescriptionLike(fileName);
    }

}