package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerPRSDao extends JpaRepository<CustomerPRS, Long> {

    List<CustomerPRS> findByPrsId(PRS prs);

    List<CustomerPRS> findByCustomerId(Customer customer);

    CustomerPRS findTopByCustomerIdOrderByPrsIdDesc(Customer customer);

}
