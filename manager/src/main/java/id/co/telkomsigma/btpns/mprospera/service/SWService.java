package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.SWManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwIdPhoto;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwSurveyPhoto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("swService")
public class SWService extends GenericService {

    @Autowired
    @Qualifier("swManager")
    private SWManager swManager;

    @Autowired
    private UserManager userManager;

    public void save(SurveyWawancara surveyWawancara) {
        swManager.save(surveyWawancara);
    }

    public void save(SwIdPhoto swIdPhoto) {
        swManager.save(swIdPhoto);
    }

    public void save(SwSurveyPhoto swSurveyPhoto) {
        swManager.save(swSurveyPhoto);
    }

    public SwIdPhoto getSwIdPhoto(String swId) {
        return swManager.getSwIdPhoto(swId);
    }

    public SwSurveyPhoto getSwSurveyPhoto(String swId) {
        return swManager.getSwSurveyPhoto(swId);
    }

    public SurveyWawancara getSWById(String swId) {
        return swManager.getSWById(swId);
    }

    public LoanProduct findByProductId(String productId) {
        return swManager.findByProductId(productId);
    }

    public SurveyWawancara getSWByCustomerId(Long customerId) {
        return swManager.findByCustomerId(customerId);
    }

}