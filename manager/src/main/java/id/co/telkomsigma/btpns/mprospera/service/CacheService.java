package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("cacheService")
public class CacheService extends GenericService {

    @Autowired
    CustomerManager customerManager;

    @Autowired
    LoanManager loanManager;

    @Autowired
    PRSManager prsManager;

    @Autowired
    SavingManager savingManager;

    @Autowired
    LocationManager locationManager;

    public void clearCache() {
        customerManager.clearCache();
        loanManager.clearCache();
        savingManager.clearCache();
        prsManager.clearCache();
        locationManager.clearCache();
    }

}