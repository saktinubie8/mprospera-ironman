package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.SavingDao;
import id.co.telkomsigma.btpns.mprospera.dao.SavingNonPRSDao;
import id.co.telkomsigma.btpns.mprospera.dao.SavingPRSDao;
import id.co.telkomsigma.btpns.mprospera.manager.SavingManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.Saving;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;

@Service("savingManagerImpl")
public class SavingManagerImpl implements SavingManager {

	@Autowired
	private SavingPRSDao savingPRSDao;
	
	@Autowired
	private SavingNonPRSDao savingNonPrsDao;

	@Autowired
	private SavingDao savingDao;

	@Override
	public List<SavingPRS> getSavingByPRSId(PRS prs, CustomerPRS customer) {
		// TODO Auto-generated method stub
		return savingPRSDao.findByPrsIdAndCustomerId(prs, customer);
	}

	@Override
	public List<SavingPRS> getSavingByPRSIdAndCustomerId(PRS prs, List<CustomerPRS> customerPRSList) {
		// TODO Auto-generated method stub
		return savingPRSDao.findByPrsIdAndCustomerIdIn(prs, customerPRSList);
	}

	@Override
	public SavingPRS getSavingPRSById(Long savingId) {
		// TODO Auto-generated method stub
		return savingPRSDao.findOne(savingId);
	}

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub
		
	}

    @Override
    @Cacheable(value = "irn.saving.prs.findSavingRTBySentraId", unless = "#result == null")
    public List<SavingPRS> findSavingRTBySentraId(Long sentraId, Date prsDate) {
        return savingPRSDao.findSavingRTBySentraId(sentraId,prsDate);
    }

	@Override
	@Cacheable(value = "irn.saving.collection.findSavingNonPRSByCustomer", unless = "#result == null")
	public List<SavingNonPRS> findSavingNonPRSByCustomer(Customer customer) {
		// TODO Auto-generated method stub
		return savingNonPrsDao.findByCustomerId(customer);
	}

	@Override
	public SavingNonPRS findSavingNonPRSById(Long id) {
		// TODO Auto-generated method stub
		return savingNonPrsDao.findOne(id);
	}

	@Override
	@Caching(evict = {
			@CacheEvict(value = "irn.saving.collection.findSavingNonPRSByCustomer", allEntries = true, beforeInvocation = true),
			@CacheEvict(value = "irn.saving.collection.isNonPrsCustomerSaving", allEntries = true, beforeInvocation = true)
	})
	public SavingNonPRS saveSavingNonPRS(SavingNonPRS savingNonPRS) {
		SavingNonPRS nonPrsFromDb = savingNonPrsDao.findOne(savingNonPRS.getSavingNonPrsId());
		if(nonPrsFromDb != null){
			nonPrsFromDb.setClearBalance(savingNonPRS.getClearBalance());
			nonPrsFromDb.setDepositAmount(savingNonPRS.getDepositAmount());
			nonPrsFromDb.setWithdrawalAmount(savingNonPRS.getWithdrawalAmount());
			nonPrsFromDb.setUpdatedBy(savingNonPRS.getUpdatedBy());
			nonPrsFromDb.setUpdatedDate(savingNonPRS.getUpdatedDate());
			savingNonPrsDao.save(nonPrsFromDb);
		}
		return savingNonPRS;
	}

	@Override
	@Cacheable(value = "irn.saving.collection.isNonPrsCustomerSaving", unless = "#result == null")
	public Boolean isNonPrsCustomerSaving(Customer customerId) {
		// TODO Auto-generated method stub
		Integer count = savingNonPrsDao.countByCustomerId(customerId);
		if (count > 0)
			return true;
		else
			return false;
	}

	@Override
	public List<Saving> findSavingByCustomerId(Customer customerId) {
		// TODO Auto-generated method stub
		return savingDao.findByCustomerId(customerId);
	}

	@Override
	public Saving findByAccountNumber(String accountNumber) {
		return savingDao.findByAccountNumber(accountNumber);
	}

	@Override
	public List<SavingPRS> findSavingPRSByPrsId(PRS prsId) {
		// TODO Auto-generated method stub
		return savingPRSDao.findSavingPRSByPrsId(prsId);
	}

}
