package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.DisbursementPhoto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DisbursementPhotoDao extends JpaRepository<DisbursementPhoto, Long> {

    DisbursementPhoto findByLoanPrsId(LoanPRS loan);

}
