package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.SavingManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.Saving;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("savingService")
public class SavingService extends GenericService {

    @Autowired
    private SavingManager savingManager;

    public List<SavingPRS> getSavingByPRSIdAndCustomerId(PRS prs, CustomerPRS customer) {
        return savingManager.getSavingByPRSId(prs, customer);

    }

    public List<SavingPRS> getSavingByPRSIdAndCustomerIdIn(PRS prs, List<CustomerPRS> customerPRSList) {
        return savingManager.getSavingByPRSIdAndCustomerId(prs, customerPRSList);

    }

    public SavingNonPRS saveSavingNonPRS(SavingNonPRS savingNonPRS) {
        return savingManager.saveSavingNonPRS(savingNonPRS);
    }

    public SavingPRS getSavingPRSById(Long savingId) {
        return savingManager.getSavingPRSById(savingId);
    }

    public List<SavingNonPRS> getSavingNonPrsByCustomer(Customer customer) {
        return savingManager.findSavingNonPRSByCustomer(customer);
    }

    public SavingNonPRS getSavingNonPrsById(Long id) {
        return savingManager.findSavingNonPRSById(id);
    }

    public Boolean isNonPrsCustomerSaving(Customer customerId) {
        return savingManager.isNonPrsCustomerSaving(customerId);
    }

    public List<Saving> findSavingByCustomerId(Customer customer) {
        return savingManager.findSavingByCustomerId(customer);
    }

    public List<SavingPRS> findSavingRTBySentraId(Long sentraId, Date prsDate) {
        return savingManager.findSavingRTBySentraId(sentraId, prsDate);
    }

    public Saving findByAccountNumber(String accountNumber) {
        return savingManager.findByAccountNumber(accountNumber);
    }

    public List<SavingPRS> findSavingPRSByPrsId(PRS prsId) {
        return savingManager.findSavingPRSByPrsId(prsId);
    }

}