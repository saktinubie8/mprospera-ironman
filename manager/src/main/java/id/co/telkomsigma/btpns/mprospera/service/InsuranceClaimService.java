package id.co.telkomsigma.btpns.mprospera.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.manager.InsuranceClaimManager;
import id.co.telkomsigma.btpns.mprospera.model.claim.InsuranceClaim;
import id.co.telkomsigma.btpns.mprospera.model.claim.InsuranceClaimPhoto;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.ApproveInsuranceClaimRequest;
import id.co.telkomsigma.btpns.mprospera.request.SubmitInsuranceClaimRequest;
import id.co.telkomsigma.btpns.mprospera.request.SubmitPhotoInsuranceClaimRequest;
import id.co.telkomsigma.btpns.mprospera.request.SyncInsuranceClaimRequest;
import id.co.telkomsigma.btpns.mprospera.response.ApproveInsuranceClaimResponse;
import id.co.telkomsigma.btpns.mprospera.response.InsuranceClaimData;
import id.co.telkomsigma.btpns.mprospera.response.SubmitInsuranceClaimResponse;

@Service
public class InsuranceClaimService {

    @Autowired
    private InsuranceClaimManager insuranceClaimManager;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private UserService userService;

    public InsuranceClaim findById(Long id) {
        return insuranceClaimManager.findById(id);
    }

    @Transactional
    public void submitInsuranceClaim(SubmitInsuranceClaimRequest request, SubmitInsuranceClaimResponse response) {
        InsuranceClaim insuranceClaim = new InsuranceClaim();
        insuranceClaim.setCreatedBy(request.getCreatedBy());
        try {
            insuranceClaim.setCreatedDate(new SimpleDateFormat("yyyy-MM-dd").parse(request.getCreatedDate()));
        } catch (ParseException e) {
            insuranceClaim.setCreatedDate(new Date());
        }
        Customer customer = customerService.getCustomerByCifId(request.getCustomerNumber());
        insuranceClaim.setCustomer(customer);
        insuranceClaim.setCustomerCifNumber(request.getCustomerNumber());
        insuranceClaim.setCustomerFlag(request.getCustomerFlag());
        insuranceClaim.setLatitude(request.getLatitude());
        insuranceClaim.setLongitude(request.getLongitude());
        insuranceClaim.setDeathCause(request.getDeathCause());
        insuranceClaim.setRrn(request.getRetrievalReferenceNumber());
        insuranceClaim.setSpouseFlag(request.getSpouseFlag());
        insuranceClaim.setStatus("DRAFT");
        insuranceClaim.setLocationId(customer.getGroup().getSentra().getLocationId());
        insuranceClaim = insuranceClaimManager.saveInsuranceClaim(insuranceClaim);
        response.setInsuranceClaimId(insuranceClaim.getId().toString());
    }

    public void rejectInsuranceClaim(SubmitInsuranceClaimRequest request) {
        InsuranceClaim insuranceClaim = insuranceClaimManager.findById(Long.parseLong(request.getInsuranceClaimId()));
        insuranceClaim.setApprovedBy(request.getApprovedBy());
        try {
            insuranceClaim.setApprovedDate(new SimpleDateFormat("yyyy-MM-dd").parse(request.getApprovedDate()));
        } catch (ParseException e) {
            insuranceClaim.setApprovedDate(new Date());
        }
        insuranceClaim.setStatus(request.getStatus());
        insuranceClaimManager.saveInsuranceClaim(insuranceClaim);
    }

    public void approveInsuranceClaim(SubmitInsuranceClaimRequest request) {
        InsuranceClaim insuranceClaim = insuranceClaimManager.findById(Long.parseLong(request.getInsuranceClaimId()));
        insuranceClaim.setApprovedBy(request.getApprovedBy());
        try {
            insuranceClaim.setApprovedDate(new SimpleDateFormat("yyyy-MM-dd").parse(request.getApprovedDate()));
        } catch (ParseException e) {
            insuranceClaim.setApprovedDate(new Date());
        }
        insuranceClaim.setStatus(request.getStatus());
        insuranceClaimManager.saveInsuranceClaim(insuranceClaim);
    }

    public void approveInsuranceClaim(ApproveInsuranceClaimRequest request) {
        InsuranceClaim insuranceClaim = insuranceClaimManager.findById(Long.parseLong(request.getInsuranceClaimId()));
        insuranceClaim.setApprovedBy(request.getApprovedBy());
        try {
            insuranceClaim.setApprovedDate(new SimpleDateFormat("yyyy-MM-dd").parse(request.getApprovedDate()));
        } catch (ParseException e) {
            insuranceClaim.setApprovedDate(new Date());
        }
        insuranceClaim.setStatus(request.getStatus());
        insuranceClaimManager.saveInsuranceClaim(insuranceClaim);
    }

    public boolean validateSubmitInsuranceClaim(SubmitInsuranceClaimRequest request, SubmitInsuranceClaimResponse response) {
        if (request.getCustomerNumber() == null || "".equals(request.getCustomerNumber())) {
            response.setResponseCode(WebGuiConstant.UNKNOWN_CUSTOMER_ID);
            return false;
        }
        if (customerService.getCustomerByCifId(request.getCustomerNumber()) == null) {
            response.setResponseCode(WebGuiConstant.UNKNOWN_CUSTOMER_ID);
            return false;
        }
        return true;
    }

    public boolean validateApproveInsuranceClaim(ApproveInsuranceClaimRequest request,
                                                 ApproveInsuranceClaimResponse response) {
        if (request.getInsuranceClaimId() == null || "".equals(request.getInsuranceClaimId())) {
            response.setResponseCode(WebGuiConstant.UNKNOWN_INSURANCE_CLAIM_ID);
            return false;
        }
        if (insuranceClaimManager.findById(Long.parseLong(request.getInsuranceClaimId())) == null) {
            response.setResponseCode(WebGuiConstant.UNKNOWN_INSURANCE_CLAIM_ID);
            return false;
        }
        if (!"APPROVED".equals(request.getStatus()) && !"REJECTED".equals(request.getStatus())) {
            response.setResponseCode(WebGuiConstant.RC_UNKNOWN_STATUS);
            return false;
        }
        return true;
    }

    public void submitPhotoInsuranceClaim(SubmitPhotoInsuranceClaimRequest request) {
        InsuranceClaimPhoto photo = new InsuranceClaimPhoto();
        InsuranceClaim claim = insuranceClaimManager.findById(Long.parseLong(request.getInsuranceClaimId()));
        photo.setInsuranceClaim(claim);
        photo.setPhoto(request.getPhoto().getBytes());
        insuranceClaimManager.saveInsuranceClaimPhoto(photo);
    }

    @Transactional
    public List<InsuranceClaimData> getInsuranceClaimData(SyncInsuranceClaimRequest request) {

        User user = userService.findUserByUsername(request.getUsername());
        String locationId = user.getOfficeCode();
        List<InsuranceClaimData> listInsuranceClaimData = new ArrayList();
//		fix insurance claim not only draft
        List<InsuranceClaim> listInsuranceClaim = insuranceClaimManager.findInsuranceClaimIsNotApprovedByLocationId(locationId);
        for (InsuranceClaim claim : listInsuranceClaim) {
            InsuranceClaimData data = new InsuranceClaimData();
            data.setClaimInsuranceId(claim.getId().toString());
            data.setCreatedBy(claim.getCreatedBy());
            data.setCreatedDate(new SimpleDateFormat("yyyy-MM-dd").format(claim.getCreatedDate()));
            data.setCustomerCifNumber(claim.getCustomerCifNumber());
            data.setCustomerFlag(claim.getCustomerFlag());
            data.setCustomerId(claim.getCustomer().getCustomerId().toString());
            data.setDeathCause(claim.getDeathCause());
            data.setSpouseFlag(claim.getSpouseFlag());
            data.setStatus(claim.getStatus());
            listInsuranceClaimData.add(data);
        }
        return listInsuranceClaimData;
    }

    public InsuranceClaimPhoto getInsuranceClaimPhotoById(String id) {
        return insuranceClaimManager.findInsuranceClaimPhotoById(Long.parseLong(id));
    }

}