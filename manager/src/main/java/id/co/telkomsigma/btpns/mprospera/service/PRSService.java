package id.co.telkomsigma.btpns.mprospera.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.prs.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.PRSManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

@Service("prsService")
public class PRSService extends GenericService {

    @Autowired
    PRSManager prsManager;

    public PRS getPRSById(Long prsId) {
        return prsManager.getPRSById(prsId);
    }

    public PRS getPrsBySentraId(Sentra sentraId) {
        return prsManager.getPRSBySentraId(sentraId);
    }

    public PRS getPRSBySentra(Sentra sentra, Date prsDate) {
        return prsManager.getPRSBySentra(sentra, prsDate);
    }

    public PRS getPRSBySentraPrevious(Sentra sentra, Date prsDate) {
        return prsManager.getPRSBySentraPrevious(sentra, prsDate);
    }

    public PRSPhoto getPrsPhotoByPrs(PRS prs) {
        return prsManager.getPrsPhotoByPrs(prs);
    }

    public void savePRS(PRS prs) {
        prsManager.savePRS(prs);
    }

    public void saveToReport(ReportTrxTable reportTrxTable) {
        prsManager.saveToReport(reportTrxTable);
    }

    public void saveToReportCollection(ReportCollection reportCollection) {
        prsManager.saveToReportCollection(reportCollection);
    }

    public List<ReportTrxTable> getRptTrxByMSInCOT(Date aDate, String userMS) {
        return prsManager.getRptTrxByMSInCOT(aDate, userMS);
    }

    public List<ReportTrxTable> getRptTrxByMSOutCOT(Date aDate, String userMS) {
        return prsManager.getRptTrxByMSOutCOT(aDate, userMS);
    }

    public BigDecimal getInAmountBeforeCOT(Date aDate, String userMS) {
        return prsManager.getInAmountBeforeCOT(aDate, userMS);
    }

    public BigDecimal getInAmountAfterCOT(Date aDate, String userMS) {
        return prsManager.getInAmountAfterCOT(aDate, userMS);
    }

    public BigDecimal getOutAmountAfterCOT(Date aDate, String userMS) {
        return prsManager.getOutAmountAfterCOT(aDate, userMS);
    }

    public BigDecimal getAmountBeforeCOT(Date aDate, List<String> listTypeTrx, String userMS) {
        return prsManager.getAmountBeforeCOT(aDate, listTypeTrx, userMS);
    }

    public PRS savePRSEvent(PRS prs, List<CustomerPRS> customerPRSList, List<LoanPRS> loanPRSList, List<SavingPRS> savingPRSList) {
        return prsManager.savePRSEvent(prs, customerPRSList, loanPRSList, savingPRSList);
    }

    public void saveMutation(MutationTrxTabel mutationTrxTabel) {
        prsManager.saveMutation(mutationTrxTabel);
    }

    public MutationTrxTabel findMutationByPrs(Long id) {
        return prsManager.findMutationByPrsId(id);
    }

    public List<LoanPRS> getSentraHasPrsByLoanPrs(Long sentraId) {
        return prsManager.getSentraHasPrsByLoanPrs(sentraId);
    }

    public List<SavingPRS> getSentraHasPrsBySavingPrs(Long sentraId) {
        return prsManager.getSentraHasPrsBySavingPrs(sentraId);
    }

    public void savePrsPhoto(PRSPhoto prsPhoto) {
        prsManager.savePrsPhoto(prsPhoto);
    }

    public List<PRS> getSentraByPrsAndPrsDate(String officeCode, Date prsDate) {
        // TODO Auto-generated method stub
        return prsManager.findByPrsAndPrsDate(officeCode, prsDate);
    }

    public List<ReportCollection> findReportByAppIdCollection(String appId) {
        return prsManager.findReportByAppIdCollection(appId);
    }

    public List<PRS> getPRSByPSAndStatus(String usernamePS, Date prsDate) {
        return prsManager.getPRSByPSAndStatus(usernamePS, prsDate);
    }

}