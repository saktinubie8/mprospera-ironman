package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.GroupDao;
import id.co.telkomsigma.btpns.mprospera.dao.SentraDao;
import id.co.telkomsigma.btpns.mprospera.manager.SentraManager;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("sentraManager")
public class SentraManagerImpl implements SentraManager {

    @Autowired
    private SentraDao sentraDao;

    @Autowired
    private GroupDao groupDao;

    @Override
    @Caching(
            evict = {
                    @CacheEvict(value = "hwk.sentra.findBySentraId", key = "#sentra.sentraId", beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.getAllSentra", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.getAllSentraPageable", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.findSentraByCreatedDate", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.findSentraByCreatedDatePageable", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.isValidSentra", key = "#sentra.sentraId", beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.getSentraByGroupId", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.findAllSentraList", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.getSentraByRrn", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.findDeletedSentraList", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "vsn.sentra.findByProsperaId", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "vsn.sentra.findAll", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "irn.sentra.findBySentraId", key = "#sentra.sentraId", beforeInvocation = true),
                    @CacheEvict(value = "irn.sentra.findByProsperaId", key = "#sentra.prosperaId", beforeInvocation = true),
                    @CacheEvict(value = "irn.sentra.getSentraByWismaId", key = "#sentra.locationId", beforeInvocation = true)
            }
    )
    public void save(Sentra sentra) {
        sentraDao.save(sentra);
    }

    @Override
    @Cacheable(value = "irn.sentra.findBySentraId", unless = "#result == null")
    public Sentra findBySentraId(Long sentraId) {
        // TODO Auto-generated method stub
        return sentraDao.findBySentraId(sentraId);
    }

    @Override
    public Page<Sentra> findByCreatedDate(String username, String loc, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        if (username.equals(null)) {
            return sentraDao.findByCreatedDateWithLoc(loc, startDate, endDate, new PageRequest(0, Integer.MAX_VALUE));
        } else {
            return sentraDao.findByCreatedDate(username, startDate, endDate, new PageRequest(0, Integer.MAX_VALUE));
        }
    }

    @Override
    @CacheEvict(value = {"irn.sentra.getSentraByWismaId", "irn.sentra.findBySentraId", "irn.sentra.findByProsperaId"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    @Cacheable(value = "irn.sentra.findByProsperaId", unless = "#result == null")
    public Sentra findByProsperaId(String prosperaId) {
        return sentraDao.findByProsperaId(prosperaId);
    }

    @Override
    public Group getGroupByGroupId(Long groupId) {
        // TODO Auto-generated method stub
        return groupDao.getGroupByGroupId(groupId);
    }

    @Override
    @Cacheable(value = "irn.sentra.getSentraByWismaId", unless = "#result == null")
    public List<Sentra> getSentraByWismaId(String wismaId) {
        // TODO Auto-generated method stub
        List<Sentra> userSentra = sentraDao.findByLocationId(wismaId);
        return userSentra;
    }

    @Override
    public Page<Sentra> getSentraByWismaIdPaging(String wismaId, PageRequest pageRequest) {
        return sentraDao.getAllSentraWithLoc(wismaId, pageRequest);
    }

    @Override
    public Page<Sentra> getSentraByWismaIdNoPaging(String wismaId) {
        return sentraDao.getAllSentraWithLoc(wismaId, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public List<Sentra> findAll() {
        // TODO Auto-generated method stub
        return sentraDao.findAll();
    }

}