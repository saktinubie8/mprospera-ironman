package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.UserDao;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.model.user.UserAndSentra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
@Service("userManager")
public class UserManagerImpl implements UserManager {

    @Autowired
    private UserDao userDao;

    @Override
    public User getUserByUsername(String username) {
        return userDao.findByUsername(username.toLowerCase());
    }

    @Override
    public void clearCache() {
    }

    @Override
    @Cacheable(value = "irn.user.userByLocId", unless = "#result == null")
    public List<User> getUserByLocId(String locId) {
        // TODO Auto-generated method stub
        List<User> userWisma = userDao.findByOfficeCode(locId);
        return userWisma;
    }

    @Override
    public List<UserAndSentra> getUserAndSentraByLocationId(String locId) {
        // TODO Auto-generated method stub
        return userDao.findUserAndSentraByLocationId(locId);
    }

    @Override
    //@Cacheable(value = "irn.user.sessionKeyByUsername", unless = "#result == null")
    public String getSessionKeyByUsername(String username) {
        return userDao.findSessionKeyByUsername(username.toLowerCase());
    }

}