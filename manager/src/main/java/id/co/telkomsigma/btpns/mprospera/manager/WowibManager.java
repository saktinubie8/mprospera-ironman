package id.co.telkomsigma.btpns.mprospera.manager;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import id.co.telkomsigma.btpns.mprospera.model.wowib.WowIbBalance;

public interface WowibManager {

    @Cacheable(value = "findById", unless = "#result == null")
    WowIbBalance findById(Long id);

    @Cacheable(value = "findByPhoneNumber", unless = "#result == null")
    WowIbBalance findByPhoneNumber(String phoneNumber);

    @Caching(
            evict = {
                    @CacheEvict(value = "findById", key = "#account.id"),
                    @CacheEvict(value = "findByPhoneNumber", key = "#account.phoneNumber")
            })
    WowIbBalance save(WowIbBalance account);

}
