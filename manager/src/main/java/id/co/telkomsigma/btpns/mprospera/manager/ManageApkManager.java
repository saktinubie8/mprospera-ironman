package id.co.telkomsigma.btpns.mprospera.manager;

public interface ManageApkManager {

    Boolean isApkValid(String version);

    void clearCache();

}