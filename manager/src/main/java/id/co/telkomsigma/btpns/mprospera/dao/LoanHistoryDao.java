package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.loan.LoanHistoryTrx;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LoanHistoryDao extends JpaRepository<LoanHistoryTrx, Long> {

}
