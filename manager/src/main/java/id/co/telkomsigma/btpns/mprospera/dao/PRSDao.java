package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.query.Param;

import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

public interface PRSDao extends JpaRepository<PRS, Long> {

    PRS findByPrsId(Long prsId);

    PRS findTopBySentraIdAndPrsDateOrderByPrsDateDesc(Sentra sentraId, @Temporal(value = TemporalType.DATE) Date prsDate);

    PRS findTopBySentraIdAndPrsDateLessThanEqualOrderByPrsDateDesc(Sentra sentraId,
                                                                   @Temporal(value = TemporalType.DATE) Date prsDate);

    PRS findTopBySentraIdOrderByPrsDateDesc(Sentra sentraId);

    @Query("SELECT p FROM PRS p INNER JOIN p.sentraId s WHERE s.locationId=:loc AND p.prsDate=:prsDate")
    List<PRS> findByPrsAndPrsDate(@Param("loc") String loc, @Param("prsDate") Date prsDate);

    @Query("SELECT p FROM PRS p INNER JOIN p.sentraId s WHERE p.createdBy=:usernamePS AND p.status in ('approved','submit') and p.updateDate >= :prsDate and p.updateDate < :prsDatePlus")
    List<PRS> findByPSAndStatusAndPrsDate(@Param("usernamePS") String usernamePS, @Param("prsDate") Date prsDate, @Param("prsDatePlus") Date prsDatePlus);

}
