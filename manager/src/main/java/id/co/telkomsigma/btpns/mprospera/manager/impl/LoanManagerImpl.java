package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.dao.DisbursementPhotoDao;
import id.co.telkomsigma.btpns.mprospera.dao.DiscountMarginApprovalHistoryDao;
import id.co.telkomsigma.btpns.mprospera.dao.DiscountMarginDao;
import id.co.telkomsigma.btpns.mprospera.dao.DiscountMarginLoanMapDao;
import id.co.telkomsigma.btpns.mprospera.dao.DiscountMarginOfficerMapDao;
import id.co.telkomsigma.btpns.mprospera.dao.LoanDao;
import id.co.telkomsigma.btpns.mprospera.dao.LoanHistoryDao;
import id.co.telkomsigma.btpns.mprospera.dao.LoanNonPrsDao;
import id.co.telkomsigma.btpns.mprospera.dao.LoanPRSDao;
import id.co.telkomsigma.btpns.mprospera.dao.NonPRSPhotoDao;
import id.co.telkomsigma.btpns.mprospera.manager.LoanManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanHistoryTrx;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.DisbursementPhoto;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApproval;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApprovalAndHistory;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApprovalHistory;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginLoanMap;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginOfficerMap;
import id.co.telkomsigma.btpns.mprospera.model.prs.NonPRSPhoto;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;

@Service("loanManager")
public class LoanManagerImpl implements LoanManager {

    @Autowired
    private LoanDao loanDao;

    @Autowired
    private LoanPRSDao loanPRSDao;

    @Autowired
    private LoanNonPrsDao loanNonPRSDao;

    @Autowired
    private LoanHistoryDao loanHistDao;

    @Autowired
    private DisbursementPhotoDao disbursementPhotoDao;

    @Autowired
    private NonPRSPhotoDao nonPrsPhotoDao;

    @Autowired
    private DiscountMarginDao discountMarginDao;

    @Autowired
    private DiscountMarginLoanMapDao discountMarginLoanMapDao;

    @Autowired
    private DiscountMarginOfficerMapDao discountMarginOfficerMapDao;

    @Autowired
    DiscountMarginApprovalHistoryDao discountMarginApprovalHistoryDao;


    @Override
    @Caching(evict = {
            @CacheEvict(value = "irn.discount.margin.findByCustomerIdAndStatusNot", allEntries = true, beforeInvocation = true)
    })
    public void save(DiscountMarginApproval discountMarginApproval) {
        discountMarginDao.save(discountMarginApproval);
    }

    @Override
    public void save(DiscountMarginLoanMap discountMarginLoanMap) {
        discountMarginLoanMapDao.save(discountMarginLoanMap);

    }

    @Override
    public void save(DiscountMarginOfficerMap discountMarginOfficerMap) {
        discountMarginOfficerMapDao.save(discountMarginOfficerMap);
    }

    @Override
    public Page<Loan> findByCreatedDate(Date startDate, Date endDate, String officeId) {
        // TODO Auto-generated method stub
        return loanDao.findByCreatedDate(startDate, endDate, new PageRequest(0, Integer.MAX_VALUE), officeId);
    }

    @Override
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    public Loan findById(long parseLong) {
        // TODO Auto-generated method stub
        return loanDao.findOne(parseLong);
    }

    @Override
    public Loan getLoanByAppId(String appId) {
        // TODO Auto-generated method stub
        return loanDao.findOneByAppId(appId);
    }

    @Override
    public List<LoanPRS> getLoanPRSByPrsId(PRS prs, List<CustomerPRS> customerPRSList) {
        // TODO Auto-generated method stub
        return loanPRSDao.findByPrsIdAndCustomerIdIn(prs, customerPRSList);
    }

    @Override
    public List<LoanPRS> getLoanPRSByPrsIdAndCustomer(PRS prs, CustomerPRS customer) {
        // TODO Auto-generated method stub
        return loanPRSDao.findByPrsIdAndCustomerId(prs, customer);
    }

    @Override
    public List<Loan> getLoanCairBySenta(Long sentraId, Date startDate, Date endDate) {
        return loanDao.findBySentraAndDisburse(sentraId, startDate, endDate, WebGuiConstant.STATUS_APPROVED);
    }

    @Override
    public DisbursementPhoto getDisbursementPhoto(LoanPRS loan) {
        // TODO Auto-generated method stub
        return disbursementPhotoDao.findByLoanPrsId(loan);
    }

    @Override
    public LoanPRS getLoanPRSById(Long loanId) {
        // TODO Auto-generated method stub
        return loanPRSDao.findOne(loanId);
    }

    @Override
    public void saveDisbursementPhoto(DisbursementPhoto disbursementPhoto) {
        disbursementPhotoDao.save(disbursementPhoto);
    }

    @Override
    public List<LoanPRS> getLoanPRSByAppId(String appId) {
        // TODO Auto-generated method stub
        return loanPRSDao.findByAppId(appId);
    }

    @Override
    public LoanPRS findTopByLoanId(Loan loan) {
        // TODO Auto-generated method stub
        return loanPRSDao.findTopByLoanIdOrderByCreatedDateDesc(loan);
    }

    @Override
    @Cacheable(value = "irn.collection.findLoanNonPRSByCustomer", unless = "#result == null")
    public List<LoanNonPRS> findLoanNonPRSByCustomer(Customer customer) {
        // TODO Auto-generated method stub
        return loanNonPRSDao.findByCustomerId(customer);
    }

    @Override
    @Cacheable(value = "irn.loan.findByLoanId", unless = "#result == null")
    public Loan findByLoanId(Long loanId) {
        // TODO Auto-generated method stub
        return loanDao.findByLoanId(loanId);
    }

    @Override
    public LoanNonPRS findLoanNonPRSById(Long id) {
        // TODO Auto-generated method stub
        return loanNonPRSDao.findOne(id);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "irn.collection.findLoanNonPRSByCustomer", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.collection.isNonPrsCustomer", allEntries = true, beforeInvocation = true)
    })
    public LoanNonPRS saveLoanNonPRS(LoanNonPRS loanNonPRS) {
        // TODO Auto-generated method stub
        LoanNonPRS nonPrsFromDb = loanNonPRSDao.findOne(loanNonPRS.getLoanNonPRSId());
        if (nonPrsFromDb != null) {
            nonPrsFromDb.setCreatedBy(loanNonPRS.getCreatedBy());
            nonPrsFromDb.setCreatedDate(nonPrsFromDb.getCreatedDate());
            nonPrsFromDb.setCustomerId(nonPrsFromDb.getCustomerId());
            nonPrsFromDb.setDueDate(nonPrsFromDb.getDueDate());
            nonPrsFromDb.setLoanId(nonPrsFromDb.getLoanId());
            nonPrsFromDb.setMarginAmount(nonPrsFromDb.getMarginAmount());
            nonPrsFromDb.setOutstandingDays(nonPrsFromDb.getOutstandingDays());
            nonPrsFromDb.setOutstandingCount(nonPrsFromDb.getOutstandingCount());
            nonPrsFromDb.setRemainingPrincipal(nonPrsFromDb.getRemainingPrincipal());
            nonPrsFromDb.setSentraId(nonPrsFromDb.getSentraId());
            nonPrsFromDb.setCurrentMargin(loanNonPRS.getCurrentMargin());
            nonPrsFromDb.setInstallmentAmount(loanNonPRS.getInstallmentAmount());
            nonPrsFromDb.setInstallmentPaymentAmount(loanNonPRS.getInstallmentPaymentAmount());
            nonPrsFromDb.setPayCommitment(loanNonPRS.getPayCommitment());
            nonPrsFromDb.setMeetingDate(loanNonPRS.getMeetingDate());
            nonPrsFromDb.setOutstandingAmount(loanNonPRS.getOutstandingAmount());
            nonPrsFromDb.setOutstandingReason(loanNonPRS.getOutstandingReason());
            nonPrsFromDb.setPeopleMeet(loanNonPRS.getPeopleMeet());
            nonPrsFromDb.setStatus(loanNonPRS.getStatus());
            nonPrsFromDb.setUpdatedBy(loanNonPRS.getUpdatedBy());
            nonPrsFromDb.setUpdatedDate(loanNonPRS.getUpdatedDate());
            nonPrsFromDb.setInputMoney(loanNonPRS.getInputMoney());
            nonPrsFromDb.setAutoDebet(loanNonPRS.getAutoDebet());
            nonPrsFromDb.setDepositMoney(loanNonPRS.getDepositMoney());
            nonPrsFromDb.setCommitmentDate(loanNonPRS.getCommitmentDate());
            nonPrsFromDb.setNextCommitmentDate(loanNonPRS.getNextCommitmentDate());
            nonPrsFromDb.setSubmitDate(loanNonPRS.getSubmitDate());
            nonPrsFromDb.setLongitude(loanNonPRS.getLongitude());
            nonPrsFromDb.setLatitude(loanNonPRS.getLatitude());
            loanNonPRSDao.save(nonPrsFromDb);
        }
        return loanNonPRS;
    }

    @Override
    public void saveNonPrsPhoto(NonPRSPhoto nonPrsPhoto) {
        // TODO Auto-generated method stub
        if (nonPrsPhoto.getNonPrsPhotoId() != null) {
            NonPRSPhoto photoFromDb = nonPrsPhotoDao.findOne(nonPrsPhoto.getNonPrsPhotoId());
            if (photoFromDb != null) {
                photoFromDb.setNonPrsPhoto(nonPrsPhoto.getNonPrsPhoto());

            } else {
                nonPrsPhotoDao.save(nonPrsPhoto);
            }
        } else {
            nonPrsPhotoDao.save(nonPrsPhoto);
        }

    }

    @Override
    public NonPRSPhoto getNonPRSPhoto(Long loan) {
        // TODO Auto-generated method stub
        return nonPrsPhotoDao.findByNonPrsId(loan);
    }

    @Override
    public LoanNonPRS findLoanNonPRSByLoanId(Loan loan) {
        // TODO Auto-generated method stub
        return loanNonPRSDao.findByLoanId(loan);
    }

    @Override
    @Cacheable(value = "irn.collection.isNonPrsCustomer", unless = "#result == null")
    public Boolean isNonPrsCustomer(Customer customerId) {
        // TODO Auto-generated method stub
        Integer count = loanNonPRSDao.countByCustomerId(customerId);
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    public LoanPRS findLastLoanPRS(Loan loan, Date createdDate) {
        return loanPRSDao.findTopByLoanIdAndCreatedDateLessThanOrderByCreatedDateDesc(loan, createdDate);
    }

    @Override
    public List<DiscountMarginApproval> getDiscountMarginApprovalByCustomerId(Long customerId) {
        // TODO Auto-generated method stub
        return discountMarginDao.findByCustomerId(customerId);
    }

    @Override
    public DiscountMarginApproval getDiscountMarginApprovalById(Long discountMarginId) {
        return discountMarginDao.findByDiscountMarginId(discountMarginId);
    }

    @Override
    public DiscountMarginApproval findLastDiscountMarginApprovalByCustomer(Long customerId) {
        return discountMarginDao.findTopByCustomerIdOrderByDiscountMarginIdDesc(customerId);
    }

    @Override
    public List<DiscountMarginLoanMap> getDiscountMarginLoanMapByDiscountMarginId(Long discountMarginId) {
        // TODO Auto-generated method stub
        return discountMarginLoanMapDao.findByDiscountMarginId(discountMarginId);
    }

    @Override
    public List<DiscountMarginApprovalHistory> getDiscountMarginApprovalHistoryByDiscountMarginId(
            Long discountMarginId) {
        // TODO Auto-generated method stub
        return discountMarginApprovalHistoryDao.findByDiscountMarginId(discountMarginId);
    }

    @Override
    public List<DiscountMarginApprovalAndHistory> getDiscountMarginApprovalAndHistory(String userId) {
        String status = WebGuiConstant.STATUS_WAITING_FOR_APPROVAL;
        return discountMarginApprovalHistoryDao.findDMAPAndDMAHistoryByStatus(status);
    }

    @Override
    public void save(DiscountMarginApprovalHistory discountMarginApprovalHistory) {
        discountMarginApprovalHistoryDao.save(discountMarginApprovalHistory);
    }

    @Override
    public DiscountMarginApprovalHistory findByDiscountMarginIdAndLevel(Long discountMarginId, String level) {
        // TODO Auto-generated method stub
        return discountMarginApprovalHistoryDao.findByDiscountMarginIdAndLevel(discountMarginId, level);
    }

    @Override
    public List<Loan> findByCustomer(Customer customer) {
        return loanDao.findByCustomer(customer);
    }

    @Override
    public DiscountMarginLoanMap getDiscountMarginLoanMapByLoanPrsIdAndDiscountMarginId(Long loanPrsId,
                                                                                        Long discountMarginId) {
        // TODO Auto-generated method stub
        return discountMarginLoanMapDao.findByLoanPrsIdAndDiscountMarginId(loanPrsId, discountMarginId);
    }

    @Override
    public List<LoanPRS> getLoanPRSListByAppIdAndDisbursementAmount(String appId) {
        // TODO Auto-generated method stub
        return loanPRSDao.findByAppIdAndDisbursementAmountIsNotNull(appId);
    }

    @Override
    public List<LoanPRS> findLoanPRSByPrsId(PRS prs) {
        // TODO Auto-generated method stub
        return loanPRSDao.findLoanPRSByPrsId(prs);
    }

    @Override
    public LoanPRS findTopByAppIdOrderByLoanPRSIdDesc(String appId) {
        // TODO Auto-generated method stub
        return loanPRSDao.findTopByAppIdOrderByLoanPRSIdDesc(appId);
    }

    @Override
    public List<LoanPRS> findBySentraAndPrsDateAndDisbursmentFlag(Long sentraId, Date prsDate, Date prsDate2,
                                                                  char disbursementFlag) {
        // TODO Auto-generated method stub
        return loanPRSDao.findBySentraAndPrsDateAndDisbursmentFlag(sentraId, prsDate, disbursementFlag);
    }

    @Override
    @Cacheable(value = "irn.discount.margin.findByCustomerIdAndStatusNot", unless = "#result == null")
    public List<DiscountMarginApproval> findByCustomerIdAndStatusNotRejected(Long customerId) {
        // TODO Auto-generated method stub
        return discountMarginDao.findByCustomerIdAndStatusNot(customerId, WebGuiConstant.STATUS_REJECTED);
    }

}