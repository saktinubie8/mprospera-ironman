package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwIdPhoto;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwSurveyPhoto;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

public interface SWManager {

    int countAll();

    Page<SurveyWawancara> findAll(List<String> kelIdList);

    Page<SurveyWawancara> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate);

    void save(SurveyWawancara surveyWawancara);

    void save(SwIdPhoto swIdPhoto);

    void save(SwSurveyPhoto swSurveyPhoto);

    SwIdPhoto getSwIdPhoto(String swId);

    SwSurveyPhoto getSwSurveyPhoto(String swId);

    SurveyWawancara getSWById(String swId);

    void clearCache();

    LoanProduct findByProductId(String productId);

    SurveyWawancara findByCustomerId(Long customerId);

}