package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;

import java.util.List;


/**
 * Created by Dzulfiqar on 11/12/15.
 */
public interface ParamManager {

    List<PRSParameter> getAllPrsParam(String paramGroup);

    SystemParameter getParamByParamName(String paramName);

    void clearCache();

    PRSParameter findByParameterId(Long parameterId);

    PRSParameter getFileDayaByFileName(String fileName);

}