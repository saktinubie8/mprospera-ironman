package id.co.telkomsigma.btpns.mprospera.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.LoanManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.DisbursementPhoto;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApproval;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApprovalAndHistory;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApprovalHistory;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginLoanMap;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginOfficerMap;
import id.co.telkomsigma.btpns.mprospera.model.prs.NonPRSPhoto;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;

@Service("loanService")
public class LoanService extends GenericService {

    @Autowired
    private LoanManager loanManager;

    public void save(DiscountMarginApproval discountMarginApproval) {
        loanManager.save(discountMarginApproval);
    }

    public void save(DiscountMarginLoanMap discountMarginLoanMap) {
        loanManager.save(discountMarginLoanMap);
    }

    public void save(DiscountMarginOfficerMap discountMarginOfficerMap) {
        loanManager.save(discountMarginOfficerMap);
    }

    public void save(DiscountMarginApprovalHistory discountMarginApprovalHistory) {
        loanManager.save(discountMarginApprovalHistory);
    }

    public Loan findById(String loanId) {
        if (loanId == null || "".equals(loanId))
            return null;
        return loanManager.findById(Long.parseLong(loanId));
    }

    public Loan getLoanByAppId(String appId) {
        return loanManager.getLoanByAppId(appId);
    }

    public List<LoanPRS> getLoanPRSByPrsId(PRS prs, List<CustomerPRS> customerPRSList) {
        return loanManager.getLoanPRSByPrsId(prs, customerPRSList);
    }

    public List<LoanPRS> getLoanPRSByPrsId(PRS prs, CustomerPRS customer) {
        return loanManager.getLoanPRSByPrsIdAndCustomer(prs, customer);
    }

    public List<Loan> getLoanCairBySentra(Long sentraId, Date startDate, Date endDate) {
        return loanManager.getLoanCairBySenta(sentraId, startDate, endDate);
    }

    public LoanPRS getLoanPRSById(Long loanId) {
        return loanManager.getLoanPRSById(loanId);
    }

    public LoanPRS getLoanPRSTop(Loan loan) {
        return loanManager.findTopByLoanId(loan);
    }

    public List<LoanPRS> getLoanPRSByAppId(String appId) {
        return loanManager.getLoanPRSByAppId(appId);
    }

    public LoanNonPRS saveLoanNonPRS(LoanNonPRS loanNonPRS) {
        return loanManager.saveLoanNonPRS(loanNonPRS);
    }

    public DisbursementPhoto getDisbursementPhoto(LoanPRS loan) {
        return loanManager.getDisbursementPhoto(loan);
    }

    public NonPRSPhoto getNonPRSPhoto(Long loan) {
        return loanManager.getNonPRSPhoto(loan);
    }

    public void saveDisbursementPhoto(DisbursementPhoto disbursementPhoto) {
        loanManager.saveDisbursementPhoto(disbursementPhoto);
    }

    public void saveNonPrsPhoto(NonPRSPhoto nonPrsPhoto) {
        loanManager.saveNonPrsPhoto(nonPrsPhoto);
    }

    public List<LoanNonPRS> getLoanNonPRSByCustomer(Customer customer) {
        return loanManager.findLoanNonPRSByCustomer(customer);
    }

    public Loan getByLoanId(Long loanId) {
        return loanManager.findByLoanId(loanId);
    }

    public LoanNonPRS getLoanNonPRSById(Long id) {
        return loanManager.findLoanNonPRSById(id);
    }

    public LoanNonPRS getLoanNonPRSByLoanId(Loan loan) {
        return loanManager.findLoanNonPRSByLoanId(loan);
    }

    public Boolean isNonPRSCustomer(Customer customerId) {
        return loanManager.isNonPrsCustomer(customerId);
    }

    public LoanPRS findLastLoanPrs(Loan loan, Date createdDate) {
        return loanManager.findLastLoanPRS(loan, createdDate);
    }

    public List<DiscountMarginApproval> getDiscountMarginApprovalByCustomerId(Long customerId) {
        return loanManager.getDiscountMarginApprovalByCustomerId(customerId);
    }

    public DiscountMarginApproval getDiscontMarginApprovalById(Long discountMarginId) {
        return loanManager.getDiscountMarginApprovalById(discountMarginId);
    }

    public DiscountMarginApproval findLastDiscountMarginApprovalByCustomer(Long customer) {
        return loanManager.findLastDiscountMarginApprovalByCustomer(customer);
    }

    public List<DiscountMarginLoanMap> getDiscountMarginLoanMapByDiscountMarginId(Long discountMarginId) {
        return loanManager.getDiscountMarginLoanMapByDiscountMarginId(discountMarginId);
    }

    public List<DiscountMarginApprovalHistory> getDiscountMarginApprovalHistoryByDiscountMarginId(
            Long discountMarginId) {
        // TODO Auto-generated method stub
        return loanManager.getDiscountMarginApprovalHistoryByDiscountMarginId(discountMarginId);
    }

    public List<DiscountMarginApprovalAndHistory> getDiscountMarginApprovalAndHistory(String userId) {
        return loanManager.getDiscountMarginApprovalAndHistory(userId);
    }

    public DiscountMarginApprovalHistory findByDiscountMarginIdAndLevel(Long discountMarginId, String level) {
        // TODO Auto-generated method stub
        return loanManager.findByDiscountMarginIdAndLevel(discountMarginId, level);
    }

    public List<Loan> findByCustomer(Customer customer) {
        return loanManager.findByCustomer(customer);
    }

    public DiscountMarginLoanMap getDiscountMarginLoanMapByLoanPrsIdAndDiscountMarginId(Long loanPrsId, Long discountMarginId) {
        return loanManager.getDiscountMarginLoanMapByLoanPrsIdAndDiscountMarginId(loanPrsId, discountMarginId);
    }

    public List<LoanPRS> getLoanPRSListByAppIdAndDisbursementAmount(String appId) {
        // TODO Auto-generated method stub
        return loanManager.getLoanPRSListByAppIdAndDisbursementAmount(appId);
    }

    public List<LoanPRS> findLoanPRSByPrsId(PRS prs) {
        return loanManager.findLoanPRSByPrsId(prs);
    }

    public LoanPRS getTopLoanPrsByAppId(String appId) {
        return loanManager.findTopByAppIdOrderByLoanPRSIdDesc(appId);
    }

    public List<LoanPRS> findBySentraAndPrsDateAndDisbursmentFlag(Long sentraId, Date prsDate, Date prsDate2, char disbursementFlag) {
        return loanManager.findBySentraAndPrsDateAndDisbursmentFlag(sentraId, prsDate, prsDate2, disbursementFlag);
    }

    public List<DiscountMarginApproval> findByCustomerIdAndStatusNotRejected(Long customerId) {
        return loanManager.findByCustomerIdAndStatusNotRejected(customerId);
    }

}