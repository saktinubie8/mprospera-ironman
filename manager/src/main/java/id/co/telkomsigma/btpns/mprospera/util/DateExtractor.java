package id.co.telkomsigma.btpns.mprospera.util;

import java.util.Calendar;
import java.util.Date;

public class DateExtractor {

    public Date getDateBeforeDays(int daysBefore) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, daysBefore);
        return cal.getTime();
    }

}
