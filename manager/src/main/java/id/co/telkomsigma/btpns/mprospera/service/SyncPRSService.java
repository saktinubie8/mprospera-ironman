package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.feign.VisionInterface;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.*;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.util.DateExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("syncPRSService")
public class SyncPRSService extends GenericService {

    @Autowired
    private VisionInterface visionInterface;

    @Autowired
    private PRSService prsService;

    @Autowired
    private ParameterService parameterService;

    @Autowired
    private UserService userService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private SWService swService;

    @Autowired
    private SavingService savingService;

    public PRSResponseList setPrsData(String fetchPrevious, Sentra sentra) {
        DateExtractor dateExtractor = new DateExtractor();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final DecimalFormat decimalFormat = new DecimalFormat("###");
        String paramApkVersion = parameterService.loadParamByParamName(WebGuiConstant.PARAMETER_CURRENT_VERSION, "1.0");
        boolean disbursementPlan = false;
        PRS prs = new PRS();
        PRSResponseList prsSet = new PRSResponseList();
        log.info("Pengganti Hari Libur : " + fetchPrevious);
        if (fetchPrevious.equals("true")) {
            prs = prsService.getPRSBySentraPrevious(sentra, new Date());
            if (prs != null) {
                if (prs.getStatus().equals("APPROVED") || prs.getStatus().equals("MANUAL_KFO")) {
                    prs = null;
                }
            }
        } else {
            prs = prsService.getPRSBySentra(sentra, new Date());
        }
        if (prs != null) {
            log.info("INITIATE DATA");
            log.info("PRS Status : " + prs.getStatus());
            prsSet.setPrsId(prs.getPrsId().toString());
            prsSet.setSentraId(prs.getSentraId().getSentraId().toString());
            prsSet.setSentraCode(prs.getSentraId().getSentraCode());
            prsSet.setSentraName(prs.getSentraId().getSentraName());
            prsSet.setReport(prs.getReport());
            if (prs.getUpdateDate() != null) {
                prsSet.setUpdatedDate(formatter.format(prs.getUpdateDate()));
            }
            prsSet.setHasPrsPhoto(
                    WebGuiConstant.TERMINAL_PRS_PHOTO_REQUEST.replace("**/{apkVersion:.+}", "/" + paramApkVersion) + "/"
                            + prs.getPrsId().toString());
            if (prs.getPrsDate() != null) {
                prsSet.setPrsDate(formatter.format(prs.getPrsDate()));
            }
            if (prs.getPrsTime() != null) {
                prsSet.setPrsTime(prs.getPrsTime());
            }
            prsSet.setStatus(prs.getStatus());
            prsSet.setBringMoney(prs.getBringMoney());
            if (prs.getPsCompanion() != null) {
                prsSet.setPsCompanion(prs.getPsCompanion());
            }
            List<PSCompanionPojo> psCompanionListTemp = new ArrayList<>();
            List<User> psCompanionList = userService.loadUserByLocationId(prs.getSentraId().getLocationId());
            for (User psCompanion : psCompanionList) {
                PSCompanionPojo psCompanionSet = new PSCompanionPojo();
                psCompanionSet.setPsId(psCompanion.getUserId().toString());
                psCompanionSet.setCode(psCompanion.getUsername());
                psCompanionSet.setPsName(psCompanion.getName());
                psCompanionListTemp.add(psCompanionSet);
            }
            prsSet.setPsIdCompanionList(psCompanionListTemp);
            List<CustomerPRSListResponse> customerPRSListTemp = new ArrayList<>();
            List<CustomerPRS> customerPRSList = customerService.getCustomerPRSByPrsId(prs);
            if (customerPRSList.isEmpty()) {
                prsSet.setCustomerList(null);
            }
            List<LoanPRS> loanPRSList = loanService.getLoanPRSByPrsId(prs, customerPRSList);
            List<SavingPRS> savingPRSList = savingService.getSavingByPRSIdAndCustomerIdIn(prs, customerPRSList);

            for (CustomerPRS customerPRS : customerPRSList) {
                CustomerPRSListResponse customerPRSSet = new CustomerPRSListResponse();
                customerPRSSet.setCustomerId(customerPRS.getCustomerId().getCustomerId().toString());
                customerPRSSet.setCustomerName(customerPRS.getCustomerId().getCustomerName());
                customerPRSSet.setCustomerCif(customerPRS.getCustomerId().getCustomerCifNumber());
                if (customerPRS.getCustomerId().getSwId() != null) {
                    customerPRSSet.setSwId(customerPRS.getCustomerId().getSwId().toString());
                }
                customerPRSSet.setCustomerPrsId(customerPRS.getCustomerPrsId().toString());
                if (customerPRS.getGroupId() != null) {
                    customerPRSSet.setGroupId(customerPRS.getGroupId().getGroupId().toString());
                    customerPRSSet.setGroupName(customerPRS.getGroupId().getGroupName());
                }
                if (customerPRS.getIsAttend() != null) {
                    customerPRSSet.setIsAttend(customerPRS.getIsAttend());
                }
                if (customerPRS.getNotAttendReason() != null) {
                    customerPRSSet.setNotAttendReason(customerPRS.getNotAttendReason().toString());
                }
                List<LoanPRSListResponse> loanPRSListTemp = new ArrayList<>();

                if (loanPRSList.isEmpty()) {
                    customerPRSSet.setLoanList(null);
                }
                for (LoanPRS loanPRS : loanPRSList) {
                    LoanPRSListResponse loanPRSSet = new LoanPRSListResponse();
                    log.info("loanPRS.CusPrsId : " + loanPRS.getCustomerId().getCustomerPrsId() + ", customerPRS.CusPrsId : " + customerPRS.getCustomerPrsId());
                    if ((loanPRS.getCustomerId().getCustomerPrsId().toString()).equals(customerPRS.getCustomerPrsId().toString())) {
                        log.info("Loan ID : " + loanPRS.getLoanId().getLoanId().toString());
                        Loan loan = loanService.findById(loanPRS.getLoanId().getLoanId().toString());
                        if (loan != null) {
                            log.info("Loan Found");
                            if (loan.getStatus().equals(WebGuiConstant.STATUS_APPROVED)
                                    || loan.getStatus().equals(WebGuiConstant.STATUS_ACTIVE)) {
                                if (loan.getProductId() != null) {
                                    LoanProduct loanProduct = swService.findByProductId(loan.getProductId().toString());
                                    if (loanProduct != null) {
                                        if (loanProduct.getProductCode().toUpperCase().startsWith("NOV")) {
                                            loanPRSSet.setIsLoanPhone("true");
                                            loanPRSSet.setProductType("phone");
                                        } else {
                                            loanPRSSet.setIsLoanPhone("false");
                                            loanPRSSet.setProductType("cash");
                                        }
                                    }
                                }
                                loanPRSSet.setLoanPrsId(loanPRS.getLoanPRSId().toString());
                                loanPRSSet.setLoanId(loanPRS.getLoanId().getLoanId().toString());
                                if (loanPRS.getLoanId().getCreatedDate() != null) {
                                    if (!"".equals(loanPRS.getLoanId().getCreatedDate())) {
                                        loanPRSSet.setLoanCreatedDate(
                                                formatDateTime.format(loanPRS.getLoanId().getCreatedDate()));
                                    }
                                }
                                loanPRSSet.setAppId(loanPRS.getAppId());
                                if (loanPRS.getDisbursementAmount() != null) {
                                    if (!decimalFormat.format(loanPRS.getDisbursementAmount()).equals("0")) {
                                        loanPRSSet.setDisbursementAmount(loanPRS.getDisbursementAmount());
                                        disbursementPlan = true;
                                    } else {
                                        loanPRSSet.setDisbursementAmount(loanPRS.getDisbursementAmount().ZERO);
                                        disbursementPlan = false;
                                    }
                                } else {
                                    loanPRSSet.setDisbursementAmount(loanPRS.getDisbursementAmount().ZERO);
                                }
                                loanPRSSet.setDisbursementFlag(String.valueOf(loanPRS.getDisbursementFlag()));
                                if (loanPRS.getCanPendingDisbursement() != null) {
                                    loanPRSSet.setCanPendingDisbursement(loanPRS.getCanPendingDisbursement());
                                }
                                if (disbursementPlan) {
                                    loanPRSSet.setHasDisbursementPhoto(
                                            WebGuiConstant.TERMINAL_GET_DISBURSEMENT_PHOTO_REQUEST
                                                    .replace("**/{apkVersion:.+}", "/" + paramApkVersion) + "/"
                                                    + loanPRS.getLoanPRSId().toString());
                                }
                                if (loanPRS.getInstallmentAmount() != null) {
                                    if (disbursementPlan) {
                                        loanPRSSet.setInstallmentAmount(loanPRS.getInstallmentAmount());
                                    } else {
                                        loanPRSSet.setInstallmentAmount(loanPRS.getInstallmentAmount());
                                    }
                                } else {
                                    loanPRSSet.setInstallmentAmount(loanPRS.getInstallmentAmount().ZERO);
                                }
                                if (loanPRS.getCurrentMargin() != null) {
                                    if (disbursementPlan) {
                                        loanPRSSet.setCurrentMargin(loanPRS.getCurrentMargin());
                                    } else {
                                        loanPRSSet.setCurrentMargin(loanPRS.getCurrentMargin());
                                    }
                                } else {
                                    loanPRSSet.setCurrentMargin(loanPRS.getCurrentMargin().ZERO);
                                }

                                if (loanPRS.getUseEmergencyFund() != null) {
                                    loanPRSSet.setUseEmergencyFund(loanPRS.getUseEmergencyFund());
                                }
                                if (loanPRS.getMarginAmount() != null) {
                                    loanPRSSet.setMarginAmount(loanPRS.getMarginAmount());
                                } else {
                                    loanPRSSet.setMarginAmount(loanPRS.getMarginAmount().ZERO);
                                }
                                if (loanPRS.getPlafonAmount() != null) {
                                    loanPRSSet.setPlafonAmount(loanPRS.getPlafonAmount());
                                } else {
                                    loanPRSSet.setPlafonAmount(loanPRS.getPlafonAmount().ZERO);
                                }
                                if (loanPRS.getOutstandingAmount() != null) {
                                    if (disbursementPlan) {
                                        loanPRSSet.setOutstandingAmount(loanPRS.getOutstandingAmount().ZERO);
                                    } else {
                                        loanPRSSet.setOutstandingAmount(loanPRS.getOutstandingAmount());
                                    }
                                } else {
                                    loanPRSSet.setOutstandingAmount(loanPRS.getOutstandingAmount().ZERO);
                                }
                                if (loanPRS.getOutstandingCount() != null) {
                                    loanPRSSet.setOutstandingCount(loanPRS.getOutstandingCount().toString());
                                } else {
                                    loanPRSSet.setOutstandingCount("0");
                                }
                                if (loanPRS.getRemainingPrincipal() != null) {
                                    loanPRSSet.setRemainingPrincipal(loanPRS.getRemainingPrincipal());
                                } else {
                                    loanPRSSet.setRemainingPrincipal(loanPRS.getRemainingPrincipal().ZERO);
                                }
                                if (loanPRS.getRemainingOutstandingPrincipal() != null) {
                                    loanPRSSet.setRemainingOutstandingPrincipal(
                                            loanPRS.getRemainingOutstandingPrincipal());
                                } else {
                                    loanPRSSet.setRemainingOutstandingPrincipal(
                                            loanPRS.getRemainingOutstandingPrincipal().ZERO);
                                }
                                if (loanPRS.getOutstandingMarginAmount() != null) {
                                    loanPRSSet.setOutstandingMarginAmount(loanPRS.getOutstandingMarginAmount());
                                } else {
                                    loanPRSSet.setOutstandingMarginAmount(loanPRS.getOutstandingMarginAmount().ZERO);
                                }
                                if (loanPRS.getDueDate() != null)
                                    loanPRSSet.setDueDate(formatter.format(loanPRS.getDueDate()));
                                if (loanPRS.getOverdueDays() != null) {
                                    if (!disbursementPlan)
                                        loanPRSSet.setOverdueDays(loanPRS.getOverdueDays().toString());
                                }
                                LoanPRS lastLoanPrs = loanService.findLastLoanPrs(loan, new Date());
                                if (lastLoanPrs != null) {
                                    DiscountMarginApproval discountMarginApproval = loanService
                                            .findLastDiscountMarginApprovalByCustomer(
                                                    lastLoanPrs.getCustomerId().getCustomerId().getCustomerId());
                                    if (discountMarginApproval != null) {
                                        if (discountMarginApproval.getStatus() != null) {
                                            if (discountMarginApproval.getStatus()
                                                    .equals(WebGuiConstant.STATUS_APPROVED)
                                                    || discountMarginApproval.getStatus()
                                                    .equals(WebGuiConstant.STATUS_WAITING_FOR_APPROVAL)) {
                                                DiscountMarginLoanMap discountMarginLoanMap = loanService
                                                        .getDiscountMarginLoanMapByLoanPrsIdAndDiscountMarginId(
                                                                lastLoanPrs.getLoanPRSId(),
                                                                discountMarginApproval.getDiscountMarginId());
                                                if (discountMarginLoanMap != null) {
                                                    loanPRSSet.setIsEarlyTermination("true");
                                                } else {
                                                    loanPRSSet.setIsEarlyTermination("false");
                                                }
                                            } else {
                                                loanPRSSet.setIsEarlyTermination("false");
                                            }
                                        }
                                    }
                                }
                                if (loanPRS.getIsEarlyTermination() != null) {
                                    if (loanPRS.getIsEarlyTermination() == true) {
                                        loanPRSSet.setIsEarlyTermination("true");
                                    } else {
                                        loanPRSSet.setIsEarlyTermination("false");
                                    }
                                }
                                if (loanPRS.getIsEarlyTerminationAdHoc() != null) {
                                    if (loanPRS.getIsEarlyTerminationAdHoc() == true) {
                                        loanPRSSet.setIsEarlyTerminationAdhoc("true");
                                    } else {
                                        loanPRSSet.setIsEarlyTerminationAdhoc("false");
                                    }
                                }
                                if (loanPRS.getEarlyTerminationReason() != null) {
                                    loanPRSSet.setEarlyTerminationReason(loanPRS.getEarlyTerminationReason());
                                }
                                if (loanPRS.getMarginDiscountDeviationFlag() != null) {
                                    if (loanPRS.getMarginDiscountDeviationFlag() == true) {
                                        loanPRSSet.setMarginDiscountDeviationFlag("true");
                                    } else {
                                        loanPRSSet.setMarginDiscountDeviationFlag("false");
                                    }
                                }
                                if (loanPRS.getMarginDiscountDeviationAmount() != null) {
                                    loanPRSSet.setMarginDiscountDeviationAmount(
                                            loanPRS.getMarginDiscountDeviationAmount());
                                } else {
                                    loanPRSSet.setMarginDiscountDeviationAmount(
                                            loanPRS.getMarginDiscountDeviationAmount().ZERO);
                                }
                                if (loanPRS.getInstallmentPaymentAmount() != null) {
                                    if (loanPRSSet.getMarginDiscountDeviationAmount().compareTo(BigDecimal.ZERO) > 0
                                            && loanPRSSet.getIsEarlyTermination().equals("true")) {
                                        loanPRSSet.setInstallmentPaymentAmount(loanPRS.getInstallmentPaymentAmount()
                                                .subtract(loanPRSSet.getMarginDiscountDeviationAmount()));
                                    } else {
                                        loanPRSSet.setInstallmentPaymentAmount(loanPRS.getInstallmentPaymentAmount());
                                    }
                                } else {
                                    loanPRSSet.setInstallmentPaymentAmount(loanPRS.getInstallmentPaymentAmount().ZERO);
                                }
                                if (loanPRS.getWowIbStatus() != null) {
                                    loanPRSSet.setWowIbStatus(loanPRS.getWowIbStatus());
                                }
                                loanPRSSet.setHasDisbursementPlan(disbursementPlan);
                                if (loanPRS.getAngsuran() != null) {
                                    loanPRSSet.setAngsuran(loanPRS.getAngsuran().toString());
                                }
                                if (loanPRS.getTenor() != null) {
                                    loanPRSSet.setTenor(loanPRS.getTenor().toString());
                                }
                                List<DiscountMarginApproval> discountMarginApprovalList = loanService
                                        .getDiscountMarginApprovalByCustomerId(
                                                customerPRS.getCustomerId().getCustomerId());
                                if (discountMarginApprovalList != null || !discountMarginApprovalList.isEmpty()) {
                                    for (DiscountMarginApproval discountMarginApproval : discountMarginApprovalList) {
                                        DiscountMarginLoanMap discountMarginLoanMap = loanService
                                                .getDiscountMarginLoanMapByLoanPrsIdAndDiscountMarginId(
                                                        loanPRS.getLoanPRSId(),
                                                        discountMarginApproval.getDiscountMarginId());
                                        if (discountMarginLoanMap != null) {
                                            if (loanPRS.getPrsId().getPrsDate()
                                                    .after(dateExtractor.getDateBeforeDays(-14))) {
                                                DiscountMarginLoanResponse discountMarginLoanResponse = new DiscountMarginLoanResponse();
                                                discountMarginLoanResponse.setAmount(discountMarginLoanMap.getAmount());
                                                discountMarginLoanResponse.setDiscountMarginId(
                                                        discountMarginLoanMap.getDiscountMarginId().toString());
                                                discountMarginLoanResponse.setStatus(discountMarginLoanMap.getStatus());
                                                loanPRSSet.setDiscountMargin(discountMarginLoanResponse);
                                            }
                                        }
                                    }
                                }
                                log.info("DISBURSEMENT PLAN : " + disbursementPlan);
                                if (!"0".equals(loanPRSSet.getOutstandingCount()) || disbursementPlan == true) {
                                    loanPRSListTemp.add(loanPRSSet);
                                }
                            }
                        }
                    }
                }
                if (disbursementPlan) {
                    Long swId = customerPRS.getCustomerId().getSwId();
                    if (swId != null) {
                        customerPRSSet.setHasIdCardPhoto(WebGuiConstant.TERMINAL_GET_ID_PHOTO_REQUEST
                                .replace("**/{apkVersion:.+}", "/" + paramApkVersion) + "/"
                                + customerPRS.getCustomerId().getCustomerId().toString());
                        customerPRSSet.setHasBusinessPlacePhoto(WebGuiConstant.TERMINAL_BUSINESS_PLACE_PHOTO_REQUEST
                                .replace("**/{apkVersion:.+}", "/" + paramApkVersion) + "/"
                                + customerPRS.getCustomerId().getCustomerId().toString());
                    } else {
                        customerPRSSet.setHasIdCardPhoto("");
                        customerPRSSet.setHasBusinessPlacePhoto("");
                    }
                }
                customerPRSSet.setLoanList(loanPRSListTemp);
                List<SavingPRSListResponse> savingPRSListTemp = new ArrayList<>();
                if (savingPRSList.isEmpty()) {
                    customerPRSSet.setSavingList(null);
                }
                for (SavingPRS savingPrs : savingPRSList) {
                    SavingPRSListResponse savingPRSSet = new SavingPRSListResponse();
                    log.info("savingPrs.CusPrsId : " + savingPrs.getCustomerId().getCustomerPrsId() + ", customerPRS.CusPrsId : " + customerPRS.getCustomerPrsId());
                    if ((savingPrs.getCustomerId().getCustomerPrsId().toString()).equals(customerPRS.getCustomerPrsId().toString())) {
                        savingPRSSet.setAccountId(savingPrs.getAccountId().toString());
                        savingPRSSet.setAccountNumber(savingPrs.getAccountNumber());
                        if (savingPrs.getWithdrawalAmount() != null) {
                            savingPRSSet.setWithdrawalAmount(savingPrs.getWithdrawalAmount());
                        } else {
                            savingPRSSet.setWithdrawalAmount(savingPrs.getWithdrawalAmount().ZERO);
                        }
                        if (savingPrs.getWithdrawalAmountPlan() != null)
                            savingPRSSet.setWithdrawalAmountPlan(savingPrs.getWithdrawalAmountPlan().toString());
                        if (savingPrs.getNextWithdrawalAmountPlan() != null)
                            savingPRSSet
                                    .setNextWithdrawalAmountPlan(savingPrs.getNextWithdrawalAmountPlan().toString());
                        if (savingPrs.getIsCloseSavingAccount() != null) {
                            if (savingPrs.getIsCloseSavingAccount() == true) {
                                savingPRSSet.setIsCloseSavingAccount("true");
                            } else {
                                savingPRSSet.setIsCloseSavingAccount("false");
                            }
                        }
                        if (savingPrs.getHoldBalance() != null) {
                            savingPRSSet.setHoldBalance(savingPrs.getHoldBalance());
                        } else {
                            savingPRSSet.setHoldBalance(savingPrs.getHoldBalance().ZERO);
                        }
                        if (savingPrs.getClearBalance() != null) {
                            savingPRSSet.setClearBalance(savingPrs.getClearBalance());
                        } else {
                            savingPRSSet.setClearBalance(savingPrs.getClearBalance().ZERO);
                        }
                        if (savingPrs.getDepositAmount() != null) {
                            savingPRSSet.setDepositAmount(savingPrs.getDepositAmount());
                        } else {
                            savingPRSSet.setDepositAmount(savingPrs.getDepositAmount().ZERO);
                        }
                        savingPRSListTemp.add(savingPRSSet);
                    }
                }
                customerPRSSet.setSavingList(savingPRSListTemp);
                customerPRSListTemp.add(customerPRSSet);
            }
            prsSet.setCustomerList(customerPRSListTemp);
        }
        return prsSet;
    }
}