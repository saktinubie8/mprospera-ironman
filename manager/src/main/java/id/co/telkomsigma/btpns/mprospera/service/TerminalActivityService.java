package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.TerminalActivityManager;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
@Service("terminalActivityService")
public class TerminalActivityService extends GenericService {

    @Autowired
    private TerminalActivityManager terminalActivityManager;

    public void saveTerminalActivityAndMessageLogs(final TerminalActivity terminalActivity, final List<MessageLogs> messageLogs) {
        if (terminalActivity.getTerminalActivityId() == null)
            terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
        terminalActivityManager.insertTerminalActivity(terminalActivity);
    }

}