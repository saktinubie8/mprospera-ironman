package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanNonPRS;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface LoanNonPrsDao extends JpaRepository<LoanNonPRS, Long> {

    LoanNonPRS findByLoanId(Loan loan);

    List<LoanNonPRS> findByCustomerId(Customer customer);

    @Query("Select count(a) from LoanNonPRS a where a.customerId = :customerId")
    Integer countByCustomerId(@Param("customerId") Customer customerId);

}
