package id.co.telkomsigma.btpns.mprospera.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.SurveyManager;
import id.co.telkomsigma.btpns.mprospera.model.survey.Survey;

@Service("surveyService")
public class SurveyService extends GenericService {

    @Autowired
    private SurveyManager surveyManager;

    public void save(Survey survey) {
        surveyManager.save(survey);
    }

}