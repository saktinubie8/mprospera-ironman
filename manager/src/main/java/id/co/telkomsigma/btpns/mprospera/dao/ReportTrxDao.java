package id.co.telkomsigma.btpns.mprospera.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.prs.ReportTrxTable;


public interface ReportTrxDao extends JpaRepository<ReportTrxTable, Long> {

    @Query("SELECT a FROM ReportTrxTable a WHERE DATEDIFF(DAY,a.approvedDate,:adate)=0 AND DATEDIFF(DAY,a.approvedDate,a.businessDate)=0"
            + " AND a.approvedBy = :approveBy"
            + " ORDER BY a.approvedDate")
    List<ReportTrxTable> InCOT_findTopByApprovedByOrderByApprovedDateDesc(@Param("adate") Date aDate, @Param("approveBy") String approveBy);

    @Query("SELECT a FROM ReportTrxTable a WHERE DATEDIFF(DAY,a.approvedDate,:adate)=0 AND DATEDIFF(DAY,a.approvedDate,a.businessDate)>0"
            + " AND a.approvedBy = :approveBy"
            + " ORDER BY a.approvedDate")
    List<ReportTrxTable> OutCOT_findTopByApprovedByOrderByApprovedDateDesc(@Param("adate") Date aDate, @Param("approveBy") String approveBy);

    //1 = Pencairan, 4 = Tarik Tabungan,
    //2 = Pembayaran Angsuran, 3 = Pelunasan Dipercepat, 5 = Setor Tabungan

    @Query("SELECT SUM(a.amount) FROM ReportTrxTable a WHERE DATEDIFF(DAY,a.approvedDate,:adate)=0 AND DATEDIFF(DAY,a.approvedDate,a.businessDate)=0"
            + "AND a.approvedBy = :approveBy AND a.typeTrx IN ('2','3','5')")
    BigDecimal sumAmountTrxInBeforeCOT(@Param("adate") Date aDate, @Param("approveBy") String approveBy);

    @Query("SELECT SUM(a.amount) FROM ReportTrxTable a WHERE DATEDIFF(DAY,a.approvedDate,:adate)=0 AND DATEDIFF(DAY,a.approvedDate,a.businessDate)>0"
            + "AND a.approvedBy = :approveBy AND a.typeTrx IN ('2','3','5')")
    BigDecimal sumAmountTrxInAfterCOT(@Param("adate") Date aDate, @Param("approveBy") String approveBy);

    @Query("SELECT SUM(a.amount) FROM ReportTrxTable a WHERE DATEDIFF(DAY,a.approvedDate,:adate)=0 AND DATEDIFF(DAY,a.approvedDate,a.businessDate)=0"
            + "AND a.approvedBy = :approveBy AND a.typeTrx IN :listTypeTrx")
    BigDecimal sumAmountTrxBeforeCOT(@Param("adate") Date aDate, @Param("listTypeTrx") List<String> listTypeTrx, @Param("approveBy") String approveBy);

}
