package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;

import javax.persistence.TemporalType;

public interface SavingPRSDao extends JpaRepository<SavingPRS, Long> {

    List<SavingPRS> findByPrsIdAndCustomerId(PRS prs, CustomerPRS customer);

    List<SavingPRS> findByPrsIdAndCustomerIdIn(PRS prs, List<CustomerPRS> customerPRSList);

    @Query("SELECT sp FROM SavingPRS sp where sp.customerId.prsId.sentraId.id =:sentraId")
    List<SavingPRS> getSentraHasPrsBySavingPrs(@Param("sentraId") Long sentraId);

    @Query("SELECT sp FROM SavingPRS sp where sp.customerId.prsId.sentraId.id =:sentraId" +
            " AND sp.customerId.prsId.prsDate = :prsDate AND sp.prsId.status = 'APPROVED'")
    List<SavingPRS> findSavingRTBySentraId(@Param("sentraId") Long sentraId, @Param("prsDate") @Temporal(value = TemporalType.DATE) Date prsDate);

    List<SavingPRS> findSavingPRSByPrsId(PRS prsId);

}
