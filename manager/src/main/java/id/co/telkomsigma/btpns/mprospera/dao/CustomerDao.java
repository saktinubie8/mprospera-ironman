package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CustomerDao extends JpaRepository<Customer, Long> {

    Customer findByCustomerCifNumber(String cifId);

    @Query("SELECT c FROM Customer c "
            + "INNER JOIN c.group g "
            + "INNER JOIN c.group.sentra st "
            + "WHERE st.sentraId = (:sentraId)")
    List<Customer> findBySentraId(@Param("sentraId") Long sentraId);

}