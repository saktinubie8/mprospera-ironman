package id.co.telkomsigma.btpns.mprospera.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.MtsTrxManager;
import id.co.telkomsigma.btpns.mprospera.model.prs.MtsTrx;


@Service("mtsTrxService")
public class MtsTrxService extends GenericService {

    @Autowired
    MtsTrxManager mtsTrxManager;

    public List<MtsTrx> getMtsTrx(Date aDate, List<String> userMS) {
        return mtsTrxManager.getMtsTrx(aDate, userMS);
    }

}