package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.prs.ReportCollection;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReportCollectionDao extends JpaRepository<ReportCollection, Long> {

    List<ReportCollection> findByAppId(String appId);

}
