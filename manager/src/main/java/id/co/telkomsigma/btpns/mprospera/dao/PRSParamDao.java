package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface PRSParamDao extends JpaRepository<PRSParameter, Long> {

    List<PRSParameter> findByParamGroupOrderByParameterId(String paramGroup);

    PRSParameter findByParameterId(@Param("parameterId") Long parameterId);

    void delete(PRSParameter parameterId);

    PRSParameter findByParamDescriptionLike(String filename);

}
