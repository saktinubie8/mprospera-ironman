package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;

public interface LoanPRSDao extends JpaRepository<LoanPRS, Long> {

	List<LoanPRS> findByPrsIdAndCustomerId(PRS prs, CustomerPRS customer);
	
	List<LoanPRS> findByPrsIdAndCustomerIdIn(PRS prs, List<CustomerPRS> customerPRSList);

	List<LoanPRS> findByAppId(String appId);
	
	List<LoanPRS> findByAppIdAndDisbursementAmountIsNotNull(String appId);

	LoanPRS findTopByLoanIdOrderByCreatedDateDesc(Loan loan);

	LoanPRS findTopByLoanIdAndCreatedDateLessThanOrderByCreatedDateDesc(Loan loan, @Temporal(value = TemporalType.DATE) Date createdDate);

	@Query("SELECT lp FROM LoanPRS lp where lp.customerId.prsId.sentraId.id =:sentraId")
	List<LoanPRS> getSentraHasPrsByLoanPrs( @Param("sentraId") Long sentraId);

	LoanPRS findByCustomerId(CustomerPRS customerPRS);

	List<LoanPRS> findLoanPRSByPrsId(PRS prsId);
	
	LoanPRS findTopByAppIdOrderByLoanPRSIdDesc(String appId);
	
	@Query("SELECT lp FROM LoanPRS lp where lp.prsId.sentraId.sentraId =:sentraId and " +
	"lp.disbursementFlag = :disbursementFlag AND lp.prsId.prsDate = :startDate")
	List<LoanPRS> findBySentraAndPrsDateAndDisbursmentFlag(@Param("sentraId")Long sentraId, @Param("startDate")@Temporal(value = TemporalType.DATE) Date prsDate, @Param("disbursementFlag")char disbursementFlag);

}
