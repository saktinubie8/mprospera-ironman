package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.MtsTrxDao;
import id.co.telkomsigma.btpns.mprospera.manager.MtsTrxManager;
import id.co.telkomsigma.btpns.mprospera.model.prs.MtsTrx;

@Service("mtsTrxManager")
public class MtsTrxManagerImpl implements MtsTrxManager {

    @Autowired
    private MtsTrxDao mtsTrxDao;

    @Override
    public List<MtsTrx> getMtsTrx(Date aDate, List<String> userMS) {
        return mtsTrxDao.findByApprovedDateByApprovedByOrderByApprovedDate(aDate, userMS);
    }

}