package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LoanProductDao extends JpaRepository<LoanProduct, Long> {

    LoanProduct findByProductId(Long productId);

    @Query("SELECT p FROM LoanProduct p ORDER BY p.productName")
    Page<LoanProduct> findAll(Pageable pageable);

}
