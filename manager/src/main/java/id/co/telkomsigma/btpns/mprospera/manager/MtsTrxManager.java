package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.prs.MtsTrx;


public interface MtsTrxManager {

    List<MtsTrx> getMtsTrx(Date aDate, List<String> listUserMS);

}