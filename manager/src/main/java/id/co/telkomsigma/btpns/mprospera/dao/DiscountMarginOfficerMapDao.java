package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginOfficerMap;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by hp on 23/05/2017.
 */
public interface DiscountMarginOfficerMapDao extends JpaRepository<DiscountMarginOfficerMap, Long> {

}
