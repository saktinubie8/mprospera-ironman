package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApprovalAndHistory;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApprovalHistory;

public interface DiscountMarginApprovalHistoryDao extends JpaRepository<DiscountMarginApprovalHistory, Long> {

    List<DiscountMarginApprovalHistory> findByDiscountMarginId(Long discountMarginId);

    @Query("SELECT new id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApprovalAndHistory(dmah.discountMarginApprovalHistoryId, "
            + " dmah.level, dmah.tanggal, dmah.limit, dmah.name, dmah.discountMarginId, dmah.role, dmah.date, dmah.status, dmah.supervisorId, dmah.approvedAmount, "
            + " dma.earlyTerminationPlanId, dma.customerId, dma.total, dma.alasan, dma.userBwmp, dma.createdBy) "
            + " FROM DiscountMarginApprovalHistory dmah, DiscountMarginApproval dma  "
            + " WHERE dmah.discountMarginId=dma.discountMarginId AND dmah.status= :status")
    List<DiscountMarginApprovalAndHistory> findDMAPAndDMAHistoryByStatus(@Param("status") String status);

    DiscountMarginApprovalHistory findByDiscountMarginIdAndLevel(Long discountMarginId, String level);

}
