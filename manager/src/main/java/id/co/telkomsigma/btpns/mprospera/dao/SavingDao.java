package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.saving.Saving;

public interface SavingDao extends JpaRepository<Saving, String> {

    List<Saving> findByCustomerId(Customer customerId);

    Saving findByAccountNumber(String accountNumber);

}
