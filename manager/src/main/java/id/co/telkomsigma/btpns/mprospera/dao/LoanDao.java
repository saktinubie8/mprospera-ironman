package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;

import javax.persistence.TemporalType;

public interface LoanDao extends JpaRepository<Loan, Long> {

    @Query("SELECT s FROM Loan s INNER JOIN s.customer c "
            + "INNER JOIN s.customer.group g "
            + "INNER JOIN s.customer.group.sentra st "
            + ", Location l WHERE st.locationId = l.locationId AND s.createdDate>=:startDate AND s.createdDate<:endDate AND l.locationId = :officeId ORDER BY s.createdDate")
    Page<Loan> findByCreatedDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
                                 Pageable pageRequest, @Param("officeId") String officeId);

    @Query("SELECT s FROM Loan s "
            + " WHERE s.loanId = :loanId")
    Loan findByLoanId(@Param("loanId") Long loanId);

    @Query("SELECT p FROM Loan p WHERE p.appId = :appId ")
    Loan findOneByAppId(@Param("appId") String appId);

    List<Loan> findByCustomer(Customer cust);

    @Query("SELECT s FROM Loan s INNER JOIN s.customer c "
            + "INNER JOIN s.customer.group g "
            + "INNER JOIN s.customer.group.sentra st "
            + "WHERE st.sentraId = :sentraId AND s.disbursementDate >= :startDate AND s.disbursementDate < :endDate AND s.status = :status")
    List<Loan> findBySentraAndDisburse(@Param("sentraId") Long sentraId, @Param("startDate") @Temporal(value = TemporalType.DATE) Date prsDate, @Param("endDate") @Temporal(value = TemporalType.DATE) Date prsDate2, @Param("status") String status);

}
