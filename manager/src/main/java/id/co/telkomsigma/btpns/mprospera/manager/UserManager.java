package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.model.user.UserAndSentra;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
public interface UserManager {

    /**
     * Gets single user by username
     *
     * @param username
     * @return user or null
     */
    User getUserByUsername(String username);

    List<User> getUserByLocId(String locId);

    List<UserAndSentra> getUserAndSentraByLocationId(String locId);

    /**
     * Gets single SessionKey by username
     *
     * @param username
     * @return user or null
     */
    String getSessionKeyByUsername(String username);

    void clearCache();

}