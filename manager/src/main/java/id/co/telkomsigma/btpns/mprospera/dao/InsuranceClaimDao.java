package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.claim.InsuranceClaim;

import org.springframework.data.jpa.repository.JpaRepository;

public interface InsuranceClaimDao extends JpaRepository<InsuranceClaim, Long> {

    List<InsuranceClaim> findByStatusInAndLocationId(List<String> string, String locationId);

}