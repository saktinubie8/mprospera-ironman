package id.co.telkomsigma.btpns.mprospera.model.sentra;

import java.sql.Time;
import java.util.Date;

public class SentraPOJO {

    private Long sentraId;
    private String areaId;
    private String sentraName;
    private String sentraLeader;
    private String sentraOwnerName;
    private Date startedDate;
    private String sentraAddress;
    private String rtRw;
    private char prsDay;
    private String longitude;
    private String latitude;
    private Boolean isDeleted = false;
    private String status;
    private Date createdDate;
    private String createdBy;
    private String prosperaId;
    private String locationId;
    private String meetingPlace;
    private String approvedBy;
    private String assignedTo;
    private String rrn;
    private Time meetingTime;
    private String province;
    private String city;
    private String kecamatan;
    private String kelurahan;
    private String collectEndTime;
    private String sentraCode;
    private Date lastPrs;
    private String postCode;

    public SentraPOJO(Long sentraId, String areaId, String sentraName, String sentraLeader, String sentraOwnerName,
                      Date startedDate, String sentraAddress, String rtRw, char prsDay, String longitude, String latitude,
                      Boolean isDeleted, String status, Date createdDate, String createdBy, String prosperaId, String locationId,
                      String meetingPlace, String approvedBy, String assignedTo, String rrn, Time meetingTime, String province,
                      String city, String kecamatan, String kelurahan, String collectEndTime, String sentraCode, Date lastPrs,
                      String postCode) {

        this.sentraId = sentraId;
        this.areaId = areaId;
        this.sentraName = sentraName;
        this.sentraLeader = sentraLeader;
        this.sentraOwnerName = sentraOwnerName;
        this.startedDate = startedDate;
        this.sentraAddress = sentraAddress;
        this.rtRw = rtRw;
        this.prsDay = prsDay;
        this.longitude = longitude;
        this.latitude = latitude;
        this.isDeleted = isDeleted;
        this.status = status;
        this.createdDate = createdDate;
        this.createdBy = createdBy;
        this.prosperaId = prosperaId;
        this.locationId = locationId;
        this.meetingPlace = meetingPlace;
        this.approvedBy = approvedBy;
        this.assignedTo = assignedTo;
        this.rrn = rrn;
        this.meetingTime = meetingTime;
        this.province = province;
        this.city = city;
        this.kecamatan = kecamatan;
        this.kelurahan = kelurahan;
        this.collectEndTime = collectEndTime;
        this.sentraCode = sentraCode;
        this.lastPrs = lastPrs;
        this.postCode = postCode;
    }

    public Long getSentraId() {
        return sentraId;
    }

    public String getAreaId() {
        return areaId;
    }

    public String getSentraName() {
        return sentraName;
    }

    public String getSentraLeader() {
        return sentraLeader;
    }

    public String getSentraOwnerName() {
        return sentraOwnerName;
    }

    public Date getStartedDate() {
        return startedDate;
    }

    public String getSentraAddress() {
        return sentraAddress;
    }

    public String getRtRw() {
        return rtRw;
    }

    public char getPrsDay() {
        return prsDay;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public String getStatus() {
        return status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getProsperaId() {
        return prosperaId;
    }

    public String getLocationId() {
        return locationId;
    }

    public String getMeetingPlace() {
        return meetingPlace;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public String getRrn() {
        return rrn;
    }

    public Time getMeetingTime() {
        return meetingTime;
    }

    public String getProvince() {
        return province;
    }

    public String getCity() {
        return city;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public String getCollectEndTime() {
        return collectEndTime;
    }

    public String getSentraCode() {
        return sentraCode;
    }

    public Date getLastPrs() {
        return lastPrs;
    }

    public String getPostCode() {
        return postCode;
    }

}
