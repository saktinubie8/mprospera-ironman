package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class PsMsRptTrxResponseList {

    private String rptId;
    private String trxTime;
    private String createdBy;
    private String updatedBy;
    private BigDecimal aMount;
    //1 = Pencairan, 2 = Pembayaran Angsuran, 3 = Pelunasan Dipercepat, 4 = Tarik Tabungan, 5 = Setor Tabungan
    private String typeTrx;

    public String getTypeTrx() {
        return typeTrx;
    }

    public void setTypeTrx(String typeTrx) {
        this.typeTrx = typeTrx;
    }

    public BigDecimal getAMount() {
        return aMount;
    }

    public void setAMount(BigDecimal actualMoney) {
        this.aMount = actualMoney;
    }

    public String getRptId() {
        return rptId;
    }

    public void setRptId(String rptId) {
        this.rptId = rptId;
    }

    public String getTrxTime() {
        return trxTime;
    }

    public void setTrxTime(String trxTime) {
        this.trxTime = trxTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String createdBy) {
        this.updatedBy = createdBy;
    }

    //	extras not the table-column
    //	set of variables needed for manual calculation
    //	used-as/get-from joint-tables criteria
    //
    private String userMSOfficeCode;
    private BigDecimal prsBringMoney;
    private Long loanProductId;
    private String prdProductCode;
    //
    private String typePsMsTrx;

    //
    //
    public String getMSOfficeCode() {
        return userMSOfficeCode;
    }

    public void setMSOfficeCode(String typeTrx) {
        this.userMSOfficeCode = typeTrx;
    }

    public BigDecimal getBringMoney() {
        return prsBringMoney;
    }

    public void setBringMoney(BigDecimal actualMoney) {
        this.prsBringMoney = actualMoney;
    }

    //
    public Long getProductId() {
        return loanProductId;
    }

    public void setProductId(Long prdId) {
        this.loanProductId = prdId;
    }

    public void setProductCode(String str) {
        this.prdProductCode = str;
    }

    public String getProductCode() {
        return prdProductCode;
    }

    //	set of variable relate to table MutationTrx
    private Date approvedDate;
    private Date businessDate;
    //private BigDecimal aMount;	//already defiend
    private BigDecimal amountTotalCollect;
    private BigDecimal amountTotalPRS;
    private BigDecimal amountBringPRS;
    //
    //private String typePsMsTrx;	//already defined

    public String getPsMsTrxType() {
        return typePsMsTrx;
    }

    public void setPsMsTrxType(String typeTrx) {
        this.typePsMsTrx = typeTrx;
    }

    public Date getApprovedDate() {
        return this.approvedDate;
    }

    public void setApprovedDate(Date aDate) {
        this.approvedDate = aDate;
    }

    public Date getBusinessDate() {
        return this.businessDate;
    }

    public void setBusinessDate(Date aDate) {
        this.businessDate = aDate;
    }

    public BigDecimal getAmountTotalCollect() {
        return this.amountTotalCollect;
    }

    public void setAmountTotalCollect(BigDecimal actualMoney) {
        this.amountTotalCollect = actualMoney;
    }

    public BigDecimal getAmountTotalPRS() {
        return this.amountTotalPRS;
    }

    public void setAmountTotalPRS(BigDecimal actualMoney) {
        this.amountTotalPRS = actualMoney;
    }

    public BigDecimal getAmountBringPRS() {
        return this.amountBringPRS;
    }

    public void setAmountBringPRS(BigDecimal actualMoney) {
        this.amountBringPRS = actualMoney;
    }
}
