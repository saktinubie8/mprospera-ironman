package id.co.telkomsigma.btpns.mprospera.model.prs;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Dzulfiqar on 22/05/2017.
 */
@Entity
@Table(name = "T_DISCOUNT_MARGIN_OFFICER_MAP")
public class DiscountMarginOfficerMap extends GenericModel {

    private Long mapping_id;
    private Long discountMarginId;
    private Long officer;
    private String status;
    private String updatedBy;
    private Date updatedDate;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getMapping_id() {
        return mapping_id;
    }

    public void setMapping_id(Long mapping_id) {
        this.mapping_id = mapping_id;
    }

    @Column(name = "officer_id")
    public Long getOfficer() {
        return officer;
    }

    public void setOfficer(Long officer) {
        this.officer = officer;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "updated_date")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "discount_margin_id")
    public Long getDiscountMarginId() {
        return discountMarginId;
    }

    public void setDiscountMarginId(Long discountMarginId) {
        this.discountMarginId = discountMarginId;
    }
}
