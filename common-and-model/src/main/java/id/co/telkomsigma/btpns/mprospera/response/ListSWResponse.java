package id.co.telkomsigma.btpns.mprospera.response;

public class ListSWResponse {

    private String swId;
    private String customerIdNumber;
    private String customerName;
    private String areaId;
    private String recommendation;
    private String createdBy;
    private String createdDate;
    private String customerRegistrationType;
    private String customerCifNumber;
    private String workType;
    private String surveyDate;
    private String houseType;
    private String isNeighborhoodRecommendation;
    private String workIncome;
    private String totalPurchase;
    private String workExpense;
    private String iir;
    private String loanAmountRequested;
    private String loanAmountRecommended;
    private String appId;

    public String getCustomerRegistrationType() {
        return customerRegistrationType;
    }

    public void setCustomerRegistrationType(String customerRegistrationType) {
        this.customerRegistrationType = customerRegistrationType;
    }

    public String getCustomerCifNumber() {
        return customerCifNumber;
    }

    public void setCustomerCifNumber(String customerCifNumber) {
        this.customerCifNumber = customerCifNumber;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        this.surveyDate = surveyDate;
    }

    public String getHouseType() {
        return houseType;
    }

    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }

    public String getIsNeighborhoodRecommendation() {
        return isNeighborhoodRecommendation;
    }

    public void setIsNeighborhoodRecommendation(String isNeighborhoodRecommendation) {
        this.isNeighborhoodRecommendation = isNeighborhoodRecommendation;
    }

    public String getWorkIncome() {
        return workIncome;
    }

    public void setWorkIncome(String workIncome) {
        this.workIncome = workIncome;
    }

    public String getTotalPurchase() {
        return totalPurchase;
    }

    public void setTotalPurchase(String totalPurchase) {
        this.totalPurchase = totalPurchase;
    }

    public String getWorkExpense() {
        return workExpense;
    }

    public void setWorkExpense(String workExpense) {
        this.workExpense = workExpense;
    }

    public String getIir() {
        return iir;
    }

    public void setIir(String iir) {
        this.iir = iir;
    }

    public String getLoanAmountRequested() {
        return loanAmountRequested;
    }

    public void setLoanAmountRequested(String loanAmountRequested) {
        this.loanAmountRequested = loanAmountRequested;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getLoanAmountRecommended() {
        return loanAmountRecommended;
    }

    public void setLoanAmountRecommended(String loanAmountRecommended) {
        this.loanAmountRecommended = loanAmountRecommended;
    }

    @Override
    public String toString() {
        return "ListSWResponse [swId=" + swId + ", customerIdNumber=" + customerIdNumber + ", customerName="
                + customerName + ", areaId=" + areaId + ", recommendation=" + recommendation + ", createdBy="
                + createdBy + ", createdDate=" + createdDate + ", customerRegistrationType=" + customerRegistrationType
                + ", customerCifNumber=" + customerCifNumber + ", workType=" + workType + ", surveyDate=" + surveyDate
                + ", houseType=" + houseType + ", isNeighborhoodRecommendation=" + isNeighborhoodRecommendation
                + ", workIncome=" + workIncome + ", totalPurchase=" + totalPurchase + ", workExpense=" + workExpense
                + ", iir=" + iir + ", loanAmountRequested=" + loanAmountRequested + ", loanAmountRecommended="
                + loanAmountRecommended + ", appId=" + appId + "]";
    }

}
