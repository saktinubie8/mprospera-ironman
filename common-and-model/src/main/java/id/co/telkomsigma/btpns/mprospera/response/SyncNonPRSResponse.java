package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class SyncNonPRSResponse extends BaseResponse {

    private List<NonPRSResponseList> sentraList;
    private String totalPage;

    public List<NonPRSResponseList> getSentraList() {
        return sentraList;
    }

    public void setSentraList(List<NonPRSResponseList> sentraList) {
        this.sentraList = sentraList;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }
}
