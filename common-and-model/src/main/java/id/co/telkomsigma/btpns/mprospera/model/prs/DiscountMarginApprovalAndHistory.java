package id.co.telkomsigma.btpns.mprospera.model.prs;

import java.math.BigDecimal;
import java.util.Date;

public class DiscountMarginApprovalAndHistory {

    private Long discountMarginApprovalHistoryId;
    private String level;
    private Date tanggal;
    private String limit;
    private String name;
    private Long discountMarginId;
    private String role;
    private String date;
    private String status;
    private String supervisorId;
    private BigDecimal approvedAmount;

    private Long earlyTerminationPlanId;
    private Long customerId;
    private BigDecimal total;
    private String alasan;
    private Long userBwmp;
    private String createdBy;

    public DiscountMarginApprovalAndHistory(Long discountMarginApprovalHistoryId, String level, Date tanggal,
                                            String limit, String name, Long discountMarginId, String role, String date, String status,
                                            String supervisorId, BigDecimal approvedAmount, Long earlyTerminationPlanId, Long customerId,
                                            BigDecimal total, String alasan, Long userBwmp, String createdBy) {
        super();
        this.discountMarginApprovalHistoryId = discountMarginApprovalHistoryId;
        this.level = level;
        this.tanggal = tanggal;
        this.limit = limit;
        this.name = name;
        this.discountMarginId = discountMarginId;
        this.role = role;
        this.date = date;
        this.status = status;
        this.supervisorId = supervisorId;
        this.approvedAmount = approvedAmount;
        this.earlyTerminationPlanId = earlyTerminationPlanId;
        this.customerId = customerId;
        this.total = total;
        this.alasan = alasan;
        this.userBwmp = userBwmp;
        this.createdBy = createdBy;
    }

    public Long getDiscountMarginApprovalHistoryId() {
        return discountMarginApprovalHistoryId;
    }

    public String getLevel() {
        return level;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public String getLimit() {
        return limit;
    }

    public String getName() {
        return name;
    }

    public Long getDiscountMarginId() {
        return discountMarginId;
    }

    public String getRole() {
        return role;
    }

    public String getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }

    public String getSupervisorId() {
        return supervisorId;
    }

    public BigDecimal getApprovedAmount() {
        return approvedAmount;
    }

    public Long getEarlyTerminationPlanId() {
        return earlyTerminationPlanId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public String getAlasan() {
        return alasan;
    }

    public Long getUserBwmp() {
        return userBwmp;
    }

    public String getCreatedBy() {
        return createdBy;
    }

}
