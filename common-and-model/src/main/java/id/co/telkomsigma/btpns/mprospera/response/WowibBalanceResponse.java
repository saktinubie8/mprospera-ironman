package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class WowibBalanceResponse extends BaseResponse {

    public List<WowIBCustomerListResponse> accountList;

    public List<WowIBCustomerListResponse> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<WowIBCustomerListResponse> accountList) {
        this.accountList = accountList;
    }

    @Override
    public String toString() {
        return "WowibBalanceResponse{" +
                "accountList=" + accountList +
                '}';
    }
}
