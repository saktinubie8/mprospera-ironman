package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class CustomerForNonPrsRequest {

    private String customerId;
    private List<LoanNonPrsRequest> loanList;
    private List<SavingNonPrsRequest> savingList;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<LoanNonPrsRequest> getLoanList() {
        return loanList;
    }

    public void setLoanList(List<LoanNonPrsRequest> loanList) {
        this.loanList = loanList;
    }

    public List<SavingNonPrsRequest> getSavingList() {
        return savingList;
    }

    public void setSavingList(List<SavingNonPrsRequest> savingList) {
        this.savingList = savingList;
    }

    @Override
    public String toString() {
        return "CustomerForNonPrsRequest [customerId=" + customerId + ", loanList=" + loanList + ", savingList="
                + savingList + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
                + super.toString() + "]";
    }

}
