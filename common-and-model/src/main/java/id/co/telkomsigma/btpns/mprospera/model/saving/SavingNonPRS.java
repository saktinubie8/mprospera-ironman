package id.co.telkomsigma.btpns.mprospera.model.saving;

import java.math.BigDecimal;
import java.util.Date;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_SAVING_NON_PRS")
public class SavingNonPRS extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long savingNonPrsId;
    private Long accountIdProspera;
    private String accountNumber;
    private BigDecimal clearBalance;
    private BigDecimal depositAmount;
    private BigDecimal withdrawalAmount;
    private Date createdDate;
    private String createdBy;
    private Customer customerId;
    private Date updatedDate;
    private String updatedBy;

    @Id
    @Column(name = "ID", nullable = false, unique = true)
    @GeneratedValue
    public Long getSavingNonPrsId() {
        return savingNonPrsId;
    }

    public void setSavingNonPrsId(Long savingNonPrsId) {
        this.savingNonPrsId = savingNonPrsId;
    }

    @Column(name = "ACCOUNT_ID_PROSPERA")
    public Long getAccountIdProspera() {
        return accountIdProspera;
    }

    public void setAccountIdProspera(Long accountIdProspera) {
        this.accountIdProspera = accountIdProspera;
    }

    @Column(name = "ACC_NUMBER", nullable = false)
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Column(name = "CLEAR_BALANCE", nullable = false)
    public BigDecimal getClearBalance() {
        return clearBalance;
    }

    public void setClearBalance(BigDecimal clearBalance) {
        this.clearBalance = clearBalance;
    }

    @Column(name = "DEPOSIT_AMT", nullable = false)
    public BigDecimal getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }

    @Column(name = "CREATED_DATE")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
    @JoinColumn(name = "customer_Id", referencedColumnName = "id", nullable = true)
    public Customer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customer customerId) {
        this.customerId = customerId;
    }

    @Column(name = "UPDATED_DATE")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "WITHDRAWAL_AMT")
    public BigDecimal getWithdrawalAmount() {
        return withdrawalAmount;
    }

    public void setWithdrawalAmount(BigDecimal withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }

}
