package id.co.telkomsigma.btpns.mprospera.model.saving;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.math.*;
import java.util.*;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;

@Entity
@Table(name = "T_SAVING_PRS")
public class SavingPRS extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = -1269427908942000777L;

    private Long accountId;
    private PRS prsId;
    private String accountNumber;
    private BigDecimal withdrawalAmount;
    private BigDecimal withdrawalAmountPlan;
    private BigDecimal nextWithdrawalAmountPlan;
    private Boolean isCloseSavingAccount;
    private BigDecimal holdBalance;
    private BigDecimal clearBalance;
    private BigDecimal depositAmount;
    private Date createdDate;
    private String createdBy;
    private CustomerPRS customerId;
    private Date updatedDate;
    private String updatedBy;
    private Long accountIdProspera;

    @Id
    @Column(name = "ID", nullable = false, unique = true)
    @GeneratedValue
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = PRS.class)
    @JoinColumn(name = "prs_Id", referencedColumnName = "id", nullable = false)
    public PRS getPrsId() {
        return prsId;
    }

    public void setPrsId(PRS prsId) {
        this.prsId = prsId;
    }

    @Column(name = "ACC_NUMBER", nullable = false)
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Column(name = "WITHDRAWAL_AMT")
    public BigDecimal getWithdrawalAmount() {
        return withdrawalAmount;
    }

    public void setWithdrawalAmount(BigDecimal withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }

    @Column(name = "WITHDRAWAL_AMT_PLAN", nullable = false)
    public BigDecimal getWithdrawalAmountPlan() {
        return withdrawalAmountPlan;
    }

    public void setWithdrawalAmountPlan(BigDecimal withdrawalAmountPlan) {
        this.withdrawalAmountPlan = withdrawalAmountPlan;
    }

    @Column(name = "WITHDRAWAL_AMT_PLAN_NEXT", nullable = false)
    public BigDecimal getNextWithdrawalAmountPlan() {
        return nextWithdrawalAmountPlan;
    }

    public void setNextWithdrawalAmountPlan(BigDecimal nextWithdrawalAmountPlan) {
        this.nextWithdrawalAmountPlan = nextWithdrawalAmountPlan;
    }

    @Column(name = "CLOSE_SAVING_ACC", nullable = false)
    public Boolean getIsCloseSavingAccount() {
        return isCloseSavingAccount;
    }

    public void setIsCloseSavingAccount(Boolean isCloseSavingAccount) {
        this.isCloseSavingAccount = isCloseSavingAccount;
    }

    @Column(name = "HOLD_BALANCE", nullable = false)
    public BigDecimal getHoldBalance() {
        return holdBalance;
    }

    public void setHoldBalance(BigDecimal holdBalance) {
        this.holdBalance = holdBalance;
    }

    @Column(name = "CLEAR_BALANCE", nullable = false)
    public BigDecimal getClearBalance() {
        return clearBalance;
    }

    public void setClearBalance(BigDecimal clearBalance) {
        this.clearBalance = clearBalance;
    }

    @Column(name = "DEPOSIT_AMT", nullable = false)
    public BigDecimal getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }

    @Column(name = "CREATED_DATE")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = CustomerPRS.class)
    @JoinColumn(name = "customer_Id", referencedColumnName = "id", nullable = false)
    public CustomerPRS getCustomerId() {
        return customerId;
    }

    public void setCustomerId(CustomerPRS customerId) {
        this.customerId = customerId;
    }

    @Column(name = "UPDATED_DATE")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "ACCOUNT_ID_PROSPERA")
    public Long getAccountIdProspera() {
        return accountIdProspera;
    }

    public void setAccountIdProspera(Long accountIdProspera) {
        this.accountIdProspera = accountIdProspera;
    }

}
