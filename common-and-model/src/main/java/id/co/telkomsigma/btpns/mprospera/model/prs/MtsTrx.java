package id.co.telkomsigma.btpns.mprospera.model.prs;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "T_MUTATION_TRX_TABLE")
public class MtsTrx extends GenericModel {

    private Long trx_id;
    private Date approvedDate;
    private String approvedBy;
    private String createdBy;
    private String typeTrx;
    private BigDecimal amountTotalCollect;
    private BigDecimal amountTotalPRS;
    private BigDecimal amountBringPRS;
    private Date businessDate;

    //	extras not the table-column
    private String userMSOfficeCode;
    private String typePsMsTrx;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getTrx_id() {
        return trx_id;
    }

    public void setTrx_id(Long trx_id) {
        this.trx_id = trx_id;
    }

    @Column(name = "approved_dt")
    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    @Column(name = "approved_by")
    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "business_dt")
    public Date getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(Date businessDate) {
        this.businessDate = businessDate;
    }

    //1 = Pencairan, 2 = Pembayaran Angsuran, 3 = Pelunasan Dipercepat, 4 = Tarik Tabungan, 5 = Setor Tabungan
    @Column(name = "trx_type")
    public String getTypeTrx() {
        return typeTrx;
    }

    public void setTypeTrx(String typeTrx) {
        this.typeTrx = typeTrx;
    }

    /*
    [bring_money] [numeric](19, 2) NULL,
    [total_collect] [numeric](19, 2) NULL,
    [total_prs_money] [numeric](19, 2) NULL,
     */
    @Column(name = "bring_money")
    public BigDecimal getAmountBringPRS() {
        return this.amountBringPRS;
    }

    public void setAmountBringPRS(BigDecimal amount) {
        this.amountBringPRS = amount;
    }

    @Column(name = "total_collect")
    public BigDecimal getAmountTotalCollect() {
        return this.amountTotalCollect;
    }

    public void setAmountTotalCollect(BigDecimal amount) {
        this.amountTotalCollect = amount;
    }

    @Column(name = "total_prs_money")
    public BigDecimal getAmountTotalPRS() {
        return this.amountTotalPRS;
    }

    public void setAmountTotalPRS(BigDecimal amount) {
        this.amountTotalPRS = amount;
    }

}
