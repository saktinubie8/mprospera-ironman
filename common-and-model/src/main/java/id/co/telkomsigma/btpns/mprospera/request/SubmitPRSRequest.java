package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

/**
 *
 *
 */
public class SubmitPRSRequest extends BaseRequest {

    private String username;
    private String sessionKey;
    private String imei;
    private String longitude;
    private String latitude;
    private String prsId;
    private String sentraId;
    private String prsPhoto;
    private String prsDate;
    private String prsEndTime;
    private String bringMoney;
    private String actualMoney;
    private String paymentMoney;
    private String withdrawalAdhoc;
    private String status;
    private String psCompanion;
    private List<CustomerPRSRequest> customerList;
    private List<DiscountMarginReq> discountMarginPlan;
    private String report;
    private String prsStartTime;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPrsId() {
        return prsId;
    }

    public void setPrsId(String prsId) {
        this.prsId = prsId;
    }

    public String getSentraId() {
        return sentraId;
    }

    public void setSentraId(String sentraId) {
        this.sentraId = sentraId;
    }

    public String getPrsPhoto() {
        return prsPhoto;
    }

    public void setPrsPhoto(String prsPhoto) {
        this.prsPhoto = prsPhoto;
    }

    public String getPrsDate() {
        return prsDate;
    }

    public void setPrsDate(String prsDate) {
        this.prsDate = prsDate;
    }

    public String getPrsEndTime() {
        return prsEndTime;
    }

    public void setPrsEndTime(String prsEndTime) {
        this.prsEndTime = prsEndTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPsCompanion() {
        return psCompanion;
    }

    public void setPsCompanion(String psCompanion) {
        this.psCompanion = psCompanion;
    }

    public List<CustomerPRSRequest> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<CustomerPRSRequest> customerList) {
        this.customerList = customerList;
    }

    public String getBringMoney() {
        return reformatMoney(bringMoney);
    }

    public void setBringMoney(String bringMoney) {
        this.bringMoney = bringMoney;
    }

    public String getActualMoney() {
        return reformatMoney(actualMoney);
    }

    public void setActualMoney(String actualMoney) {
        this.actualMoney = actualMoney;
    }

    public String getPaymentMoney() {
        return reformatMoney(paymentMoney);
    }

    public void setPaymentMoney(String paymentMoney) {
        this.paymentMoney = paymentMoney;
    }

    public String getWithdrawalAdhoc() {
        return reformatMoney(withdrawalAdhoc);
    }

    public void setWithdrawalAdhoc(String withdrawalAdhoc) {
        this.withdrawalAdhoc = withdrawalAdhoc;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public List<DiscountMarginReq> getDiscountMarginPlan() {
        return discountMarginPlan;
    }

    public void setDiscountMarginPlan(List<DiscountMarginReq> discountMarginPlan) {
        this.discountMarginPlan = discountMarginPlan;
    }

    public String getPrsStartTime() {
        return prsStartTime;
    }

    public void setPrsStartTime(String prsStartTime) {
        this.prsStartTime = prsStartTime;
    }

    @Override
    public String toString() {
        return "SubmitPRSRequest{" +
                "username='" + username + '\'' +
                ", retrievalReferenceNumber='" + getRetrievalReferenceNumber() + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", imei='" + imei + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", prsId='" + prsId + '\'' +
                ", sentraId='" + sentraId + '\'' +
                ", prsPhoto='" + prsPhoto + '\'' +
                ", prsDate='" + prsDate + '\'' +
                ", prsEndTime='" + prsEndTime + '\'' +
                ", bringMoney='" + bringMoney + '\'' +
                ", actualMoney='" + actualMoney + '\'' +
                ", paymentMoney='" + paymentMoney + '\'' +
                ", withdrawalAdhoc='" + withdrawalAdhoc + '\'' +
                ", status='" + status + '\'' +
                ", psCompanion='" + psCompanion + '\'' +
                ", customerList=" + customerList +
                ", discountMarginPlan=" + discountMarginPlan +
                ", report='" + report + '\'' +
                ", prsStartTime='" + prsStartTime + '\'' +
                '}';
    }
}
