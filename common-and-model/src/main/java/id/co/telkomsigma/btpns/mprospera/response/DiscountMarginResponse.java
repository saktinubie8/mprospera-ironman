package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

/**
 * Created by Dzulfiqar on 22/05/2017.
 */
public class DiscountMarginResponse extends BaseResponse {

    private String discountMarginId;
    private String status;
    private String createdBy;
    private String customerId;
    private String localId;
    private String batch;
    private List<DiscountMarginApprovalHistoryResponse> approvalHistories;

    //forList
    private List<DiscountMarginResponse> discountMarginList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public List<DiscountMarginApprovalHistoryResponse> getApprovalHistories() {
        return approvalHistories;
    }

    public void setApprovalHistories(List<DiscountMarginApprovalHistoryResponse> approvalHistories) {
        this.approvalHistories = approvalHistories;
    }

    public String getDiscountMarginId() {
        return discountMarginId;
    }

    public void setDiscountMarginId(String discountMarginId) {
        this.discountMarginId = discountMarginId;
    }

    public List<DiscountMarginResponse> getDiscountMarginList() {
        return discountMarginList;
    }

    public void setDiscountMarginList(List<DiscountMarginResponse> discountMarginList) {
        this.discountMarginList = discountMarginList;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
