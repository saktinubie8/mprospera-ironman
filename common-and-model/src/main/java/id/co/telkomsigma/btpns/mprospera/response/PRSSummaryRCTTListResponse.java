package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

public class PRSSummaryRCTTListResponse extends BaseResponse {

    private List<PRSSummaryRCTTResponse> prsList;

    public List<PRSSummaryRCTTResponse> getPrsList() {
        if (prsList == null)
            prsList = new ArrayList<>();
        return prsList;
    }

    public void setPrsList(List<PRSSummaryRCTTResponse> prsList) {
        this.prsList = prsList;
    }

}
