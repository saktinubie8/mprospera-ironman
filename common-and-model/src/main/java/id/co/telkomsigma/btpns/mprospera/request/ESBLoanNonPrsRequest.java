package id.co.telkomsigma.btpns.mprospera.request;


public class ESBLoanNonPrsRequest {

    private String appid;
    private String installmentPaymentAmount;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getInstallmentPaymentAmount() {
        return installmentPaymentAmount;
    }

    public void setInstallmentPaymentAmount(String installmentPaymentAmount) {
        this.installmentPaymentAmount = installmentPaymentAmount;
    }

    @Override
    public String toString() {
        return "ESBLoanNonPrsRequest [appid=" + appid + ", installmentPaymentAmount=" + installmentPaymentAmount
                + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
                + "]";
    }

}
