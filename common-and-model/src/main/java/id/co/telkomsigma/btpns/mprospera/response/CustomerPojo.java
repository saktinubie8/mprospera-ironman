package id.co.telkomsigma.btpns.mprospera.response;

public class CustomerPojo {

    private String customerId;
    private String customerCifNumber;
    private String customerName;
    private String customerIdNumber;
    private String createdDate;
    private String customerAreaId;
    private String swId;
    private String pdkId;
    private String createdBy;
    private String customerStatus;
    private String customerLevelId;
    private String assignedUsername;
    private String prosperaId;
    private String groupId;

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getCustomerLevelId() {
        return customerLevelId;
    }

    public void setCustomerLevelId(String customerLevelId) {
        this.customerLevelId = customerLevelId;
    }

    public String getAssignedUsername() {
        return assignedUsername;
    }

    public void setAssignedUsername(String assignedUsername) {
        this.assignedUsername = assignedUsername;
    }

    public String getProsperaId() {
        return prosperaId;
    }

    public void setProsperaId(String prosperaId) {
        this.prosperaId = prosperaId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerCifNumber() {
        return customerCifNumber;
    }

    public void setCustomerCifNumber(String customerCifNumber) {
        this.customerCifNumber = customerCifNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCustomerAreaId() {
        return customerAreaId;
    }

    public void setCustomerAreaId(String customerAreaId) {
        this.customerAreaId = customerAreaId;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getPdkId() {
        return pdkId;
    }

    public void setPdkId(String pdkId) {
        this.pdkId = pdkId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

}
