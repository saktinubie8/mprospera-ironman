package id.co.telkomsigma.btpns.mprospera.response;

public class ListStatusPRSResponse {

    private String sentraId;
    private String statusPrs;
    private String updatedDate;
    private String prsId;

    public String getSentraId() {
        return sentraId;
    }

    public void setSentraId(String sentraId) {
        this.sentraId = sentraId;
    }

    public String getStatusPrs() {
        return statusPrs;
    }

    public void setStatusPrs(String statusPrs) {
        this.statusPrs = statusPrs;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getPrsId() {
        return prsId;
    }

    public void setPrsId(String prsId) {
        this.prsId = prsId;
    }

    @Override
    public String toString() {
        return "ListStatusPRSResponse{" +
                "sentraId='" + sentraId + '\'' +
                ", statusPrs='" + statusPrs + '\'' +
                ", updatedDate='" + updatedDate + '\'' +
                ", prsId='" + prsId + '\'' +
                '}';
    }
}
