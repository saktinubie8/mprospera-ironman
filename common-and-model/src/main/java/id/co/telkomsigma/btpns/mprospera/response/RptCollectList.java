package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;


public class RptCollectList {

    private String rptId;
    private String strVisitTime;

    public String getRptId() {
        return rptId;
    }

    public void setRptId(String rptId) {
        this.rptId = rptId;
    }

    public String getVisitTime() {
        return strVisitTime;
    }

    public void setVisitTime(String trxTime) {
        this.strVisitTime = trxTime;
    }

    private String strCommitDate;    //tglkomit
    private BigDecimal aMount;        //paidAmount
    private BigDecimal amountToCollect;

    public String getCommitDate() {
        return this.strCommitDate;
    }

    public void setCommitDate(String strDate) {
        this.strCommitDate = strDate;
    }

    public BigDecimal getAmountToCollect() {
        return this.amountToCollect;
    }

    public void setAmountToCollect(BigDecimal actualMoney) {
        this.amountToCollect = actualMoney;
    }

    //
    public BigDecimal getAMount() {
        return aMount;
    }

    public void setAMount(BigDecimal actualMoney) {
        this.aMount = actualMoney;
    }

    private String strPeopleMeet;

    //
    public String getPeopleMeet() {
        return strPeopleMeet;
    }

    public void setPeopleMeet(String strPM) {
        this.strPeopleMeet = strPM;
    }

}

