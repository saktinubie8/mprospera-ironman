package id.co.telkomsigma.btpns.mprospera.response;

import id.co.telkomsigma.btpns.mprospera.request.CustomerListResponse;

import java.util.List;

/**
 * Created by Dzulfiqar on 2/06/2017.
 */
public class DiscountMarginListResponse extends BaseResponse {

    private List<CustomerListResponse> customerList;

    public List<CustomerListResponse> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<CustomerListResponse> customerList) {
        this.customerList = customerList;
    }
}
