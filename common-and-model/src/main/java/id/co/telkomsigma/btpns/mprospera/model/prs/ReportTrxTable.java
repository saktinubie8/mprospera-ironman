package id.co.telkomsigma.btpns.mprospera.model.prs;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "T_REPORT_TRX_TABLE")
public class ReportTrxTable extends GenericModel {

    private Long trx_id;
    private Date approvedDate;
    private String approvedBy;
    private String createdBy;
    private String typeTrx;
    private BigDecimal amount;
    private Date businessDate;
    private String appId;
    private String accountNumber;
    private Long prsId;

    //	extras not the table-column
    private String userMSOfficeCode;
    private BigDecimal prsBringMoney;
    private Long loanProductId;
    private String prdProductCode;
    //
    private String typePsMsTrx;
    //

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getTrx_id() {
        return trx_id;
    }

    public void setTrx_id(Long trx_id) {
        this.trx_id = trx_id;
    }

    @Column(name = "approved_dt")
    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    @Column(name = "approved_by")
    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    //1 = Pencairan, 2 = Pembayaran Angsuran, 3 = Pelunasan Dipercepat, 4 = Tarik Tabungan, 5 = Setor Tabungan
    @Column(name = "trx_type")
    public String getTypeTrx() {
        return typeTrx;
    }

    public void setTypeTrx(String typeTrx) {
        this.typeTrx = typeTrx;
    }

    @Column(name = "amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(name = "business_dt")
    public Date getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(Date businessDate) {
        this.businessDate = businessDate;
    }

    @Column(name = "app_id")
    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    @Column(name = "acc_number")
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Column(name = "prs_id")
    public Long getPrsId() {
        return prsId;
    }

    public void setPrsId(Long prsId) {
        this.prsId = prsId;
    }

}
