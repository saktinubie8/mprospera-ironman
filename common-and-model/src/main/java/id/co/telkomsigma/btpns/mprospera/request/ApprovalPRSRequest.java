package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class ApprovalPRSRequest extends BaseRequest {

    private String imei;
    private String username;
    private String userMs;
    private String password;
    private String sessionKey;
    private String longitude;
    private String latitude;
    private String prsId;
    private String totalPRSMoney;
    private String status;
    private String tokenChallenge;
    private String tokenResponse;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPrsId() {
        return prsId;
    }

    public void setPrsId(String prsId) {
        this.prsId = prsId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTokenChallenge() {
        return tokenChallenge;
    }

    public void setTokenChallenge(String tokenChallenge) {
        this.tokenChallenge = tokenChallenge;
    }

    public String getTokenResponse() {
        return tokenResponse;
    }

    public void setTokenResponse(String tokenResponse) {
        this.tokenResponse = tokenResponse;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserMs() {
        return userMs;
    }

    public void setUserMs(String userMs) {
        this.userMs = userMs;
    }

    public String getTotalPRSMoney() {
        return totalPRSMoney;
    }

    public void setTotalPRSMoney(String totalPRSMoney) {
        this.totalPRSMoney = totalPRSMoney;
    }

    @Override
    public String toString() {
        return "ApprovalPRSRequest{" +
                "imei='" + imei + '\'' +
                ", username='" + username + '\'' +
                ", userMs='" + userMs + '\'' +
                ", password='" + password + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", prsId='" + prsId + '\'' +
                ", totalPRSMoney='" + totalPRSMoney + '\'' +
                ", status='" + status + '\'' +
                ", tokenChallenge='" + tokenChallenge + '\'' +
                ", tokenResponse='" + tokenResponse + '\'' +
                '}';
    }

}
