package id.co.telkomsigma.btpns.mprospera.response;

/**
 * Created by Dzulfiqar on 22/05/2017.
 */
public class DiscountMarginApprovalHistoryResponse {

    private String date;
    private String level;
    private String name;
    private String role;
    private String limit;
    private String tanggal;
    private String status;
    private String supervisorId;
    private String discountMarginId;
    private String approvedAmount;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(String supervisorId) {
        this.supervisorId = supervisorId;
    }

    public String getDiscountMarginId() {
        return discountMarginId;
    }

    public void setDiscountMarginId(String discountMarginId) {
        this.discountMarginId = discountMarginId;
    }

    public String getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(String approvedAmount) {
        this.approvedAmount = approvedAmount;
    }
}
