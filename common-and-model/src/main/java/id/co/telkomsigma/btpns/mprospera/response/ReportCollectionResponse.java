package id.co.telkomsigma.btpns.mprospera.response;


import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class ReportCollectionResponse extends BaseResponse {

    private List<RptCollectList> rptList;

    @Override
    public String toString() {
        return "ReportCollectionResponse [rptList=" + rptList
                + "]";
    }

    public List<RptCollectList> getRptList() {
        return rptList;
    }

    public void setRptList(List<RptCollectList> theList) {
        this.rptList = theList;
    }

}
