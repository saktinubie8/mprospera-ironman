package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;
import java.util.List;

public class LoanHistResponse extends BaseResponse {

    private String appId;
    private BigDecimal outstandingAmount;
    private BigDecimal outstandingMargin;
    private BigDecimal installmentAmount;
    private BigDecimal installmentCount;
    private BigDecimal plafond;
    private BigDecimal tenor;
    private String swDate;
    private String maturityDate;
    private String status;
    private String lastPaymentDate;
    private String disbursementDate;
    private BigDecimal totalDanaTalangan;
    private List<HistoryList> historyList;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public BigDecimal getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(BigDecimal outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }

    public BigDecimal getOutstandingMargin() {
        return outstandingMargin;
    }

    public void setOutstandingMargin(BigDecimal outstandingMargin) {
        this.outstandingMargin = outstandingMargin;
    }

    public BigDecimal getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(BigDecimal installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public String getLastPaymentDate() {
        return lastPaymentDate;
    }

    public void setLastPaymentDate(String lastPaymentDate) {
        this.lastPaymentDate = lastPaymentDate;
    }

    public BigDecimal getTotalDanaTalangan() {
        return totalDanaTalangan;
    }

    public void setTotalDanaTalangan(BigDecimal totalDanaTalangan) {
        this.totalDanaTalangan = totalDanaTalangan;
    }

    public List<HistoryList> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<HistoryList> historyList) {
        this.historyList = historyList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getInstallmentCount() {
        return installmentCount;
    }

    public void setInstallmentCount(BigDecimal installmentCount) {
        this.installmentCount = installmentCount;
    }

    public String getSwDate() {
        return swDate;
    }

    public void setSwDate(String swDate) {
        this.swDate = swDate;
    }

    public String getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(String maturityDate) {
        this.maturityDate = maturityDate;
    }

    public String getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(String disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    public BigDecimal getPlafond() {
        return plafond;
    }

    public void setPlafond(BigDecimal plafond) {
        this.plafond = plafond;
    }

    public BigDecimal getTenor() {
        return tenor;
    }

    public void setTenor(BigDecimal tenor) {
        this.tenor = tenor;
    }

    @Override
    public String toString() {
        return "LoanHistResponse [appId=" + appId + ", outstandingAmount=" + outstandingAmount + ", outstandingMargin="
                + outstandingMargin + ", installmentAmount=" + installmentAmount + ", lastPaymentDate="
                + lastPaymentDate + ", totalDanaTalangan=" + totalDanaTalangan + ", historyList=" + historyList + "]";
    }

}
