package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SyncPRSResponse extends BaseResponse {

    private List<PRSResponseList> prsList;
    private List<String> sentraHasPrsDataList;

    public List<PRSResponseList> getPrsList() {
        return prsList;
    }

    public void setPrsList(List<PRSResponseList> prsList) {
        this.prsList = prsList;
    }

    public List<String> getSentraHasPrsDataList() {
        return sentraHasPrsDataList;
    }

    public void setSentraHasPrsDataList(List<String> sentraHasPrsDataList) {
        this.sentraHasPrsDataList = sentraHasPrsDataList;
    }

    @Override
    public String toString() {
        return "SyncPRSResponse{" +
                "prsList=" + prsList +
                ", sentraHasPrsDataList=" + sentraHasPrsDataList +
                '}';
    }
}
