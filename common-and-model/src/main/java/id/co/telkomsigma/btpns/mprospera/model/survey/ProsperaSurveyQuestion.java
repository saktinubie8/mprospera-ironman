package id.co.telkomsigma.btpns.mprospera.model.survey;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "M_PROSPERA_SURVEY_QUESTION")
public class ProsperaSurveyQuestion extends GenericModel {

    private Long surveyQuestionId;
    private ProsperaSurveyType surveyType;
    private ProsperaQuestion question;
    private Integer order;
    private Boolean isMandatory;

    @Id
    @Column(name = "survey_question_id", unique = true, nullable = false)
    public Long getSurveyQuestionId() {
        return surveyQuestionId;
    }

    public void setSurveyQuestionId(Long surveyQuestionId) {
        this.surveyQuestionId = surveyQuestionId;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = ProsperaSurveyType.class)
    @JoinColumn(name = "survey_id", referencedColumnName = "survey_id", nullable = true)
    public ProsperaSurveyType getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(ProsperaSurveyType surveyType) {
        this.surveyType = surveyType;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = ProsperaQuestion.class)
    @JoinColumn(name = "question_id", referencedColumnName = "question_id", nullable = true)
    public ProsperaQuestion getQuestion() {
        return question;
    }

    public void setQuestion(ProsperaQuestion question) {
        this.question = question;
    }

    @Column(name = "question_order", nullable = true)
    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @Column(name = "mandatory", nullable = true)
    public Boolean getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(Boolean isMandatory) {
        this.isMandatory = isMandatory;
    }

}
