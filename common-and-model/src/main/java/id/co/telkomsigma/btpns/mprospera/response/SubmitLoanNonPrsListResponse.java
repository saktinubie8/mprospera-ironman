package id.co.telkomsigma.btpns.mprospera.response;

public class SubmitLoanNonPrsListResponse {

    private String loanNonPrsId;

    public String getLoanNonPrsId() {
        return loanNonPrsId;
    }

    public void setLoanNonPrsId(String loanNonPrsId) {
        this.loanNonPrsId = loanNonPrsId;
    }

}
