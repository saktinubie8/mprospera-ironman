package id.co.telkomsigma.btpns.mprospera.model.claim;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_INSURANCE_CLAIM_PHOTO")
public class InsuranceClaimPhoto extends GenericModel {

    private Long id;
    private InsuranceClaim insuranceClaim;
    private byte[] photo;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = InsuranceClaim.class)
    @JoinColumn(name = "insurance_claim_id", referencedColumnName = "id", nullable = true)
    public InsuranceClaim getInsuranceClaim() {
        return insuranceClaim;
    }

    public void setInsuranceClaim(InsuranceClaim insuranceClaim) {
        this.insuranceClaim = insuranceClaim;
    }

    @Column(name = "photo", nullable = true)
    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

}