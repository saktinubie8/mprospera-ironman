package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SummaryRcttMsResponse extends BaseResponse {

    private List<SummaryRcttPrsList> prsList;

    public List<SummaryRcttPrsList> getPrsList() {
        if (prsList == null)
            prsList = new ArrayList<SummaryRcttPrsList>();
        return prsList;
    }

    public void setPrsList(List<SummaryRcttPrsList> prsList) {
        this.prsList = prsList;
    }

}
