package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class SubmitNonPRSResponse extends BaseResponse {

    private List<SubmitLoanNonPrsListResponse> loanListNonPrsId;

    public List<SubmitLoanNonPrsListResponse> getLoanListNonPrsId() {
        return loanListNonPrsId;
    }

    public void setLoanListNonPrsId(List<SubmitLoanNonPrsListResponse> loanListNonPrsId) {
        this.loanListNonPrsId = loanListNonPrsId;
    }

}
