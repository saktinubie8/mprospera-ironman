package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class PsMsMtsTrxList {

    private String rptId;
    private String trxTime;
    private String createdBy;
    private String updatedBy;
    private Date approvedDate;
    private Date businessDate;
    private BigDecimal aMount;
    private BigDecimal amountTotalCollect;
    private BigDecimal amountTotalPRS;
    private BigDecimal amountBringPRS;
    private String typePsMsTrx;

    public String getRptId() {
        return rptId;
    }

    public void setRptId(String rptId) {
        this.rptId = rptId;
    }

    public String getTrxTime() {
        return trxTime;
    }

    public void setTrxTime(String trxTime) {
        this.trxTime = trxTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String createdBy) {
        this.updatedBy = createdBy;
    }

    public String getPsMsTrxType() {
        return typePsMsTrx;
    }

    public void setPsMsTrxType(String typeTrx) {
        this.typePsMsTrx = typeTrx;
    }

    public Date getApprovedDate() {
        return this.approvedDate;
    }

    public void setApprovedDate(Date aDate) {
        this.approvedDate = aDate;
    }

    public Date getBusinessDate() {
        return this.businessDate;
    }

    public void setBusinessDate(Date aDate) {
        this.businessDate = aDate;
    }

    public BigDecimal getAmountTotalCollect() {
        return this.amountTotalCollect;
    }

    public void setAmountTotalCollect(BigDecimal actualMoney) {
        this.amountTotalCollect = actualMoney;
    }

    public BigDecimal getAmountTotalPRS() {
        return this.amountTotalPRS;
    }

    public void setAmountTotalPRS(BigDecimal actualMoney) {
        this.amountTotalPRS = actualMoney;
    }

    public BigDecimal getAmountBringPRS() {
        return this.amountBringPRS;
    }

    public void setAmountBringPRS(BigDecimal actualMoney) {
        this.amountBringPRS = actualMoney;
    }

    public BigDecimal getAMount() {
        return aMount;
    }

    public void setAMount(BigDecimal actualMoney) {
        this.aMount = actualMoney;
    }

}
