package id.co.telkomsigma.btpns.mprospera.request;


public class ESBSavingNonPrsRequest {

    private String accountNumber;
    private String depositAmount;
    private String withdrawalAmount;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(String depositAmount) {
        this.depositAmount = depositAmount;
    }

    public String getWithdrawalAmount() {
        return withdrawalAmount;
    }

    public void setWithdrawalAmount(String withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }

    @Override
    public String toString() {
        return "ESBSavingNonPrsRequest [accountNumber=" + accountNumber + ", depositAmount=" + depositAmount
                + ", withdrawalAmount=" + withdrawalAmount + ", getClass()=" + getClass() + ", hashCode()="
                + hashCode() + ", toString()=" + super.toString() + "]";
    }

}
