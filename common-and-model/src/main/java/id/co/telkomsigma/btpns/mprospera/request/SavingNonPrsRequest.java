package id.co.telkomsigma.btpns.mprospera.request;

import java.math.BigDecimal;

public class SavingNonPrsRequest {

    private String savingNonPrsId;
    private String accountNumber;
    private BigDecimal clearBalance;
    private BigDecimal depositAmount;
    private BigDecimal withdrawalAmount;

    public String getSavingNonPrsId() {
        return savingNonPrsId;
    }

    public void setSavingNonPrsId(String savingNonPrsId) {
        this.savingNonPrsId = savingNonPrsId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getClearBalance() {
        return clearBalance;
    }

    public void setClearBalance(BigDecimal clearBalance) {
        this.clearBalance = clearBalance;
    }

    public BigDecimal getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }

    public BigDecimal getWithdrawalAmount() {
        return withdrawalAmount;
    }

    public void setWithdrawalAmount(BigDecimal withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }

    @Override
    public String toString() {
        return "SavingNonPrsRequest [savingNonPrsId=" + savingNonPrsId + ", accountNumber=" + accountNumber
                + ", clearBalance=" + clearBalance + ", depositAmount=" + depositAmount + ", withdrawalAmount="
                + withdrawalAmount + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
                + super.toString() + "]";
    }

}
