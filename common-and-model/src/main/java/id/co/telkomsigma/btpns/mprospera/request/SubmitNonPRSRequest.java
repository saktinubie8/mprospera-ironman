package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

public class SubmitNonPRSRequest extends BaseRequest {

    private String username;
    private String sessionKey;
    private String imei;
    private String longitude;
    private String latitude;
    private List<SentraForNonPrsRequest> sentraList;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public List<SentraForNonPrsRequest> getSentraList() {
        return sentraList;
    }

    public void setSentraList(List<SentraForNonPrsRequest> sentraList) {
        this.sentraList = sentraList;
    }

    @Override
    public String toString() {
        return "SubmitNonPRSRequest{" +
                "username='" + username + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", imei='" + imei + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", sentraList=" + sentraList +
                '}';
    }

}
