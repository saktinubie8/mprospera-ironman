package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class CheckStatusPRSResponse extends BaseResponse {

    private List<SentraPRSListResponse> sentraList;

    public List<SentraPRSListResponse> getSentraList() {
        return sentraList;
    }

    public void setSentraList(List<SentraPRSListResponse> sentraList) {
        this.sentraList = sentraList;
    }

    @Override
    public String toString() {
        return "CheckStatusPRSResponse [sentraList=" + sentraList + ", getResponseCode()=" + getResponseCode()
                + ", getResponseMessage()=" + getResponseMessage() + ", toString()=" + super.toString()
                + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
    }

}
