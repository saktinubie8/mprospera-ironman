package id.co.telkomsigma.btpns.mprospera.model.prs;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

/**
 * Created by Dzulfiqar on 22/05/2017.
 */
@Entity
@Table(name = "T_DISCOUNT_MARGIN_APPROVAL_HISTORY")
public class DiscountMarginApprovalHistory extends GenericModel {

    private Long discountMarginApprovalHistoryId;
    private String level;
    private Date tanggal;
    private String limit;
    private String name;
    private Long discountMarginId;
    private String role;
    private String date;
    private String status;
    private String supervisorId;
    private BigDecimal approvedAmount;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getDiscountMarginApprovalHistoryId() {
        return discountMarginApprovalHistoryId;
    }

    public void setDiscountMarginApprovalHistoryId(Long discountMarginApprovalHistoryId) {
        this.discountMarginApprovalHistoryId = discountMarginApprovalHistoryId;
    }

    @Column(name = "discount_margin_id")
    public Long getDiscountMarginId() {
        return discountMarginId;
    }

    public void setDiscountMarginId(Long discountMarginId) {
        this.discountMarginId = discountMarginId;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "level")
    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Column(name = "tanggal")
    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    @Column(name = "limit")
    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "role")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Column(name = "date")
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Column(name = "supervisor_id")
    public String getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(String supervisorId) {
        this.supervisorId = supervisorId;
    }

    @Column(name = "approved_amt")
    public BigDecimal getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(BigDecimal approvedAmount) {
        this.approvedAmount = approvedAmount;
    }
}
