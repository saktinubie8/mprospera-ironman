package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

public class SyncInsuranceClaimResponse extends BaseResponse {

    private List<InsuranceClaimData> claimList = new ArrayList();

    public List<InsuranceClaimData> getClaimList() {
        return claimList;
    }

    public void setClaimList(List<InsuranceClaimData> claimList) {
        this.claimList = claimList;
    }

}
