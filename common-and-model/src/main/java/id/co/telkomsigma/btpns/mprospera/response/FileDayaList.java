package id.co.telkomsigma.btpns.mprospera.response;

/**
 * Created by Dzulfiqar on 15/05/2017.
 */
public class FileDayaList {

    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
