package id.co.telkomsigma.btpns.mprospera.response;

public class DiscountMarginApprovalResponse extends BaseResponse {

    private String discountMarginId;
    private String localId;

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getDiscountMarginId() {

        return discountMarginId;
    }

    public void setDiscountMarginId(String discountMarginId) {
        this.discountMarginId = discountMarginId;
    }
}
