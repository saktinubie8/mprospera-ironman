package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class LoanResponse extends BaseResponse {

    private String loanId;

    // for list
    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private List<LoanListResponse> loanList;

    // for prospera purpose
    private String prospectId;


    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public String getProspectId() {
        return prospectId;
    }

    public void setProspectId(String prospectId) {
        this.prospectId = prospectId;
    }

    public List<LoanListResponse> getLoanList() {
        if (loanList == null)
            loanList = new ArrayList<>();
        return loanList;
    }

    public void setLoanList(List<LoanListResponse> loanList) {
        this.loanList = loanList;
    }

}
