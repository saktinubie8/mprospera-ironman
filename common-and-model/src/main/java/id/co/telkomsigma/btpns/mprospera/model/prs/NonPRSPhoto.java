package id.co.telkomsigma.btpns.mprospera.model.prs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanNonPRS;

@Entity
@Table(name = "T_NON_PRS_PHOTO")
public class NonPRSPhoto extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long nonPrsPhotoId;
    private Long nonPrsId;
    private byte[] nonPrsPhoto;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getNonPrsPhotoId() {
        return nonPrsPhotoId;
    }

    public void setNonPrsPhotoId(Long nonPrsPhotoId) {
        this.nonPrsPhotoId = nonPrsPhotoId;
    }

    @Column(name = "NON_PRS_ID")
    public Long getNonPrsId() {
        return nonPrsId;
    }

    public void setNonPrsId(Long nonPrsId) {
        this.nonPrsId = nonPrsId;
    }

    @Column(name = "NON_PRS_PHOTO", nullable = true, length = MAX_LENGTH_RAW)
    public byte[] getNonPrsPhoto() {
        return nonPrsPhoto;
    }

    public void setNonPrsPhoto(byte[] nonPrsPhoto) {
        this.nonPrsPhoto = nonPrsPhoto;
    }

}
