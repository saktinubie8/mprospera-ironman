package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class CustomerPRSRequest {

    private String customerId;
    private String customerPrsId;
    private String isAttend;
    private String notAttendReason;
    private String isEarlyTerminationPlan;
    private DiscountMarginReq discountMarginReq;
    private List<LoanPRSRequest> loanList;
    private List<SavingPRSRequest> savingList;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getIsAttend() {
        return isAttend;
    }

    public void setIsAttend(String isAttend) {
        this.isAttend = isAttend;
    }

    public String getNotAttendReason() {
        return notAttendReason;
    }

    public void setNotAttendReason(String notAttendReason) {
        this.notAttendReason = notAttendReason;
    }

    public List<LoanPRSRequest> getLoanList() {
        return loanList;
    }

    public void setLoanList(List<LoanPRSRequest> loanList) {
        this.loanList = loanList;
    }

    public List<SavingPRSRequest> getSavingList() {
        return savingList;
    }

    public void setSavingList(List<SavingPRSRequest> savingList) {
        this.savingList = savingList;
    }

    public String getCustomerPrsId() {
        return customerPrsId;
    }

    public void setCustomerPrsId(String customerPrsId) {
        this.customerPrsId = customerPrsId;
    }

    public String getIsEarlyTerminationPlan() {
        return isEarlyTerminationPlan;
    }

    public void setIsEarlyTerminationPlan(String isEarlyTerminationPlan) {
        this.isEarlyTerminationPlan = isEarlyTerminationPlan;
    }

    public DiscountMarginReq getDiscountMarginReq() {
        return discountMarginReq;
    }

    public void setDiscountMarginReq(DiscountMarginReq discountMarginReq) {
        this.discountMarginReq = discountMarginReq;
    }

    @Override
    public String toString() {
        return "CustomerPRSRequest{" +
                "customerId='" + customerId + '\'' +
                ", customerPrsId='" + customerPrsId + '\'' +
                ", isAttend='" + isAttend + '\'' +
                ", notAttendReason='" + notAttendReason + '\'' +
                ", isEarlyTerminationPlan='" + isEarlyTerminationPlan + '\'' +
                ", discountMarginReq=" + discountMarginReq +
                ", loanList=" + loanList +
                ", savingList=" + savingList +
                '}';
    }

}
