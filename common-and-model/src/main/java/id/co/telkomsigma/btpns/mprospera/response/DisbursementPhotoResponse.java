package id.co.telkomsigma.btpns.mprospera.response;

import java.util.Arrays;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class DisbursementPhotoResponse extends BaseResponse {

    private byte[] photo;

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "DisbursementPhotoResponse [photo=" + Arrays.toString(photo) + ", getResponseCode()="
                + getResponseCode() + ", getResponseMessage()=" + getResponseMessage() + ", toString()="
                + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
    }

}
