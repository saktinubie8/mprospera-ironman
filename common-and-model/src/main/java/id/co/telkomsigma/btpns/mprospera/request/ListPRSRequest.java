package id.co.telkomsigma.btpns.mprospera.request;

public class ListPRSRequest extends BaseRequest {

    private String username;
    private String imei;
    private String sessionKey;
    private String longitude;
    private String latitude;
    private String prsDate;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPrsDate() {
        return prsDate;
    }

    public void setPrsDate(String prsDate) {
        this.prsDate = prsDate;
    }

    @Override
    public String toString() {
        return "ListPRSRequest{" +
                "username='" + username + '\'' +
                ", imei='" + imei + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", prsDate='" + prsDate + '\'' +
                '}';
    }
}
