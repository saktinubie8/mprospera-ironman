package id.co.telkomsigma.btpns.mprospera.model.user;


public class UserAndSentra {

    private String username;
    private String name;
    private String assignedTo;

    public UserAndSentra(String username, String name) {
        super();
        this.username = username;
        this.name = name;
    }

    public UserAndSentra(String username, String name, String assignedTo) {
        super();
        this.username = username;
        this.name = name;
        this.assignedTo = assignedTo;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

}
