package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class AssignPsResponse {

    private String sentraId;
    private String sentraName;
    private String assignedTo;
    private String userFullName;
    private String status;

    public String getSentraId() {
        return sentraId;
    }

    public void setSentraId(String sentraId) {
        this.sentraId = sentraId;
    }

    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AssignPsResponse{" +
                "sentraId='" + sentraId + '\'' +
                ", sentraName='" + sentraName + '\'' +
                ", assignedTo='" + assignedTo + '\'' +
                ", userFullName='" + userFullName + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
