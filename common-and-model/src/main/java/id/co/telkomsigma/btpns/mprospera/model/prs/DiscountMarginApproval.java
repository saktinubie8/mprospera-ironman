package id.co.telkomsigma.btpns.mprospera.model.prs;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

/**
 * Created by Dzulfiqar on 22/05/2017.
 */
@Entity
@Table(name = "T_DISCOUNT_MARGIN_APPROVAL")
public class DiscountMarginApproval extends GenericModel {

    private Long discountMarginId;
    private Long earlyTerminationPlanId;
    private Long customerId;
    private BigDecimal total;
    private String alasan;
    private String status;
    private Long userBwmp;
    private String createdBy;


    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getDiscountMarginId() {
        return discountMarginId;
    }

    public void setDiscountMarginId(Long discountMarginId) {
        this.discountMarginId = discountMarginId;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "user_bwmp")
    public Long getUserBwmp() {
        return userBwmp;
    }

    public void setUserBwmp(Long userBwmp) {
        this.userBwmp = userBwmp;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "early_termination_plan_id")
    public Long getEarlyTerminationPlanId() {
        return earlyTerminationPlanId;
    }

    public void setEarlyTerminationPlanId(Long earlyTerminationPlanId) {
        this.earlyTerminationPlanId = earlyTerminationPlanId;
    }

    @Column(name = "customer_id")
    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Column(name = "total")
    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Column(name = "alasan")
    public String getAlasan() {
        return alasan;
    }

    public void setAlasan(String alasan) {
        this.alasan = alasan;
    }
}
