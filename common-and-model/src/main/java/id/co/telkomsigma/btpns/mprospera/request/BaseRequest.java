package id.co.telkomsigma.btpns.mprospera.request;

public class BaseRequest {
    private String transmissionDateAndTime;
    private String retrievalReferenceNumber;

    public String getTransmissionDateAndTime() {
        return transmissionDateAndTime;
    }

    public void setTransmissionDateAndTime(String transmissionDateAndTime) {
        this.transmissionDateAndTime = transmissionDateAndTime;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    @Override
    public String toString() {
        return "BaseRequest{" +
                "transmissionDateAndTime='" + transmissionDateAndTime + '\'' +
                ", retrievalReferenceNumber='" + retrievalReferenceNumber + '\'' +
                '}';
    }

    public String reformatMoney(String str) {
        if (null == str) return "";
        if (str.contains("E")) {
            long d = Double.valueOf(str).longValue();
            return String.valueOf(d);
        } else return str;
    }

}
