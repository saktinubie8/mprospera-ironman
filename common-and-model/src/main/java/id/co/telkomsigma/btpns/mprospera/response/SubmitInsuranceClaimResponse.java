package id.co.telkomsigma.btpns.mprospera.response;

public class SubmitInsuranceClaimResponse extends BaseResponse {

    private String insuranceClaimId;

    public String getInsuranceClaimId() {
        return insuranceClaimId;
    }

    public void setInsuranceClaimId(String insuranceClaimId) {
        this.insuranceClaimId = insuranceClaimId;
    }

}
