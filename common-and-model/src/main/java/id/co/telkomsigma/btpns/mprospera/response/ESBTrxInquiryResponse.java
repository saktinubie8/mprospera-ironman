package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class ESBTrxInquiryResponse extends BaseResponse {

    private String status;
    private String sisaMarjin;
    private String sisaPokok;
    private String nominalAngsuran;
    private String lastPaymentDate;
    private String totalDanaTalangan;
    private List<TrxHistory> trxHistories;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSisaMarjin() {
        return sisaMarjin;
    }

    public void setSisaMarjin(String sisaMarjin) {
        this.sisaMarjin = sisaMarjin;
    }

    public String getSisaPokok() {
        return sisaPokok;
    }

    public void setSisaPokok(String sisaPokok) {
        this.sisaPokok = sisaPokok;
    }

    public String getNominalAngsuran() {
        return nominalAngsuran;
    }

    public void setNominalAngsuran(String nominalAngsuran) {
        this.nominalAngsuran = nominalAngsuran;
    }

    public String getLastPaymentDate() {
        return lastPaymentDate;
    }

    public void setLastPaymentDate(String lastPaymentDate) {
        this.lastPaymentDate = lastPaymentDate;
    }

    public String getTotalDanaTalangan() {
        return totalDanaTalangan;
    }

    public void setTotalDanaTalangan(String totalDanaTalangan) {
        this.totalDanaTalangan = totalDanaTalangan;
    }

    public List<TrxHistory> getTrxHistories() {
        return trxHistories;
    }

    public void setTrxHistories(List<TrxHistory> trxHistories) {
        this.trxHistories = trxHistories;
    }

    @Override
    public String toString() {
        return "ESBTrxInquiryResponse [status=" + status + ", sisaMarjin=" + sisaMarjin + ", sisaPokok=" + sisaPokok
                + ", nominalAngsuran=" + nominalAngsuran + ", lastPaymentDate=" + lastPaymentDate
                + ", totalDanaTalangan=" + totalDanaTalangan + ", trxHistories=" + trxHistories + "]";
    }

}
