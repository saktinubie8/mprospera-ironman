package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;

public class DiscountMarginLoanResponse {

    private String discountMarginId;
    private BigDecimal amount;
    private String status;

    public String getDiscountMarginId() {
        return discountMarginId;
    }

    public void setDiscountMarginId(String discountMarginId) {
        this.discountMarginId = discountMarginId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
