package id.co.telkomsigma.btpns.mprospera.response;

public class FileDayaResponse {

    byte[] file;
    String fileType;

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

}
