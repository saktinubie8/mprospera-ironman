package id.co.telkomsigma.btpns.mprospera.response;

public class InsuranceClaimData {

    private String claimInsuranceId;
    private String customerCifNumber;
    private String spouseFlag;
    private String customerFlag;
    private String createdBy;
    private String createdDate;
    private String deathCause;
    private String customerId;
    private String status;

    public String getDeathCause() {
        return deathCause;
    }

    public void setDeathCause(String deathCause) {
        this.deathCause = deathCause;
    }

    public String getClaimInsuranceId() {
        return claimInsuranceId;
    }

    public void setClaimInsuranceId(String claimInsuranceId) {
        this.claimInsuranceId = claimInsuranceId;
    }

    public String getCustomerCifNumber() {
        return customerCifNumber;
    }

    public void setCustomerCifNumber(String customerCifNumber) {
        this.customerCifNumber = customerCifNumber;
    }

    public String getSpouseFlag() {
        return spouseFlag;
    }

    public void setSpouseFlag(String spouseFlag) {
        this.spouseFlag = spouseFlag;
    }

    public String getCustomerFlag() {
        return customerFlag;
    }

    public void setCustomerFlag(String customerFlag) {
        this.customerFlag = customerFlag;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
