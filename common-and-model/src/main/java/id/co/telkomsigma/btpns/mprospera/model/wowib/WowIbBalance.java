package id.co.telkomsigma.btpns.mprospera.model.wowib;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by fujitsuPC on 5/16/2017.
 */

@Entity
@Table(name = "T_WOWIB_BALANCE")
public class WowIbBalance extends GenericModel {
    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    private Long id;

    @Column(name = "phone_number", nullable = true)
    private String phoneNumber;

    @Column(name = "balance", nullable = true)
    private BigDecimal balance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
