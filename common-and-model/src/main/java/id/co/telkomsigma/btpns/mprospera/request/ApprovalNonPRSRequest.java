package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class ApprovalNonPRSRequest extends BaseRequest {

    private String imei;
    private String username;
    private String userMs;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private String sessionKey;
    private String longitude;
    private String latitude;
    private String totalCollect;
    private List<CustomerForNonPrsRequest> customerList;
    private String tokenChallenge;
    private String tokenResponse;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public List<CustomerForNonPrsRequest> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<CustomerForNonPrsRequest> customerList) {
        this.customerList = customerList;
    }

    public String getTokenChallenge() {
        return tokenChallenge;
    }

    public void setTokenChallenge(String tokenChallenge) {
        this.tokenChallenge = tokenChallenge;
    }

    public String getTokenResponse() {
        return tokenResponse;
    }

    public void setTokenResponse(String tokenResponse) {
        this.tokenResponse = tokenResponse;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserMs() {
        return userMs;
    }

    public void setUserMs(String userMs) {
        this.userMs = userMs;
    }

    public String getTotalCollect() {
        return totalCollect;
    }

    public void setTotalCollect(String totalCollect) {
        this.totalCollect = totalCollect;
    }

    @Override
    public String toString() {
        return "ApprovalNonPRSRequest{" +
                "imei='" + imei + '\'' +
                ", username='" + username + '\'' +
                ", userMs='" + userMs + '\'' +
                ", password='" + password + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", totalCollect='" + totalCollect + '\'' +
                ", customerList=" + customerList +
                ", tokenChallenge='" + tokenChallenge + '\'' +
                ", tokenResponse='" + tokenResponse + '\'' +
                '}';
    }

}
