package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.response.DiscountMarginApprovalHistoryResponse;
import id.co.telkomsigma.btpns.mprospera.response.LoanListDiscountMarginResponse;

public class CustomerListResponse {

    private String customerId;
    private String wismaId;
    private String wismaCode;
    private String wismaName;
    private String discountMarginId;
    private String requestedAmount;
    private String cifNumber;
    private String customerName;
    private String sentraName;
    private String groupName;
    private List<LoanListDiscountMarginResponse> loanList;
    private List<DiscountMarginApprovalHistoryResponse> approvalHistories;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getWismaId() {
        return wismaId;
    }

    public void setWismaId(String wismaId) {
        this.wismaId = wismaId;
    }

    public String getWismaCode() {
        return wismaCode;
    }

    public void setWismaCode(String wismaCode) {
        this.wismaCode = wismaCode;
    }

    public String getWismaName() {
        return wismaName;
    }

    public void setWismaName(String wismaName) {
        this.wismaName = wismaName;
    }

    public String getDiscountMarginId() {
        return discountMarginId;
    }

    public void setDiscountMarginId(String discountMarginId) {
        this.discountMarginId = discountMarginId;
    }

    public String getRequestedAmount() {
        return requestedAmount;
    }

    public void setRequestedAmount(String requestedAmount) {
        this.requestedAmount = requestedAmount;
    }

    public List<LoanListDiscountMarginResponse> getLoanList() {
        return loanList;
    }

    public void setLoanList(List<LoanListDiscountMarginResponse> loanList) {
        this.loanList = loanList;
    }

    public List<DiscountMarginApprovalHistoryResponse> getApprovalHistories() {
        return approvalHistories;
    }

    public void setApprovalHistories(List<DiscountMarginApprovalHistoryResponse> approvalHistories) {
        this.approvalHistories = approvalHistories;
    }

    public String getCifNumber() {
        return cifNumber;
    }

    public void setCifNumber(String cifNumber) {
        this.cifNumber = cifNumber;
    }

    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}
