package id.co.telkomsigma.btpns.mprospera.request;

public class PRSPrepare {

    private String sentraId;
    private String bringMoney;

    public String getSentraId() {
        return sentraId;
    }

    public void setSentraId(String sentraId) {
        this.sentraId = sentraId;
    }

    public String getBringMoney() {
        return bringMoney;
    }

    public void setBringMoney(String bringMoney) {
        this.bringMoney = bringMoney;
    }
}
