package id.co.telkomsigma.btpns.mprospera.request;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SubmitInsuranceClaimRequest extends BaseRequest {
    protected String customerNumber;
    protected String spouseFlag;
    protected String customerFlag;
    protected String deathCause;
    protected String oldStatus;
    protected String newStatus;

    protected String insuranceClaimId;
    protected String username;
    protected String sessionKey;
    protected String imei;
    protected String longitude;
    protected String latitude;
    protected String status;
    protected String action;

    protected String approvedBy;
    protected String approvedDate;

    protected String createdBy;
    protected String createdDate;

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerCifNumber) {
        this.customerNumber = customerCifNumber;
    }

    public String getSpouseFlag() {
        return spouseFlag;
    }

    public void setSpouseFlag(String spouseFlag) {
        this.spouseFlag = spouseFlag;
    }

    public String getCustomerFlag() {
        return customerFlag;
    }

    public void setCustomerFlag(String customerFlag) {
        this.customerFlag = customerFlag;
    }

    public String getDeathCause() {
        return deathCause;
    }

    public void setDeathCause(String deathCause) {
        this.deathCause = deathCause;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInsuranceClaimId() {
        return insuranceClaimId;
    }

    public void setInsuranceClaimId(String insuranceClaimId) {
        this.insuranceClaimId = insuranceClaimId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(String oldStatus) {
        this.oldStatus = oldStatus;
    }

    public String getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }

    @Override
    public String toString() {
        return "SubmitInsuranceClaimRequest{" +
                "customerNumber='" + customerNumber + '\'' +
                ", spouseFlag='" + spouseFlag + '\'' +
                ", customerFlag='" + customerFlag + '\'' +
                ", deathCause='" + deathCause + '\'' +
                ", oldStatus='" + oldStatus + '\'' +
                ", newStatus='" + newStatus + '\'' +
                ", insuranceClaimId='" + insuranceClaimId + '\'' +
                ", username='" + username + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", imei='" + imei + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", status='" + status + '\'' +
                ", action='" + action + '\'' +
                ", approvedBy='" + approvedBy + '\'' +
                ", approvedDate='" + approvedDate + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate='" + createdDate + '\'' +
                '}';
    }
}
