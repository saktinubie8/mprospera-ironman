package id.co.telkomsigma.btpns.mprospera.model.prs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;

@Entity
@Table(name = "T_DISBURSEMENT_PHOTO")
public class DisbursementPhoto extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 8355291868728814110L;

    private Long disbursementPhotoId;
    private byte[] disbursementPhoto;
    private LoanPRS loanPrsId;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getDisbursementPhotoId() {
        return disbursementPhotoId;
    }

    public void setDisbursementPhotoId(Long disbursementPhotoId) {
        this.disbursementPhotoId = disbursementPhotoId;
    }

    @Column(name = "photo", nullable = true, length = 2000000)
    public byte[] getDisbursementPhoto() {
        return disbursementPhoto;
    }

    public void setDisbursementPhoto(byte[] disbursementPhoto) {
        this.disbursementPhoto = disbursementPhoto;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = LoanPRS.class)
    @JoinColumn(name = "LOAN_ID", referencedColumnName = "id")
    public LoanPRS getLoanPrsId() {
        return loanPrsId;
    }

    public void setLoanPrsId(LoanPRS loanPrsId) {
        this.loanPrsId = loanPrsId;
    }

}
