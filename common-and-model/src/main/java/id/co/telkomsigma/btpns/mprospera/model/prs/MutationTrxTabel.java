package id.co.telkomsigma.btpns.mprospera.model.prs;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "T_MUTATION_TRX_TABLE")
public class MutationTrxTabel extends GenericModel {

    private Long mutationId;
    private Date approvedDate;
    private Date businessDate;
    private BigDecimal bringMoney;
    private BigDecimal totalPRSMoney;
    private BigDecimal totalCollect;
    private String trxType;
    private String createdBy;
    private String approvedBy;
    private Long prsId;

    @Column(name = "prs_id")
    public Long getPrsId() {
        return prsId;
    }

    public void setPrsId(Long prsId) {
        this.prsId = prsId;
    }

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getMutationId() {
        return mutationId;
    }

    public void setMutationId(Long mutationId) {
        this.mutationId = mutationId;
    }

    @Column(name = "approved_dt")
    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    @Column(name = "business_dt")
    public Date getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(Date businessDate) {
        this.businessDate = businessDate;
    }

    @Column(name = "bring_money")
    public BigDecimal getBringMoney() {
        return bringMoney;
    }

    public void setBringMoney(BigDecimal bringMoney) {
        this.bringMoney = bringMoney;
    }

    @Column(name = "total_prs_money")
    public BigDecimal getTotalPRSMoney() {
        return totalPRSMoney;
    }

    public void setTotalPRSMoney(BigDecimal totalPRSMoney) {
        this.totalPRSMoney = totalPRSMoney;
    }

    @Column(name = "total_collect")
    public BigDecimal getTotalCollect() {
        return totalCollect;
    }

    public void setTotalCollect(BigDecimal totalCollect) {
        this.totalCollect = totalCollect;
    }

    @Column(name = "trx_type")
    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "approved_by")
    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }
}
