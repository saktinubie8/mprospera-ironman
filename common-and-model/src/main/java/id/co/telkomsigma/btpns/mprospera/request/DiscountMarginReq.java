package id.co.telkomsigma.btpns.mprospera.request;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Dzulfiqar on 22/05/2017.
 */
public class DiscountMarginReq extends BaseRequest {
    private String username;
    private String sessionKey;
    private String imei;
    private Long discountMarginId;
    private Long customerId;
    private BigDecimal total;
    private String alasan;
    private String status;
    private Long userBwmp;
    private String createdBy;
    private List<LoanPRSRequest> loans;
    private String localId;
    private List<DiscountMarginApprovalHistoryRequest> approvalHistories;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Long getDiscountMarginId() {
        return discountMarginId;
    }

    public void setDiscountMarginId(Long discountMarginId) {
        this.discountMarginId = discountMarginId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getAlasan() {
        return alasan;
    }

    public void setAlasan(String alasan) {
        this.alasan = alasan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getUserBwmp() {
        return userBwmp;
    }

    public void setUserBwmp(Long userBwmp) {
        this.userBwmp = userBwmp;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public List<LoanPRSRequest> getLoans() {
        return loans;
    }

    public void setLoans(List<LoanPRSRequest> loans) {
        this.loans = loans;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public List<DiscountMarginApprovalHistoryRequest> getApprovalHistories() {
        return approvalHistories;
    }

    public void setApprovalHistories(List<DiscountMarginApprovalHistoryRequest> approvalHistories) {
        this.approvalHistories = approvalHistories;
    }

}
