package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.List;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PRSSummaryRCTTResponse {

    private String psAssign;
    private List<LoanPRSListResponse> loanList;
    private List<SavingPRSListResponse> savingList;

    public String getPsAssign() {
        return psAssign;
    }

    public void setPsAssign(String psAssign) {
        this.psAssign = psAssign;
    }

    public List<LoanPRSListResponse> getLoanList() {
        return loanList;
    }

    public void setLoanList(List<LoanPRSListResponse> loanList) {
        this.loanList = loanList;
    }

    public List<SavingPRSListResponse> getSavingList() {
        return savingList;
    }

    public void setSavingList(List<SavingPRSListResponse> savingList) {
        this.savingList = savingList;
    }

}
