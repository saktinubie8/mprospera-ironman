package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class ListPRSResponse extends BaseResponse {

    private List<ListStatusPRSResponse> sentraList;

    public List<ListStatusPRSResponse> getSentraList() {
        return sentraList;
    }

    public void setSentraList(List<ListStatusPRSResponse> sentraList) {
        this.sentraList = sentraList;
    }

    @Override
    public String toString() {
        return "ListPRSResponse{" +
                "sentraList=" + sentraList +
                '}';
    }
}
