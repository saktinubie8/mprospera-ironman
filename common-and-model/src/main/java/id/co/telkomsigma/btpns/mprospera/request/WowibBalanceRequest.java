package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;


public class WowibBalanceRequest extends BaseRequest {

    private String imei;
    private String username;
    private String sessionKey;
    private List<String> listPrsId;


    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public List<String> getListPrsId() {
        return listPrsId;
    }

    public void setListPrsId(List<String> listPrs) {
        this.listPrsId = listPrs;
    }

    @Override
    public String toString() {
        return "WowibBalanceRequest [imei=" + imei + ", username=" + username + ", sessionKey=" + sessionKey
                + ", listPrsId=" + listPrsId + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime()
                + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber() + "]";
    }

}
