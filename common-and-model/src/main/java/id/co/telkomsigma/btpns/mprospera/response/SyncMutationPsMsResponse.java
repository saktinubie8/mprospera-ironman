package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;
import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/*
 * SyncMutationPsMsResponse
 *
 * use in ..webservice/psTrxMutation
 * raw data from:
 *  table PRS via list of PsMsPRS
 *  table 't_loan_non-prs' via list of PsMsNonPRS
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SyncMutationPsMsResponse extends BaseResponse {

    private List<PsMsPRSResponseList> prsList;
    private List<PsMsNonPRSResponseList> nonPrsList;
    private String cutOffTime;
    private List<PsMsMtsTrxList> trxList;
    private BigDecimal inAmountBeforeCOT;
    private BigDecimal outAmountBeforeCOT;
    private BigDecimal inAmountAfterCOT;
    private BigDecimal outAmountAfterCOT;

    public List<PsMsPRSResponseList> getPrsList() {
        return prsList;
    }

    public void setPrsList(List<PsMsPRSResponseList> prsList) {
        this.prsList = prsList;
    }

    public List<PsMsNonPRSResponseList> getNonPrsList() {
        return nonPrsList;
    }

    public void setNonPrsList(List<PsMsNonPRSResponseList> nonPrsList) {
        this.nonPrsList = nonPrsList;
    }

    public String getCutOffTime() {
        return cutOffTime;
    }

    public void setCutOffTime(String cutOffTime) {
        this.cutOffTime = cutOffTime;
    }

    @Override
    public String toString() {
        return "SyncMutationPsMsResponse [trxList=" + trxList
                + ", in Amount Before COT=" + inAmountBeforeCOT
                + ", out Amount Before COT=" + outAmountBeforeCOT
                + ", in Amount After COT=" + inAmountAfterCOT
                + ", out Amount After COT=" + outAmountAfterCOT
                + "]";
    }

    public List<PsMsMtsTrxList> getTrxList() {
        return trxList;
    }

    public void setTrxList(List<PsMsMtsTrxList> nonPrsList) {
        this.trxList = nonPrsList;
    }

    public BigDecimal getInAmountBeforeCOT() {
        return inAmountBeforeCOT;
    }

    public BigDecimal getOutAmountBeforeCOT() {
        return outAmountBeforeCOT;
    }

    public BigDecimal getInAmountAfterCOT() {
        return inAmountAfterCOT;
    }

    public BigDecimal getOutAmountAfterCOT() {
        return outAmountAfterCOT;
    }

    public void setInAmountBeforeCOT(BigDecimal Amount) {
        this.inAmountBeforeCOT = Amount;
    }

    public void setOutAmountBeforeCOT(BigDecimal Amount) {
        this.outAmountBeforeCOT = Amount;
    }

    public void setInAmountAfterCOT(BigDecimal Amount) {
        this.inAmountAfterCOT = Amount;
    }

    public void setOutAmountAfterCOT(BigDecimal Amount) {
        this.outAmountAfterCOT = Amount;
    }

}
