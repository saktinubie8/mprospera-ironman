package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;

public class LoanListDiscountMarginResponse {

    private String loanId;
    private String loanPrsId;
    private String appid;
    private BigDecimal sisaPokok;
    private BigDecimal sisaMargin;

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public String getLoanPrsId() {
        return loanPrsId;
    }

    public void setLoanPrsId(String loanPrsId) {
        this.loanPrsId = loanPrsId;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public BigDecimal getSisaPokok() {
        return sisaPokok;
    }

    public void setSisaPokok(BigDecimal sisaPokok) {
        this.sisaPokok = sisaPokok;
    }

    public BigDecimal getSisaMargin() {
        return sisaMargin;
    }

    public void setSisaMargin(BigDecimal sisaMargin) {
        this.sisaMargin = sisaMargin;
    }

}
