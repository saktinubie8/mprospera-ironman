package id.co.telkomsigma.btpns.mprospera.model.prs;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "T_REPORT_COLLECTION")
public class ReportCollection extends GenericModel {

    private Long reportId;
    private String appId;
    private BigDecimal installmentPaymentAmount;
    private Date paymentDate;
    private Date commitmentDate;
    private Long peopleMeet;
    private BigDecimal outstandingAmount;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    @Column(name = "app_id")
    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    @Column(name = "installment_pay_amt")
    public BigDecimal getInstallmentPaymentAmount() {
        return installmentPaymentAmount;
    }

    public void setInstallmentPaymentAmount(BigDecimal installmentPaymentAmount) {
        this.installmentPaymentAmount = installmentPaymentAmount;
    }

    @Column(name = "payment_dt")
    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Column(name = "commitment_dt")
    public Date getCommitmentDate() {
        return commitmentDate;
    }

    public void setCommitmentDate(Date commitmentDate) {
        this.commitmentDate = commitmentDate;
    }

    @Column(name = "people_meet")
    public Long getPeopleMeet() {
        return peopleMeet;
    }

    public void setPeopleMeet(Long peopleMeet) {
        this.peopleMeet = peopleMeet;
    }

    @Column(name = "outstanding_amt")
    public BigDecimal getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(BigDecimal outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }
}
