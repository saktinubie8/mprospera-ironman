package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class CustomerPRSListResponse {

    private String customerId;
    private String customerName;
    private String customerCif;
    private String swId;
    private String customerPrsId;
    private String groupId;
    private String groupName;
    private String hasIdCardPhoto;
    private String hasBusinessPlacePhoto;
    private Boolean isAttend;
    private String notAttendReason;
    private List<LoanPRSListResponse> loanList;
    private List<SavingPRSListResponse> savingList;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerPrsId() {
        return customerPrsId;
    }

    public void setCustomerPrsId(String customerPrsId) {
        this.customerPrsId = customerPrsId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getHasIdCardPhoto() {
        return hasIdCardPhoto;
    }

    public void setHasIdCardPhoto(String hasIdCardPhoto) {
        this.hasIdCardPhoto = hasIdCardPhoto;
    }

    public Boolean getIsAttend() {
        return isAttend;
    }

    public void setIsAttend(Boolean isAttend) {
        this.isAttend = isAttend;
    }

    public String getNotAttendReason() {
        return notAttendReason;
    }

    public void setNotAttendReason(String notAttendReason) {
        this.notAttendReason = notAttendReason;
    }

    public List<LoanPRSListResponse> getLoanList() {
        return loanList;
    }

    public void setLoanList(List<LoanPRSListResponse> loanList) {
        this.loanList = loanList;
    }

    public List<SavingPRSListResponse> getSavingList() {
        return savingList;
    }

    public void setSavingList(List<SavingPRSListResponse> savingList) {
        this.savingList = savingList;
    }

    public String getHasBusinessPlacePhoto() {
        return hasBusinessPlacePhoto;
    }

    public void setHasBusinessPlacePhoto(String hasBusinessPlacePhoto) {
        this.hasBusinessPlacePhoto = hasBusinessPlacePhoto;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerCif() {
        return customerCif;
    }

    public void setCustomerCif(String customerCif) {
        this.customerCif = customerCif;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return "CustomerPRSListResponse{" +
                "customerId='" + customerId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerCif='" + customerCif + '\'' +
                ", swId='" + swId + '\'' +
                ", customerPrsId='" + customerPrsId + '\'' +
                ", groupId='" + groupId + '\'' +
                ", groupName='" + groupName + '\'' +
                ", hasIdCardPhoto='" + hasIdCardPhoto + '\'' +
                ", hasBusinessPlacePhoto='" + hasBusinessPlacePhoto + '\'' +
                ", isAttend=" + isAttend +
                ", notAttendReason='" + notAttendReason + '\'' +
                ", loanList=" + loanList +
                ", savingList=" + savingList +
                '}';
    }
}
