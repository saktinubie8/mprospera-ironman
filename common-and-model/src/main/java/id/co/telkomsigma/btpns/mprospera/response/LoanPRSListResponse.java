package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class LoanPRSListResponse extends BaseResponse {

    private String loanId;
    private String loanCreatedDate;
    private String loanPrsId;
    private String appId;
    private BigDecimal disbursementAmount;
    private String disbursementFlag;
    private boolean canPendingDisbursement;
    private String hasDisbursementPhoto;
    private BigDecimal installmentAmount;
    private BigDecimal currentMargin;
    private BigDecimal installmentPaymentAmount;
    private boolean useEmergencyFund;
    private BigDecimal marginAmount;
    private BigDecimal plafonAmount;
    private BigDecimal outstandingAmount;
    private String outstandingCount;
    private BigDecimal remainingPrincipal;
    private String dueDate;
    private String overdueDays;
    private String isEarlyTermination;
    private String isEarlyTerminationAdhoc;
    private String earlyTerminationPlan;
    private String earlyTerminationReason;
    private String marginDiscountDeviationFlag;
    private BigDecimal marginDiscountDeviationAmount;
    private boolean wowIbStatus;
    private boolean hasDisbursementPlan;
    private String isLoanPhone;
    private String productType;
    private String angsuran;
    private String tenor;
    private DiscountMarginLoanResponse discountMargin;
    private BigDecimal remainingOutstandingPrincipal;
    private BigDecimal outstandingMarginAmount;

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public String getLoanPrsId() {
        return loanPrsId;
    }

    public void setLoanPrsId(String loanPrsId) {
        this.loanPrsId = loanPrsId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public BigDecimal getDisbursementAmount() {
        return disbursementAmount;
    }

    public void setDisbursementAmount(BigDecimal disbursementAmount) {
        this.disbursementAmount = disbursementAmount;
    }

    public String getDisbursementFlag() {
        return disbursementFlag;
    }

    public void setDisbursementFlag(String disbursementFlag) {
        this.disbursementFlag = disbursementFlag;
    }

    public boolean getCanPendingDisbursement() {
        return canPendingDisbursement;
    }

    public void setCanPendingDisbursement(boolean canPendingDisbursement) {
        this.canPendingDisbursement = canPendingDisbursement;
    }

    public String getHasDisbursementPhoto() {
        return hasDisbursementPhoto;
    }

    public void setHasDisbursementPhoto(String hasDisbursementPhoto) {
        this.hasDisbursementPhoto = hasDisbursementPhoto;
    }

    public BigDecimal getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(BigDecimal installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public BigDecimal getCurrentMargin() {
        return currentMargin;
    }

    public void setCurrentMargin(BigDecimal currentMargin) {
        this.currentMargin = currentMargin;
    }

    public BigDecimal getInstallmentPaymentAmount() {
        return installmentPaymentAmount;
    }

    public void setInstallmentPaymentAmount(BigDecimal installmentPaymentAmount) {
        this.installmentPaymentAmount = installmentPaymentAmount;
    }

    public boolean getUseEmergencyFund() {
        return useEmergencyFund;
    }

    public void setUseEmergencyFund(boolean useEmergencyFund) {
        this.useEmergencyFund = useEmergencyFund;
    }

    public BigDecimal getMarginAmount() {
        return marginAmount;
    }

    public void setMarginAmount(BigDecimal marginAmount) {
        this.marginAmount = marginAmount;
    }

    public BigDecimal getPlafonAmount() {
        return plafonAmount;
    }

    public void setPlafonAmount(BigDecimal plafonAmount) {
        this.plafonAmount = plafonAmount;
    }

    public BigDecimal getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(BigDecimal outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }

    public BigDecimal getRemainingPrincipal() {
        return remainingPrincipal;
    }

    public void setRemainingPrincipal(BigDecimal remainingPrincipal) {
        this.remainingPrincipal = remainingPrincipal;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getOverdueDays() {
        return overdueDays;
    }

    public void setOverdueDays(String overdueDays) {
        this.overdueDays = overdueDays;
    }

    public String getIsEarlyTermination() {
        return isEarlyTermination;
    }

    public void setIsEarlyTermination(String isEarlyTermination) {
        this.isEarlyTermination = isEarlyTermination;
    }

    public String getEarlyTerminationPlan() {
        return earlyTerminationPlan;
    }

    public void setEarlyTerminationPlan(String earlyTerminationPlan) {
        this.earlyTerminationPlan = earlyTerminationPlan;
    }

    public String getEarlyTerminationReason() {
        return earlyTerminationReason;
    }

    public void setEarlyTerminationReason(String earlyTerminationReason) {
        this.earlyTerminationReason = earlyTerminationReason;
    }

    public String getMarginDiscountDeviationFlag() {
        return marginDiscountDeviationFlag;
    }

    public void setMarginDiscountDeviationFlag(
            String marginDiscountDeviationFlag) {
        this.marginDiscountDeviationFlag = marginDiscountDeviationFlag;
    }

    public BigDecimal getMarginDiscountDeviationAmount() {
        return marginDiscountDeviationAmount;
    }

    public void setMarginDiscountDeviationAmount(
            BigDecimal marginDiscountDeviationAmount) {
        this.marginDiscountDeviationAmount = marginDiscountDeviationAmount;
    }

    public boolean isWowIbStatus() {
        return wowIbStatus;
    }

    public void setWowIbStatus(boolean wowIbStatus) {
        this.wowIbStatus = wowIbStatus;
    }

    public boolean getHasDisbursementPlan() {
        return hasDisbursementPlan;
    }

    public void setHasDisbursementPlan(boolean hasDisbursementPlan) {
        this.hasDisbursementPlan = hasDisbursementPlan;
    }

    public String getOutstandingCount() {
        return outstandingCount;
    }

    public void setOutstandingCount(String outstandingCount) {
        this.outstandingCount = outstandingCount;
    }

    public String getIsLoanPhone() {
        return isLoanPhone;
    }

    public void setIsLoanPhone(String isLoanPhone) {
        this.isLoanPhone = isLoanPhone;
    }

    public String getAngsuran() {
        return angsuran;
    }

    public void setAngsuran(String angsuran) {
        this.angsuran = angsuran;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public DiscountMarginLoanResponse getDiscountMargin() {
        return discountMargin;
    }

    public void setDiscountMargin(DiscountMarginLoanResponse discountMargin) {
        this.discountMargin = discountMargin;
    }

    public String getIsEarlyTerminationAdhoc() {
        return isEarlyTerminationAdhoc;
    }

    public void setIsEarlyTerminationAdhoc(String isEarlyTerminationAdhoc) {
        this.isEarlyTerminationAdhoc = isEarlyTerminationAdhoc;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getLoanCreatedDate() {
        return loanCreatedDate;
    }

    public void setLoanCreatedDate(String loanCreatedDate) {
        this.loanCreatedDate = loanCreatedDate;
    }

    public BigDecimal getRemainingOutstandingPrincipal() {
        return remainingOutstandingPrincipal;
    }

    public void setRemainingOutstandingPrincipal(BigDecimal remainingOutstandingPrincipal) {
        this.remainingOutstandingPrincipal = remainingOutstandingPrincipal;
    }

    public BigDecimal getOutstandingMarginAmount() {
        return outstandingMarginAmount;
    }

    public void setOutstandingMarginAmount(BigDecimal outstandingMarginAmount) {
        this.outstandingMarginAmount = outstandingMarginAmount;
    }


    @Override
    public String toString() {
        return "LoanPRSListResponse{" +
                "loanId='" + loanId + '\'' +
                ", loanCreatedDate='" + loanCreatedDate + '\'' +
                ", loanPrsId='" + loanPrsId + '\'' +
                ", appId='" + appId + '\'' +
                ", disbursementAmount=" + disbursementAmount +
                ", disbursementFlag='" + disbursementFlag + '\'' +
                ", canPendingDisbursement=" + canPendingDisbursement +
                ", hasDisbursementPhoto='" + hasDisbursementPhoto + '\'' +
                ", installmentAmount=" + installmentAmount +
                ", currentMargin=" + currentMargin +
                ", installmentPaymentAmount=" + installmentPaymentAmount +
                ", useEmergencyFund=" + useEmergencyFund +
                ", marginAmount=" + marginAmount +
                ", plafonAmount=" + plafonAmount +
                ", outstandingAmount=" + outstandingAmount +
                ", outstandingCount='" + outstandingCount + '\'' +
                ", remainingPrincipal=" + remainingPrincipal +
                ", dueDate='" + dueDate + '\'' +
                ", overdueDays='" + overdueDays + '\'' +
                ", isEarlyTermination='" + isEarlyTermination + '\'' +
                ", isEarlyTerminationAdhoc='" + isEarlyTerminationAdhoc + '\'' +
                ", earlyTerminationPlan='" + earlyTerminationPlan + '\'' +
                ", earlyTerminationReason='" + earlyTerminationReason + '\'' +
                ", marginDiscountDeviationFlag='" + marginDiscountDeviationFlag + '\'' +
                ", marginDiscountDeviationAmount=" + marginDiscountDeviationAmount +
                ", wowIbStatus=" + wowIbStatus +
                ", hasDisbursementPlan=" + hasDisbursementPlan +
                ", isLoanPhone='" + isLoanPhone + '\'' +
                ", productType='" + productType + '\'' +
                ", angsuran='" + angsuran + '\'' +
                ", tenor='" + tenor + '\'' +
                ", discountMargin=" + discountMargin +
                ", remainingOutstandingPrincipal=" + remainingOutstandingPrincipal +
                ", outstandingMarginAmount=" + outstandingMarginAmount +
                '}';
    }
}
