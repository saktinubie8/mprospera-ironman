package id.co.telkomsigma.btpns.mprospera.response;

public class WowIBCustomerListResponse {

    private String customerId;
    private String balance;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "WowIBCustomerListResponse{" +
                "customerId='" + customerId + '\'' +
                ", balance='" + balance + '\'' +
                '}';
    }
}
