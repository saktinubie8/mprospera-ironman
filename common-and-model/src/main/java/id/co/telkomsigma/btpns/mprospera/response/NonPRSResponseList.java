package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class NonPRSResponseList {

    private String sentraId;
    private String sentraName;
    private String sentraCode;
    private String mmsCode;
    private String mmsName;
    private List<CustomerNonPRSListResponse> customerList;

    public String getMmsCode() {
        return mmsCode;
    }

    public void setMmsCode(String mmsCode) {
        this.mmsCode = mmsCode;
    }

    public String getMmsName() {
        return mmsName;
    }

    public void setMmsName(String mmsName) {
        this.mmsName = mmsName;
    }

    public String getSentraId() {
        return sentraId;
    }

    public void setSentraId(String sentraId) {
        this.sentraId = sentraId;
    }

    public List<CustomerNonPRSListResponse> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<CustomerNonPRSListResponse> customerList) {
        this.customerList = customerList;
    }

    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

    public String getSentraCode() {
        return sentraCode;
    }

    public void setSentraCode(String sentraCode) {
        this.sentraCode = sentraCode;
    }

    @Override
    public String toString() {
        return "NonPRSResponseList{" +
                "sentraId='" + sentraId + '\'' +
                ", sentraName='" + sentraName + '\'' +
                ", sentraCode='" + sentraCode + '\'' +
                ", mmsCode='" + mmsCode + '\'' +
                ", mmsName='" + mmsName + '\'' +
                ", customerList=" + customerList +
                '}';
    }
}
