package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class LoanNonPRSListResponse {

    private String loanNonPrsId;
    private String loanId;
    private String loanCreatedDate;
    private String appId;
    private BigDecimal installmentAmount;
    private BigDecimal currentMargin;
    private BigDecimal installmentPaymentAmount;
    private BigDecimal inputMoney;
    private BigDecimal depositMoney;
    private BigDecimal autoDebet;
    private BigDecimal remainingPrincipal;
    private BigDecimal marginAmount;
    private String overdueDays;
    private BigDecimal outstandingAmount;
    private String outstandingCount;
    private String outstandingReason;
    private String meetingDate;
    private String dueDate;
    private String peopleMeet;
    private boolean payCommitment;
    private String nonPrsPhoto;
    private String status;
    private String createdBy;
    private String createdDate;
    private String commitmentDate;
    private String nextCommitmentDate;
    private String updatedBy;
    private String updatedDate;
    private String submitDate;

    public String getLoanNonPrsId() {
        return loanNonPrsId;
    }

    public void setLoanNonPrsId(String loanNonPrsId) {
        this.loanNonPrsId = loanNonPrsId;
    }

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public BigDecimal getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(BigDecimal installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public BigDecimal getCurrentMargin() {
        return currentMargin;
    }

    public void setCurrentMargin(BigDecimal currentMargin) {
        this.currentMargin = currentMargin;
    }

    public String getOverdueDays() {
        return overdueDays;
    }

    public void setOverdueDays(String overdueDays) {
        this.overdueDays = overdueDays;
    }

    public BigDecimal getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(BigDecimal outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }

    public String getOutstandingReason() {
        return outstandingReason;
    }

    public void setOutstandingReason(String outstandingReason) {
        this.outstandingReason = outstandingReason;
    }

    public String getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(String meetingDate) {
        this.meetingDate = meetingDate;
    }

    public String getPeopleMeet() {
        return peopleMeet;
    }

    public void setPeopleMeet(String peopleMeet) {
        this.peopleMeet = peopleMeet;
    }

    public boolean getPayCommitment() {
        return payCommitment;
    }

    public void setPayCommitment(boolean payCommitment) {
        this.payCommitment = payCommitment;
    }

    public String getNonPrsPhoto() {
        return nonPrsPhoto;
    }

    public void setNonPrsPhoto(String nonPrsPhoto) {
        this.nonPrsPhoto = nonPrsPhoto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getInstallmentPaymentAmount() {
        return installmentPaymentAmount;
    }

    public void setInstallmentPaymentAmount(BigDecimal installmentPaymentAmount) {
        this.installmentPaymentAmount = installmentPaymentAmount;
    }

    public String getOutstandingCount() {
        return outstandingCount;
    }

    public void setOutstandingCount(String outstandingCount) {
        this.outstandingCount = outstandingCount;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public BigDecimal getRemainingPrincipal() {
        return remainingPrincipal;
    }

    public void setRemainingPrincipal(BigDecimal remainingPrincipal) {
        this.remainingPrincipal = remainingPrincipal;
    }

    public BigDecimal getMarginAmount() {
        return marginAmount;
    }

    public void setMarginAmount(BigDecimal marginAmount) {
        this.marginAmount = marginAmount;
    }

    public BigDecimal getInputMoney() {
        return inputMoney;
    }

    public void setInputMoney(BigDecimal inputMoney) {
        this.inputMoney = inputMoney;
    }

    public BigDecimal getDepositMoney() {
        return depositMoney;
    }

    public void setDepositMoney(BigDecimal depositMoney) {
        this.depositMoney = depositMoney;
    }

    public BigDecimal getAutoDebet() {
        return autoDebet;
    }

    public void setAutoDebet(BigDecimal autoDebet) {
        this.autoDebet = autoDebet;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCommitmentDate() {
        return commitmentDate;
    }

    public void setCommitmentDate(String commitmentDate) {
        this.commitmentDate = commitmentDate;
    }

    public String getNextCommitmentDate() {
        return nextCommitmentDate;
    }

    public void setNextCommitmentDate(String nextCommitmentDate) {
        this.nextCommitmentDate = nextCommitmentDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getLoanCreatedDate() {
        return loanCreatedDate;
    }

    public void setLoanCreatedDate(String loanCreatedDate) {
        this.loanCreatedDate = loanCreatedDate;
    }

    @Override
    public String toString() {
        return "LoanNonPRSListResponse{" +
                "loanNonPrsId='" + loanNonPrsId + '\'' +
                ", loanId='" + loanId + '\'' +
                ", loanCreatedDate='" + loanCreatedDate + '\'' +
                ", appId='" + appId + '\'' +
                ", installmentAmount=" + installmentAmount +
                ", currentMargin=" + currentMargin +
                ", installmentPaymentAmount=" + installmentPaymentAmount +
                ", inputMoney=" + inputMoney +
                ", depositMoney=" + depositMoney +
                ", autoDebet=" + autoDebet +
                ", remainingPrincipal=" + remainingPrincipal +
                ", marginAmount=" + marginAmount +
                ", overdueDays='" + overdueDays + '\'' +
                ", outstandingAmount=" + outstandingAmount +
                ", outstandingCount='" + outstandingCount + '\'' +
                ", outstandingReason='" + outstandingReason + '\'' +
                ", meetingDate='" + meetingDate + '\'' +
                ", dueDate='" + dueDate + '\'' +
                ", peopleMeet='" + peopleMeet + '\'' +
                ", payCommitment=" + payCommitment +
                ", nonPrsPhoto='" + nonPrsPhoto + '\'' +
                ", status='" + status + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", commitmentDate='" + commitmentDate + '\'' +
                ", nextCommitmentDate='" + nextCommitmentDate + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedDate='" + updatedDate + '\'' +
                ", submitDate='" + submitDate + '\'' +
                '}';
    }
}
