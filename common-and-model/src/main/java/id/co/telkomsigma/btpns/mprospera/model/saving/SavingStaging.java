package id.co.telkomsigma.btpns.mprospera.model.saving;

import java.util.Date;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "STAGING_M_SAVING_ACCOUNT")
public class SavingStaging extends GenericModel {

    private String savingId;
    private String status;
    private String accountNumber;
    private String customerName;
    private Date createdDate;
    protected String reffId;
    protected Integer modulus;

    @Column(name = "modulus")
    public Integer getModulus() {
        return modulus;
    }

    public void setModulus(Integer modulus) {
        this.modulus = modulus;
    }

    @Column(name = "reff_id")
    public String getReffId() {
        return reffId;
    }

    public void setReffId(String reffId) {
        this.reffId = reffId;
    }

    @Column(name = "CREATED_DT")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Id
    @Column(name = "id", nullable = false, unique = false)
    public String getSavingId() {
        return savingId;
    }

    public void setSavingId(String savingId) {
        this.savingId = savingId;
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "ACCOUNT_NUMBER")
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Column(name = "CUSTOMER_NAME")
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

}
