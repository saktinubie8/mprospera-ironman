package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class PsMsNonPRSResponseList {

    private String loanNonPrsId;
    private String collectEndTime;
    private BigDecimal inputMoney;
    private BigDecimal depositAmount;
    private String updatedBy;
    private String createdBy;

    public String getLoanNonPrsId() {
        return loanNonPrsId;
    }

    public void setLoanNonPrsId(String loanNonPrsId) {
        this.loanNonPrsId = loanNonPrsId;
    }

    public String getCollectEndTime() {
        return collectEndTime;
    }

    public void setCollectEndTime(String collectEndTime) {
        this.collectEndTime = collectEndTime;
    }

    public BigDecimal getInputMoney() {
        return inputMoney;
    }

    public void setInputMoney(BigDecimal inputMoney) {
        this.inputMoney = inputMoney;
    }

    public BigDecimal getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String updatedBy) {
        this.createdBy = updatedBy;
    }
}
