package id.co.telkomsigma.btpns.mprospera.response;


import java.util.List;
import java.math.BigDecimal;



/*
 * RptMutationPsMsResponse
 *
 * use in ..webservice/psmsTrxMutation
 * raw data from table ReportTrx via list of PsMsRptTrx
 *
 */
public class RptMutationPsMsResponse extends BaseResponse {

    private List<PsMsRptTrxResponseList> trxList;
    private BigDecimal inAmountBeforeCOT;
    private BigDecimal outAmountBeforeCOT;
    private BigDecimal inAmountAfterCOT;
    private BigDecimal outAmountAfterCOT;

    public List<PsMsRptTrxResponseList> getTrxList() {
        return trxList;
    }

    public void setTrxList(List<PsMsRptTrxResponseList> nonPrsList) {
        this.trxList = nonPrsList;
    }

    public BigDecimal getInAmountBeforeCOT() {
        return inAmountBeforeCOT;
    }

    public BigDecimal getOutAmountBeforeCOT() {
        return outAmountBeforeCOT;
    }

    public BigDecimal getInAmountAfterCOT() {
        return inAmountAfterCOT;
    }

    public BigDecimal getOutAmountAfterCOT() {
        return outAmountAfterCOT;
    }

    public void setInAmountBeforeCOT(BigDecimal Amount) {
        this.inAmountBeforeCOT = Amount;
    }

    public void setOutAmountBeforeCOT(BigDecimal Amount) {
        this.outAmountBeforeCOT = Amount;
    }

    public void setInAmountAfterCOT(BigDecimal Amount) {
        this.inAmountAfterCOT = Amount;
    }

    public void setOutAmountAfterCOT(BigDecimal Amount) {
        this.outAmountAfterCOT = Amount;
    }

    @Override
    public String toString() {
        return "RptMutationPsMsResponse [trxList=" + trxList
                + ", in Amount Before COT=" + inAmountBeforeCOT
                + ", out Amount Before COT=" + outAmountBeforeCOT
                + ", in Amount After COT=" + inAmountAfterCOT
                + ", out Amount After COT=" + outAmountAfterCOT
                + "]";
    }

}
