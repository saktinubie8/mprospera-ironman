package id.co.telkomsigma.btpns.mprospera.request;

import java.util.Arrays;

public class ESBLoanRefreshRequest extends BaseRequest {

    private String username;
    private String imei;
    private ESBLoan[] loanList;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public ESBLoan[] getLoanList() {
        return loanList;
    }

    public void setLoanList(ESBLoan[] loanList) {
        this.loanList = loanList;
    }

    @Override
    public String toString() {
        return "ProsperaLoanRefreshRequest [username=" + username + ", imei=" + imei + ", loanList="
                + Arrays.toString(loanList) + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime()
                + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber() + "]";
    }

}
