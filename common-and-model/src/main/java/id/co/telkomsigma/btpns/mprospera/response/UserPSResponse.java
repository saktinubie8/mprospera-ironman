package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class UserPSResponse extends BaseResponse {

    List<AssignPsResponse> sentraList;

    public List<AssignPsResponse> getSentraList() {
        return sentraList;
    }

    public void setSentraList(List<AssignPsResponse> sentraList) {
        this.sentraList = sentraList;
    }

}
