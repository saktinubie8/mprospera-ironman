package id.co.telkomsigma.btpns.mprospera.response;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class PRSParameterResponse extends BaseResponse {

    private List<ReasonListResponse> reasonList;
    private byte[] file;
    private String fieldType;
    private List<DenomListResponse> denomList;
    private String prayer;
    private String promise;
    private String pathFile;
    private List<FileDayaList> filePathList;
    private List<PeopleMeetResponse> peopleList;
    private List<OutstandingReasonResponse> outstandingReasonList;

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public List<ReasonListResponse> getReasonList() {
        return reasonList;
    }

    public void setReasonList(List<ReasonListResponse> reasonList) {
        this.reasonList = reasonList;
    }

    public List<DenomListResponse> getDenomList() {
        return denomList;
    }

    public void setDenomList(List<DenomListResponse> denomList) {
        this.denomList = denomList;
    }

    public String getPrayer() {
        return prayer;
    }

    public void setPrayer(String prayer) {
        this.prayer = prayer;
    }

    public String getPromise() {
        return promise;
    }

    public void setPromise(String promise) {
        this.promise = promise;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public List<PeopleMeetResponse> getPeopleList() {
        return peopleList;
    }

    public void setPeopleList(List<PeopleMeetResponse> peopleList) {
        this.peopleList = peopleList;
    }

    public List<FileDayaList> getFilePathList() {
        return filePathList;
    }

    public void setFilePathList(List<FileDayaList> filePathList) {
        this.filePathList = filePathList;
    }

    public List<OutstandingReasonResponse> getOutstandingReasonList() {
        return outstandingReasonList;
    }

    public void setOutstandingReasonList(List<OutstandingReasonResponse> outstandingReasonList) {
        this.outstandingReasonList = outstandingReasonList;
    }

    @Override
    public String toString() {
        return "PRSParameterResponse{" +
                "reasonList=" + reasonList +
                ", file=" + Arrays.toString(file) +
                ", fieldType='" + fieldType + '\'' +
                ", denomList=" + denomList +
                ", prayer='" + prayer + '\'' +
                ", promise='" + promise + '\'' +
                ", pathFile='" + pathFile + '\'' +
                ", filePathList=" + filePathList +
                ", peopleList=" + peopleList +
                ", outstandingReasonList=" + outstandingReasonList +
                '}';
    }

}
