package id.co.telkomsigma.btpns.mprospera.request;


public class SavingPRSRequest extends BaseRequest {

    private String accountId;
    private String accountNumber;
    private String withdrawalAmount;
    private String nextWithdrawalAmountPlan;
    private String isCloseSavingAccount;
    private String clearBalance;
    private String depositAmount;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getWithdrawalAmount() {
        return reformatMoney(withdrawalAmount);
    }

    public void setWithdrawalAmount(String withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }

    public String getNextWithdrawalAmountPlan() {
        return reformatMoney(nextWithdrawalAmountPlan);
    }

    public void setNextWithdrawalAmountPlan(String nextWithdrawalAmountPlan) {
        this.nextWithdrawalAmountPlan = nextWithdrawalAmountPlan;
    }

    public String getIsCloseSavingAccount() {
        return isCloseSavingAccount;
    }

    public void setIsCloseSavingAccount(String isCloseSavingAccount) {
        this.isCloseSavingAccount = isCloseSavingAccount;
    }

    public String getClearBalance() {
        return reformatMoney(clearBalance);
    }

    public void setClearBalance(String clearBalance) {
        this.clearBalance = clearBalance;
    }

    public String getDepositAmount() {
        return reformatMoney(depositAmount);
    }

    public void setDepositAmount(String depositAmount) {
        this.depositAmount = depositAmount;
    }

    @Override
    public String toString() {
        return "SavingPRSRequest{" +
                "accountId='" + accountId + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", withdrawalAmount='" + withdrawalAmount + '\'' +
                ", nextWithdrawalAmountPlan='" + nextWithdrawalAmountPlan + '\'' +
                ", isCloseSavingAccount='" + isCloseSavingAccount + '\'' +
                ", clearBalance='" + clearBalance + '\'' +
                ", depositAmount='" + depositAmount + '\'' +
                '}';
    }
}
