package id.co.telkomsigma.btpns.mprospera.constant;

/**
 * Created by daniel on 4/17/15.
 */
public final class WebGuiConstant {

    public static final String HAZELCAST_INSTANCE_NAME = "telkomsigma-cache";

    /**
     * URL CAPTCHA
     */
    private static final String CAPTCHA_PATH = "/captcha";
    public static final String CAPTCHA_REGISTRATION_PATH_MAPPING = CAPTCHA_PATH + "-registration*";
    public static final String CAPTCHA_RESEND_ACTIVATION_LINK_PATH_MAPPING = CAPTCHA_PATH + "-resend-activation*";
    public static final String CAPTCHA_RESET_PASSWORD_PATH_MAPPING = CAPTCHA_PATH + "-reset-password*";
    public static final String CAPTCHA_PATH_SECURITY_MAPPING = CAPTCHA_PATH + "**";

    /**
     * URL GLOBAL
     */
    private static final String AFTER_LOGIN_PATH = "/home";
    public static final String TERMINAL_ECHO_PATH = "/webservice/echo**";
    public static final String TERMINAL_LOGIN_APPROVAL_PATH = "/webservice/loginApprove/{apkVersion:.+}";
    public static final String TERMINAL_PREPARE_PRS_APPROVAL = "/webservice/preparePRSApproval/{apkVersion:.+}";

    public static final String CLEAR_ALL_CACHE = "/clearCache**";

    public static final String PARAMETER_CUT_OFF_TIME = "android.cut.off.time";

    /**
     * RESPONSE CODE
     */
    public static final String RC_SUCCESS = "00";
    public static final String RC_FAILED = "01";
    public static final String RC_USER_LOCK = "02";
    public static final String RC_USER_DISABLE = "03";
    public static final String RC_TERMINAL_NOT_FOUND = "04";
    public static final String RC_INVALID_PASSWORD = "05";
    public static final String RC_INVALID_SESSION_KEY = "06";
    public static final String RC_ROLE_CANNOT_ACCESS_MOBILE = "07";
    public static final String INVALID_SDA_CATEGORY = "08";
    public static final String UNKNOWN_PARENT_AREA = "09";
    public static final String UNKNOWN_SDA_ID = "10";
    public static final String UNKNOWN_SENTRA_ID = "11";
    public static final String UNKNOWN_PDK_ID = "12";
    public static final String UNKNOWN_SW_ID = "13";
    public static final String UNKNOWN_CUSTOMER_ID = "14";
    public static final String INVALID_APK = "15";
    public static final String UNKNOWN_MM_ID = "16";
    public static final String UNKNOWN_PM_ID = "17";
    public static final String UNKNOWN_LOAN_ID = "18";
    public static final String UNKNOWN_SURVEY_ID = "19";
    public static final String RC_USERNAME_NOTEXISTS = "20";
    public static final String RC_IMEI_NOTEXISTS = "21";
    public static final String RC_SESSIONKEY_NOTEXISTS = "22";
    public static final String RC_IMEI_NOTVALID_FORMAT = "23";
    public static final String RC_USER_INVALID_CITY = "24";
    public static final String RC_ALREADY_APPROVED = "25";
    public static final String RC_UNKNOWN_STATUS = "26";
    public static final String RC_ALREADY_CLOSED = "27";
    public static final String RC_ALREADY_REJECTED = "28";
    public static final String RC_ALREADY_CANCEL = "29";
    public static final String RC_SENTRA_NOTAPPROVED = "30";
    public static final String UNKNOWN_APP_ID = "31";
    public static final String RC_USERNAME_USED = "32";
    public static final String RC_SENTRAGROUP_NOTAPPROVED = "33";
    public static final String RC_SENTRAGROUP_NULL = "34";
    public static final String UNKNOWN_CIF_NUMBER = "35";
    public static final String RC_PRS_NULL = "36";
    public static final String RC_PRS_PHOTO_NULL = "37";
    public static final String RC_PRS_ALREADY_SUBMIT = "38";
    public static final String UNKNOWN_LOAN_PRS = "39";
    public static final String SW_ALREADY_EXIST = "40";
    public static final String ROLE_NULL = "41";
    public static final String ID_PHOTO_NULL = "42";
    public static final String SURVEY_PHOTO_NULL = "43";
    public static final String PRS_REJECTED = "44";
    public static final String UNKNOWN_SAVING = "45";
    public static final String NO_LOAN_HIST = "46";
    public static final String NO_SAVING_HIST = "47";
    public static final String INVALID_KECAMATAN = "48";
    public static final String INVALID_KELURAHAN = "49";
    public static final String NULL_AP3R = "50";
    public static final String AP3R_ALREADY_EXIST = "51";
    public static final String SW_PRODUCT_NULL = "52";
    public static final String SW_APPROVED_FAILED = "53";
    public static final String PHOTO_ALREADY_EXIST = "54";
    public static final String USER_CANNOT_APPROVE = "55";
    public static final String UNKNOWN_INSURANCE_CLAIM_ID = "56";
    public static final String RC_FILE_DAYA_NULL = "57";
    public static final String EARLY_TERMINATION_PLAN_NULL = "58";
    public static final String UNREGISTERED_USER = "59";
    public static final String CUSTOMER_WAIT_APPROVED = "60";
    public static final String RC_LOCATION_ID_NOT_FOUND = "61";
    public static final String APPID_NULL = "62";
    public static final String GROUP_LEADER_NULL = "63";
    public static final String INVALID_RRN = "64";
    public static final String AP3R_NULL = "65";
    public static final String DEVIATION_NOT_APPROVED = "66";
    public static final String DEVIATION_NULL = "67";
    public static final String SENTRA_PHOTO_NULL = "68";
    public static final String SW_NOT_APPROVED = "69";
    public static final String DEVIATION_CANNOT_DELETE = "70";
    public static final String PHOTO_NULL = "71";
    public static final String RC_GENERAL_ERROR = "XX";

    /**
     * Constant STATUS
     */
    public static final String STATUS_WAITING_FOR_APPROVAL = "WAITING FOR APPROVAL";

    public static final String PARAMETER_CURRENT_VERSION = "android.apk.version.current";

    public static final String ENDPOINT_MPROSPERA = "MPROSPERA";

    /**
     * Constant STATUS
     */
    public static final String STATUS_DRAFT = "DRAFT";
    public static final String STATUS_APPROVED = "APPROVED";
    public static final String STATUS_SUBMIT = "SUBMIT";
    public static final String STATUS_CLOSED = "CLOSED";
    public static final String STATUS_REJECTED = "REJECTED";
    public static final String STATUS_CANCEL = "CANCEL";
    public static final String STATUS_NA = "N/A";
    public static final String STATUS_DONE = "DONE";
    public static final String STATUS_APPROVED_MS = "APPROVED-MS";
    public static final String STATUS_INPROCESS = "IN_PROCESS";
    public static final String STATUS_ACTIVE = "ACTIVE";

    /**
     * URL WebService earlyTerminationReason
     */
    public static final String TERMINAL_SYNC_EARLY_TERMINATION_REASON = "/webservice/sync/param/earlyTerminationReason**";
    public static final String TERMINAL_SYNC_EARLY_TERMINATION_REASON_REQUEST = TERMINAL_SYNC_EARLY_TERMINATION_REASON
            + "/{apkVersion:.+}";

    public static final String EARLY_TERMINATION_REASON_PARAM = "early_termination_reason";

    /**
     * URL WebService dayaFile
     */
    public static final String TERMINAL_SYNC_DAYA_FILE = "/webservice/sync/param/dayaFile**";
    public static final String TERMINAL_SYNC_DAYA_FILE_REQUEST = TERMINAL_SYNC_DAYA_FILE + "/{apkVersion:.+}";

    public static final String TERMINAL_SYNC_DAYA_FILE_ID_REQUEST = TERMINAL_SYNC_DAYA_FILE_REQUEST + "/{fileName:.+}";

    public static final String DAYA_FILE_PARAM = "daya_file";

    /**
     * URL WebService doa dan janji
     */
    public static final String TERMINAL_SYNC_PRAYER_PROMISE = "/webservice/sync/param/prayerPromise**";
    public static final String TERMINAL_SYNC_PRAYER_PROMISE_REQUEST = TERMINAL_SYNC_PRAYER_PROMISE + "/{apkVersion:.+}";

    public static final String PRAYER_PROMISE_PARAM = "prayer_promise";

    /**
     * URL WebService Alasan tidak hadir
     */
    public static final String TERMINAL_SYNC_NOT_ATTEND_REASON = "/webservice/sync/param/notAttendReason**";
    public static final String TERMINAL_SYNC_NOT_ATTEND_REASON_REQUEST = TERMINAL_SYNC_NOT_ATTEND_REASON
            + "/{apkVersion:.+}";

    public static final String NOT_ATTEND_REASON_PARAM = "not_attend_reason";

    /**
     * URL WebService Orang yang ditemui
     */
    public static final String TERMINAL_SYNC_PEOPLE_MEET = "/webservice/sync/param/peopleMeet**";
    public static final String TERMINAL_SYNC_PEOPLE_MEET_REQUEST = TERMINAL_SYNC_PEOPLE_MEET + "/{apkVersion:.+}";

    public static final String PEOPLE_MEET_PARAM = "people_meet";

    /**
     * URL WebService alasan menunggak
     */
    public static final String TERMINAL_SYNC_OUTSTANDING_REASON = "/webservice/sync/param/outstandingReason**";
    public static final String TERMINAL_SYNC_OUTSTANDING_REASON_REQUEST = TERMINAL_SYNC_OUTSTANDING_REASON
            + "/{apkVersion:.+}";

    public static final String OUTSTANDING_REASON_PARAM = "outstanding_reason";

    /**
     * URL WebService Ambil Foto Pencairan
     */
    public static final String TERMINAL_GET_DISBURSEMENT_PHOTO = "/webservice/prs/getDisbursementPhoto**";
    public static final String TERMINAL_GET_DISBURSEMENT_PHOTO_REQUEST = TERMINAL_GET_DISBURSEMENT_PHOTO
            + "/{apkVersion:.+}";

    public static final String TERMINAL_GET_DISBURSEMENT_PHOTO_LINK_REQUEST = TERMINAL_GET_DISBURSEMENT_PHOTO_REQUEST
            + "/{id:.+}";

    /**
     * URL WebService Submit Foto Pencairan
     */
    public static final String TERMINAL_SUBMIT_DISBURSEMENT_PHOTO = "/webservice/prs/submitDisbursementPhoto**";
    public static final String TERMINAL_SUBMIT_DISBURSEMENT_PHOTO_REQUEST = TERMINAL_SUBMIT_DISBURSEMENT_PHOTO
            + "/{apkVersion:.+}";

    /**
     * URL WebService Sinkronisasi PRS
     */
    public static final String TERMINAL_SYNC_PRS = "/webservice/prs/sync**";
    public static final String TERMINAL_SYNC_PRS_REQUEST = TERMINAL_SYNC_PRS + "/{apkVersion:.+}";

    public static final String TERMINAL_LIST_PRS = "/webservice/prs/list**";
    public static final String TERMINAL_LIST_PRS_REQUEST = TERMINAL_LIST_PRS + "/{apkVersion:.+}";

    /**
     * URL WebService Sinkronisasi PRS V2
     */
    public static final String TERMINAL_SYNC_PRS_V2 = "/webservice/prs/sync/v2**";
    public static final String TERMINAL_SYNC_PRS_V2_REQUEST = TERMINAL_SYNC_PRS_V2 + "/{apkVersion:.+}";

    /**
     * URL WebService Sinkronisasi NON PRS V2
     */
    public static final String TERMINAL_SYNC_NON_PRS_V2 = "/webservice/nonPrs/sync/v2**";
    public static final String TERMINAL_SYNC_NON_PRS_V2_REQUEST = TERMINAL_SYNC_NON_PRS_V2 + "/{apkVersion:.+}";

    /**
     * URL WebService Inquery Loan History
     */
    public static final String TERMINAL_INQUERY_LOAN_HIST = "/webservice/inquery/loanHistory**";
    public static final String TERMINAL_INQUERY_LOAN_HIST_REQUEST = TERMINAL_INQUERY_LOAN_HIST + "/{apkVersion:.+}";

    /**
     * URL WebService Inquery Saving History
     */
    public static final String TERMINAL_INQUERY_SAVING_HIST = "/webservice/inquery/savingHistory**";
    public static final String TERMINAL_INQUERY_SAVING_HIST_REQUEST = TERMINAL_INQUERY_SAVING_HIST + "/{apkVersion:.+}";

    /**
     * URL WebService Inquery Status Saving
     */
    public static final String TERMINAL_INQUERY_SAVING = "/webservice/inquery/saving**";
    public static final String TERMINAL_INQUERY_SAVING_REQUEST = TERMINAL_INQUERY_SAVING + "/{apkVersion:.+}";

    /**
     * URL WebService Submit PRS
     */
    public static final String TERMINAL_SUBMIT_PRS = "/webservice/prs/submitPRS**";
    public static final String TERMINAL_SUBMIT_PRS_REQUEST = TERMINAL_SUBMIT_PRS + "/{apkVersion:.+}";

    /**
     * URL WebService Submit PRS Manual KFO
     */
    public static final String TERMINAL_SUBMIT_PRS_MANUAL_KFO = "/webservice/prs/submitPRSManual**";
    public static final String TERMINAL_SUBMIT_PRS_MANUAL_KFO_REQUEST = TERMINAL_SUBMIT_PRS_MANUAL_KFO + "/{apkVersion:.+}";

    /**
     * URL WebService Ambil Foto KTP
     */
    public static final String TERMINAL_GET_ID_PHOTO = "/webservice/prs/idCardPhoto**";
    public static final String TERMINAL_GET_ID_PHOTO_REQUEST = TERMINAL_GET_ID_PHOTO + "/{apkVersion:.+}";
    public static final String TERMINAL_GET_ID_PHOTO_LINK_REQUEST = TERMINAL_GET_ID_PHOTO_REQUEST + "/{id:.+}";

    /**
     * URL WebService Ambil Foto Tempat Usaha
     */
    public static final String TERMINAL_BUSINESS_PLACE_PHOTO = "/webservice/prs/businessPlacePhoto**";
    public static final String TERMINAL_BUSINESS_PLACE_PHOTO_REQUEST = TERMINAL_BUSINESS_PLACE_PHOTO
            + "/{apkVersion:.+}";
    public static final String TERMINAL_BUSINESS_PLACE_PHOTO_LINK_REQUEST = TERMINAL_BUSINESS_PLACE_PHOTO_REQUEST
            + "/{id:.+}";

    /**
     * URL WebService Ambil Foto Tempat Usaha Untuk Inquery
     */
    public static final String TERMINAL_INQUERY_BUSINESS_PLACE_PHOTO = "/webservice/inquery/businessPlacePhoto**";
    public static final String TERMINAL_INQUERY_BUSINESS_PLACE_PHOTO_REQUEST = TERMINAL_INQUERY_BUSINESS_PLACE_PHOTO
            + "/{apkVersion:.+}";
    public static final String TERMINAL_INQUERY_BUSINESS_PLACE_PHOTO_LINK_REQUEST = TERMINAL_INQUERY_BUSINESS_PLACE_PHOTO_REQUEST
            + "/{id:.+}";

    /**
     * URL WebService Ambil Foto Aktifitas PRS
     */
    public static final String TERMINAL_PRS_PHOTO = "/webservice/prs/prsPhoto**";
    public static final String TERMINAL_PRS_PHOTO_REQUEST = TERMINAL_PRS_PHOTO + "/{apkVersion:.+}";
    public static final String TERMINAL_PRS_PHOTO_LINK_REQUEST = TERMINAL_PRS_PHOTO_REQUEST + "/{id:.+}";

    /**
     * URL WebService Refresh Kewajiban
     */
    public static final String TERMINAL_LOAN_REFRESH = "/webservice/prs/refreshLoanAmount**";
    public static final String TERMINAL_LOAN_REFRESH_REQUEST = TERMINAL_LOAN_REFRESH + "/{apkVersion:.+}";

    /**
     * URL WebService WOWIB
     */
    public static final String TERMINAL_WOWIB_INQ_BAL = "/webservice/wowib/balance/inquiry**";
    public static final String TERMINAL_WOWIB_INQ_BAL_REQUEST = TERMINAL_WOWIB_INQ_BAL + "/{apkVersion:.+}";

    /**
     * URL WebService Approval PRS
     */
    public static final String TERMINAL_APPROVAL_PRS = "/webservice/prs/approval**";
    public static final String TERMINAL_APPROVAL_PRS_REQUEST = TERMINAL_APPROVAL_PRS + "/{apkVersion:.+}";

    /**
     * URL WebService Approval PRS V2
     */
    public static final String TERMINAL_APPROVAL_PRS_V2 = "/webservice/prs/approval/v2**";
    public static final String TERMINAL_APPROVAL_PRS_V2_REQUEST = TERMINAL_APPROVAL_PRS_V2 + "/{apkVersion:.+}";

    /**
     * URL WebService Sinkronisasi NON PRS
     */
    public static final String TERMINAL_SYNC_NON_PRS = "/webservice/nonPrs/sync**";
    public static final String TERMINAL_SYNC_NON_PRS_REQUEST = TERMINAL_SYNC_NON_PRS + "/{apkVersion:.+}";

    /**
     * URL WebService Submit NON PRS
     */
    public static final String TERMINAL_SUBMIT_NON_PRS = "/webservice/nonPrs/submitNonPRS**";
    public static final String TERMINAL_SUBMIT_NON_PRS_REQUEST = TERMINAL_SUBMIT_NON_PRS + "/{apkVersion:.+}";

    /**
     * URL WebService Submit Foto Non PRS
     */
    public static final String TERMINAL_SUBMIT_NON_PRS_PHOTO = "/webservice/nonPrs/submitNonPrsPhoto**";
    public static final String TERMINAL_SUBMIT_NON_PRS_PHOTO_REQUEST = TERMINAL_SUBMIT_NON_PRS_PHOTO
            + "/{apkVersion:.+}";

    /**
     * URL WebService Ambil Foto Aktifitas NON PRS
     */
    public static final String TERMINAL_NON_PRS_PHOTO = "/webservice/nonPrs/nonPrsPhoto**";
    public static final String TERMINAL_NON_PRS_PHOTO_REQUEST = TERMINAL_NON_PRS_PHOTO + "/{apkVersion:.+}";
    public static final String TERMINAL_NON_PRS_PHOTO_LINK_REQUEST = TERMINAL_NON_PRS_PHOTO_REQUEST + "/{id:.+}";

    /**
     * URL WebService Approval PRS
     */
    public static final String TERMINAL_APPROVAL_NON_PRS = "/webservice/nonPrs/approval**";
    public static final String TERMINAL_APPROVAL_NON_PRS_REQUEST = TERMINAL_APPROVAL_NON_PRS + "/{apkVersion:.+}";

    /**
     * URL WebService Approval PRS
     */
    public static final String TERMINAL_APPROVAL_NON_PRS_V2 = "/webservice/nonPrs/approval/v2**";
    public static final String TERMINAL_APPROVAL_NON_PRS_V2_REQUEST = TERMINAL_APPROVAL_NON_PRS_V2 + "/{apkVersion:.+}";

    /**
     * URL WebService Cek Status PRS
     */
    public static final String TERMINAL_CHECK_PRS_STATUS = "/webservice/prs/checkStatus**";
    public static final String TERMINAL_CHECK_PRS_STATUS_REQUEST = TERMINAL_CHECK_PRS_STATUS + "/{apkVersion:.+}";

    /**
     * URL WebService Assign PS
     */
    public static final String TERMINAL_ASSIGN_PS = "/webservice/assignPs**";
    public static final String TERMINAL_ASSIGN_PS_REQUEST = TERMINAL_ASSIGN_PS + "/{apkVersion:.+}";

    /**
     * URL WebService Inquery NON PRS
     */
    public static final String TERMINAL_INQUERY_NON_PRS = "/webservice/inquery/nonPrs**";
    public static final String TERMINAL_INQUERY_NON_PRS_REQUEST = TERMINAL_INQUERY_NON_PRS + "/{apkVersion:.+}";

    /**
     * URL WebService Sync User PS
     */
    public static final String TERMINAL_SYNC_USER_PS = "/webservice/sync/userPs**";
    public static final String TERMINAL_SYNC_USER_PS_REQUEST = TERMINAL_SYNC_USER_PS + "/{apkVersion:.+}";

    /**
     * URL WebService Inquery PRS Rekap MS
     */
    public static final String TERMINAL_INQUERY_PRS = "/webservice/inquery/prs**";
    public static final String TERMINAL_INQUERY_PRS_REQUEST = TERMINAL_INQUERY_PRS + "/{apkVersion:.+}";

    /**
     * URL WebService Sinkronisasi Mutasi Serah Terima PS/MS
     */
    public static final String TERMINAL_SYNC_MUTATION_PS_MS = "/webservice/psTrxMutation";
    public static final String TERMINAL_SYNC_MUTATION_PS_MS_REQUEST = TERMINAL_SYNC_MUTATION_PS_MS + "/{apkVersion:.+}";
    //	new version:
    public static final String TERMINAL_RPT_MUTATION_PS_MS = "/webservice/psmsTrxMutation";
    public static final String TERMINAL_RPT_MUTATION_PS_MS_REQUEST = TERMINAL_RPT_MUTATION_PS_MS + "/{apkVersion:.+}";

    /**
     * URL WebService Claim Insurance
     */
    public static final String CLAIM_INSURANCE = "/webservice/claim/insurance";
    public static final String CLAIM_INSURANCE_SUBMIT_REQUEST = CLAIM_INSURANCE + "/submit/{apkVersion:.+}";
    public static final String CLAIM_INSURANCE_APPROVE_REQUEST = CLAIM_INSURANCE + "/approve/{apkVersion:.+}";
    public static final String CLAIM_INSURANCE_PHOTO_REQUEST = CLAIM_INSURANCE + "/photo/{apkVersion:.+}";
    public static final String CLAIM_INSURANCE_SYNC_REQUEST = CLAIM_INSURANCE + "/sync/{apkVersion:.+}";
    public static final String CLAIM_INSURANCE_PHOTO_LINK_REQUEST = CLAIM_INSURANCE + "/photoLink/{apkVersion:.+}/{id:.+}";

    /**
     * URL WebService Cek Summary RCTT By MS
     */
    public static final String TERMINAL_CHECK_SUM_RCTT = "/webservice/cekRctt**";
    public static final String TERMINAL_CHECK_SUM_RCTT_REQUEST = TERMINAL_CHECK_SUM_RCTT + "/{apkVersion:.+}";

    /**
     * URL WebService Submit Discount Margin
     */
    public static final String DISCOUNT_MARGIN = "/webservice/prs/discountMarginApproval";
    public static final String DISCOUNT_MARGIN_SUBMIT_REQUEST = DISCOUNT_MARGIN + "/{apkVersion:.+}";

    /**
     * URL WebService Discount Margin Request List
     */
    public static final String DISCOUNT_MARGIN_LIST = "/webservice/prs/discountMarginList";
    public static final String DISCOUNT_MARGIN_LIST_REQUEST = DISCOUNT_MARGIN_LIST + "/{apkVersion:.+}";

    /**
     * URL WebService Submit Foto PRS
     */
    public static final String TERMINAL_SUBMIT_PRS_PHOTO = "/webservice/prs/submitPrsPhoto**";
    public static final String TERMINAL_SUBMIT_PRS_PHOTO_REQUEST = TERMINAL_SUBMIT_PRS_PHOTO
            + "/{apkVersion:.+}";

    /**
     * URL WebService Get Report Collection
     */
    public static final String TERMINAL_GET_REPORT_COLLECTION = "/webservice/nonPrs/getReportCollection**";
    public static final String TERMINAL_GET_REPORT_COLLECTION_REQUEST = TERMINAL_GET_REPORT_COLLECTION
            + "/{apkVersion:.+}";

    /**
     * URL WebService Get Customer Inquiry
     */
    public static final String TERMINAL_GET_CUSTOMER_INQUIRY = "/webservice/customerInquiry";
    public static final String TERMINAL_GET_CUSTOMER_INQUIRY_REQUEST = TERMINAL_GET_CUSTOMER_INQUIRY + "/{apkVersion:.+}";

    /**
     * URL WebService denom
     */
    public static final String TERMINAL_SYNC_DENOM = "/webservice/sync/param/denom**";
    public static final String TERMINAL_SYNC_DENOM_REQUEST = TERMINAL_SYNC_DENOM + "/{apkVersion:.+}";

    public static final String DENOM_PARAM = "denom";

}