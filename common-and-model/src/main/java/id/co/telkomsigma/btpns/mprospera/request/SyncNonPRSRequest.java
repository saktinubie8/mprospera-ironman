package id.co.telkomsigma.btpns.mprospera.request;

public class SyncNonPRSRequest extends BaseRequest {

    private String username;
    private String imei;
    private String sessionKey;
    private String longitude;
    private String latitude;
    private String page;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    @Override
    public String toString() {
        return "SyncNonPRSRequest{" +
                "username='" + username + '\'' +
                ", imei='" + imei + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", page='" + page + '\'' +
                '}';
    }

}
