package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SentraResponse extends BaseResponse {

    private String sentraId;
    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private List<SentraListResponse> sentraList;

    public String getSentraId() {
        return sentraId;
    }

    public void setSentraId(String sentraId) {
        this.sentraId = sentraId;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<SentraListResponse> getSentraList() {
        if (sentraList == null)
            sentraList = new ArrayList<>();
        return sentraList;
    }

    public void setSentraList(List<SentraListResponse> sentraList) {
        this.sentraList = sentraList;
    }

}
