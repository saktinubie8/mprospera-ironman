package id.co.telkomsigma.btpns.mprospera.model.prs;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Dzulfiqar on 2/06/2017.
 */
@Entity
@Table(name = "T_EARLY_TERMINATION_PLAN")
public class EarlyTerminationPlan extends GenericModel {

    private Long earlyTerminationPlanId;
    private Long customerPrsId;
    private BigDecimal discountMarginAmount;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getEarlyTerminationPlanId() {
        return earlyTerminationPlanId;
    }

    public void setEarlyTerminationPlanId(Long earlyTerminationPlanId) {
        this.earlyTerminationPlanId = earlyTerminationPlanId;
    }

    @Column(name = "disc_margin_amt")
    public BigDecimal getDiscountMarginAmount() {
        return discountMarginAmount;
    }

    public void setDiscountMarginAmount(BigDecimal discountMarginAmount) {
        this.discountMarginAmount = discountMarginAmount;
    }

    @Column(name = "customer_prs_id")
    public Long getCustomerPrsId() {
        return customerPrsId;
    }

    public void setCustomerPrsId(Long customerPrsId) {
        this.customerPrsId = customerPrsId;
    }
}
