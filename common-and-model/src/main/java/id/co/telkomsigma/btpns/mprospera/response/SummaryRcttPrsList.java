package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

public class SummaryRcttPrsList {

    private String prsId;
    private String createdBy;

    private List<CustomerPRSListResponse> customerList;

    public String getPrsId() {
        return prsId;
    }

    public void setPrsId(String prsId) {
        this.prsId = prsId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public List<CustomerPRSListResponse> getCustomerList() {
        if (customerList == null)
            customerList = new ArrayList<CustomerPRSListResponse>();
        return customerList;
    }

    public void setCustomerList(List<CustomerPRSListResponse> customerList) {
        this.customerList = customerList;
    }

}
