package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class PsMsPRSResponseList {

    private String prsId;
    private String prsEndTime;
    private BigDecimal actualMoney;
    private BigDecimal paymentMoney;
    private BigDecimal withdrawalAdhoc;
    private String createdBy;
    private String updatedBy;

    public String getPrsId() {
        return prsId;
    }

    public void setPrsId(String prsId) {
        this.prsId = prsId;
    }

    public String getPrsEndTime() {
        return prsEndTime;
    }

    public void setPrsEndTime(String prsEndTime) {
        this.prsEndTime = prsEndTime;
    }

    public BigDecimal getActualMoney() {
        return actualMoney;
    }

    public void setActualMoney(BigDecimal actualMoney) {
        this.actualMoney = actualMoney;
    }

    public BigDecimal getPaymentMoney() {
        return paymentMoney;
    }

    public void setPaymentMoney(BigDecimal paymentMoney) {
        this.paymentMoney = paymentMoney;
    }

    public BigDecimal getWithdrawalAdhoc() {
        return withdrawalAdhoc;
    }

    public void setWithdrawalAdhoc(BigDecimal withdrawalAdhoc) {
        this.withdrawalAdhoc = withdrawalAdhoc;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String createdBy) {
        this.updatedBy = createdBy;
    }

    private BigDecimal aMount;
    //1 = Pencairan, 2 = Pembayaran Angsuran, 3 = Pelunasan Dipercepat, 4 = Tarik Tabungan, 5 = Setor Tabungan
    private String typeTrx;

    public String getTypeTrx() {
        return typeTrx;
    }

    public void setTypeTrx(String typeTrx) {
        this.typeTrx = typeTrx;
    }

    public BigDecimal getAMount() {
        return aMount;
    }

    public void setAMount(BigDecimal actualMoney) {
        this.aMount = actualMoney;
    }
}

