package id.co.telkomsigma.btpns.mprospera.model.loan;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

@Entity
@Table(name = "T_LOAN_HIST_TRX")
public class LoanHistoryTrx extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long loanHistId;
    private String appId;
    private Customer customerId;
    private Date trxDate;
    private String description;
    private BigDecimal amountTrx;
    private BigDecimal outstandingAmount;
    private Long outstandingCount;

    @Id
    @Column(name = "ID", nullable = false, unique = true)
    @GeneratedValue
    public Long getLoanHistId() {
        return loanHistId;
    }

    public void setLoanHistId(Long loanHistId) {
        this.loanHistId = loanHistId;
    }

    @Column(name = "app_id")
    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Customer.class)
    @JoinColumn(name = "customer_Id", referencedColumnName = "id", nullable = true)
    public Customer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customer customerId) {
        this.customerId = customerId;
    }

    @Column(name = "trx_date")
    public Date getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(Date trxDate) {
        this.trxDate = trxDate;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "trx_amt")
    public BigDecimal getAmountTrx() {
        return amountTrx;
    }

    public void setAmountTrx(BigDecimal amountTrx) {
        this.amountTrx = amountTrx;
    }

    @Column(name = "outstanding_amt")
    public BigDecimal getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(BigDecimal outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }

    @Column(name = "outstanding_count")
    public Long getOutstandingCount() {
        return outstandingCount;
    }

    public void setOutstandingCount(Long outstandingCount) {
        this.outstandingCount = outstandingCount;
    }
}
