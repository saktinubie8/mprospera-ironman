package id.co.telkomsigma.btpns.mprospera.request;

public class PrsPhotoRequest extends BaseRequest {

    private String imei;
    private String username;
    private String sessionKey;
    private String longitude;
    private String latitude;
    private String prsId;
    private String PrsPhoto;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPrsPhoto() {
        return PrsPhoto;
    }

    public void setPrsPhoto(String prsPhoto) {
        PrsPhoto = prsPhoto;
    }

    public String getPrsId() {
        return prsId;
    }

    public void setPrsId(String prsId) {
        this.prsId = prsId;
    }

}
