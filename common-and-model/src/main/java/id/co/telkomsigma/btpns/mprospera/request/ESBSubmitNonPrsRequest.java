package id.co.telkomsigma.btpns.mprospera.request;

import java.io.Serializable;

public class ESBSubmitNonPrsRequest extends BaseRequest implements Serializable {

    private String username;
    private String imei;
    private ESBCustomerNonPrsRequest[] customerList;
    private String tokenChallenge;
    private String tokenResponse;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public ESBCustomerNonPrsRequest[] getCustomerList() {
        return customerList;
    }

    public void setCustomerList(ESBCustomerNonPrsRequest[] customerList) {
        this.customerList = customerList;
    }

    public String getTokenChallenge() {
        return tokenChallenge;
    }

    public void setTokenChallenge(String tokenChallenge) {
        this.tokenChallenge = tokenChallenge;
    }

    public String getTokenResponse() {
        return tokenResponse;
    }

    public void setTokenResponse(String tokenResponse) {
        this.tokenResponse = tokenResponse;
    }

}
