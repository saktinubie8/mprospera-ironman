package id.co.telkomsigma.btpns.mprospera.model.prs;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_DISCOUNT_MARGIN_LOAN_MAP")
public class DiscountMarginLoanMap {

    private Long discountMarginLoanMapId;
    private Long discountMarginId;
    private Long loanId;
    private Long loanPrsId;
    private String status;
    private BigDecimal amount;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getDiscountMarginLoanMapId() {
        return discountMarginLoanMapId;
    }

    public void setDiscountMarginLoanMapId(Long discountMarginLoanMapId) {
        this.discountMarginLoanMapId = discountMarginLoanMapId;
    }

    @Column(name = "discount_margin_id")
    public Long getDiscountMarginId() {
        return discountMarginId;
    }

    public void setDiscountMarginId(Long discountMarginId) {
        this.discountMarginId = discountMarginId;
    }

    @Column(name = "loan_id")
    public Long getLoanId() {
        return loanId;
    }

    public void setLoanId(Long loanId) {
        this.loanId = loanId;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "loan_prs_id")
    public Long getLoanPrsId() {
        return loanPrsId;
    }

    public void setLoanPrsId(Long loanPrsId) {
        this.loanPrsId = loanPrsId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
