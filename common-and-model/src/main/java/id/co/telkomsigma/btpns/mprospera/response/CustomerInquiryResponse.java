package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;
import java.util.Date;

public class CustomerInquiryResponse extends BaseResponse {

    private String appId;
    private String customerId;
    private BigDecimal outstandingMargin;
    private BigDecimal installmentAmount;
    private BigDecimal installmentCount;
    private Date swDate;
    private String statusPembiayaan;
    private BigDecimal remainingPrincipal;
    private BigDecimal danaTalangan;
    private Date maturityDate;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public BigDecimal getOutstandingMargin() {
        return outstandingMargin;
    }

    public void setOutstandingMargin(BigDecimal outstandingMargin) {
        this.outstandingMargin = outstandingMargin;
    }

    public BigDecimal getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(BigDecimal installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public BigDecimal getInstallmentCount() {
        return installmentCount;
    }

    public void setInstallmentCount(BigDecimal installmentCount) {
        this.installmentCount = installmentCount;
    }

    public Date getSwDate() {
        return swDate;
    }

    public void setSwDate(Date swDate) {
        this.swDate = swDate;
    }

    public String getStatusPembiayaan() {
        return statusPembiayaan;
    }

    public void setStatusPembiayaan(String statusPembiayaan) {
        this.statusPembiayaan = statusPembiayaan;
    }

    public BigDecimal getRemainingPrincipal() {
        return remainingPrincipal;
    }

    public void setRemainingPrincipal(BigDecimal remainingPrincipal) {
        this.remainingPrincipal = remainingPrincipal;
    }

    public BigDecimal getDanaTalangan() {
        return danaTalangan;
    }

    public void setDanaTalangan(BigDecimal danaTalangan) {
        this.danaTalangan = danaTalangan;
    }

    public Date getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(Date maturityDate) {
        this.maturityDate = maturityDate;
    }

}
