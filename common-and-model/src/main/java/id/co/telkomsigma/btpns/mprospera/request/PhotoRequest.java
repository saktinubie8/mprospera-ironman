package id.co.telkomsigma.btpns.mprospera.request;

public class PhotoRequest extends BaseRequest {

    private String imei;
    private String username;
    private String sessionKey;
    private String longitude;
    private String latitude;
    private String prsId;
    private String loanPrsId;
    private String disbursementPhoto;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPrsId() {
        return prsId;
    }

    public void setPrsId(String prsId) {
        this.prsId = prsId;
    }

    public String getDisbursementPhoto() {
        return disbursementPhoto;
    }

    public void setDisbursementPhoto(String disbursementPhoto) {
        this.disbursementPhoto = disbursementPhoto;
    }

    public String getLoanPrsId() {
        return loanPrsId;
    }

    public void setLoanPrsId(String loanPrsId) {
        this.loanPrsId = loanPrsId;
    }

    @Override
    public String toString() {
        return "PhotoRequest [imei=" + imei + ", username=" + username + ", sessionKey=" + sessionKey + ", longitude="
                + longitude + ", latitude=" + latitude + ", prsId=" + prsId + ", loanPrsId=" + loanPrsId
                + ", disbursementPhoto=" + disbursementPhoto + ", getTransmissionDateAndTime()="
                + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber()
                + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
                + "]";
    }

}
