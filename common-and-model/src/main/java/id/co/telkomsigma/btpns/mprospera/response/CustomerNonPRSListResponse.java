package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class CustomerNonPRSListResponse {

    private String customerId;
    private String customerName;
    private String customerCif;
    private String swId;
    private String groupId;
    private String groupName;
    private List<LoanNonPRSListResponse> loanList;
    private List<SavingNonPRSListResponse> savingList;

    public List<LoanNonPRSListResponse> getLoanList() {
        return loanList;
    }

    public void setLoanList(List<LoanNonPRSListResponse> loanList) {
        this.loanList = loanList;
    }

    public List<SavingNonPRSListResponse> getSavingList() {
        return savingList;
    }

    public void setSavingList(List<SavingNonPRSListResponse> savingList) {
        this.savingList = savingList;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerCif() {
        return customerCif;
    }

    public void setCustomerCif(String customerCif) {
        this.customerCif = customerCif;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return "CustomerNonPRSListResponse{" +
                "customerId='" + customerId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerCif='" + customerCif + '\'' +
                ", swId='" + swId + '\'' +
                ", groupId='" + groupId + '\'' +
                ", groupName='" + groupName + '\'' +
                ", loanList=" + loanList +
                ", savingList=" + savingList +
                '}';
    }
}
