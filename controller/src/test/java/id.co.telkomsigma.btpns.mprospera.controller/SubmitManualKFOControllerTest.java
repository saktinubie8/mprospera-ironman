package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.controller.webservice.WebServiceSubmitManualKFOController;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;
import id.co.telkomsigma.btpns.mprospera.request.SubmitPRSRequest;
import id.co.telkomsigma.btpns.mprospera.response.SubmitPRSResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class SubmitManualKFOControllerTest {

    @TestConfiguration
    static class SubmitManualKFOControllerTestContextConfiguration {

        @Bean
        public WebServiceSubmitManualKFOController webServiceSubmitManualKFOController() {
            return new WebServiceSubmitManualKFOController();
        }

        @Bean
        public Locale locale() {
            return new Locale("id", "ID");
        }

    }

    @Autowired
    private WebServiceSubmitManualKFOController webServiceSubmitManualKFOController;

    @MockBean
    private WSValidationService wsValidationService;

    @MockBean
    private PRSService prsService;

    @MockBean
    private SubmitPRSService submitPRSService;

    @MockBean
    private TerminalActivityService terminalActivityService;

    @MockBean
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @MockBean
    private TerminalService terminalService;

    @MockBean
    private SentraService sentraService;


    String username = "w0211f";
    String imei = "358525070497940";
    String apkVersion = "3.70";
    String sessionKey = "0beba6ac-2b04-4889-866f-b36dea07aff6";
    String prsId = "3463563";
    String rrn = "51815849045412215005330936140004";
    PRS prs = new PRS();
    SubmitPRSRequest submitPRSRequest = new SubmitPRSRequest();
    SubmitPRSResponse submitPRSResponse = new SubmitPRSResponse();
    List<LoanPRS> loanPRSForUpdate = new ArrayList<>();
    List<SavingPRS> savingPRSForUpdate = new ArrayList<>();
    List<CustomerPRS> customerPRSForUpdate = new ArrayList<>();

    @Before
    public void setUp() {
        submitPRSResponse.setResponseCode("00");
        submitPRSResponse.setRetrievalReferenceNumber(rrn);
        prs.setStatus("DRAFT");
        prs.setPrsId(Long.parseLong(prsId));
        Mockito.when(wsValidationService.wsValidation(username, imei, sessionKey, apkVersion)).thenReturn("00");
        Mockito.when(prsService.getPRSById(Long.parseLong(prsId))).thenReturn(prs);
        Mockito.when(prsService.savePRSEvent(prs, customerPRSForUpdate, loanPRSForUpdate, savingPRSForUpdate)).thenReturn(prs);
    }

    @Test
    public void WhenSubmitManualKFO_ThenReturnSuccess() throws IOException {
        SubmitPRSRequest submitRequest = new SubmitPRSRequest();
        submitRequest.setUsername(username);
        submitRequest.setImei(imei);
        submitRequest.setSessionKey(sessionKey);
        submitRequest.setStatus("MANUAL_KFO");
        submitRequest.setPrsId(prsId);
        submitRequest.setRetrievalReferenceNumber(rrn);
        SubmitPRSResponse result = webServiceSubmitManualKFOController.doSubmitPRSManualKfo(submitRequest, apkVersion);
        assertEquals(result.getResponseCode(), submitPRSResponse.getResponseCode());
        assertEquals(result.getRetrievalReferenceNumber(), submitPRSResponse.getRetrievalReferenceNumber());
    }

}
