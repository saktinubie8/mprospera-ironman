package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.controller.webservice.RESTClient;
import id.co.telkomsigma.btpns.mprospera.controller.webservice.WebServicePRSController;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.request.ListPRSRequest;
import id.co.telkomsigma.btpns.mprospera.response.ListPRSResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class ListStatusPRSControllerTest {

    @TestConfiguration
    static class ListStatusPRSControllerTestContextConfiguration {
        @Bean
        public WebServicePRSController webServicePRSController() {
            return new WebServicePRSController();
        }

        @Bean
        public Locale locale() {
            return new Locale("id", "ID");
        }
    }

    @Autowired
    private WebServicePRSController webServicePRSController;

    @MockBean
    private WSValidationService wsValidationService;

    @MockBean
    private PRSService prsService;

    @MockBean
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @MockBean
    private TerminalActivityService terminalActivityService;

    @MockBean
    private TerminalService terminalService;

    @MockBean
    private SentraService sentraService;

    @MockBean
    private UserService userService;

    @MockBean
    private AreaService areaService;

    @MockBean
    private ParameterService parameterService;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private LoanService loanService;

    @MockBean
    private SavingService savingService;

    @MockBean
    private SWService swService;

    @MockBean
    private RESTClient restClient;

    @MockBean
    private ProsperaEncryptionService prosperaEncryptionService;

    @MockBean
    private AuditLogService auditLogService;


    String username = "w0211f";
    String imei = "358525070497940";
    String apkVersion = "3.70";
    String sessionKey = "0beba6ac-2b04-4889-866f-b36dea07aff6";
    List<PRS> prsList = new ArrayList<>();
    ListPRSResponse listPRSResponse = new ListPRSResponse();

    @Before
    public void setUp() {
        listPRSResponse.setResponseCode("00");
        Mockito.when(wsValidationService.wsValidation(username, imei, sessionKey, apkVersion)).thenReturn("00");
        Mockito.when(prsService.getPRSByPSAndStatus(username, new Date())).thenReturn(prsList);

    }

    @Test
    public void WhenGetListStatusPRS_ThenShouldBeFound() throws IOException {
        ListPRSRequest listPRSRequest = new ListPRSRequest();
        listPRSRequest.setPrsDate("20180530");
        listPRSRequest.setImei(imei);
        listPRSRequest.setSessionKey(sessionKey);
        listPRSRequest.setUsername(username);
        ListPRSResponse result = webServicePRSController.list(listPRSRequest, apkVersion);
        assertEquals(result.getResponseCode(), listPRSResponse.getResponseCode());
    }

}
