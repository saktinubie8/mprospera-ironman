package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.prs.MtsTrx;
import id.co.telkomsigma.btpns.mprospera.model.prs.NonPRSPhoto;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.ReportTrxTable;
import id.co.telkomsigma.btpns.mprospera.model.saving.Saving;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.*;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller("webServiceInqueryController")
public class WebServiceInqueryController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private ParameterService parameterService;

    @Autowired
    private UserService userService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private SavingService savingService;

    @Autowired
    private PRSService prsService;

    @Autowired
    private MtsTrxService mtsTrxService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private SWService swService;

    @Autowired
    private RESTClient restClient;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_INQUERY_LOAN_HIST_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    LoanHistResponse inqueryLoan(@RequestBody final LoanHistRequest request,
                                 @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final LoanHistResponse response = new LoanHistResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("loanHistory Try to processing inquery loan history request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Get Loan History");
                Loan loan = loanService.findById(request.getLoanId());
                if (loan == null) {
                    response.setResponseCode(WebGuiConstant.NO_LOAN_HIST);
                    log.error("NO LOAN HISTORY");
                    String labelNoLoan = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelNoLoan);
                    log.info("RESPONSE MESSAGE : " + response);
                    return response;
                }
                ESBTrxInquiryRequest reqInqToEsb = new ESBTrxInquiryRequest();
                reqInqToEsb.setTransmissionDateAndTime(request.getTransmissionDateAndTime());
                reqInqToEsb.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
                reqInqToEsb.setImei(request.getImei());
                reqInqToEsb.setUsername(request.getUsername());
                reqInqToEsb.setAccountId(loan.getProsperaId());
                reqInqToEsb.setAccountType("1");
                log.info("SEND REQUEST TO ESB");
                ESBTrxInquiryResponse historyTrx = restClient.trxInquiry(reqInqToEsb);
                if (historyTrx.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                    log.info("GET SUCCESS RESPONSE FROM ESB");
                    List<HistoryList> historyList = new ArrayList<>();
                    for (TrxHistory loanHist : historyTrx.getTrxHistories()) {
                        HistoryList loanHistoryList = new HistoryList();
                        loanHistoryList.setUpdatedDate(loanHist.getTanggal().replace("-", ""));
                        loanHistoryList.setDescription(loanHist.getNarasi());
                        loanHistoryList.setAmount(new BigDecimal(loanHist.getAmount()));
                        historyList.add(loanHistoryList);
                    }
                    response.setInstallmentAmount(new BigDecimal(historyTrx.getNominalAngsuran()));
                    response.setOutstandingAmount(new BigDecimal(historyTrx.getSisaPokok()));
                    response.setOutstandingMargin(new BigDecimal(historyTrx.getSisaMarjin()));
                    response.setTotalDanaTalangan(new BigDecimal(historyTrx.getTotalDanaTalangan()));
                    if (loan.getDisbursementDate() != null) {
                        response.setDisbursementDate(formatter.format(loan.getDisbursementDate()));
                    }
                    response.setPlafond(loan.getPlafond());
                    response.setStatus(historyTrx.getStatus());
                    response.setHistoryList(historyList);
                    response.setInstallmentCount(new BigDecimal(historyTrx.getLastPaymentDate()));
                    LoanPRS loanPrs = loanService.getLoanPRSTop(loan);
                    if (loanPrs != null) {
                        response.setAppId(loanPrs.getAppId());
                        response.setTenor(new BigDecimal(loanPrs.getTenor()));
                        if (loanPrs.getDueDate() != null) {
                            response.setMaturityDate(formatter.format(loanPrs.getDueDate()));
                        }
                        if (loanPrs.getLoanId().getSwId() != null) {
                            SurveyWawancara sw = swService.getSWById(loanPrs.getLoanId().getSwId().toString());
                            if (sw.getSurveyDate() != null) {
                                response.setSwDate(formatter.format(sw.getSurveyDate()));
                            }
                        }
                    }
                } else {
                    log.error("NO LOAN HISTORY");
                    response.setResponseCode(WebGuiConstant.NO_LOAN_HIST);
                    String labelNoLoan = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelNoLoan);
                    log.info("RESPONSE MESSAGE : " + response);
                    return response;
                }
            }
        } catch (Exception e) {
            log.error("loanHistory error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("loanHistory error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_INQ_LOANHIST);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("loanHistory RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("loanHistory saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_INQUERY_SAVING_HIST_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SavingHistResponse inquerySaving(@RequestBody final SavingHistRequest request,
                                     @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final SavingHistResponse response = new SavingHistResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("savingHistory Try to processing inquery saving history request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Get Saving History");
                Saving saving = savingService.findByAccountNumber(request.getAccountId());
                if (saving == null) {
                    log.error("NO SAVING HISTORY");
                    response.setResponseCode(WebGuiConstant.NO_SAVING_HIST);
                    String labelNoLoan = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelNoLoan);
                    log.info("RESPONSE MESSAGE : " + response);
                    return response;
                }
                ESBTrxInquiryRequest reqInqToEsb = new ESBTrxInquiryRequest();
                reqInqToEsb.setTransmissionDateAndTime(request.getTransmissionDateAndTime());
                reqInqToEsb.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
                reqInqToEsb.setUsername(request.getUsername());
                reqInqToEsb.setImei(request.getImei());
                reqInqToEsb.setAccountId(saving.getProsperaId().toString());
                reqInqToEsb.setAccountType("2");
                log.info("SEND REQUEST TO ESB");
                ESBTrxInquiryResponse historyTrx = restClient.trxInquiry(reqInqToEsb);
                if (historyTrx.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                    log.info("GET SUCCESS RESPONSE FROM ESB");
                    List<HistoryList> historyList = new ArrayList<>();
                    for (TrxHistory savingHist : historyTrx.getTrxHistories()) {
                        HistoryList savingHistoryList = new HistoryList();
                        savingHistoryList.setUpdatedDate(savingHist.getTanggal().replace("-", ""));
                        savingHistoryList.setDescription(savingHist.getNarasi());
                        savingHistoryList.setAmount(new BigDecimal(savingHist.getAmount()));
                        historyList.add(savingHistoryList);
                    }
                    response.setHistoryList(historyList);
                } else {
                    log.error("NO SAVING HISTORY");
                    response.setResponseCode(WebGuiConstant.NO_SAVING_HIST);
                    String labelNoLoan = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelNoLoan);
                    log.info("RESPONSE MESSAGE : " + response);
                    return response;
                }
            }
        } catch (Exception e) {
            log.error("savingHistory error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("savingHistory error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_INQ_SAVINGHIST);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("savingHistory RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("savingHistory saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_INQUERY_NON_PRS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncNonPRSResponse syncNonPrs(@RequestBody final SyncNonPRSRequest request,
                                  @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final SyncNonPRSResponse response = new SyncNonPRSResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("inqNonPrs Try to processing Inquiry Collection request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                String paramApkVersion = parameterService.loadParamByParamName(WebGuiConstant.PARAMETER_CURRENT_VERSION, "1.0");
                log.info("Starting Get User Data");
                User user = (User) userService.loadUserByUsername(request.getUsername());
                log.info("Finishing get user data");
                log.info("Starting Get Sentra for Non PRS");
                List<Sentra> sentraForNonPRS = sentraService.getSentraByWismaId(user.getOfficeCode());
                log.info("Finishing Get Sentra for Non PRS");
                List<NonPRSResponseList> nonPrsListTemp = new ArrayList<>();
                log.info("START MAPPING DATA");
                for (Sentra sentra : sentraForNonPRS) {
                    NonPRSResponseList nonPrsSet = new NonPRSResponseList();
                    nonPrsSet.setSentraId(sentra.getSentraId().toString());
                    Location mms = areaService.findLocationById(sentra.getLocationId());
                    nonPrsSet.setMmsCode(mms.getLocationCode());
                    nonPrsSet.setMmsName(mms.getName());
                    List<Customer> customerList = customerService.getBySentraId(sentra);
                    List<CustomerNonPRSListResponse> customerForNonPrsListTemp = new ArrayList<>();
                    for (Customer customer : customerList) {
                        if (loanService.isNonPRSCustomer(customer) && savingService.isNonPrsCustomerSaving(customer)) {
                            CustomerNonPRSListResponse customerNonPrs = new CustomerNonPRSListResponse();
                            if (customer.getCustomerId() != null) {
                                customerNonPrs.setCustomerId(customer.getCustomerId().toString());
                            } else {
                                customerNonPrs.setCustomerId("");
                            }
                            customerNonPrs.setCustomerName(customer.getCustomerName());
                            List<LoanNonPRS> loanNonPrsList = loanService.getLoanNonPRSByCustomer(customer);
                            List<LoanNonPRSListResponse> loanNonPrsListTemp = new ArrayList<>();
                            for (LoanNonPRS loanNonPrs : loanNonPrsList) {
                                LoanNonPRSListResponse loanNonPrsSet = new LoanNonPRSListResponse();
                                loanNonPrsSet.setAppId(loanNonPrs.getLoanId().getAppId());
                                if (loanNonPrs.getCurrentMargin() != null) {
                                    loanNonPrsSet.setCurrentMargin(loanNonPrs.getCurrentMargin());
                                } else {
                                    loanNonPrsSet.setCurrentMargin(loanNonPrs.getCurrentMargin().ZERO);
                                }
                                if (loanNonPrs.getInstallmentAmount() != null) {
                                    loanNonPrsSet.setInstallmentAmount(loanNonPrs.getInstallmentAmount());
                                } else {
                                    loanNonPrsSet.setInstallmentAmount(loanNonPrs.getInstallmentAmount().ZERO);
                                }
                                if (loanNonPrs.getInstallmentPaymentAmount() != null) {
                                    loanNonPrsSet.setInstallmentPaymentAmount(loanNonPrs.getInstallmentPaymentAmount());
                                } else {
                                    loanNonPrsSet
                                            .setInstallmentPaymentAmount(loanNonPrs.getInstallmentPaymentAmount().ZERO);
                                }
                                if (loanNonPrs.getInputMoney() != null) {
                                    loanNonPrsSet.setInputMoney(loanNonPrs.getInputMoney());
                                } else {
                                    loanNonPrsSet.setInputMoney(loanNonPrs.getInputMoney().ZERO);
                                }
                                if (loanNonPrs.getDepositMoney() != null) {
                                    loanNonPrsSet.setDepositMoney(loanNonPrs.getDepositMoney());
                                } else {
                                    loanNonPrsSet.setDepositMoney(loanNonPrs.getDepositMoney().ZERO);
                                }
                                if (loanNonPrs.getAutoDebet() != null) {
                                    loanNonPrsSet.setAutoDebet(loanNonPrs.getAutoDebet());
                                } else {
                                    loanNonPrsSet.setAutoDebet(loanNonPrs.getAutoDebet().ZERO);
                                }
                                if (loanNonPrs.getRemainingPrincipal() != null) {
                                    loanNonPrsSet.setRemainingPrincipal(loanNonPrs.getRemainingPrincipal());
                                } else {
                                    loanNonPrsSet.setRemainingPrincipal(loanNonPrs.getRemainingPrincipal().ZERO);
                                }
                                if (loanNonPrs.getMarginAmount() != null) {
                                    loanNonPrsSet.setMarginAmount(loanNonPrs.getMarginAmount());
                                } else {
                                    loanNonPrsSet.setMarginAmount(loanNonPrs.getMarginAmount().ZERO);
                                }
                                loanNonPrsSet.setLoanId(loanNonPrs.getLoanId().getLoanId().toString());
                                loanNonPrsSet.setLoanNonPrsId(loanNonPrs.getLoanNonPRSId().toString());
                                if (loanNonPrs.getMeetingDate() != null) {
                                    loanNonPrsSet.setMeetingDate(formatter.format(loanNonPrs.getMeetingDate()));
                                } else {
                                    loanNonPrsSet.setMeetingDate("");
                                }
                                if (loanNonPrs.getDueDate() != null) {
                                    loanNonPrsSet.setDueDate(formatter.format(loanNonPrs.getDueDate()));
                                } else {
                                    loanNonPrsSet.setDueDate("");
                                }
                                loanNonPrsSet.setCreatedBy(loanNonPrs.getCreatedBy());
                                if (loanNonPrs.getUpdatedDate() != null)
                                    loanNonPrsSet.setCreatedDate(formatter.format(loanNonPrs.getUpdatedDate()));
                                NonPRSPhoto photo = loanService.getNonPRSPhoto(loanNonPrs.getLoanNonPRSId());
                                if (photo != null)
                                    loanNonPrsSet.setNonPrsPhoto(
                                            WebGuiConstant.TERMINAL_NON_PRS_PHOTO_REQUEST.replace("**/{apkVersion:.+}",
                                                    "/" + paramApkVersion) + "/" + photo.getNonPrsPhotoId().toString());
                                loanNonPrsSet.setOutstandingAmount(loanNonPrs.getOutstandingAmount());
                                if (loanNonPrs.getOutstandingCount() != null)
                                    loanNonPrsSet.setOutstandingCount(loanNonPrs.getOutstandingCount().toString());
                                if (loanNonPrs.getOutstandingReason() != null) {
                                    loanNonPrsSet.setOutstandingReason(
                                            loanNonPrs.getOutstandingReason().getParameterId().toString());
                                } else {
                                    loanNonPrsSet.setOutstandingReason("");
                                }
                                Date curdate = new Date();
                                String dueDate = formatter.format(loanNonPrs.getDueDate());
                                try {
                                    Long diff = curdate.getTime() - formatter.parse(dueDate).getTime();
                                    Long diffDays = diff / (24 * 60 * 60 * 1000);
                                    loanNonPrsSet.setOverdueDays(diffDays.toString());
                                } catch (ParseException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                loanNonPrsSet.setPayCommitment(loanNonPrs.getPayCommitment());
                                if (loanNonPrs.getPeopleMeet() != null) {
                                    loanNonPrsSet.setPeopleMeet(loanNonPrs.getPeopleMeet().getParameterId().toString());
                                } else {
                                    loanNonPrsSet.setPeopleMeet("");
                                }
                                loanNonPrsSet.setStatus(loanNonPrs.getStatus());
                                loanNonPrsSet.setCreatedBy(loanNonPrs.getCreatedBy());
                                if (loanNonPrs.getUpdatedDate() != null)
                                    loanNonPrsSet.setCreatedDate(formatter.format(loanNonPrs.getUpdatedDate()));
                                loanNonPrsListTemp.add(loanNonPrsSet);
                            }
                            if (!loanNonPrsListTemp.isEmpty()) {
                                customerNonPrs.setLoanList(loanNonPrsListTemp);
                                List<SavingNonPRS> savingNonPrsList = savingService.getSavingNonPrsByCustomer(customer);
                                List<SavingNonPRSListResponse> savingNonPrsListTemp = new ArrayList<>();
                                for (SavingNonPRS savingNonPrs : savingNonPrsList) {
                                    SavingNonPRSListResponse savingNonPrsSet = new SavingNonPRSListResponse();
                                    savingNonPrsSet.setAccountNumber(savingNonPrs.getAccountNumber());
                                    savingNonPrsSet.setClearBalance(savingNonPrs.getClearBalance());
                                    savingNonPrsSet.setDepositAmount(savingNonPrs.getDepositAmount());
                                    savingNonPrsSet.setSavingNonPrsId(savingNonPrs.getSavingNonPrsId().toString());
                                    savingNonPrsListTemp.add(savingNonPrsSet);
                                }
                                if (!savingNonPrsListTemp.isEmpty())
                                    customerNonPrs.setSavingList(savingNonPrsListTemp);
                                customerForNonPrsListTemp.add(customerNonPrs);
                            }
                        }
                    }
                    if (!customerForNonPrsListTemp.isEmpty())
                        nonPrsSet.setCustomerList(customerForNonPrsListTemp);
                    nonPrsListTemp.add(nonPrsSet);
                }
                response.setSentraList(nonPrsListTemp);
                log.info("FINISHING MAPPING DATA");
            }
        } catch (UsernameNotFoundException e) {
            log.error("inqNonPrs error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("inqNonPrs error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_INQ_NONPRS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("inqNonPrs RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("inqNonPrs saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_INQUERY_PRS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncPRSResponse syncPrs(@RequestBody final SyncPRSRequest request,
                            @PathVariable("apkVersion") String apkVersion) throws IOException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        final DecimalFormat decimalFormat = new DecimalFormat("###");
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final SyncPRSResponse response = new SyncPRSResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("inqPrs Try to processing inquiry PRS request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                User user = (User) userService.loadUserByUsername(request.getUsername());
                log.info("Finishing get user data");
                log.info("Starting Get Sentra for PRS");
                List<Sentra> sentraForPRS = sentraService.getSentraByWismaId(user.getOfficeCode());
                log.info("Get PRS");
                boolean disbursementPlan = false;
                log.info("" + request.getSentraList());
                List<PRSResponseList> prsListTemp = new ArrayList<>();
                log.info("START MAPPING DATA");
                for (Sentra sentraList : sentraForPRS) {
                    Sentra sentra = sentraService.findBySentraId(sentraList.getSentraId().toString());
                    PRS prs = prsService.getPRSBySentraPrevious(sentra, new Date());
                    PRSResponseList prsSet = new PRSResponseList();
                    if (prs != null) {
                        if (prs.getStatus().equals("APPROVED")) {
                            prsSet.setPrsId(prs.getPrsId().toString());
                            prsSet.setSentraId(prs.getSentraId().getSentraId().toString());
                            if (prs.getUpdateDate() != null)
                                prsSet.setUpdatedDate(formatter.format(prs.getUpdateDate()));
                            if (prs.getPrsDate() != null)
                                prsSet.setPrsDate(formatter.format(prs.getPrsDate()));
                            if (prs.getBringMoney() != null) {
                                prsSet.setBringMoney(prs.getBringMoney());
                            } else {
                                prsSet.setBringMoney(prs.getBringMoney().ZERO);
                            }
                            if (prs.getActualMoney() != null) {
                                prsSet.setActualMoney(prs.getActualMoney());
                            } else {
                                prsSet.setActualMoney(prs.getActualMoney().ZERO);
                            }
                            if (prs.getPaymentMoney() != null) {
                                prsSet.setPaymentMoney(prs.getPaymentMoney());
                            } else {
                                prsSet.setPaymentMoney(prs.getPaymentMoney().ZERO);
                            }
                            if (prs.getWithdrawalAdhoc() != null) {
                                prsSet.setWithdrawalAdhoc(prs.getWithdrawalAdhoc());
                            } else {
                                prsSet.setWithdrawalAdhoc(prs.getWithdrawalAdhoc().ZERO);
                            }
                            prsSet.setStatus(prs.getStatus());
                            List<CustomerPRSListResponse> customerPRSListTemp = new ArrayList<>();
                            List<CustomerPRS> customerPRSList = customerService.getCustomerPRSByPrsId(prs);
                            if (customerPRSList.isEmpty())
                                prsSet.setCustomerList(null);
                            for (CustomerPRS customerPRS : customerPRSList) {
                                CustomerPRSListResponse customerPRSSet = new CustomerPRSListResponse();
                                customerPRSSet.setCustomerId(customerPRS.getCustomerId().getCustomerId().toString());
                                customerPRSSet.setCustomerPrsId(customerPRS.getCustomerPrsId().toString());
                                if (customerPRS.getGroupId() != null)
                                    customerPRSSet.setGroupId(customerPRS.getGroupId().getGroupId().toString());
                                List<LoanPRSListResponse> loanPRSListTemp = new ArrayList<>();
                                List<LoanPRS> loanPRSList = loanService.getLoanPRSByPrsId(prs, customerPRS);
                                if (loanPRSList.isEmpty())
                                    customerPRSSet.setLoanList(null);
                                for (LoanPRS loanPRS : loanPRSList) {
                                    LoanPRSListResponse loanPRSSet = new LoanPRSListResponse();
                                    loanPRSSet.setLoanPrsId(loanPRS.getLoanPRSId().toString());
                                    loanPRSSet.setLoanId(loanPRS.getLoanId().getLoanId().toString());
                                    loanPRSSet.setAppId(loanPRS.getAppId());
                                    if (loanPRS.getDisbursementAmount() != null) {
                                        if (!decimalFormat.format(loanPRS.getDisbursementAmount()).equals("0")) {
                                            loanPRSSet.setDisbursementAmount(loanPRS.getDisbursementAmount());
                                            disbursementPlan = true;
                                        } else {
                                            loanPRSSet.setDisbursementAmount(loanPRS.getDisbursementAmount().ZERO);
                                            disbursementPlan = false;
                                        }
                                    } else {
                                        loanPRSSet.setDisbursementAmount(loanPRS.getDisbursementAmount().ZERO);
                                    }
                                    loanPRSSet.setDisbursementFlag("" + loanPRS.getDisbursementFlag());
                                    if (loanPRS.getCanPendingDisbursement() != null) {
                                        loanPRSSet.setCanPendingDisbursement(loanPRS.getCanPendingDisbursement());
                                    }
                                    if (loanPRS.getInstallmentAmount() != null) {
                                        if (disbursementPlan) {
                                            loanPRSSet.setInstallmentAmount(loanPRS.getInstallmentAmount());
                                        } else {
                                            loanPRSSet.setInstallmentAmount(loanPRS.getInstallmentAmount());
                                        }
                                    } else {
                                        loanPRSSet.setInstallmentAmount(loanPRS.getInstallmentAmount().ZERO);
                                    }
                                    if (loanPRS.getCurrentMargin() != null) {
                                        if (disbursementPlan) {
                                            loanPRSSet.setCurrentMargin(loanPRS.getCurrentMargin());
                                        } else {
                                            loanPRSSet.setCurrentMargin(loanPRS.getCurrentMargin());
                                        }
                                    } else {
                                        loanPRSSet.setCurrentMargin(loanPRS.getCurrentMargin().ZERO);
                                    }
                                    if (loanPRS.getInstallmentPaymentAmount() != null) {
                                        loanPRSSet.setInstallmentPaymentAmount(loanPRS.getInstallmentPaymentAmount());
                                    } else {
                                        loanPRSSet.setInstallmentPaymentAmount(loanPRS.getInstallmentPaymentAmount().ZERO);
                                    }
                                    if (loanPRS.getUseEmergencyFund() != null) {
                                        loanPRSSet.setUseEmergencyFund(loanPRS.getUseEmergencyFund());
                                    }
                                    if (loanPRS.getMarginAmount() != null) {
                                        loanPRSSet.setMarginAmount(loanPRS.getMarginAmount());
                                    } else {
                                        loanPRSSet.setMarginAmount(loanPRS.getMarginAmount().ZERO);
                                    }
                                    if (loanPRS.getPlafonAmount() != null) {
                                        loanPRSSet.setPlafonAmount(loanPRS.getPlafonAmount());
                                    } else {
                                        loanPRSSet.setPlafonAmount(loanPRS.getPlafonAmount().ZERO);
                                    }
                                    if (loanPRS.getOutstandingAmount() != null) {
                                        if (disbursementPlan) {
                                            loanPRSSet.setOutstandingAmount(loanPRS.getOutstandingAmount().ZERO);
                                        } else {
                                            loanPRSSet.setOutstandingAmount(loanPRS.getOutstandingAmount());
                                        }
                                    } else {
                                        loanPRSSet.setOutstandingAmount(loanPRS.getOutstandingAmount().ZERO);
                                    }
                                    if (loanPRS.getOutstandingCount() != null) {
                                        loanPRSSet.setOutstandingCount(loanPRS.getOutstandingCount().toString());
                                    } else {
                                        loanPRSSet.setOutstandingCount("0");
                                    }
                                    if (loanPRS.getRemainingPrincipal() != null) {
                                        loanPRSSet.setRemainingPrincipal(loanPRS.getRemainingPrincipal());
                                    } else {
                                        loanPRSSet.setRemainingPrincipal(loanPRS.getRemainingPrincipal().ZERO);
                                    }
                                    if (loanPRS.getDueDate() != null)
                                        loanPRSSet.setDueDate(formatter.format(loanPRS.getDueDate()));
                                    if (loanPRS.getOverdueDays() != null) {
                                        if (!disbursementPlan)
                                            loanPRSSet.setOverdueDays(loanPRS.getOverdueDays().toString());
                                    }
                                    if (loanPRS.getIsEarlyTermination() != null) {
                                        if (loanPRS.getIsEarlyTermination() == true) {
                                            loanPRSSet.setIsEarlyTermination("true");
                                        } else {
                                            loanPRSSet.setIsEarlyTermination("false");
                                        }
                                    }
                                    if (loanPRS.getEarlyTerminationReason() != null)
                                        loanPRSSet.setEarlyTerminationReason(loanPRS.getEarlyTerminationReason());

                                    if (loanPRS.getMarginDiscountDeviationFlag() != null) {
                                        if (loanPRS.getMarginDiscountDeviationFlag() == true) {
                                            loanPRSSet.setMarginDiscountDeviationFlag("true");
                                        } else {
                                            loanPRSSet.setMarginDiscountDeviationFlag("false");
                                        }
                                    }
                                    if (loanPRS.getMarginDiscountDeviationAmount() != null) {
                                        loanPRSSet.setMarginDiscountDeviationAmount(
                                                loanPRS.getMarginDiscountDeviationAmount());
                                    } else {
                                        loanPRSSet.setMarginDiscountDeviationAmount(
                                                loanPRS.getMarginDiscountDeviationAmount().ZERO);
                                    }
                                    if (loanPRS.getWowIbStatus() != null)
                                        loanPRSSet.setWowIbStatus(loanPRS.getWowIbStatus());

                                    loanPRSSet.setHasDisbursementPlan(disbursementPlan);
                                    if (!disbursementPlan && loanPRSSet.getDueDate() == null) {
                                        loanPRSSet.setInstallmentAmount(loanPRS.getInstallmentAmount().ZERO);
                                        loanPRSSet.setCurrentMargin(loanPRS.getCurrentMargin().ZERO);
                                    }
                                    loanPRSListTemp.add(loanPRSSet);
                                }
                                customerPRSSet.setLoanList(loanPRSListTemp);
                                List<SavingPRSListResponse> savingPRSListTemp = new ArrayList<>();
                                List<SavingPRS> savingPRSList = savingService.getSavingByPRSIdAndCustomerId(prs,
                                        customerPRS);
                                if (savingPRSList.isEmpty())
                                    customerPRSSet.setSavingList(null);
                                for (SavingPRS savingPrs : savingPRSList) {
                                    SavingPRSListResponse savingPRSSet = new SavingPRSListResponse();
                                    savingPRSSet.setAccountId(savingPrs.getAccountId().toString());
                                    savingPRSSet.setAccountNumber(savingPrs.getAccountNumber());
                                    if (savingPrs.getWithdrawalAmount() != null) {
                                        savingPRSSet.setWithdrawalAmount(savingPrs.getWithdrawalAmount());
                                    } else {
                                        savingPRSSet.setWithdrawalAmount(savingPrs.getWithdrawalAmount().ZERO);
                                    }
                                    if (savingPrs.getWithdrawalAmountPlan() != null)
                                        savingPRSSet
                                                .setWithdrawalAmountPlan(savingPrs.getWithdrawalAmountPlan().toString());
                                    if (savingPrs.getNextWithdrawalAmountPlan() != null)
                                        savingPRSSet.setNextWithdrawalAmountPlan(
                                                savingPrs.getNextWithdrawalAmountPlan().toString());
                                    if (savingPrs.getIsCloseSavingAccount() != null) {
                                        if (savingPrs.getIsCloseSavingAccount() == true) {
                                            savingPRSSet.setIsCloseSavingAccount("true");
                                        } else {
                                            savingPRSSet.setIsCloseSavingAccount("false");
                                        }
                                    }
                                    if (savingPrs.getHoldBalance() != null) {
                                        savingPRSSet.setHoldBalance(savingPrs.getHoldBalance());
                                    } else {
                                        savingPRSSet.setHoldBalance(savingPrs.getHoldBalance().ZERO);
                                    }
                                    if (savingPrs.getClearBalance() != null) {
                                        savingPRSSet.setClearBalance(savingPrs.getClearBalance());
                                    } else {
                                        savingPRSSet.setClearBalance(savingPrs.getClearBalance().ZERO);
                                    }
                                    if (savingPrs.getDepositAmount() != null) {
                                        savingPRSSet.setDepositAmount(savingPrs.getDepositAmount());
                                    } else {
                                        savingPRSSet.setDepositAmount(savingPrs.getDepositAmount().ZERO);
                                    }
                                    savingPRSListTemp.add(savingPRSSet);
                                }
                                customerPRSSet.setSavingList(savingPRSListTemp);
                                customerPRSListTemp.add(customerPRSSet);
                            }
                            prsSet.setCustomerList(customerPRSListTemp);
                            prsListTemp.add(prsSet);
                        }
                    }
                }
                response.setPrsList(prsListTemp);
                log.info("FINISHING MAPPING DATA");
            }
        } catch (Exception e) {
            log.error("inqPrs error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("inqPrs error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_PRS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("inqPrs RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("inqPrs saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_INQUERY_SAVING_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SavingResponse inqueryStatusSaving(@RequestBody final SavingRequest request,
                                       @PathVariable("apkVersion") String apkVersion) throws IOException {


        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final SavingResponse response = new SavingResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("inqSaving Try to processing inquiry saving request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Get Saving");
                Customer customer = customerService.findById(request.getCustomerId());
                if (customer == null) {
                    response.setResponseCode(WebGuiConstant.UNKNOWN_CUSTOMER_ID);
                    String labelNoLoan = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelNoLoan);
                    return response;
                }
                List<Saving> savingList = savingService.findSavingByCustomerId(customer);
                response.setCustomerId(customer.getCustomerId().toString());
                response.setCustomerName(customer.getCustomerName());
                List<SavingListResponse> savingListResponse = new ArrayList<>();
                for (Saving saving : savingList) {
                    SavingListResponse savingMap = new SavingListResponse();
                    savingMap.setAccountNumber(saving.getAccountNumber());
                    savingMap.setStatus(saving.getStatus());
                    savingListResponse.add(savingMap);
                }
                response.setSavingList(savingListResponse);
                log.info("FINISHING MAPPING DATA");
            }
        } catch (Exception e) {
            log.error("inqSaving error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("inqSaving error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_INQ_SAVING);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("inqSaving RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("inqSaving saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_RPT_MUTATION_PS_MS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncMutationPsMsResponse syncMutationPsMs(@RequestBody final SyncMutationPsMsRequest request,
                                              @PathVariable("apkVersion") String apkVersion) throws ParseException, IOException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        SimpleDateFormat formatterCutOffTime = new SimpleDateFormat("HH:mm");
        final SyncMutationPsMsResponse response = new SyncMutationPsMsResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        response.setCutOffTime(parameterService.loadParamByParamName(WebGuiConstant.PARAMETER_CUT_OFF_TIME, "15:00"));
        try {
            log.info("psmsTrxMutation Try to processing sync Mutation PS MS request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Starting Get User Data");
                User user = (User) userService.loadUserByUsername(request.getUsername());
                log.info("Finishing get user data");
                log.info("Starting Get users name of same Wisma/Location: " + user.getOfficeCode());
                List<User> userOfWisma = userService.loadUserByLocationId(user.getOfficeCode());
                List<String> listUserName = new ArrayList<>();
                if (userOfWisma != null) {
                    for (User listUserTmp : userOfWisma) {
                        User userTmp = listUserTmp;
                        if (userTmp != null && userTmp.getRoleUser() != null) {
                            listUserName.add(userTmp.getUsername());
                        }
                    }
                }
                log.info("Finishing Get same Wisma/Location Users name");
                //	moved from below
                String date = formatter.format(new Date());
                Date curdate = formatter.parse(date);
                //
                log.info("Starting Get MtsTrxList for PS MS");
                List<PsMsMtsTrxList> mtsTrxListTemp = new ArrayList<>();
                List<MtsTrx> listMtsTrxByMS = mtsTrxService.getMtsTrx(curdate, listUserName);
                BigDecimal amount1 = BigDecimal.ZERO;
                BigDecimal amount2 = BigDecimal.ZERO;
                BigDecimal amount3 = BigDecimal.ZERO;
                BigDecimal amount4 = BigDecimal.ZERO;
                for (MtsTrx rptTrxByMS : listMtsTrxByMS) {
                    PsMsMtsTrxList trxSet = new PsMsMtsTrxList();
                    trxSet.setRptId(rptTrxByMS.getTrx_id().toString());
                    trxSet.setPsMsTrxType(rptTrxByMS.getTypeTrx());
                    trxSet.setTrxTime(formatterCutOffTime.format(rptTrxByMS.getApprovedDate()));
                    if (rptTrxByMS.getCreatedBy() != null) {
                        trxSet.setCreatedBy(rptTrxByMS.getCreatedBy());
                    }
                    trxSet.setUpdatedBy(rptTrxByMS.getApprovedBy());
                    if (rptTrxByMS.getTypeTrx().equals("PRS-Keluar")) {
                        if (rptTrxByMS.getAmountBringPRS() != null) {
                            trxSet.setAMount(rptTrxByMS.getAmountBringPRS());
                            if (getZeroTimeDate(rptTrxByMS.getBusinessDate()).equals(getZeroTimeDate(rptTrxByMS.getApprovedDate()))) {
                                amount2 = amount2.add(rptTrxByMS.getAmountBringPRS());
                            } else {
                                amount4 = amount4.add(rptTrxByMS.getAmountBringPRS());
                            }
                        } else {
                            trxSet.setAMount(rptTrxByMS.getAmountBringPRS().ZERO);
                        }
                    } else if (rptTrxByMS.getTypeTrx().equals("PRS-Masuk")) {
                        if (rptTrxByMS.getAmountTotalPRS() != null) {
                            trxSet.setAMount(rptTrxByMS.getAmountTotalPRS());
                            if (getZeroTimeDate(rptTrxByMS.getBusinessDate()).equals(getZeroTimeDate(rptTrxByMS.getApprovedDate()))) {
                                amount1 = amount1.add(rptTrxByMS.getAmountTotalPRS());
                            } else {
                                amount3 = amount3.add(rptTrxByMS.getAmountTotalPRS());
                            }
                        } else {
                            trxSet.setAMount(rptTrxByMS.getAmountTotalPRS().ZERO);
                        }
                    } else if (rptTrxByMS.getTypeTrx().equals("Penagihan")) {
                        if (rptTrxByMS.getAmountTotalCollect() != null) {
                            trxSet.setAMount(rptTrxByMS.getAmountTotalCollect());
                            if (getZeroTimeDate(rptTrxByMS.getBusinessDate()).equals(getZeroTimeDate(rptTrxByMS.getApprovedDate()))) {
                                amount1 = amount1.add(rptTrxByMS.getAmountTotalCollect());
                            } else {
                                amount3 = amount3.add(rptTrxByMS.getAmountTotalCollect());
                            }
                        } else {
                            trxSet.setAMount(rptTrxByMS.getAmountTotalCollect().ZERO);
                        }
                    }
                    mtsTrxListTemp.add(trxSet);
                }
                if (mtsTrxListTemp.isEmpty()) {
                    response.setTrxList(new ArrayList<>(0));
                } else {
                    response.setTrxList(mtsTrxListTemp);
                }
                log.info("Finishing Get MtsTrxList for PS MS");
                log.info("port summary-amount to response");
                if (amount1 != null) {
                    response.setInAmountBeforeCOT(amount1);
                } else {
                    response.setInAmountBeforeCOT(amount1.ZERO);
                }
                if (amount2 != null) {
                    response.setOutAmountBeforeCOT(amount2);
                } else {
                    response.setOutAmountBeforeCOT(amount2.ZERO);
                }
                if (amount3 != null) {
                    response.setInAmountAfterCOT(amount3);
                } else {
                    response.setInAmountAfterCOT(amount3.ZERO);
                }
                if (amount4 != null) {
                    response.setOutAmountAfterCOT(amount4);
                } else {
                    response.setOutAmountAfterCOT(amount4.ZERO);
                }
            }
        } catch (Exception e) {
            log.error("psmsTrxMutation error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("psmsTrxMutation error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_RPT_MUTATION_PS_MS_REQUEST);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("psmsTrxMutation RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("psmsTrxMutation saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    public static Date getZeroTimeDate(Date fecha) {
        Date res = fecha;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        res = calendar.getTime();
        return res;
    }

    //	BEGIN OF WEBSERVICE ON MUTATION PS MS USING REPORT TRX TABLE

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_MUTATION_PS_MS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    RptMutationPsMsResponse RptMutationPsMs(@RequestBody final SyncMutationPsMsRequest request,
                                            @PathVariable("apkVersion") String apkVersion) throws ParseException, IOException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        SimpleDateFormat formatterCutOffTime = new SimpleDateFormat("HH:mm");
        final RptMutationPsMsResponse response = new RptMutationPsMsResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("psTrxMutation Try processing Report Mutation PS MS request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                //	moved from below
                String date = formatter.format(new Date());
                Date curdate = formatter.parse(date);
                //
                log.info("Starting Get User Data");
                User user = (User) userService.loadUserByUsername(request.getUsername());
                log.info("Finishing get user data");
                log.info("Starting Get users name of same Wisma/Location: " + user.getOfficeCode());
                List<User> userOfWisma = userService.loadUserByLocationId(user.getOfficeCode());
                List<String> listUserName = new ArrayList<>();
                if (userOfWisma != null) {
                    for (User listUserTmp : userOfWisma) {
                        User userTmp = listUserTmp;
                        if (userTmp != null && userTmp.getRoleUser() != null) {
                            listUserName.add(userTmp.getUsername());
                        }
                    }
                }
                log.info("Finishing Get same Wisma/Location Users name");
                log.info("Starting Get TrxList for PS MS");
                List<PsMsRptTrxResponseList> trxListTemp = new ArrayList<>();
                List<ReportTrxTable> listRptTrxByMS = prsService.getRptTrxByMSInCOT(curdate, username);
                for (ReportTrxTable rptTrxByMS : listRptTrxByMS) {
                    PsMsRptTrxResponseList trxSet = new PsMsRptTrxResponseList();
                    trxSet.setRptId(rptTrxByMS.getTrx_id().toString());
                    trxSet.setTrxTime(formatterCutOffTime.format(rptTrxByMS.getApprovedDate()));
                    trxSet.setCreatedBy(rptTrxByMS.getCreatedBy());
                    trxSet.setUpdatedBy(rptTrxByMS.getApprovedBy());
                    trxSet.setTypeTrx(rptTrxByMS.getTypeTrx());
                    trxSet.setAMount(rptTrxByMS.getAmount());
                    trxListTemp.add(trxSet);
                }
                List<ReportTrxTable> listRptTrxByMS2 = prsService.getRptTrxByMSOutCOT(curdate, username);
                for (ReportTrxTable rptTrxByMS2 : listRptTrxByMS2) {
                    PsMsRptTrxResponseList trxSet2 = new PsMsRptTrxResponseList();
                    trxSet2.setRptId(rptTrxByMS2.getTrx_id().toString());
                    trxSet2.setTrxTime(formatterCutOffTime.format(rptTrxByMS2.getApprovedDate()));
                    trxSet2.setCreatedBy(rptTrxByMS2.getCreatedBy());
                    trxSet2.setUpdatedBy(rptTrxByMS2.getApprovedBy());
                    trxSet2.setTypeTrx(rptTrxByMS2.getTypeTrx());
                    trxSet2.setAMount(rptTrxByMS2.getAmount());
                    trxListTemp.add(trxSet2);
                }
                if (trxListTemp.isEmpty()) {
                    response.setTrxList(new ArrayList<>(0));
                } else {
                    response.setTrxList(trxListTemp);
                }
                log.info("Finishing Get TrxList for PS MS");
                log.info("Starting Get Summary Amount for PS MS ");
                log.info("with parameterized TrxType");
                List<String> listTrxType = new ArrayList<>();
                listTrxType.add("1");
                listTrxType.add("4");
                BigDecimal amount5 = prsService.getAmountBeforeCOT(curdate, listTrxType, username);
                response.setOutAmountBeforeCOT(amount5);
                log.info("without parameterized TrxType");
                BigDecimal amount1 = prsService.getInAmountBeforeCOT(curdate, username);
                BigDecimal amount3 = prsService.getInAmountAfterCOT(curdate, username);
                BigDecimal amount4 = prsService.getOutAmountAfterCOT(curdate, username);
                log.info("port to response");
                if (amount1 != null) {
                    response.setInAmountBeforeCOT(amount1);
                } else {
                    response.setInAmountBeforeCOT(amount1.ZERO);
                }
                if (amount5 != null) {
                    response.setOutAmountBeforeCOT(amount5);
                } else {
                    response.setOutAmountBeforeCOT(amount5.ZERO);
                }
                if (amount3 != null) {
                    response.setInAmountAfterCOT(amount3);
                } else {
                    response.setInAmountAfterCOT(amount3.ZERO);
                }
                if (amount4 != null) {
                    response.setOutAmountAfterCOT(amount4);
                } else {
                    response.setOutAmountAfterCOT(amount4.ZERO);
                }
                log.info("Finishing Get Summary Amount for PS MS");
            }
        } catch (UsernameNotFoundException e) {
            log.error("psTrxMutation error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("psTrxMutation error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_MUTATION_PS_MS_REQUEST);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("psTrxMutation RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("psTrxMutation saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }
    //	END   OF WEBSERVICE ON MUTATION PS MS USING REPORT TRX TABLE

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_CUSTOMER_INQUIRY_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    CustomerInquiryResponse CustomerInquiry(@RequestBody final CustomerInquiryRequest request,
                                            @PathVariable("apkVersion") String apkVersion) throws ParseException, IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final CustomerInquiryResponse response = new CustomerInquiryResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("customerInquiry Try processing Customer inquiry request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                LoanPRS loanPrs = loanService.getTopLoanPrsByAppId(request.getAppId());
                log.info("START MAPPING DATA");
                if (loanPrs != null) {
                    if (loanPrs.getPrsId().getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                        response.setAppId(loanPrs.getAppId());
                        response.setInstallmentAmount(loanPrs.getInstallmentAmount());
                        response.setInstallmentCount(new BigDecimal(loanPrs.getAngsuran()));
                        response.setMaturityDate(loanPrs.getDueDate());
                        response.setOutstandingMargin(loanPrs.getMarginAmount());
                        response.setRemainingPrincipal(loanPrs.getRemainingPrincipal());
                        response.setStatusPembiayaan("");
                        SurveyWawancara sw = swService.getSWById(loanPrs.getLoanId().getSwId().toString());
                        response.setSwDate(sw.getSurveyDate());
                        List<LoanPRS> loanPrsList = loanService.getLoanPRSByAppId(loanPrs.getAppId());
                        BigDecimal danaTalangan = BigDecimal.ZERO;
                        for (LoanPRS loanPRS : loanPrsList) {
                            if (loanPRS.getUseEmergencyFund() == true) {
                                danaTalangan.add(loanPrs.getInstallmentPaymentAmount());
                            }
                        }
                        response.setDanaTalangan(danaTalangan);
                        log.info("FINISHING MAPPING DATA");
                    }
                }
            }
        } catch (Exception e) {
            log.error("customerInquiry error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("customerInquiry error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_CUSTOMER_INQUIRY_REQUEST);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("customerInquiry RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("customerInquiry saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }
}