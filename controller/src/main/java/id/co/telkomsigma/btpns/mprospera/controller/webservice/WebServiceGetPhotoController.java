package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.prs.DisbursementPhoto;
import id.co.telkomsigma.btpns.mprospera.model.prs.NonPRSPhoto;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRSPhoto;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwIdPhoto;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwSurveyPhoto;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.PhotoRequest;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceGetPhotoController")
public class WebServiceGetPhotoController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private PRSService prsService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SWService swService;

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_PRS_PHOTO_LINK_REQUEST, method = {RequestMethod.POST})
    public void getPRSPhoto(final HttpServletRequest request, final HttpServletResponse response,
                            @PathVariable("apkVersion") String apkVersion, @PathVariable("id") String id,
                            @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                            @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                            @RequestHeader(value = "username", required = true) final String username,
                            @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                            @RequestHeader(value = "imei", required = true) final String imei) throws Exception {

        final PhotoRequest requestMapping = new PhotoRequest();
        requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
        requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
        requestMapping.setUsername(username);
        requestMapping.setSessionKey(sessionKey);
        requestMapping.setImei(imei);
        final PRSPhotoResponse responseCode = new PRSPhotoResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("prsPhoto Try to processing get prs activity photo request: " + jsonUtils.toJson(requestMapping));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                log.info("RC : " + validation);
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey + " ,apk version : " + apkVersion);
                try {
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Get PRS Photo");
                PRS prs = prsService.getPRSById(Long.parseLong(id));
                PRSPhoto prsPhoto = prsService.getPrsPhotoByPrs(prs);
                if (prsPhoto != null) {
                    responseCode.setPhoto(prsPhoto.getPrsPhoto());
                    try {
                        response.setContentType("image/jpg;charset=utf-8");
                        response.getOutputStream().write(Base64.decodeBase64(responseCode.getPhoto()));
                        response.getOutputStream().flush();
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.RC_PRS_PHOTO_NULL);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                    try {
                        response.getWriter().write((new JsonUtils().toJson(responseCode)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            log.error("prsPhoto error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("prsPhoto error: " + e.getMessage());
            try {
                response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(requestMapping.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_PRSPHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(requestMapping.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(requestMapping.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(requestMapping.getUsername().trim());

                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("prsPhoto RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("prsPhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_DISBURSEMENT_PHOTO_LINK_REQUEST, method = {RequestMethod.POST})
    public void getDisbursementPhoto(final HttpServletRequest request, final HttpServletResponse response,
                                     @PathVariable("apkVersion") String apkVersion, @PathVariable("id") String id,
                                     @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                                     @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                                     @RequestHeader(value = "username", required = true) final String username,
                                     @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                                     @RequestHeader(value = "imei", required = true) final String imei) throws Exception {

        final PhotoRequest requestMapping = new PhotoRequest();
        requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
        requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
        requestMapping.setUsername(username);
        requestMapping.setSessionKey(sessionKey);
        requestMapping.setImei(imei);
        final DisbursementPhotoResponse responseCode = new DisbursementPhotoResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("getDisbursementPhoto Try to processing get disbursement photo request: " + jsonUtils.toJson(requestMapping));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                log.info("RC : " + validation);
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey + " ,apk version : " + apkVersion);
                try {
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Get Disbursement Photo");
                LoanPRS loanPRS = loanService.getLoanPRSById(Long.parseLong(id));
                DisbursementPhoto photo = loanService.getDisbursementPhoto(loanPRS);
                if (photo != null) {
                    responseCode.setPhoto(photo.getDisbursementPhoto());
                    try {
                        response.setContentType("image/jpg;charset=utf-8");
                        response.getOutputStream().write(Base64.decodeBase64(responseCode.getPhoto()));
                        response.getOutputStream().flush();
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.PHOTO_NULL);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                    try {
                        response.getWriter().write((new JsonUtils().toJson(responseCode)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            log.error("getDisbursementPhoto error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("getDisbursementPhoto error: " + e.getMessage());
            try {
                response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(requestMapping.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_DISBURSEMENT_PHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(requestMapping.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(requestMapping.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(requestMapping.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("getDisbursementPhoto RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("getDisbursementPhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_ID_PHOTO_LINK_REQUEST, method = {RequestMethod.POST})
    public void getIdPhoto(final HttpServletRequest request, final HttpServletResponse response,
                           @PathVariable("apkVersion") String apkVersion, @PathVariable("id") String customerId,
                           @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                           @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                           @RequestHeader(value = "username", required = true) final String username,
                           @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                           @RequestHeader(value = "imei", required = true) final String imei) throws Exception {

        final PhotoRequest requestMapping = new PhotoRequest();
        requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
        requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
        requestMapping.setUsername(username);
        requestMapping.setSessionKey(sessionKey);
        requestMapping.setImei(imei);
        final IDCardPhotoResponse responseCode = new IDCardPhotoResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("idCardPhoto Try to processing get id card photo request: " + jsonUtils.toJson(requestMapping));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
                try {
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Get ID Card Photo");
                SurveyWawancara sw = swService.getSWByCustomerId(Long.parseLong(customerId));
                SwIdPhoto photo = swService.getSwIdPhoto(sw.getSwId().toString());
                if (photo != null) {
                    responseCode.setPhoto(photo.getIdPhoto());
                    try {
                        response.setContentType("image/jpg;charset=utf-8");
                        response.getOutputStream().write(Base64.decodeBase64(responseCode.getPhoto()));
                        response.getOutputStream().flush();
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.PHOTO_NULL);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                    try {
                        response.getWriter().write((new JsonUtils().toJson(responseCode)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            log.error("idCardPhoto error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("idCardPhoto error: " + e.getMessage());
            try {
                response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(requestMapping.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_ID_PHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(requestMapping.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(requestMapping.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(requestMapping.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("idCardPhoto RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("idCardPhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_BUSINESS_PLACE_PHOTO_LINK_REQUEST, method = {RequestMethod.POST})
    public void getBusinessPlacePhoto(final HttpServletRequest request, final HttpServletResponse response,
                                      @PathVariable("apkVersion") String apkVersion, @PathVariable("id") String customerId,
                                      @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                                      @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                                      @RequestHeader(value = "username", required = true) final String username,
                                      @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                                      @RequestHeader(value = "imei", required = true) final String imei) throws Exception {

        final PhotoRequest requestMapping = new PhotoRequest();
        requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
        requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
        requestMapping.setUsername(username);
        requestMapping.setSessionKey(sessionKey);
        requestMapping.setImei(imei);
        final BusinessPlacePhotoResponse responseCode = new BusinessPlacePhotoResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("businessPlacePhoto Try to processing get business place photo request: " + jsonUtils.toJson(requestMapping));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
                try {
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Get Business Place Photo");
                SurveyWawancara sw = swService.getSWByCustomerId(Long.parseLong(customerId));
                SwSurveyPhoto photo = swService.getSwSurveyPhoto(sw.getSwId().toString());
                if (photo != null) {
                    if (photo.getSurveyPhoto() != null) {
                        responseCode.setPhoto(photo.getSurveyPhoto());
                        try {
                            response.setContentType("image/jpg;charset=utf-8");
                            response.getOutputStream().write(Base64.decodeBase64(responseCode.getPhoto()));
                            response.getOutputStream().flush();
                        } catch (Exception e) {
                            log.error(e.getMessage(), e);
                        }
                    } else {
                        responseCode.setResponseCode(WebGuiConstant.PHOTO_NULL);
                        String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(labelError);
                        try {
                            response.getWriter().write((new JsonUtils().toJson(responseCode)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.PHOTO_NULL);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                    try {
                        response.getWriter().write((new JsonUtils().toJson(responseCode)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            log.error("businessPlacePhoto error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("businessPlacePhoto error: " + e.getMessage());
            try {
                response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(requestMapping.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_business_Place_Photo);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(requestMapping.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(requestMapping.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(requestMapping.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("businessPlacePhoto RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("businessPlacePhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_NON_PRS_PHOTO_LINK_REQUEST, method = {RequestMethod.POST})
    public void getNonPRSPhoto(final HttpServletRequest request, final HttpServletResponse response,
                               @PathVariable("apkVersion") String apkVersion, @PathVariable("id") String id,
                               @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                               @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                               @RequestHeader(value = "username", required = true) final String username,
                               @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                               @RequestHeader(value = "imei", required = true) final String imei,
                               @RequestHeader(value = "longitude", required = true) final String longitude,
                               @RequestHeader(value = "latitude", required = true) final String latitude) throws Exception {

        final PhotoRequest requestMapping = new PhotoRequest();
        requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
        requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
        requestMapping.setUsername(username);
        requestMapping.setSessionKey(sessionKey);
        requestMapping.setImei(imei);
        requestMapping.setLongitude(longitude);
        requestMapping.setLatitude(latitude);
        final NonPrsPhotoResponse responseCode = new NonPrsPhotoResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("nonPrsPhoto Try to processing get non prs photo request: " + jsonUtils.toJson(requestMapping));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
                try {
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Get Collection Photo");
                LoanNonPRS loanNonPRS = loanService.getLoanNonPRSById(Long.parseLong(id));
                NonPRSPhoto photo = loanService.getNonPRSPhoto(loanNonPRS.getLoanNonPRSId());
                if (photo != null) {
                    responseCode.setPhoto(photo.getNonPrsPhoto());
                    try {
                        response.setContentType("image/jpg;charset=utf-8");
                        response.getOutputStream().write(Base64.decodeBase64(responseCode.getPhoto()));
                        response.getOutputStream().flush();
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.PHOTO_NULL);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                    try {
                        response.getWriter().write((new JsonUtils().toJson(responseCode)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            log.error("nonPrsPhoto error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("nonPrsPhoto error: " + e.getMessage());
            try {
                response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(requestMapping.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_NON_PRSPHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(requestMapping.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(requestMapping.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(requestMapping.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("nonPrsPhoto RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("nonPrsPhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_INQUERY_BUSINESS_PLACE_PHOTO_LINK_REQUEST, method = {RequestMethod.POST})
    public void getInqueryBusinessPlacePhoto(final HttpServletRequest request, final HttpServletResponse response,
                                             @PathVariable("apkVersion") String apkVersion, @PathVariable("id") String id,
                                             @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                                             @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                                             @RequestHeader(value = "username", required = true) final String username,
                                             @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                                             @RequestHeader(value = "imei", required = true) final String imei,
                                             @RequestHeader(value = "longitude", required = true) final String longitude,
                                             @RequestHeader(value = "latitude", required = true) final String latitude) throws Exception {

        final PhotoRequest requestMapping = new PhotoRequest();
        requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
        requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
        requestMapping.setUsername(username);
        requestMapping.setSessionKey(sessionKey);
        requestMapping.setImei(imei);
        requestMapping.setLatitude(latitude);
        requestMapping.setLongitude(longitude);
        final BusinessPlacePhotoResponse responseCode = new BusinessPlacePhotoResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("businessPlacePhoto Try to processing get business place photo request: " + jsonUtils.toJson(requestMapping));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
                try {
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Get Business Place Photo");
                Customer customer = customerService.findById(id);
                if (customer.getSwId() != null) {
                    SurveyWawancara swId = swService.getSWById(customer.getSwId().toString());
                    if (swId != null) {
                        SwSurveyPhoto photo = swService.getSwSurveyPhoto(swId.getSwId().toString());
                        if (photo != null) {
                            responseCode.setPhoto(photo.getSurveyPhoto());
                            try {
                                response.setContentType("image/jpg;charset=utf-8");
                                response.getOutputStream().write(Base64.decodeBase64(responseCode.getPhoto()));
                                response.getOutputStream().flush();
                            } catch (Exception e) {
                                log.error(e.getMessage(), e);
                            }
                        } else {
                            responseCode.setResponseCode(WebGuiConstant.PHOTO_NULL);
                            String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                            responseCode.setResponseMessage(labelError);
                            try {
                                response.getWriter().write((new JsonUtils().toJson(responseCode)));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        responseCode.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                        String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(labelError);
                        try {
                            response.getWriter().write((new JsonUtils().toJson(responseCode)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                    try {
                        response.getWriter().write((new JsonUtils().toJson(responseCode)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            log.error("businessPlacePhoto error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("businessPlacePhoto error: " + e.getMessage());
            try {
                response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(requestMapping.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_INQ_business_Place_Photo);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(requestMapping.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(requestMapping.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(requestMapping.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("businessPlacePhoto RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("businessPlacePhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
    }
}
