package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.SubmitPRSRequest;
import id.co.telkomsigma.btpns.mprospera.response.SubmitPRSResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceSubmitManualKFOController")
public class WebServiceSubmitManualKFOController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private PRSService prsService;

    @Autowired
    private SubmitPRSService submitPRSService;

    @Value("${allow.submit:1}")
    private String allowSubmit;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_SUBMIT_PRS_MANUAL_KFO_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SubmitPRSResponse doSubmitPRSManualKfo(@RequestBody final SubmitPRSRequest request,
                                           @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final SubmitPRSResponse response = new SubmitPRSResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        response.setResponseMessage("Sukses");
        try {
            log.info("submitPRSManual INCOMING SUBMIT PRS MANUAL KFO MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                log.info("Try to processing submit PRS Manual KFO request");
                if (request.getStatus().equals(WebGuiConstant.STATUS_DRAFT) || request.getStatus().equals(WebGuiConstant.STATUS_APPROVED)
                        || request.getStatus().equals(WebGuiConstant.STATUS_REJECTED) || request.getStatus().equals(WebGuiConstant.STATUS_SUBMIT)) {
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_STATUS);
                    String labelUnknown = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelUnknown);
                    log.error("UNKNOWN STATUS");
                    log.info("RESPONSE MESSAGE : " + response.toString());
                    return response;
                }
                // PROSES KE TABLE PRS --> HANYA BISA UPDATE
                // cari id PRSnya dulu
                if (request.getPrsId() == null) {
                    response.setResponseCode(WebGuiConstant.RC_FAILED);
                    String labelUnknown = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelUnknown);
                    log.error("PRS ID REQUEST NULL");
                    log.info("RESPONSE MESSAGE : " + response.toString());
                    return response;
                }
                PRS prs = prsService.getPRSById(Long.parseLong(request.getPrsId()));
                List<LoanPRS> loanPRSForUpdate = new ArrayList<>();
                List<SavingPRS> savingPRSForUpdate = new ArrayList<>();
                List<CustomerPRS> customerPRSForUpdate = new ArrayList<>();
                if (prs == null) {
                    // idprs tidak ditemukan, RC gagal
                    response.setResponseCode(WebGuiConstant.RC_FAILED);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                } else {
                    if (prs.getStatus().equals(WebGuiConstant.STATUS_SUBMIT) || prs.getStatus().equals(WebGuiConstant.STATUS_APPROVED)
                            || prs.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        String labelUnknown = getMessage("webservice.rc.label." + WebGuiConstant.RC_PRS_ALREADY_SUBMIT);
                        response.setResponseMessage(labelUnknown);
                        return response;
                    }
                    submitPRSService.submitManualKFO(request, prs, customerPRSForUpdate, loanPRSForUpdate, savingPRSForUpdate);
                    PRS prsSave = prsService.savePRSEvent(prs, customerPRSForUpdate, loanPRSForUpdate, savingPRSForUpdate);
                    if (prsSave == null) {
                        response.setResponseCode(WebGuiConstant.RC_FAILED);
                        String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                        response.setResponseMessage(labelFailed);
                        log.error("Inserting PRS Failed");
                    }
                    response.setPrsId(prsSave.getPrsId().toString());
                }
            }
            response.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        } catch (NumberFormatException e) {
            log.error("submitPRSManual error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("submitPRSManual error: " + e.getMessage());
        } finally {
            try {
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_PRS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("submitPRSManual RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("submitPRSManual saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }
}
