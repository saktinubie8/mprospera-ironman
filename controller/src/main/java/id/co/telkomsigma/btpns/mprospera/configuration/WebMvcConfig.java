package id.co.telkomsigma.btpns.mprospera.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.github.isrsal.logging.LoggingFilter;

import java.util.Locale;

import javax.servlet.Filter;

/**
 * Created by daniel on 3/26/15.
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @Value("${default.locale.lang}")
    private String localeLanguage;

    @Value("${default.locale.country}")
    private String localeCountry;

    @Value("${default.timezone.lang}")
    private String timezoneLanguage;

    @Value("${default.timezone.country}")
    private String timezoneCountry;

    private static Locale language;
    private static Locale timezone;

    @Autowired
    ApplicationContext applicationContext;

    @Bean(name = "language")
    public Locale getLanguage() {
        if (language == null) {
            language = new Locale(localeLanguage, localeCountry);
        }
        return language;
    }

    @Bean(name = "timezone")
    public Locale getTimezone() {
        if (timezone == null) {
            timezone = new Locale(timezoneLanguage, timezoneCountry);
        }
        return timezone;
    }

    @Bean(name = "localeResolver")
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(getLanguage());
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Bean
    public FilterRegistrationBean logFilterRegistration() {
        FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
        filterRegBean.setFilter(logFilter());
        filterRegBean.addUrlPatterns("/webservice/*");
        filterRegBean.setEnabled(true);
        return filterRegBean;
    }

    @Bean
    public Filter logFilter() {
        return new LoggingFilter();
    }

}