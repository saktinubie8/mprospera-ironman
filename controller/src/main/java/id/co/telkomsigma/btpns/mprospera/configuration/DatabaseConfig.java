package id.co.telkomsigma.btpns.mprospera.configuration;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

@Configuration
@EnableConfigurationProperties({DataSourceProperties.class})
public class DatabaseConfig {
    protected Log log = LogFactory.getLog(this.getClass());

    /*
     * @Autowired(required = false) private MBeanExporter mbeanExporter;
     */

	/*@Bean(name = "primaryDataSource")
	@Qualifier("primaryDataSource")
	@Primary
	@ConfigurationProperties(prefix = "spring.datasource")
	@ConditionalOnProperty(prefix = "spring.datasource", name = { "url" })
	public DataSource primary() {
		return DataSourceBuilder.create().build();
	}*/

    @Bean(destroyMethod = "", name = "primaryDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    //@ConditionalOnProperty(prefix = "spring.datasource", name = { "jndi-name" })
    public DataSource primaryDataSource(DataSourceProperties properties) {
        // DataSource ds_pooled = null;
        JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
        DataSource dataSource = dataSourceLookup.getDataSource(properties.getJndiName());

        /*	 * try {
         * log.info("URL : "+parseDS.getConnection().getMetaData().getURL());
         * DataSource dataSource =
         * DataSources.unpooledDataSource(parseDS.getConnection
         * ().getMetaData().getURL()); ds_pooled =
         * DataSources.pooledDataSource(dataSource); } catch (SQLException e) {
         * // TODO Auto-generated catch block e.printStackTrace(); }
         */
        // excludeMBeanIfNecessary(dataSource, "dataSource");
        System.out.println("SQL JNDI" + properties.getJndiName());
        log.info("SQL JNDI" + properties.getJndiName());
        return dataSource;
    }

    /*
     * private void excludeMBeanIfNecessary(Object candidate, String beanName) {
     * if ((this.mbeanExporter != null) &&
     * (JmxUtils.isMBean(candidate.getClass())))
     * this.mbeanExporter.addExcludedBean(beanName); }
     */
}
