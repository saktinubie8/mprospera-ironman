package id.co.telkomsigma.btpns.mprospera.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.request.LoanPRSRequest;
import id.co.telkomsigma.btpns.mprospera.service.LoanService;

public class DiscountMarginCalculator {

    @Autowired
    LoanService loanService;

    public Map<Long, BigDecimal> calculateDiscountMarginAmount(List<LoanPRSRequest> loans, BigDecimal total) {
        Map<Long, BigDecimal> mapOfPercentage = new HashMap<Long, BigDecimal>();
        BigDecimal totalSisaMarjin = BigDecimal.ZERO;
        for (LoanPRSRequest loan : loans) {
            LoanPRS loanPrs = loanService.getLoanPRSById(Long.parseLong(loan.getLoanPrsId()));
            totalSisaMarjin.add(loanPrs.getMarginAmount());
        }
        for (LoanPRSRequest loan : loans) {
            LoanPRS loanPrs = loanService.getLoanPRSById(Long.parseLong(loan.getLoanPrsId()));
            BigDecimal oneHundredPercent = new BigDecimal(100).divide(new BigDecimal(100));
            BigDecimal percentage = loanPrs.getMarginAmount().divide(totalSisaMarjin).multiply(oneHundredPercent).multiply(total);
            BigDecimal discount = loanPrs.getMarginAmount().multiply(percentage.divide(new BigDecimal(100)));
            BigDecimal hasilDiscount = loanPrs.getMarginAmount().subtract(discount);
            mapOfPercentage.put(Long.parseLong(loan.getLoanPrsId()), hasilDiscount);
        }
        return mapOfPercentage;
    }

}
