package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.prs.MutationTrxTabel;
import id.co.telkomsigma.btpns.mprospera.model.prs.ReportCollection;
import id.co.telkomsigma.btpns.mprospera.model.prs.ReportTrxTable;
import id.co.telkomsigma.btpns.mprospera.request.*;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.prs.NonPRSPhoto;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

@Controller("webServiceNonPRSController")
public class WebServiceNonPrsController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ParameterService parameterService;

    @Autowired
    private PRSService prsService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private UserService userService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private SavingService savingService;

    @Autowired
    private RESTClient restClient;

    @Autowired
    private ProsperaEncryptionService prosperaEncryptionService;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_NON_PRS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncNonPRSResponse syncNonPrs(@RequestBody final SyncNonPRSRequest request,
                                  @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final SyncNonPRSResponse response = new SyncNonPRSResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("nonPrsSync Try to processing sync Collection request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                String paramApkVersion = parameterService.loadParamByParamName(WebGuiConstant.PARAMETER_CURRENT_VERSION,
                        "1.0");
                log.info("Starting Get User Data");
                User user = (User) userService.loadUserByUsername(request.getUsername());
                log.info("Finishing get user data");
                log.info("Starting Get Sentra for Non PRS");
                String page = request.getPage();
                Page<Sentra> sentraForNonPRS = sentraService.getSentraByWismaIdPaging(user.getOfficeCode(), page);
                log.info("Finishing Get Sentra for Non PRS");
                List<NonPRSResponseList> nonPrsListTemp = new ArrayList<>();
                log.info("START MAPPING DATA");
                for (Sentra sentra : sentraForNonPRS) {
                    NonPRSResponseList nonPrsSet = new NonPRSResponseList();
                    nonPrsSet.setSentraId(sentra.getSentraId().toString());
                    nonPrsSet.setSentraCode(sentra.getSentraCode());
                    nonPrsSet.setSentraName(sentra.getSentraName());
                    List<Customer> customerList = customerService.getBySentraId(sentra);
                    List<CustomerNonPRSListResponse> customerForNonPrsListTemp = new ArrayList<>();
                    for (Customer customer : customerList) {
                        if (loanService.isNonPRSCustomer(customer) && savingService.isNonPrsCustomerSaving(customer)) {
                            CustomerNonPRSListResponse customerNonPrs = new CustomerNonPRSListResponse();
                            if (customer.getCustomerId() != null) {
                                customerNonPrs.setCustomerId(customer.getCustomerId().toString());
                                customerNonPrs.setCustomerCif(customer.getCustomerCifNumber());
                                if (customer.getSwId() != null) {
                                    customerNonPrs.setSwId(customer.getSwId().toString());
                                }
                                if (customer.getGroup() != null) {
                                    customerNonPrs.setGroupId(customer.getGroup().getGroupId().toString());
                                    customerNonPrs.setGroupName(customer.getGroup().getGroupName());
                                }
                            } else {
                                customerNonPrs.setCustomerId("");
                            }
                            customerNonPrs.setCustomerName(customer.getCustomerName());
                            List<LoanNonPRS> loanNonPrsList = loanService.getLoanNonPRSByCustomer(customer);
                            List<LoanNonPRSListResponse> loanNonPrsListTemp = new ArrayList<>();
                            for (LoanNonPRS loanNonPrs : loanNonPrsList) {
                                LoanNonPRSListResponse loanNonPrsSet = new LoanNonPRSListResponse();
                                loanNonPrsSet.setAppId(loanNonPrs.getLoanId().getAppId());
                                loanNonPrsSet.setLoanCreatedDate(formatDateTime.format(loanNonPrs.getLoanId().getCreatedDate()));
                                if (loanNonPrs.getCurrentMargin() != null) {
                                    loanNonPrsSet.setCurrentMargin(loanNonPrs.getCurrentMargin());
                                } else {
                                    loanNonPrsSet.setCurrentMargin(loanNonPrs.getCurrentMargin().ZERO);
                                }
                                if (loanNonPrs.getInstallmentAmount() != null) {
                                    loanNonPrsSet.setInstallmentAmount(loanNonPrs.getInstallmentAmount());
                                } else {
                                    loanNonPrsSet.setInstallmentAmount(loanNonPrs.getInstallmentAmount().ZERO);
                                }
                                if (loanNonPrs.getInstallmentPaymentAmount() != null) {
                                    loanNonPrsSet.setInstallmentPaymentAmount(loanNonPrs.getInstallmentPaymentAmount());
                                } else {
                                    loanNonPrsSet
                                            .setInstallmentPaymentAmount(loanNonPrs.getInstallmentPaymentAmount().ZERO);
                                }
                                if (loanNonPrs.getRemainingPrincipal() != null) {
                                    loanNonPrsSet.setRemainingPrincipal(loanNonPrs.getRemainingPrincipal());
                                } else {
                                    loanNonPrsSet.setRemainingPrincipal(loanNonPrs.getRemainingPrincipal().ZERO);
                                }
                                if (loanNonPrs.getMarginAmount() != null) {
                                    loanNonPrsSet.setMarginAmount(loanNonPrs.getMarginAmount());
                                } else {
                                    loanNonPrsSet.setMarginAmount(loanNonPrs.getMarginAmount().ZERO);
                                }
                                loanNonPrsSet.setLoanId(loanNonPrs.getLoanId().getLoanId().toString());
                                loanNonPrsSet.setLoanNonPrsId(loanNonPrs.getLoanNonPRSId().toString());
                                if (loanNonPrs.getMeetingDate() != null) {
                                    loanNonPrsSet.setMeetingDate(formatter.format(loanNonPrs.getMeetingDate()));
                                } else {
                                    loanNonPrsSet.setMeetingDate("");
                                }
                                if (loanNonPrs.getDueDate() != null) {
                                    loanNonPrsSet.setDueDate(formatter.format(loanNonPrs.getDueDate()));
                                } else {
                                    loanNonPrsSet.setDueDate("");
                                }
                                loanNonPrsSet.setNonPrsPhoto(
                                        WebGuiConstant.TERMINAL_NON_PRS_PHOTO_REQUEST.replace("**/{apkVersion:.+}",
                                                "/" + paramApkVersion) + "/" + loanNonPrs.getLoanNonPRSId());
                                loanNonPrsSet.setOutstandingAmount(loanNonPrs.getOutstandingAmount());
                                if (loanNonPrs.getOutstandingCount() != null)
                                    loanNonPrsSet.setOutstandingCount(loanNonPrs.getOutstandingCount().toString());
                                if (loanNonPrs.getOutstandingReason() != null) {
                                    loanNonPrsSet.setOutstandingReason(
                                            loanNonPrs.getOutstandingReason().getParameterId().toString());
                                } else {
                                    loanNonPrsSet.setOutstandingReason("");
                                }
                                Date curdate = new Date();
                                if (loanNonPrs.getDueDate() != null) {
                                    Long diff = curdate.getTime() - loanNonPrs.getDueDate().getTime();
                                    Long diffDays = diff / (24 * 60 * 60 * 1000);
                                    loanNonPrsSet.setOverdueDays(diffDays.toString());
                                }
                                loanNonPrsSet.setPayCommitment(loanNonPrs.getPayCommitment());
                                if (loanNonPrs.getPeopleMeet() != null) {
                                    loanNonPrsSet.setPeopleMeet(loanNonPrs.getPeopleMeet().getParameterId().toString());
                                } else {
                                    loanNonPrsSet.setPeopleMeet("");
                                }
                                if (loanNonPrs.getCommitmentDate() != null) {
                                    loanNonPrsSet.setCommitmentDate(formatter.format(loanNonPrs.getCommitmentDate()));
                                } else {
                                    loanNonPrsSet.setCommitmentDate("");
                                }
                                if (loanNonPrs.getNextCommitmentDate() != null) {
                                    loanNonPrsSet.setNextCommitmentDate(formatter.format(loanNonPrs.getNextCommitmentDate()));
                                } else {
                                    loanNonPrsSet.setNextCommitmentDate("");
                                }
                                if (loanNonPrs.getUpdatedDate() != null) {
                                    loanNonPrsSet.setUpdatedDate(formatter.format(loanNonPrs.getUpdatedDate()));
                                } else {
                                    loanNonPrsSet.setUpdatedDate("");
                                }
                                if (loanNonPrs.getSubmitDate() != null) {
                                    loanNonPrsSet.setSubmitDate(formatter.format(loanNonPrs.getSubmitDate()));
                                } else {
                                    loanNonPrsSet.setSubmitDate("");
                                }
                                loanNonPrsSet.setCreatedBy(loanNonPrs.getCreatedBy());
                                loanNonPrsSet.setCreatedDate(formatter.format(loanNonPrs.getCreatedDate()));
                                loanNonPrsSet.setUpdatedBy(loanNonPrs.getUpdatedBy());
                                loanNonPrsSet.setInputMoney(loanNonPrs.getInputMoney());
                                loanNonPrsSet.setStatus(loanNonPrs.getStatus());
                                loanNonPrsListTemp.add(loanNonPrsSet);
                            }
                            if (!loanNonPrsListTemp.isEmpty()) {
                                customerNonPrs.setLoanList(loanNonPrsListTemp);
                            }
                            List<SavingNonPRS> savingNonPrsList = savingService.getSavingNonPrsByCustomer(customer);
                            List<SavingNonPRSListResponse> savingNonPrsListTemp = new ArrayList<>();
                            for (SavingNonPRS savingNonPrs : savingNonPrsList) {
                                SavingNonPRSListResponse savingNonPrsSet = new SavingNonPRSListResponse();
                                savingNonPrsSet.setAccountNumber(savingNonPrs.getAccountNumber());
                                savingNonPrsSet.setClearBalance(savingNonPrs.getClearBalance());
                                savingNonPrsSet.setDepositAmount(savingNonPrs.getDepositAmount());
                                savingNonPrsSet.setSavingNonPrsId(savingNonPrs.getSavingNonPrsId().toString());
                                savingNonPrsListTemp.add(savingNonPrsSet);
                            }
                            if (!savingNonPrsListTemp.isEmpty())
                                customerNonPrs.setSavingList(savingNonPrsListTemp);
                            customerForNonPrsListTemp.add(customerNonPrs);
                        }
                    }
                    if (!customerForNonPrsListTemp.isEmpty())
                        nonPrsSet.setCustomerList(customerForNonPrsListTemp);
                    nonPrsListTemp.add(nonPrsSet);
                }
                response.setSentraList(nonPrsListTemp);
                log.info("Total Page of Sentra : " + sentraForNonPRS.getTotalPages());
                if (sentraForNonPRS.getTotalPages() == 0) {
                    response.setTotalPage((String.valueOf(1)));
                } else {
                    response.setTotalPage((String.valueOf(sentraForNonPRS.getTotalPages())));
                }
                log.info("Finishing get non prs");
            }
        } catch (Exception e) {
            log.error("nonPrsSync error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("nonPrsSync error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_NON_PRS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("nonPrsSync RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("nonPrsSync saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SUBMIT_NON_PRS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SubmitNonPRSResponse doSubmitNonPRS(@RequestBody final SubmitNonPRSRequest request,
                                        @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final SubmitNonPRSResponse response = new SubmitNonPRSResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("submitNonPRS Try to processing submit Non PRS request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("trying to submit non PRS ");
                List<SubmitLoanNonPrsListResponse> loanNPIdList = new ArrayList<>();
                List<SubmitLoanNonPrsListResponse> loanNonPrsId = new ArrayList<>();
                for (SentraForNonPrsRequest sentra : request.getSentraList()) {
                    Sentra sentraMap = sentraService.findBySentraId(sentra.getSentraId());
                    sentraMap.setCollectEndTime(sentra.getCollectEndTime());
                    sentraService.save(sentraMap);
                    for (CustomerForNonPrsRequest customer : sentra.getCustomerList()) {
                        for (LoanNonPrsRequest loan : customer.getLoanList()) {
                            SubmitLoanNonPrsListResponse loanId = new SubmitLoanNonPrsListResponse();
                            LoanNonPRS loanNonPRSSet = loanService
                                    .getLoanNonPRSById(Long.parseLong(loan.getLoanNonPrsId()));
                            if (loanNonPRSSet == null) {
                                log.error("UNKNOWN LOAN ID");
                                response.setResponseCode(WebGuiConstant.UNKNOWN_LOAN_ID);
                                String labelUnknownLoan = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(labelUnknownLoan);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            }
                            if (loanNonPRSSet.getStatus().equals(WebGuiConstant.STATUS_SUBMIT)) {
                                log.error("STATUS ALREADY SUBMIT");
                                response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                                String labelUnknownLoan = getMessage("webservice.rc.label." + WebGuiConstant.RC_PRS_ALREADY_SUBMIT);
                                response.setResponseMessage(labelUnknownLoan);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            }
                            if (loanNonPRSSet.getStatus().equals(WebGuiConstant.RC_SUCCESS)) {
                                log.error("STATUS ALREADY APPROVED");
                                response.setResponseCode(WebGuiConstant.RC_ALREADY_APPROVED);
                                String labelUnknownLoan = getMessage("webservice.rc.label." + WebGuiConstant.RC_ALREADY_APPROVED);
                                response.setResponseMessage(labelUnknownLoan);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            }
                            loanNonPRSSet.setLatitude(request.getLatitude());
                            loanNonPRSSet.setLongitude(request.getLongitude());
                            loanNonPRSSet.setUpdatedBy(request.getUsername());
                            loanNonPRSSet.setCreatedBy(request.getUsername());
                            loanNonPRSSet.setCreatedDate(new Date());
                            loanNonPRSSet.setUpdatedDate(new Date());
                            loanNonPRSSet.setSubmitDate(new Date());
                            loanNonPRSSet.setCurrentMargin(loan.getCurrentMargin());
                            loanNonPRSSet.setInstallmentAmount(loan.getInstallmentAmount());
                            loanNonPRSSet.setInstallmentPaymentAmount(loan.getInstallmentPaymentAmount());
                            loanNonPRSSet.setInputMoney(loan.getInputMoney());
                            loanNonPRSSet.setDepositMoney(loan.getDepositMoney());
                            loanNonPRSSet.setAutoDebet(loan.getAutoDebet());
                            if (loan.getMeetingDate() != null)
                                try {
                                    if (!loan.getMeetingDate().equals(""))
                                        loanNonPRSSet.setMeetingDate(formatter.parse(loan.getMeetingDate()));
                                } catch (ParseException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }
                            if (loan.getCommitmentDate() != null) {
                                try {
                                    if (!loan.getCommitmentDate().equals(""))
                                        loanNonPRSSet.setCommitmentDate(formatter.parse(loan.getCommitmentDate()));
                                } catch (ParseException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }
                            }
                            if (loan.getNextCommitmentDate() != null) {
                                try {
                                    if (!loan.getNextCommitmentDate().equals(""))
                                        loanNonPRSSet.setNextCommitmentDate(formatter.parse(loan.getNextCommitmentDate()));
                                } catch (ParseException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }
                            }
                            loanNonPRSSet.setOutstandingAmount(loan.getOutstandingAmount());
                            if (loan.getOverdueDays() != null)
                                loanNonPRSSet.setOutstandingDays(Long.parseLong(loan.getOverdueDays()));
                            PRSParameter prsParamOutstanding = new PRSParameter();
                            if (loan.getOutstandingReason() != null) {
                                if (!loan.getOutstandingReason().equals("")) {
                                    prsParamOutstanding = parameterService
                                            .findByParameterId(Long.parseLong(loan.getOutstandingReason()));
                                    loanNonPRSSet.setOutstandingReason(prsParamOutstanding);
                                }
                            }
                            loanNonPRSSet.setPayCommitment(loan.isPayCommitment());
                            PRSParameter prsParamPeople = new PRSParameter();
                            if (loan.getPeopleMeet() != null && !"".equals(loan.getPeopleMeet())) {
                                prsParamPeople = parameterService.findByParameterId(Long.parseLong(loan.getPeopleMeet()));
                                loanNonPRSSet.setPeopleMeet(prsParamPeople);
                            }
                            loanNonPRSSet.setStatus("SUBMIT");
                            LoanNonPRS loanNonPRSSave = loanService.saveLoanNonPRS(loanNonPRSSet);
                            if (loanNonPRSSave == null) {
                                response.setResponseCode(WebGuiConstant.RC_FAILED);
                                String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(labelFailed);
                                log.error("Submit Loan Non PRS Failed");
                            }
                            loanId.setLoanNonPrsId(loanNonPRSSet.getLoanNonPRSId().toString());
                            loanNonPrsId.add(loanId);
                        }
                        for (SavingNonPrsRequest saving : customer.getSavingList()) {
                            SavingNonPRS savingNonPRSSet = savingService
                                    .getSavingNonPrsById(Long.parseLong(saving.getSavingNonPrsId()));
                            if (savingNonPRSSet == null) {
                                log.error("UNKNOWN SAVING");
                                response.setResponseCode(WebGuiConstant.UNKNOWN_SAVING);
                                String labelUnknownSaving = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(labelUnknownSaving);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            }
                            savingNonPRSSet.setClearBalance(saving.getClearBalance());
                            savingNonPRSSet.setDepositAmount(saving.getDepositAmount());
                            savingNonPRSSet.setWithdrawalAmount(saving.getWithdrawalAmount());
                            savingNonPRSSet.setUpdatedBy(request.getUsername());
                            savingNonPRSSet.setUpdatedDate(new Date());
                            SavingNonPRS savingNonPRSSave = savingService.saveSavingNonPRS(savingNonPRSSet);
                            if (savingNonPRSSave == null) {
                                response.setResponseCode(WebGuiConstant.RC_FAILED);
                                String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(labelFailed);
                                log.error("Submit Saving Non PRS Failed");
                            }
                        }
                    }
                }
                for (SubmitLoanNonPrsListResponse loanNPList : loanNonPrsId) {
                    SubmitLoanNonPrsListResponse loanNP = new SubmitLoanNonPrsListResponse();
                    loanNP.setLoanNonPrsId(loanNPList.getLoanNonPrsId());
                    loanNPIdList.add(loanNP);
                }
                response.setLoanListNonPrsId(loanNPIdList);
            }
        } catch (Exception e) {
            log.error("submitNonPRS error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("submitNonPRS error: " + e.getMessage());
        } finally {
            try {
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_PRS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("submitNonPRS RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("submitNonPRS saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SUBMIT_NON_PRS_PHOTO_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    NonPrsPhotoResponse submitNonPrsPhoto(@RequestBody final NonPrsPhotoRequest request,
                                          @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final NonPrsPhotoResponse response = new NonPrsPhotoResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("submitNonPrsPhoto Try to processing Submit Collection Photo request: " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("trying to submit collection photo ");
                LoanNonPRS loanNonPrs = loanService.getLoanNonPRSById(Long.parseLong(request.getLoanNonPrsId()));
                if (loanNonPrs != null) {
                    NonPRSPhoto nonPrsPhoto = loanService.getNonPRSPhoto(loanNonPrs.getLoanNonPRSId());
                    if (nonPrsPhoto != null) {
                        nonPrsPhoto.setNonPrsPhoto(request.getNonPrsPhoto().getBytes());
                        loanService.saveNonPrsPhoto(nonPrsPhoto);
                    } else {
                        nonPrsPhoto = new NonPRSPhoto();
                        nonPrsPhoto.setNonPrsId(loanNonPrs.getLoanNonPRSId());
                        nonPrsPhoto.setNonPrsPhoto(request.getNonPrsPhoto().getBytes());
                        loanService.saveNonPrsPhoto(nonPrsPhoto);
                    }
                } else {
                    log.error("UNKNOWN LOAN ID");
                    response.setResponseCode(WebGuiConstant.UNKNOWN_LOAN_ID);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                }
            }
        } catch (Exception e) {
            log.error("submitNonPrsPhoto error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("submitNonPrsPhoto error: " + e.getMessage());
        } finally {
            try {
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_DISBURSEMENT_PHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("submitNonPrsPhoto RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_APPROVAL_NON_PRS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    ApprovalNonPRSResponse approvalNonPRS(@RequestBody final ApprovalNonPRSRequest request,
                                          @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final ApprovalNonPRSResponse response = new ApprovalNonPRSResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("nonPrsApproval INCOMING APPROVAL COLLECTION MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("trying to approve Collection ");
                User user_role = userService.findUserByUsername(request.getUserMs());
                User user_login = userService.findUserByUsername(request.getUsername());
                if (user_role == null && user_login == null) {
                    log.error("UNAUTHORIZED USER");
                    response.setResponseCode(WebGuiConstant.USER_CANNOT_APPROVE);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    return response;
                }
                if (!user_role.getOfficeCode().equals(user_login.getOfficeCode())) {
                    response.setResponseCode(WebGuiConstant.USER_CANNOT_APPROVE);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    return response;
                }
                if ("1".equals(user_role.getRoleUser())) {
                    response.setResponseCode(WebGuiConstant.USER_CANNOT_APPROVE);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    return response;
                }
                ESBSubmitNonPrsRequest forESB = new ESBSubmitNonPrsRequest();
                forESB.setImei(request.getImei());
                forESB.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
                forESB.setTransmissionDateAndTime(request.getTransmissionDateAndTime());
                List<String> usernameList = new ArrayList<>();
                List<ESBCustomerNonPrsRequest> customerForESBList = new ArrayList<>();
                LoanNonPRS loanNonPrs = new LoanNonPRS();
                for (CustomerForNonPrsRequest customer : request.getCustomerList()) {
                    ESBCustomerNonPrsRequest customerForESB = new ESBCustomerNonPrsRequest();
                    Customer customerId = customerService.findById(customer.getCustomerId());
                    List<ESBLoanNonPrsRequest> loanForESBList = new ArrayList<>();
                    for (LoanNonPrsRequest loan : customer.getLoanList()) {
                        String user = new String();
                        ESBLoanNonPrsRequest loanForESB = new ESBLoanNonPrsRequest();
                        loanNonPrs = loanService.getLoanNonPRSById(Long.parseLong(loan.getLoanNonPrsId()));
                        if (loanNonPrs != null) {
                            if (loan.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                                loanNonPrs.setStatus(loan.getStatus());
                                loanNonPrs.setUpdatedBy(request.getUserMs());
                                loanNonPrs.setUpdatedDate(new Date());
                                LoanNonPRS loanNonPRSSave = loanService.saveLoanNonPRS(loanNonPrs);
                                if (loanNonPRSSave == null) {
                                    response.setResponseCode(WebGuiConstant.RC_FAILED);
                                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                                    response.setResponseMessage(labelFailed);
                                    log.info("Rejected Loan Non PRS Failed");
                                    return response;
                                }
                            }
                            if (loanNonPrs.getStatus().equals(WebGuiConstant.STATUS_SUBMIT)) {
                                log.info("APPID TO APPROVE : " + loanNonPrs.getLoanId().getAppId());
                                loanForESB.setAppid(loanNonPrs.getLoanId().getAppId());
                                if (loanNonPrs.getInstallmentPaymentAmount() != null) {
                                    loanForESB.setInstallmentPaymentAmount(
                                            loanNonPrs.getInstallmentPaymentAmount().toString().replace(".", ""));
                                    if (loanForESB.getInstallmentPaymentAmount().equals("000"))
                                        loanForESB.setInstallmentPaymentAmount("");
                                }
                                log.info(loanNonPrs.getLoanId().getAppId());
                                loanForESBList.add(loanForESB);
                            }
                        }
                        log.info(loanNonPrs.getLoanNonPRSId());
                        log.info(loanForESBList.isEmpty());
                        log.info(loanForESBList.size());
                        user = loanNonPrs.getCreatedBy();
                        usernameList.add(user);
                    }
                    if (!loanNonPrs.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                        customerForESB.setCustomerId(customerId.getProsperaId());
                        customerForESB.setLoanList(loanForESBList.toArray(new ESBLoanNonPrsRequest[0]));
                        List<ESBSavingNonPrsRequest> savingForESBList = new ArrayList<>();
                        for (SavingNonPrsRequest saving : customer.getSavingList()) {
                            ESBSavingNonPrsRequest savingForESB = new ESBSavingNonPrsRequest();
                            SavingNonPRS savingNonPrs = savingService
                                    .getSavingNonPrsById(Long.parseLong(saving.getSavingNonPrsId()));
                            if (savingNonPrs != null) {
                                savingForESB.setAccountNumber(savingNonPrs.getAccountNumber());
                                if (savingNonPrs.getDepositAmount() != null) {
                                    savingForESB.setDepositAmount(savingNonPrs.getDepositAmount().toString().replace(".", ""));
                                    if (savingForESB.getDepositAmount().equals("000"))
                                        savingForESB.setDepositAmount("");
                                }
                                if (savingNonPrs.getWithdrawalAmount() != null) {
                                    savingForESB.setWithdrawalAmount(
                                            savingNonPrs.getWithdrawalAmount().toString().replace(".", ""));
                                    if (savingForESB.getWithdrawalAmount().equals("000"))
                                        savingForESB.setWithdrawalAmount("");
                                }
                            }
                            savingForESBList.add(savingForESB);
                        }
                        customerForESB.setSavingList(savingForESBList.toArray(new ESBSavingNonPrsRequest[0]));
                        customerForESBList.add(customerForESB);
                    }
                }
                forESB.setUsername(usernameList.get(0));
                forESB.setCustomerList(customerForESBList.toArray(new ESBCustomerNonPrsRequest[0]));
                forESB.setTokenChallenge(request.getTokenChallenge());
                forESB.setTokenResponse(request.getTokenResponse());
                try {
                    log.info("INCOMING LOGIN MESSAGE : " + jsonUtils.toJson(request));
                    ProsperaLoginRequest prosperaLogin = new ProsperaLoginRequest();
                    prosperaLogin.setUsername(request.getUserMs().trim());
                    try {
                        prosperaLogin.setPassword(prosperaEncryptionService.encryptField(request.getPassword().trim()));
                    } catch (Exception e1) {
                        log.error("PASSWORD ENCRYPTION ERROR..., " + e1.getMessage());
                    }
                    prosperaLogin.setImei(request.getImei().trim());
                    prosperaLogin.setTransmissionDateAndTime(request.getTransmissionDateAndTime().trim());
                    prosperaLogin.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber().trim());
                    log.info("Try to send login request");
                    ProsperaLoginResponse prosperaLoginResponse = restClient.login(prosperaLogin);
                    log.info("Get Response from ESB : " + prosperaLoginResponse.getResponseCode());
                    response.setResponseCode(prosperaLoginResponse.getResponseCode());
                    response.setResponseMessage(prosperaLoginResponse.getResponseMessage());
                    if (!prosperaLoginResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                        response.setResponseCode(prosperaLoginResponse.getResponseCode());
                        String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                        response.setResponseMessage(labelFailed);
                        return response;
                    }
                    ESBSubmitNonPrsResponse submitNonPrsResponse = restClient.submitNonPrs(forESB);
                    //Jika RC dari Prospera 00 maka di proses seperti biasa
                    //Jika RC dari Prospera 09 di proses seperti biasa, namun business date +1, dan rc ke android menjadi 00
                    if (submitNonPrsResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS) || submitNonPrsResponse.getResponseCode().equals("09")) {
                        //cut off flag to set business date to +1
                        Boolean cutOff = false;
                        if (submitNonPrsResponse.getResponseCode().equals("09"))
                            cutOff = true;
                        //Date configuration for current and current+1
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(new Date());
                        Date currDate = cal.getTime();
                        cal.add(Calendar.DATE, 1);
                        Date currDatePlusOne = cal.getTime();
                        MutationTrxTabel mutationTrxTabel = new MutationTrxTabel();
                        mutationTrxTabel.setApprovedBy(request.getUserMs());
                        mutationTrxTabel.setApprovedDate(new Date());
                        mutationTrxTabel.setCreatedBy(request.getUsername());
                        mutationTrxTabel.setTrxType("Penagihan");
                        if (cutOff) {
                            mutationTrxTabel.setBusinessDate(currDatePlusOne);
                        } else {
                            mutationTrxTabel.setBusinessDate(currDate);
                        }
                        BigDecimal totalCollect = new BigDecimal(0);
                        ReportCollection reportCollection = new ReportCollection();
                        for (ESBCustomerNonPrsRequest esbCustomerResponse : forESB.getCustomerList()) {
                            List<BigDecimal> inputMoneyCustomer = new ArrayList<>();
                            for (ESBLoanNonPrsRequest esbLoanResponse : esbCustomerResponse.getLoanList()) {
                                Loan loan = loanService.getLoanByAppId(esbLoanResponse.getAppid());
                                loanNonPrs = loanService.getLoanNonPRSByLoanId(loan);
                                loanNonPrs.setUpdatedBy(request.getUserMs());
                                loanNonPrs.setUpdatedDate(new Date());
                                loanNonPrs.setStatus(WebGuiConstant.STATUS_APPROVED);
                                LoanNonPRS loanNonPRSSave = loanService.saveLoanNonPRS(loanNonPrs);
                                reportCollection.setAppId(loan.getAppId());
                                if (loanNonPrs.getNextCommitmentDate() != null)
                                    reportCollection.setCommitmentDate(loanNonPrs.getNextCommitmentDate());
                                reportCollection.setPaymentDate(loanNonPrs.getSubmitDate());
                                reportCollection.setOutstandingAmount(loanNonPrs.getOutstandingAmount());
                                if (loanNonPrs.getPeopleMeet() != null)
                                    reportCollection.setPeopleMeet(loanNonPrs.getPeopleMeet().getParameterId());
                                if (loanNonPRSSave == null) {
                                    response.setResponseCode(WebGuiConstant.RC_FAILED);
                                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                                    response.setResponseMessage(labelFailed);
                                    log.error("Approval Loan Non PRS Failed");
                                }
                                ReportTrxTable reportTrxLoanTable = new ReportTrxTable();
                                reportTrxLoanTable.setAppId(loan.getAppId());
                                reportTrxLoanTable.setApprovedBy(request.getUserMs());
                                reportTrxLoanTable.setCreatedBy(request.getUsername());
                                reportTrxLoanTable.setApprovedDate(currDate);
                                if (cutOff)
                                    reportTrxLoanTable.setBusinessDate(currDatePlusOne);
                                else
                                    reportTrxLoanTable.setBusinessDate(currDate);
                                if (loanNonPrs.getInstallmentPaymentAmount() != null) {
                                    if (loanNonPrs.getInstallmentPaymentAmount().compareTo(BigDecimal.ZERO) != 0) {
                                        reportTrxLoanTable.setTypeTrx("2");
                                        reportTrxLoanTable.setAmount(loanNonPrs.getInstallmentPaymentAmount());
                                        reportCollection.setInstallmentPaymentAmount(loanNonPrs.getInstallmentPaymentAmount());
                                        prsService.saveToReport(reportTrxLoanTable);
                                        prsService.saveToReportCollection(reportCollection);
                                    }
                                }
                                inputMoneyCustomer.add(loanNonPrs.getInputMoney());
                            }
                            for (ESBSavingNonPrsRequest savingCollection : esbCustomerResponse.getSavingList()) {
                                ReportTrxTable reportTrxSavingTable = new ReportTrxTable();
                                reportTrxSavingTable.setAccountNumber(savingCollection.getAccountNumber());
                                reportTrxSavingTable.setApprovedBy(request.getUserMs());
                                reportTrxSavingTable.setCreatedBy(request.getUsername());
                                reportTrxSavingTable.setApprovedDate(currDate);
                                if (cutOff)
                                    reportTrxSavingTable.setBusinessDate(currDatePlusOne);
                                else
                                    reportTrxSavingTable.setBusinessDate(currDate);
                                if (savingCollection.getWithdrawalAmount() != null) {
                                    reportTrxSavingTable.setTypeTrx("4");
                                    if (NumberUtils.isNumber(savingCollection.getWithdrawalAmount())) {
                                        if (new BigDecimal(savingCollection.getWithdrawalAmount()).compareTo(BigDecimal.ZERO) != 0) {
                                            reportTrxSavingTable.setAmount(new BigDecimal(savingCollection.getWithdrawalAmount().substring(0, savingCollection.getWithdrawalAmount().length() - 2)));
                                            prsService.saveToReport(reportTrxSavingTable);
                                        }
                                    }
                                }
                                if (savingCollection.getDepositAmount() != null) {
                                    reportTrxSavingTable.setTypeTrx("5");
                                    if (NumberUtils.isNumber(savingCollection.getDepositAmount())) {
                                        if (new BigDecimal(savingCollection.getDepositAmount()).compareTo(BigDecimal.ZERO) != 0) {
                                            reportTrxSavingTable.setAmount(new BigDecimal(savingCollection.getDepositAmount().substring(0, savingCollection.getDepositAmount().length() - 2)));
                                            prsService.saveToReport(reportTrxSavingTable);
                                        }
                                    }
                                }
                            }
                            totalCollect = totalCollect.add(inputMoneyCustomer.get(0));
                        }
                        mutationTrxTabel.setTotalCollect(totalCollect);
                        prsService.saveMutation(mutationTrxTabel);
                    } else {
                        response.setResponseCode(submitNonPrsResponse.getResponseCode());
                        response.setResponseMessage(submitNonPrsResponse.getResponseMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (NumberFormatException e) {
            log.error("nonPrsApproval error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("nonPrsApproval error: " + e.getMessage());
        } finally {
            try {
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_APPROVAL_PRS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("nonPrsApproval RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("nonPrsApproval saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_REPORT_COLLECTION_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    ReportCollectionResponse getReportCollection(@RequestBody final ReportCollectionRequest request,
                                                 @PathVariable("apkVersion") String apkVersion) throws IOException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final ReportCollectionResponse response = new ReportCollectionResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("getReportCollection Try to processing approval Get Report Collection request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("trying to get Collection Data");
                List<RptCollectList> rptListTemp = new ArrayList<RptCollectList>();
                Customer customer = customerService.findById(request.getCustomerId());
                if (customer != null) {
                    List<Loan> loanList = loanService.findByCustomer(customer);
                    for (Loan loan : loanList) {
                        List<ReportCollection> collectionData = prsService.findReportByAppIdCollection(loan.getAppId());
                        for (ReportCollection rptColl : collectionData) {
                            RptCollectList rptCollSet = new RptCollectList();
                            rptCollSet.setRptId(rptColl.getReportId().toString());
                            if (rptColl.getInstallmentPaymentAmount() != null) {
                                rptCollSet.setAMount(rptColl.getInstallmentPaymentAmount());
                            } else {
                                rptCollSet.setAMount(rptColl.getInstallmentPaymentAmount().ZERO);
                            }
                            if (rptColl.getOutstandingAmount() != null) {
                                rptCollSet.setAmountToCollect(rptColl.getOutstandingAmount());
                            } else {
                                rptCollSet.setAmountToCollect(rptColl.getOutstandingAmount().ZERO);
                            }
                            if (rptColl.getCommitmentDate() != null) {
                                rptCollSet.setCommitDate(formatter.format(rptColl.getCommitmentDate()));
                            } else {
                                rptCollSet.setCommitDate("");
                            }
                            if (rptColl.getPaymentDate() != null) {
                                rptCollSet.setVisitTime(formatter.format(rptColl.getPaymentDate()));
                            } else {
                                rptCollSet.setVisitTime("");
                            }
                            if (rptColl.getPeopleMeet() != null) {
                                PRSParameter prspPeople = parameterService.findByParameterId(rptColl.getPeopleMeet());
                                if (prspPeople != null && prspPeople.getParamValueString() != null) {
                                    rptCollSet.setPeopleMeet(prspPeople.getParamValueString());
                                } else {
                                    rptCollSet.setPeopleMeet("");
                                }
                            } else {
                                rptCollSet.setPeopleMeet("");
                            }
                            rptListTemp.add(rptCollSet);
                        }
                    }
                    if (rptListTemp.isEmpty()) {
                        response.setRptList(new ArrayList<RptCollectList>(0));
                    } else {
                        response.setRptList(rptListTemp);
                    }
                } else {
                    response.setResponseCode(WebGuiConstant.UNKNOWN_CUSTOMER_ID);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                }
            }
        } catch (Exception e) {
            log.error("getReportCollection error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("getReportCollection error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_REPORT_COLLECTION_REQUEST);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("getReportCollection RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("getReportCollection saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }
}
