package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.prs.MutationTrxTabel;
import id.co.telkomsigma.btpns.mprospera.model.prs.ReportCollection;
import id.co.telkomsigma.btpns.mprospera.model.prs.ReportTrxTable;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.*;
import id.co.telkomsigma.btpns.mprospera.response.ApprovalNonPRSResponse;
import id.co.telkomsigma.btpns.mprospera.response.ESBSubmitNonPrsResponse;
import id.co.telkomsigma.btpns.mprospera.response.ProsperaLoginResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Controller("webServiceNewApprovalCollectionController")
public class WebServiceNewApprovalCollectionController extends GenericController {

    JsonUtils jsonUtils = new JsonUtils();

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private UserService userService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private SavingService savingService;

    @Autowired
    private ProsperaEncryptionService prosperaEncryptionService;

    @Autowired
    private RESTClient restClient;

    @Autowired
    private PRSService prsService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    @RequestMapping(value = WebGuiConstant.TERMINAL_APPROVAL_NON_PRS_V2_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    ApprovalNonPRSResponse approvalNonPRSV2(@RequestBody final ApprovalNonPRSRequest request,
                                            @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final ApprovalNonPRSResponse response = new ApprovalNonPRSResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("nonPrsApprovalV2 INCOMING APPROVAL COLLECTION V2 MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("trying to approve Collection ");
                User user_role = userService.findUserByUsername(request.getUserMs());
                User user_login = userService.findUserByUsername(request.getUsername());
                if (user_role == null && user_login == null) {
                    log.error("UNAUTHORIZED USER");
                    response.setResponseCode(WebGuiConstant.USER_CANNOT_APPROVE);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    return response;
                }
                if (!user_role.getOfficeCode().equals(user_login.getOfficeCode())) {
                    response.setResponseCode(WebGuiConstant.UNREGISTERED_USER);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    return response;
                }
                if (request.getUserMs().equals(request.getUsername())) {
                    response.setResponseCode(WebGuiConstant.USER_CANNOT_APPROVE);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    return response;
                }
                ESBSubmitNonPrsRequest forESB = new ESBSubmitNonPrsRequest();
                forESB.setImei(request.getImei());
                forESB.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
                forESB.setTransmissionDateAndTime(request.getTransmissionDateAndTime());
                List<String> usernameList = new ArrayList<>();
                List<ESBCustomerNonPrsRequest> customerForESBList = new ArrayList<>();
                LoanNonPRS loanNonPrs = new LoanNonPRS();
                for (CustomerForNonPrsRequest customer : request.getCustomerList()) {
                    ESBCustomerNonPrsRequest customerForESB = new ESBCustomerNonPrsRequest();
                    Customer customerId = customerService.findById(customer.getCustomerId());
                    List<ESBLoanNonPrsRequest> loanForESBList = new ArrayList<>();
                    for (LoanNonPrsRequest loan : customer.getLoanList()) {
                        String user = new String();
                        ESBLoanNonPrsRequest loanForESB = new ESBLoanNonPrsRequest();
                        loanNonPrs = loanService.getLoanNonPRSById(Long.parseLong(loan.getLoanNonPrsId()));
                        if (loanNonPrs != null) {
                            if(loanNonPrs.getStatus().equals(WebGuiConstant.STATUS_APPROVED)){
                                response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                                String labelFailed = getMessage("webservice.rc.label." + WebGuiConstant.RC_ALREADY_APPROVED);
                                response.setResponseMessage(labelFailed);
                                return response;
                            }
                            if (loan.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                                loanNonPrs.setStatus(loan.getStatus());
                                loanNonPrs.setUpdatedBy(request.getUserMs());
                                loanNonPrs.setUpdatedDate(new Date());
                                LoanNonPRS loanNonPRSSave = loanService.saveLoanNonPRS(loanNonPrs);
                                if (loanNonPRSSave == null) {
                                    response.setResponseCode(WebGuiConstant.RC_FAILED);
                                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                                    response.setResponseMessage(labelFailed);
                                    return response;
                                }
                            }
                            if (loanNonPrs.getStatus().equals(WebGuiConstant.STATUS_SUBMIT)) {
                                log.info("APPID TO APPROVE : " + loanNonPrs.getLoanId().getAppId());
                                loanForESB.setAppid(loanNonPrs.getLoanId().getAppId());
                                if (loanNonPrs.getInstallmentPaymentAmount() != null) {
                                    loanForESB.setInstallmentPaymentAmount(
                                            loanNonPrs.getInstallmentPaymentAmount().toString().replace(".", ""));
                                    if (loanForESB.getInstallmentPaymentAmount().equals("000"))
                                        loanForESB.setInstallmentPaymentAmount("");
                                }
                                log.info(loanNonPrs.getLoanId().getAppId());
                                loanForESBList.add(loanForESB);
                            }
                        }
                        log.info(loanNonPrs.getLoanNonPRSId());
                        log.info(loanForESBList.isEmpty());
                        log.info(loanForESBList.size());
                        user = loanNonPrs.getCreatedBy();
                        usernameList.add(user);
                    }
                    if (!loanNonPrs.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                        customerForESB.setCustomerId(customerId.getProsperaId());
                        customerForESB.setLoanList(loanForESBList.toArray(new ESBLoanNonPrsRequest[0]));
                        List<ESBSavingNonPrsRequest> savingForESBList = new ArrayList<>();
                        for (SavingNonPrsRequest saving : customer.getSavingList()) {
                            ESBSavingNonPrsRequest savingForESB = new ESBSavingNonPrsRequest();
                            SavingNonPRS savingNonPrs = savingService
                                    .getSavingNonPrsById(Long.parseLong(saving.getSavingNonPrsId()));
                            if (savingNonPrs != null) {
                                savingForESB.setAccountNumber(savingNonPrs.getAccountNumber());
                                if (savingNonPrs.getDepositAmount() != null) {
                                    savingForESB.setDepositAmount(savingNonPrs.getDepositAmount().toString().replace(".", ""));
                                    if (savingForESB.getDepositAmount().equals("000"))
                                        savingForESB.setDepositAmount("");
                                }
                                if (savingNonPrs.getWithdrawalAmount() != null) {
                                    savingForESB.setWithdrawalAmount(
                                            savingNonPrs.getWithdrawalAmount().toString().replace(".", ""));
                                    if (savingForESB.getWithdrawalAmount().equals("000"))
                                        savingForESB.setWithdrawalAmount("");
                                }
                            }
                            savingForESBList.add(savingForESB);
                        }
                        customerForESB.setSavingList(savingForESBList.toArray(new ESBSavingNonPrsRequest[0]));
                        customerForESBList.add(customerForESB);
                    }
                }

                forESB.setUsername(usernameList.get(0));
                forESB.setCustomerList(customerForESBList.toArray(new ESBCustomerNonPrsRequest[0]));
                forESB.setTokenChallenge(request.getTokenChallenge());
                forESB.setTokenResponse(request.getTokenResponse());
                try {
                    log.info("INCOMING LOGIN MESSAGE : " + jsonUtils.toJson(request));
                    ProsperaLoginRequest prosperaLogin = new ProsperaLoginRequest();
                    prosperaLogin.setUsername(request.getUserMs().trim());
                    try {
                        prosperaLogin.setPassword(prosperaEncryptionService.encryptField(request.getPassword().trim()));
                    } catch (Exception e1) {
                        log.error("PASSWORD ENCRYPTION ERROR..., " + e1.getMessage());
                    }
                    prosperaLogin.setImei(request.getImei().trim());
                    prosperaLogin.setTransmissionDateAndTime(request.getTransmissionDateAndTime().trim());
                    prosperaLogin.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber().trim());
                    log.info("Try to send login request");
                    ProsperaLoginResponse prosperaLoginResponse = restClient.login(prosperaLogin);
                    log.info("Get Response from ESB : " + prosperaLoginResponse.getResponseCode());
                    response.setResponseCode(prosperaLoginResponse.getResponseCode());
                    response.setResponseMessage(prosperaLoginResponse.getResponseMessage());
                    if (!prosperaLoginResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                        response.setResponseCode(prosperaLoginResponse.getResponseCode());
                        String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                        response.setResponseMessage(labelFailed);
                        return response;
                    }
                    ESBSubmitNonPrsResponse submitNonPrsResponse = restClient.submitNonPrs(forESB);
                    //Jika RC dari Prospera 00 maka di proses seperti biasa
                    //Jika RC dari Prospera 09 di proses seperti biasa, namun business date +1, dan rc ke android menjadi 00
                    if (submitNonPrsResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS) || submitNonPrsResponse.getResponseCode().equals("09")) {
                        //cut off flag to set business date to +1
                        Boolean cutOff = false;
                        if (submitNonPrsResponse.getResponseCode().equals("09"))
                            cutOff = true;
                        //Date configuration for current and current+1
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(new Date());
                        Date currDate = cal.getTime();
                        cal.add(Calendar.DATE, 1);
                        Date currDatePlusOne = cal.getTime();
                        MutationTrxTabel mutationTrxTabel = new MutationTrxTabel();
                        mutationTrxTabel.setApprovedBy(request.getUserMs());
                        mutationTrxTabel.setApprovedDate(new Date());
                        mutationTrxTabel.setCreatedBy(request.getUsername());
                        mutationTrxTabel.setTrxType("Penagihan");
                        if (cutOff) {
                            mutationTrxTabel.setBusinessDate(currDatePlusOne);
                        } else {
                            mutationTrxTabel.setBusinessDate(currDate);
                        }
                        BigDecimal totalCollect = new BigDecimal(0);
                        ReportCollection reportCollection = new ReportCollection();
                        for (ESBCustomerNonPrsRequest esbCustomerResponse : forESB.getCustomerList()) {
                            List<BigDecimal> inputMoneyCustomer = new ArrayList<>();
                            for (ESBLoanNonPrsRequest esbLoanResponse : esbCustomerResponse.getLoanList()) {
                                Loan loan = loanService.getLoanByAppId(esbLoanResponse.getAppid());
                                loanNonPrs = loanService.getLoanNonPRSByLoanId(loan);
                                loanNonPrs.setUpdatedBy(request.getUserMs());
                                loanNonPrs.setUpdatedDate(new Date());
                                loanNonPrs.setStatus(WebGuiConstant.STATUS_APPROVED);
                                LoanNonPRS loanNonPRSSave = loanService.saveLoanNonPRS(loanNonPrs);
                                reportCollection.setAppId(loan.getAppId());
                                if (loanNonPrs.getNextCommitmentDate() != null)
                                    reportCollection.setCommitmentDate(loanNonPrs.getNextCommitmentDate());
                                reportCollection.setPaymentDate(loanNonPrs.getSubmitDate());
                                reportCollection.setOutstandingAmount(loanNonPrs.getOutstandingAmount());
                                if (loanNonPrs.getPeopleMeet() != null)
                                    reportCollection.setPeopleMeet(loanNonPrs.getPeopleMeet().getParameterId());
                                if (loanNonPRSSave == null) {
                                    response.setResponseCode(WebGuiConstant.RC_FAILED);
                                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                                    response.setResponseMessage(labelFailed);
                                    log.error("Approval Loan Non PRS Failed");
                                }
                                ReportTrxTable reportTrxLoanTable = new ReportTrxTable();
                                reportTrxLoanTable.setAppId(loan.getAppId());
                                reportTrxLoanTable.setApprovedBy(request.getUserMs());
                                reportTrxLoanTable.setCreatedBy(request.getUsername());
                                reportTrxLoanTable.setApprovedDate(currDate);
                                if (cutOff)
                                    reportTrxLoanTable.setBusinessDate(currDatePlusOne);
                                else
                                    reportTrxLoanTable.setBusinessDate(currDate);
                                if (loanNonPrs.getInstallmentPaymentAmount() != null) {
                                    if (loanNonPrs.getInstallmentPaymentAmount().compareTo(BigDecimal.ZERO) != 0) {
                                        reportTrxLoanTable.setTypeTrx("2");
                                        reportTrxLoanTable.setAmount(loanNonPrs.getInstallmentPaymentAmount());
                                        reportCollection.setInstallmentPaymentAmount(loanNonPrs.getInstallmentPaymentAmount());
                                        prsService.saveToReport(reportTrxLoanTable);
                                        prsService.saveToReportCollection(reportCollection);
                                    }
                                }
                                inputMoneyCustomer.add(loanNonPrs.getInputMoney());
                            }
                            for (ESBSavingNonPrsRequest savingCollection : esbCustomerResponse.getSavingList()) {
                                ReportTrxTable reportTrxSavingTable = new ReportTrxTable();
                                reportTrxSavingTable.setAccountNumber(savingCollection.getAccountNumber());
                                reportTrxSavingTable.setApprovedBy(request.getUserMs());
                                reportTrxSavingTable.setCreatedBy(request.getUsername());
                                reportTrxSavingTable.setApprovedDate(currDate);
                                if (cutOff)
                                    reportTrxSavingTable.setBusinessDate(currDatePlusOne);
                                else
                                    reportTrxSavingTable.setBusinessDate(currDate);
                                if (savingCollection.getWithdrawalAmount() != null) {
                                    reportTrxSavingTable.setTypeTrx("4");
                                    if (NumberUtils.isNumber(savingCollection.getWithdrawalAmount())) {
                                        if (new BigDecimal(savingCollection.getWithdrawalAmount()).compareTo(BigDecimal.ZERO) != 0) {
                                            reportTrxSavingTable.setAmount(new BigDecimal(savingCollection.getWithdrawalAmount().substring(0, savingCollection.getWithdrawalAmount().length() - 2)));
                                            prsService.saveToReport(reportTrxSavingTable);
                                        }
                                    }
                                }
                                if (savingCollection.getDepositAmount() != null) {
                                    reportTrxSavingTable.setTypeTrx("5");
                                    if (NumberUtils.isNumber(savingCollection.getDepositAmount())) {
                                        if (new BigDecimal(savingCollection.getDepositAmount()).compareTo(BigDecimal.ZERO) != 0) {
                                            reportTrxSavingTable.setAmount(new BigDecimal(savingCollection.getDepositAmount().substring(0, savingCollection.getDepositAmount().length() - 2)));
                                            prsService.saveToReport(reportTrxSavingTable);
                                        }
                                    }
                                }
                            }
                            totalCollect = totalCollect.add(inputMoneyCustomer.get(0));
                        }
                        mutationTrxTabel.setTotalCollect(totalCollect);
                        prsService.saveMutation(mutationTrxTabel);
                    } else {
                        response.setResponseCode(submitNonPrsResponse.getResponseCode());
                        response.setResponseMessage(submitNonPrsResponse.getResponseMessage());
                        return response;
                    }
                } catch (KeyManagementException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            log.error("nonPrsApprovalV2 error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("nonPrsApprovalV2 error: " + e.getMessage());
        } finally {
            try {
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_APPROVAL_PRS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("nonPrsApprovalV2 RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("nonPrsApprovalV2 saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }
}
