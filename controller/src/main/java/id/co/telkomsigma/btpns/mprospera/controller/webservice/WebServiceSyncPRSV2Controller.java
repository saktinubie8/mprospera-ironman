package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.SentraRequest;
import id.co.telkomsigma.btpns.mprospera.request.SyncPRSRequest;
import id.co.telkomsigma.btpns.mprospera.response.PRSResponseList;
import id.co.telkomsigma.btpns.mprospera.response.SyncPRSResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceSyncPRSV2Controller")
public class WebServiceSyncPRSV2Controller extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ParameterService parameterService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private PRSService prsService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private SyncPRSService syncPRSService;

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_PRS_V2_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncPRSResponse syncPrsV2(@RequestBody final SyncPRSRequest request,
                              @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final SyncPRSResponse response = new SyncPRSResponse();
        //Set Response Sukses terlebih dulu
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("prs/sync/v2 INCOMING REQUEST SYNC PRS MESSAGE : " + jsonUtils.toJson(request));
            // Validasi messaging request dari Android
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Get PRS");
                List<PRSResponseList> prsListTemp = new ArrayList<>();
                //Set Sentra yang memiliki data - data saving/loan di PRS
                List<String> sentraHasPrsDataList = new ArrayList<>();
                log.info("START MAPPING DATA PRS");
                for (SentraRequest sentraList : request.getSentraList()) {
                    if (sentraList.getSentraId() != null) {
                        Sentra sentra = sentraService.findBySentraId(sentraList.getSentraId());
                        List<LoanPRS> sentraHasPrsByLoanPrs = prsService.getSentraHasPrsByLoanPrs(Long.parseLong(sentraList.getSentraId()));
                        List<SavingPRS> sentraHasPrsBySavingPrs = prsService.getSentraHasPrsBySavingPrs(Long.parseLong(sentraList.getSentraId()));
                        if (sentraHasPrsByLoanPrs != null && sentraHasPrsBySavingPrs != null) {
                            if (!sentraHasPrsByLoanPrs.isEmpty() && !sentraHasPrsBySavingPrs.isEmpty()) {
                                sentraHasPrsDataList.add(sentraList.getSentraId());
                            }
                        }
                        PRSResponseList prsResponseList = syncPRSService.setPrsData(request.getFetchPreviousPrs(), sentra);
                        if (prsResponseList.getSentraId() != null) {
                            prsListTemp.add(prsResponseList);
                        }
                    }
                }
                log.info("FINISH MAPPING DATA PRS");
                response.setPrsList(prsListTemp);
                response.setSentraHasPrsDataList(sentraHasPrsDataList);
            }
        } catch (NumberFormatException e) {
            log.error("prs/sync/v2 error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("prsSyncV2 error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_PRS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("prs/sync/v2 RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("prs/sync/v2 saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }
}
