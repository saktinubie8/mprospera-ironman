package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.model.user.UserAndSentra;
import id.co.telkomsigma.btpns.mprospera.request.AssignPSRequest;
import id.co.telkomsigma.btpns.mprospera.request.ESBAssignPSRequest;
import id.co.telkomsigma.btpns.mprospera.request.SentraForAssignPSRequest;
import id.co.telkomsigma.btpns.mprospera.request.UserPSRequest;
import id.co.telkomsigma.btpns.mprospera.response.AssignPsResponse;
import id.co.telkomsigma.btpns.mprospera.response.BaseResponse;
import id.co.telkomsigma.btpns.mprospera.response.ESBAssignPSResponse;
import id.co.telkomsigma.btpns.mprospera.response.UserPSResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class WebServiceAssignPSController extends GenericController {

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private RESTClient restClient;

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private UserService userService;

    @Autowired
    private SentraService sentraService;

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_ASSIGN_PS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    BaseResponse doRequest(@RequestBody AssignPSRequest request,
                           @PathParam("apkVersion") String versionNumber) throws KeyManagementException, NoSuchAlgorithmException, Exception {

        BaseResponse response = new BaseResponse();
        Terminal terminal = new Terminal();
        try {
            log.info("assignPs INCOMING ASSIGN PS MESSAGE : " + jsonUtils.toJson(request));
            // Create terminal
            if (terminalService.loadTerminalByImei(request.getImei().trim()) == null) {
                log.error("TERMINAL NOT FOUND...");
                response.setResponseCode("01");
                response.setResponseMessage("Unknown terminal");
                log.info("RESPONSE MESSAGE : " + response);
                return response;
            } else {
                log.info("TERMINAL FOUND");
                terminal = terminalService.loadTerminalByImei(request.getImei().trim());
            }
            log.info("Mapping data for ESB");
            ESBAssignPSRequest esbRequest = new ESBAssignPSRequest();
            esbRequest.setUsername(request.getUserPs().trim());
            esbRequest.setImei(request.getImei().trim());
            esbRequest.setTransmissionDateAndTime(request.getTransmissionDateAndTime().trim());
            esbRequest.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber().trim());
            esbRequest.setApprovedBy(request.getUsername().trim());
            List<SentraForAssignPSRequest> sentraList = new ArrayList<>();
            for (SentraForAssignPSRequest sentra : request.getSentraList()) {
                Sentra sentraFromDb = sentraService.findBySentraId(sentra.getSentraId());
                SentraForAssignPSRequest sentraForESB = new SentraForAssignPSRequest();
                if (sentraFromDb != null) {
                    sentraForESB.setSentraId(sentraFromDb.getProsperaId());
                    sentraList.add(sentraForESB);
                }
            }
            esbRequest.setSentraList(sentraList);

            log.info("Send Data to Prospera via ESB");
            // access url and get the response
            ESBAssignPSResponse esbResponse = restClient.assignPS(esbRequest);
            if (!esbResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(esbResponse.getResponseCode());
                response.setResponseMessage(esbResponse.getResponseMessage());
            }
            response.setResponseCode(esbResponse.getResponseCode());
            response.setResponseMessage(esbResponse.getResponseMessage());
            log.info("Updating sentra data");
            for (SentraForAssignPSRequest sentraListFromESB : sentraList) {
                Sentra sentraFromDb = sentraService.findByProsperaId(sentraListFromESB.getSentraId());
                if (sentraFromDb != null) {
                    log.info("Sentra Code : " + sentraFromDb.getSentraCode());
                    log.info("Last PRS : " + sentraFromDb.getLastPrs());
                    sentraFromDb.setAssignedTo(request.getUserPs());
                    sentraService.save(sentraFromDb);
                }
            }
        } catch (Exception e) {
            log.error("assignPs error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("assignPs error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_ASSIGN_PS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("assignPs RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("assignPs saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_USER_PS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    UserPSResponse syncUserPs(@RequestBody final UserPSRequest request,
                              @PathVariable("apkVersion") String versionNumber) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final UserPSResponse response = new UserPSResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("sync/userPs Try to processing SYNC User Sentra Mapping request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, versionNumber);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
                log.info("RESPONSE MESSAGE : " + response);
                return response;
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Validation success, get user ps");
                User userMS = (User) userService.loadUserByUsername(request.getUsername());
                log.info("Starting mapping message");
                List<Sentra> sentraList = sentraService.getSentraByWismaId(userMS.getOfficeCode());
                List<AssignPsResponse> userSentraList = new ArrayList<>();
                for (Sentra sentra : sentraList) {
                    AssignPsResponse sentraMap = new AssignPsResponse();
                    sentraMap.setSentraId(sentra.getSentraId().toString());
                    sentraMap.setSentraName(sentra.getSentraName());
                    sentraMap.setAssignedTo(sentra.getAssignedTo());
                    if (sentra.getAssignedTo() != null) {
                        if (!sentra.getAssignedTo().equals("")) {
                            User user = userService.findUserByUsername(sentra.getAssignedTo().trim());
                            if (user != null) {
                                if (user.getRoleUser() != null) {
                                    if (user.getRoleUser().equals("2")) {
                                        sentraMap.setAssignedTo("");
                                        sentraMap.setUserFullName("");
                                        sentraMap.setStatus("");
                                    } else {
                                        if (user.isEnabled()) {
                                            sentraMap.setUserFullName(user.getName());
                                        }
                                    }
                                } else {
                                    sentraMap.setAssignedTo("");
                                    sentraMap.setUserFullName("");
                                }
                            } else {
                                sentraMap.setUserFullName("");
                            }
                        } else {
                            sentraMap.setAssignedTo("");
                            sentraMap.setUserFullName("");
                        }
                    } else {
                        sentraMap.setAssignedTo("");
                        sentraMap.setUserFullName("");
                    }
                    userSentraList.add(sentraMap);
                }
                List<UserAndSentra> userAndSentraList = userService.getUserAndSentraByLocationId(userMS.getOfficeCode());
                for (UserAndSentra user : userAndSentraList) {
                    AssignPsResponse sentraMap = new AssignPsResponse();
                    sentraMap.setAssignedTo(user.getUsername());
                    sentraMap.setUserFullName(user.getName());
                    userSentraList.add(sentraMap);
                }
                response.setSentraList(userSentraList);
            }
            log.info("Finishing mapping message");
        } catch (Exception e) {
            log.error("sync/userPs error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("sync/userPs error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_USERPS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("sync/userPs saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }
}
