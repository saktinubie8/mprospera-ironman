package id.co.telkomsigma.btpns.mprospera.util;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.vfs.VirtualFile;
import org.slf4j.Marker;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class InfoHelper {

    protected final Log log = LogFactory.getLog(getClass());
    private static InfoHelper instance = null;

    @Value("${build.branch}")
    private String buildBranch;

    @Value("${build.changes}")
    private String buildChanges;

    @Value("${build.version}")
    private String buildVersion;


    public static InfoHelper getInstance() {
        if (null == instance) instance = new InfoHelper();
        return instance;
    }

    public String getInfo(Object klas) {
        StringBuffer rsp = new StringBuffer();
        try {
            rsp.append("{");
            rsp.append("\"lastBuild\":\"".concat(new SimpleDateFormat("dd-MM-YYYY HH:mm:ss").format(getClassBuildTime(klas))).concat("\","));
            rsp.append("\"version\":\"".concat(buildVersion).concat("\","));
            rsp.append("\"branch\":\"".concat(buildBranch).concat("\","));
            rsp.append("\"changes\":\"".concat(buildChanges).concat("\""));
            rsp.append("}");
        } catch (Exception e) {
            log.error(Marker.ANY_MARKER, e);
        }
        log.info("#>getInfo response: " + rsp.toString());
        return rsp.toString();
    }


    private Date getClassBuildTime(Object klas) {
        Date d = null;
        Class<?> currentClass = klas.getClass(); //  new Object(){}.getClass().getEnclosingClass();
        URL resource = currentClass.getResource(currentClass.getSimpleName() + ".class");
        if (resource != null) {
            if (resource.getProtocol().equals("file")) {
                try {
                    d = new Date(new File(resource.toURI()).lastModified());
                } catch (URISyntaxException e) {
                    log.error(Marker.ANY_MARKER, e);
                }
            } else if (resource.getProtocol().equals("vfs")) {
                try {
                    VirtualFile vf = (VirtualFile) resource.getContent();
                    d = new Date(vf.getLastModified());
                } catch (Exception e) {
                    log.error(Marker.ANY_MARKER, e);
                }
            } else if (resource.getProtocol().equals("jar")) {
                String path = resource.getPath();
                d = new Date(new File(path.substring(5, path.indexOf("!"))).lastModified());
            } else if (resource.getProtocol().equals("zip")) {
                String path = resource.getPath();
                File jarFileOnDisk = new File(path.substring(0, path.indexOf("!")));
                try (JarFile jf = new JarFile(jarFileOnDisk)) {
                    ZipEntry ze = jf.getEntry(path.substring(path.indexOf("!") + 2));//Skip the ! and the /
                    long zeTimeLong = ze.getTime();
                    Date zeTimeDate = new Date(zeTimeLong);
                    d = zeTimeDate;
                } catch (IOException | RuntimeException e) {
                    log.error(Marker.ANY_MARKER, e);
                }
            }
        }
        return d;
    }

}
