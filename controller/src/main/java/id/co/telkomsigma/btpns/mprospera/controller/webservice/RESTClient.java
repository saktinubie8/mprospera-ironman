package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import com.fasterxml.jackson.core.JsonParseException;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.saf.Saf;
import id.co.telkomsigma.btpns.mprospera.request.*;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.net.ssl.*;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Component("restClient")
public class RESTClient {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    private String HOST;
    private String PORT;
    private String MAIN_URI;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private SafService safService;

    @Autowired
    ParameterService parameterService;

    @PostConstruct
    protected void init() {
        HOST = parameterService.loadParamByParamName("esb.host", "https://10.7.17.163");
        PORT = parameterService.loadParamByParamName("esb.port", "8443");
        MAIN_URI = parameterService.loadParamByParamName("esb.uri", "/btpns/mprospera");
    }

    public Boolean echoSuccess() {
        String url = HOST + ":" + PORT + MAIN_URI + "/echo";
        RestTemplate restTemplate = getRestTemplate();
        try {
            LOGGER.debug("SEND ECHO...");
            ResponseEntity<String> response = restTemplate.postForEntity(url, null, String.class);
            LOGGER.debug("ECHO SUCCESS...");
            return response.getStatusCode().is2xxSuccessful();
        } catch (HttpClientErrorException e) {
            HttpStatus httpStatus = e.getStatusCode();
            LOGGER.error("ECHO FAILED..., " + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
            return e.getStatusCode().is2xxSuccessful();
        } catch (Exception e) {
            LOGGER.error("ECHO FAILED..., " + e.getMessage());
            return false;
        }
    }

    public ProsperaLoginResponse login(ProsperaLoginRequest request)
            throws NoSuchAlgorithmException, KeyManagementException {
        ProsperaLoginResponse response = new ProsperaLoginResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/login";
            LOGGER.debug("POST URL : " + url);
            try {
                LOGGER.debug("Request payload : " + request.toString());
                response = getRestTemplate().postForObject(url, request, ProsperaLoginResponse.class);
                LOGGER.debug("Got response..., " + response.toString());
                return response;
            } catch (HttpClientErrorException e) {
                HttpStatus httpStatus = e.getStatusCode();
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("ProsperaLoginResponse ESB " + httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                return response;
            } catch (Exception e) {
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("login ESB Error: " + e.getMessage());
                return response;
            }
        } else {
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
            return response;
        }
    }

    public ESBSubmitPrsResponse submitPrs(ESBSubmitPrsRequest request)
            throws NoSuchAlgorithmException, KeyManagementException {
        ESBSubmitPrsResponse response = new ESBSubmitPrsResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/submit/prs";
            LOGGER.info("POST URL : " + url);
            try {
                JsonUtils jsonUtils = new JsonUtils();
                try {
                    LOGGER.info("MESSAGE TO ESB : " + jsonUtils.toJson(request));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                response = getRestTemplate().postForObject(url, request, ESBSubmitPrsResponse.class);
                LOGGER.info("Got response..., " + response.toString());
                return response;
            } catch (HttpClientErrorException e) {
                HttpStatus httpStatus = e.getStatusCode();
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("submitPrs ESB " + httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                return response;
            } catch (Exception e) {
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("submitPrs ESB Error: " + e.getMessage());
                return response;
            }
        } else {
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
            return response;
        }
    }

    public ESBSubmitNonPrsResponse submitNonPrs(ESBSubmitNonPrsRequest request)
            throws NoSuchAlgorithmException, KeyManagementException {
        ESBSubmitNonPrsResponse response = new ESBSubmitNonPrsResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/submit/nonprs";
            LOGGER.debug("POST URL : " + url);
            try {
                response = getRestTemplate().postForObject(url, request, ESBSubmitNonPrsResponse.class);
                LOGGER.debug("Got response..., " + response.toString());
                return response;
            } catch (HttpClientErrorException e) {
                HttpStatus httpStatus = e.getStatusCode();
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("submitNonPrs ESB " + httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                return response;
            } catch (Exception e) {
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("submitNonPrs ESB Error: " + e.getMessage());
                return response;
            }
        } else {
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
            return response;
        }
    }

    public ESBAssignPSResponse assignPS(ESBAssignPSRequest request)
            throws NoSuchAlgorithmException, KeyManagementException {
        ESBAssignPSResponse response = new ESBAssignPSResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/ps/assign";
            LOGGER.debug("POST URL : " + url);
            try {
                response = getRestTemplate().postForObject(url, request, ESBAssignPSResponse.class);
                LOGGER.debug("Got response..., " + response.toString());
                return response;
            } catch (HttpClientErrorException e) {
                HttpStatus httpStatus = e.getStatusCode();
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("assignPS ESB " + httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                return response;
            } catch (Exception e) {
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("assignPS ESB Error: " + e.getMessage());
                return response;
            }
        } else {
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
            return response;
        }
    }

    public SubmitInsuranceClaimResponse claimInsurance(SubmitInsuranceClaimRequest request) {
        SubmitInsuranceClaimResponse response = new SubmitInsuranceClaimResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/claim/insurance";
            LOGGER.debug("POST URL : " + url);
            try {
                response = getRestTemplate().postForObject(url, request, SubmitInsuranceClaimResponse.class);
                LOGGER.debug("Got response..., " + response.toString());
                return response;
            } catch (HttpClientErrorException e) {
                HttpStatus httpStatus = e.getStatusCode();
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("claimInsurance ESB " + httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                return response;
            } catch (Exception e) {
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("claimInsurance ESB Error: " + e.getMessage());
                return response;
            }
        } else {
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
            return response;
        }
    }

    public ESBTrxInquiryResponse trxInquiry(ESBTrxInquiryRequest request) {
        ESBTrxInquiryResponse response = new ESBTrxInquiryResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/trx/inquiry";
            LOGGER.debug("POST URL : " + url);
            try {
                response = getRestTemplate().postForObject(url, request, ESBTrxInquiryResponse.class);
                LOGGER.debug("Got response..., " + response.toString());
                return response;
            } catch (HttpClientErrorException e) {
                HttpStatus httpStatus = e.getStatusCode();
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("trxInquiry ESB " + httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                return response;
            } catch (Exception e) {
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("trxInquiry ESB Error: " + e.getMessage());
                return response;
            }
        } else {
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
            return response;
        }
    }

    protected void addSaf(String json, String method, String url, String rc, String rm) {
        LOGGER.debug("ADDING TO SAF..., " + json);
        Saf saf = new Saf();
        saf.setMethod(method);
        saf.setUrl(url);
        saf.setResponseCode(rc);
        saf.setResponseMessage(rm);
        saf.setCreatedDate(new Date());
        saf.setRawMessage(json.getBytes());
        try {
            safService.save(saf);
            LOGGER.debug("SAF IS SAVED SUCCESFULLY...");
        } catch (Exception e) {
            LOGGER.debug("FAIL SAVING SAF...");
        }
    }

    protected RestTemplate getRestTemplate() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
                    throws CertificateException {
            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
                    throws CertificateException {
            }
        }};
        // Install the all-trusting trust manager
        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
        } catch (NoSuchAlgorithmException e1) {
            LOGGER.debug("SSL INSTANCE FETCHING FAILED..., " + e1.getMessage());
        }
        try {
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            LOGGER.debug("SSL CONTEXT INITIALIZING FAILED..., " + e.getMessage());
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        final HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        RestTemplate restTemplate = new RestTemplate(new SimpleClientHttpRequestFactory() {
            @Override
            protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
                if (connection instanceof HttpsURLConnection) {
                    ((HttpsURLConnection) connection).setHostnameVerifier(allHostsValid);
                }
                super.prepareConnection(connection, httpMethod);
            }
        });
        return restTemplate;
    }

  //  @Scheduled(cron = "${saf.scheduler}")
    public void safScheduler() throws JsonParseException, IOException {
        try {
            Thread.sleep(new Random().nextInt(Integer.MAX_VALUE) * 10);
        } catch (InterruptedException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        if (echoSuccess()) {
            List<Saf> safs = safService.findAll();
            for (final Saf saf : safs) {
                LOGGER.debug("SAF RUNNING ==============================================================================");
                if (saf.getMethod().toUpperCase().equals("ADDSW")) {

                } else if (saf.getMethod().toUpperCase().equals("ADDSENTRA")) {

                } else if (saf.getMethod().toUpperCase().equals("ADDGROUP")) {

                } else if (saf.getMethod().toUpperCase().equals("ADDCUSTOMER")) {

                } else if (saf.getMethod().toUpperCase().equals("ADDLOAN")) {

                } else if (saf.getMethod().toUpperCase().equals("INPUTSURVEY")) {
                    String url = saf.getUrl();
                    ESBSurveyRequest request = new ESBSurveyRequest();
                    synchronized (request) {
                        LOGGER.debug("SAF == POST URL : " + url);
                        try {
                            request = (ESBSurveyRequest) new JsonUtils().fromJson(new String(saf.getRawMessage()), ESBSurveyRequest.class);
                        } catch (JsonParseException e1) {
                            LOGGER.debug("SAF == JSON PARSE FAILED, " + e1.getMessage());
                        } catch (IOException e1) {
                            LOGGER.debug("SAF == IO EXCEPTION, " + e1.getMessage());
                        }
                        try {
                            ESBSurveyResponse response = getRestTemplate().postForObject(url, request, ESBSurveyResponse.class);
                            LOGGER.debug("SAF == Got response..., " + response.toString());
                            if (response.getResponseCode().equals("00")) {
                                try {
                                    safService.delete(saf.getId());
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., SAF REMOVED...");
                                } catch (Exception e) {
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., FAILED TO REMOVE SAF");
                                }
                            } else
                                LOGGER.debug("SAF == MESSAGING FAILED..., FAILED TO REMOVE SAF");
                        } catch (HttpClientErrorException e) {
                            HttpStatus httpStatus = e.getStatusCode();
                            LOGGER.debug("SAF == MESSAGING FAILED..., RC: XX, RM: " + "ESB " + httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                        }
                    }
                }
                LOGGER.debug("SAF END ==================================================================================\n");
            }
        }
    }
}
