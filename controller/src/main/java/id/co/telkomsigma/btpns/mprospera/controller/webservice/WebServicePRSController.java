package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import id.co.telkomsigma.btpns.mprospera.constant.PRSConstant;
import id.co.telkomsigma.btpns.mprospera.request.*;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.util.DateExtractor;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.prs.DisbursementPhoto;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApproval;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApprovalAndHistory;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginApprovalHistory;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginLoanMap;
import id.co.telkomsigma.btpns.mprospera.model.prs.DiscountMarginOfficerMap;
import id.co.telkomsigma.btpns.mprospera.model.prs.EarlyTerminationPlan;
import id.co.telkomsigma.btpns.mprospera.model.prs.MutationTrxTabel;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRSPhoto;
import id.co.telkomsigma.btpns.mprospera.model.prs.ReportTrxTable;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwIdPhoto;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwSurveyPhoto;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.service.AreaService;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.CustomerService;
import id.co.telkomsigma.btpns.mprospera.service.LoanService;
import id.co.telkomsigma.btpns.mprospera.service.PRSService;
import id.co.telkomsigma.btpns.mprospera.service.ParameterService;
import id.co.telkomsigma.btpns.mprospera.service.ProsperaEncryptionService;
import id.co.telkomsigma.btpns.mprospera.service.SWService;
import id.co.telkomsigma.btpns.mprospera.service.SavingService;
import id.co.telkomsigma.btpns.mprospera.service.SentraService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

@Controller("webServicePRSController")
@Transactional
public class WebServicePRSController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private PRSService prsService;

    @Autowired
    private UserService userService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private ParameterService parameterService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private SavingService savingService;

    @Autowired
    private SWService swService;

    @Autowired
    private RESTClient restClient;

    @Autowired
    private ProsperaEncryptionService prosperaEncryptionService;

    @Autowired
    private AuditLogService auditLogService;

    @Value("${allow.submit:1}")
    private String allowSubmit;

    @Value("${allow.approve:1}")
    private String allowApprove;

    DateExtractor dateExtractor = new DateExtractor();

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    final DecimalFormat decimalFormat = new DecimalFormat("###");

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_LIST_PRS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    ListPRSResponse list(@RequestBody final ListPRSRequest request,
                         @PathVariable("apkVersion") String apkVersion) throws IOException {

        log.info("Try to processing List PRS Status request : " + jsonUtils.toJson(request));
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final ListPRSResponse response = new ListPRSResponse();
        // Set Response Sukses terlebih dulu
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        response.setResponseMessage("Sukses");
        // Validasi messaging request dari Android
        String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
        Date prsDate = new Date();
        try {
            prsDate = formatter.parse(request.getPrsDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
            response.setResponseCode(validation);
            String label = getMessage("webservice.rc.label." + response.getResponseCode());
            response.setResponseMessage(label);
            log.info("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                    + sessionKey);
        } else {
            List<ListStatusPRSResponse> prsListTemp = new ArrayList<>();
            List<PRS> prs = prsService.getPRSByPSAndStatus(request.getUsername(), prsDate);
            if (prs.size() > 0) {
                for (PRS listPrs : prs) {
                    ListStatusPRSResponse prsSet = new ListStatusPRSResponse();
                    prsSet.setSentraId(listPrs.getSentraId().getSentraId().toString());
                    prsSet.setPrsId(listPrs.getPrsId().toString());
                    prsSet.setStatusPrs(listPrs.getStatus());
                    prsSet.setUpdatedDate(listPrs.getUpdateDate() != null ? listPrs.getUpdateDate().toString() : "");
                    prsListTemp.add(prsSet);
                }
            }
            response.setSentraList(prsListTemp);
        }
        log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
        response.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        return response;
    }

    // Sinkronisasi PRS
    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_PRS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncPRSResponse syncPrs(@RequestBody final SyncPRSRequest request,
                            @PathVariable("apkVersion") String apkVersion) throws IOException {

        log.info("Try to processing sync PRS request");
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final SyncPRSResponse response = new SyncPRSResponse();
        // Set Response Sukses terlebih dulu
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("syncPrs INCOMING REQUEST SYNC PRS MESSAGE : " + jsonUtils.toJson(request));
            // Validasi messaging request dari Android
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Get PRS");
                String paramApkVersion = parameterService.loadParamByParamName(WebGuiConstant.PARAMETER_CURRENT_VERSION,
                        "1.0");
                boolean disbursementPlan = false;
                List<PRSResponseList> prsListTemp = new ArrayList<>();
                // Set Sentra yang memiliki data - data saving/loan di PRS
                List<String> sentraHasPrsDataList = new ArrayList<>();
                for (SentraRequest sentraList : request.getSentraList()) {
                    Sentra sentra = sentraService.findBySentraId(sentraList.getSentraId());
                    List<LoanPRS> sentraHasPrsByLoanPrs = prsService
                            .getSentraHasPrsByLoanPrs(Long.parseLong(sentraList.getSentraId()));
                    List<SavingPRS> sentraHasPrsBySavingPrs = prsService
                            .getSentraHasPrsBySavingPrs(Long.parseLong(sentraList.getSentraId()));
                    if (sentraHasPrsByLoanPrs != null && sentraHasPrsBySavingPrs != null) {
                        if (!sentraHasPrsByLoanPrs.isEmpty() && !sentraHasPrsBySavingPrs.isEmpty()) {
                            sentraHasPrsDataList.add(sentraList.getSentraId());
                        }
                    }
                    // Jika Fetch Previous PRS true
                    if (request.getFetchPreviousPrs().equals("true")) {
                        PRS prs = prsService.getPRSBySentraPrevious(sentra, new Date());
                        PRSResponseList prsSet = new PRSResponseList();
                        if (prs != null) {
                            if (prs.getStatus().equals("DRAFT") || prs.getStatus().equals("SUBMIT")
                                    || prs.getStatus().equals("IN_PROCESS")) {
                                prsSet.setPrsId(prs.getPrsId().toString());
                                prsSet.setSentraId(prs.getSentraId().getSentraId().toString());
                                prsSet.setSentraCode(prs.getSentraId().getSentraCode());
                                prsSet.setSentraName(prs.getSentraId().getSentraName());
                                prsSet.setReport(prs.getReport());
                                if (prs.getUpdateDate() != null)
                                    prsSet.setUpdatedDate(formatter.format(prs.getUpdateDate()));
                                PRSPhoto photo = prsService.getPrsPhotoByPrs(prs);
                                if (photo != null)
                                    prsSet.setHasPrsPhoto(
                                            WebGuiConstant.TERMINAL_PRS_PHOTO_REQUEST.replace("**/{apkVersion:.+}",
                                                    "/" + paramApkVersion) + "/" + prs.getPrsId().toString());
                                if (prs.getPrsDate() != null)
                                    prsSet.setPrsDate(formatter.format(prs.getPrsDate()));
                                if (prs.getPrsTime() != null)
                                    prsSet.setPrsTime(prs.getPrsTime());
                                prsSet.setStatus(prs.getStatus());
                                prsSet.setBringMoney(prs.getBringMoney());
                                if (prs.getPsCompanion() != null)
                                    prsSet.setPsCompanion(prs.getPsCompanion());
                                List<PSCompanionPojo> psCompanionListTemp = new ArrayList<>();
                                List<User> psCompanionList = userService
                                        .loadUserByLocationId(prs.getSentraId().getLocationId());
                                for (User psCompanion : psCompanionList) {
                                    PSCompanionPojo psCompanionSet = new PSCompanionPojo();
                                    psCompanionSet.setPsId(psCompanion.getUserId().toString());
                                    psCompanionSet.setCode(psCompanion.getUsername());
                                    psCompanionSet.setPsName(psCompanion.getName());
                                    psCompanionListTemp.add(psCompanionSet);
                                }
                                prsSet.setPsIdCompanionList(psCompanionListTemp);
                                List<CustomerPRSListResponse> customerPRSListTemp = new ArrayList<>();
                                List<CustomerPRS> customerPRSList = customerService.getCustomerPRSByPrsId(prs);
                                if (customerPRSList.isEmpty())
                                    prsSet.setCustomerList(null);
                                for (CustomerPRS customerPRS : customerPRSList) {
                                    CustomerPRSListResponse customerPRSSet = new CustomerPRSListResponse();
                                    customerPRSSet
                                            .setCustomerId(customerPRS.getCustomerId().getCustomerId().toString());
                                    customerPRSSet.setCustomerName(customerPRS.getCustomerId().getCustomerName());
                                    customerPRSSet.setCustomerCif(customerPRS.getCustomerId().getCustomerCifNumber());
                                    if (customerPRS.getCustomerId().getSwId() != null) {
                                        customerPRSSet.setSwId(customerPRS.getCustomerId().getSwId().toString());
                                    }
                                    customerPRSSet.setCustomerPrsId(customerPRS.getCustomerPrsId().toString());
                                    if (customerPRS.getGroupId() != null) {
                                        customerPRSSet.setGroupId(customerPRS.getGroupId().getGroupId().toString());
                                        customerPRSSet.setGroupName(customerPRS.getGroupId().getGroupName());
                                    }
                                    if (customerPRS.getIsAttend() != null)
                                        customerPRSSet.setIsAttend(customerPRS.getIsAttend());
                                    if (customerPRS.getNotAttendReason() != null)
                                        customerPRSSet.setNotAttendReason(customerPRS.getNotAttendReason().toString());
                                    List<LoanPRSListResponse> loanPRSListTemp = new ArrayList<>();
                                    List<LoanPRS> loanPRSList = loanService.getLoanPRSByPrsId(prs, customerPRS);
                                    if (loanPRSList.isEmpty())
                                        customerPRSSet.setLoanList(null);
                                    for (LoanPRS loanPRS : loanPRSList) {
                                        LoanPRSListResponse loanPRSSet = new LoanPRSListResponse();
                                        Loan loan = loanService.findById(loanPRS.getLoanId().getLoanId().toString());
                                        if (loan != null) {
                                            if (loan.getStatus().equals(WebGuiConstant.STATUS_APPROVED)
                                                    || loan.getStatus().equals(WebGuiConstant.STATUS_ACTIVE)) {
                                                if (loan.getProductId() != null) {
                                                    LoanProduct loanProduct = swService
                                                            .findByProductId(loan.getProductId().toString());
                                                    if (loanProduct != null) {
                                                        if (loanProduct.getProductCode().toUpperCase()
                                                                .startsWith("NOV")) {
                                                            loanPRSSet.setIsLoanPhone("true");
                                                            loanPRSSet.setProductType("phone");
                                                        } else {
                                                            loanPRSSet.setIsLoanPhone("false");
                                                            loanPRSSet.setProductType("cash");
                                                        }
                                                    }
                                                }
                                                loanPRSSet.setLoanPrsId(loanPRS.getLoanPRSId().toString());
                                                loanPRSSet.setLoanId(loanPRS.getLoanId().getLoanId().toString());
                                                loanPRSSet.setLoanCreatedDate(
                                                        formatDateTime.format(loanPRS.getLoanId().getCreatedDate()));
                                                loanPRSSet.setAppId(loanPRS.getAppId());
                                                if (loanPRS.getDisbursementAmount() != null) {
                                                    if (!decimalFormat.format(loanPRS.getDisbursementAmount())
                                                            .equals("0")) {
                                                        loanPRSSet
                                                                .setDisbursementAmount(loanPRS.getDisbursementAmount());
                                                        disbursementPlan = true;
                                                    } else {
                                                        loanPRSSet.setDisbursementAmount(
                                                                loanPRS.getDisbursementAmount().ZERO);
                                                        disbursementPlan = false;
                                                    }
                                                } else {
                                                    loanPRSSet.setDisbursementAmount(
                                                            loanPRS.getDisbursementAmount().ZERO);
                                                }
                                                loanPRSSet.setDisbursementFlag(
                                                        String.valueOf(loanPRS.getDisbursementFlag()));
                                                if (loanPRS.getCanPendingDisbursement() != null) {
                                                    loanPRSSet.setCanPendingDisbursement(
                                                            loanPRS.getCanPendingDisbursement());
                                                }
                                                if (disbursementPlan) {
                                                    DisbursementPhoto disbursementPhoto = loanService
                                                            .getDisbursementPhoto(loanPRS);
                                                    if (disbursementPhoto != null)
                                                        loanPRSSet.setHasDisbursementPhoto(
                                                                WebGuiConstant.TERMINAL_GET_DISBURSEMENT_PHOTO_REQUEST
                                                                        .replace("**/{apkVersion:.+}",
                                                                                "/" + paramApkVersion)
                                                                        + "/" + loanPRS.getLoanPRSId().toString());
                                                }
                                                if (loanPRS.getInstallmentAmount() != null) {
                                                    if (disbursementPlan) {
                                                        loanPRSSet.setInstallmentAmount(loanPRS.getInstallmentAmount());
                                                    } else {
                                                        loanPRSSet.setInstallmentAmount(loanPRS.getInstallmentAmount());
                                                    }
                                                } else {
                                                    loanPRSSet
                                                            .setInstallmentAmount(loanPRS.getInstallmentAmount().ZERO);
                                                }
                                                if (loanPRS.getCurrentMargin() != null) {
                                                    if (disbursementPlan) {
                                                        loanPRSSet.setCurrentMargin(loanPRS.getCurrentMargin());
                                                    } else {
                                                        loanPRSSet.setCurrentMargin(loanPRS.getCurrentMargin());
                                                    }
                                                } else {
                                                    loanPRSSet.setCurrentMargin(loanPRS.getCurrentMargin().ZERO);
                                                }
                                                if (loanPRS.getUseEmergencyFund() != null) {
                                                    loanPRSSet.setUseEmergencyFund(loanPRS.getUseEmergencyFund());
                                                }
                                                if (loanPRS.getMarginAmount() != null) {
                                                    loanPRSSet.setMarginAmount(loanPRS.getMarginAmount());
                                                } else {
                                                    loanPRSSet.setMarginAmount(loanPRS.getMarginAmount().ZERO);
                                                }
                                                if (loanPRS.getPlafonAmount() != null) {
                                                    loanPRSSet.setPlafonAmount(loanPRS.getPlafonAmount());
                                                } else {
                                                    loanPRSSet.setPlafonAmount(loanPRS.getPlafonAmount().ZERO);
                                                }
                                                if (loanPRS.getOutstandingAmount() != null) {
                                                    if (disbursementPlan) {
                                                        loanPRSSet.setOutstandingAmount(
                                                                loanPRS.getOutstandingAmount().ZERO);
                                                    } else {
                                                        loanPRSSet.setOutstandingAmount(loanPRS.getOutstandingAmount());
                                                    }
                                                } else {
                                                    loanPRSSet
                                                            .setOutstandingAmount(loanPRS.getOutstandingAmount().ZERO);
                                                }
                                                if (loanPRS.getOutstandingCount() != null) {
                                                    loanPRSSet.setOutstandingCount(
                                                            loanPRS.getOutstandingCount().toString());
                                                } else {
                                                    loanPRSSet.setOutstandingCount("0");
                                                }
                                                if (loanPRS.getRemainingPrincipal() != null) {
                                                    loanPRSSet.setRemainingPrincipal(loanPRS.getRemainingPrincipal());
                                                } else {
                                                    loanPRSSet.setRemainingPrincipal(
                                                            loanPRS.getRemainingPrincipal().ZERO);
                                                }
                                                if (loanPRS.getRemainingOutstandingPrincipal() != null) {
                                                    loanPRSSet.setRemainingOutstandingPrincipal(
                                                            loanPRS.getRemainingOutstandingPrincipal());
                                                } else {
                                                    loanPRSSet.setRemainingOutstandingPrincipal(
                                                            loanPRS.getRemainingOutstandingPrincipal().ZERO);
                                                }
                                                if (loanPRS.getOutstandingMarginAmount() != null) {
                                                    loanPRSSet.setOutstandingMarginAmount(
                                                            loanPRS.getOutstandingMarginAmount());
                                                } else {
                                                    loanPRSSet.setOutstandingMarginAmount(
                                                            loanPRS.getOutstandingMarginAmount().ZERO);
                                                }
                                                if (loanPRS.getDueDate() != null)
                                                    loanPRSSet.setDueDate(formatter.format(loanPRS.getDueDate()));
                                                if (loanPRS.getOverdueDays() != null) {
                                                    if (!disbursementPlan)
                                                        loanPRSSet.setOverdueDays(loanPRS.getOverdueDays().toString());
                                                }
                                                LoanPRS lastLoanPrs = loanService.findLastLoanPrs(loan, new Date());
                                                if (lastLoanPrs != null) {
                                                    DiscountMarginApproval discountMarginApproval = loanService
                                                            .findLastDiscountMarginApprovalByCustomer(lastLoanPrs
                                                                    .getCustomerId().getCustomerId().getCustomerId());
                                                    if (discountMarginApproval != null) {
                                                        if (discountMarginApproval.getStatus() != null) {
                                                            if (discountMarginApproval.getStatus()
                                                                    .equals(WebGuiConstant.STATUS_APPROVED)
                                                                    || discountMarginApproval.getStatus().equals(
                                                                    WebGuiConstant.STATUS_WAITING_FOR_APPROVAL)) {
                                                                loanPRSSet.setIsEarlyTermination("true");
                                                            } else {
                                                                loanPRSSet.setIsEarlyTermination("false");
                                                            }
                                                        }
                                                    }
                                                }
                                                if (loanPRS.getIsEarlyTermination() != null) {
                                                    if (loanPRS.getIsEarlyTermination() == true) {
                                                        loanPRSSet.setIsEarlyTermination("true");
                                                    } else {
                                                        loanPRSSet.setIsEarlyTermination("false");
                                                    }
                                                }
                                                if (loanPRS.getIsEarlyTerminationAdHoc() != null) {
                                                    if (loanPRS.getIsEarlyTerminationAdHoc() == true) {
                                                        loanPRSSet.setIsEarlyTerminationAdhoc("true");
                                                    } else {
                                                        loanPRSSet.setIsEarlyTerminationAdhoc("false");
                                                    }
                                                }
                                                if (loanPRS.getEarlyTerminationReason() != null)
                                                    loanPRSSet.setEarlyTerminationReason(
                                                            loanPRS.getEarlyTerminationReason());

                                                if (loanPRS.getMarginDiscountDeviationFlag() != null) {
                                                    if (loanPRS.getMarginDiscountDeviationFlag() == true) {
                                                        loanPRSSet.setMarginDiscountDeviationFlag("true");
                                                    } else {
                                                        loanPRSSet.setMarginDiscountDeviationFlag("false");
                                                    }
                                                }
                                                if (loanPRS.getMarginDiscountDeviationAmount() != null) {
                                                    loanPRSSet.setMarginDiscountDeviationAmount(
                                                            loanPRS.getMarginDiscountDeviationAmount());
                                                } else {
                                                    loanPRSSet.setMarginDiscountDeviationAmount(
                                                            loanPRS.getMarginDiscountDeviationAmount().ZERO);
                                                }
                                                if (loanPRS.getInstallmentPaymentAmount() != null) {
                                                    if (loanPRSSet.getMarginDiscountDeviationAmount()
                                                            .compareTo(BigDecimal.ZERO) > 0
                                                            && loanPRSSet.getIsEarlyTermination().equals("true")) {
                                                        loanPRSSet.setInstallmentPaymentAmount(
                                                                loanPRS.getInstallmentPaymentAmount().subtract(
                                                                        loanPRSSet.getMarginDiscountDeviationAmount()));
                                                    } else {
                                                        loanPRSSet.setInstallmentPaymentAmount(
                                                                loanPRS.getInstallmentPaymentAmount());
                                                    }
                                                } else {
                                                    loanPRSSet.setInstallmentPaymentAmount(
                                                            loanPRS.getInstallmentPaymentAmount().ZERO);
                                                }
                                                if (loanPRS.getWowIbStatus() != null)
                                                    loanPRSSet.setWowIbStatus(loanPRS.getWowIbStatus());
                                                loanPRSSet.setHasDisbursementPlan(disbursementPlan);
                                                if (loanPRS.getAngsuran() != null)
                                                    loanPRSSet.setAngsuran(loanPRS.getAngsuran().toString());
                                                if (loanPRS.getTenor() != null)
                                                    loanPRSSet.setTenor(loanPRS.getTenor().toString());
                                                loanPRSListTemp.add(loanPRSSet);
                                            }
                                        }
                                    }
                                    if (disbursementPlan) {
                                        Long swId = customerPRS.getCustomerId().getSwId();
                                        if (swId != null) {
                                            SwIdPhoto idPhoto = swService.getSwIdPhoto(swId.toString());
                                            if (idPhoto != null)
                                                customerPRSSet
                                                        .setHasIdCardPhoto(WebGuiConstant.TERMINAL_GET_ID_PHOTO_REQUEST
                                                                .replace("**/{apkVersion:.+}", "/" + paramApkVersion)
                                                                + "/" + customerPRS.getCustomerId().getCustomerId()
                                                                .toString());
                                            SwSurveyPhoto surveyPhoto = swService.getSwSurveyPhoto(swId.toString());
                                            if (surveyPhoto != null)
                                                customerPRSSet.setHasBusinessPlacePhoto(
                                                        WebGuiConstant.TERMINAL_BUSINESS_PLACE_PHOTO_REQUEST
                                                                .replace("**/{apkVersion:.+}", "/" + paramApkVersion)
                                                                + "/" + customerPRS.getCustomerId().getCustomerId()
                                                                .toString());
                                        } else {
                                            customerPRSSet.setHasIdCardPhoto("");
                                            customerPRSSet.setHasBusinessPlacePhoto("");
                                        }
                                    }
                                    customerPRSSet.setLoanList(loanPRSListTemp);
                                    List<SavingPRSListResponse> savingPRSListTemp = new ArrayList<>();
                                    List<SavingPRS> savingPRSList = savingService.getSavingByPRSIdAndCustomerId(prs,
                                            customerPRS);
                                    if (savingPRSList.isEmpty())
                                        customerPRSSet.setSavingList(null);
                                    for (SavingPRS savingPrs : savingPRSList) {
                                        SavingPRSListResponse savingPRSSet = new SavingPRSListResponse();
                                        savingPRSSet.setAccountId(savingPrs.getAccountId().toString());
                                        savingPRSSet.setAccountNumber(savingPrs.getAccountNumber());
                                        if (savingPrs.getWithdrawalAmount() != null) {
                                            savingPRSSet.setWithdrawalAmount(savingPrs.getWithdrawalAmount());
                                        } else {
                                            savingPRSSet.setWithdrawalAmount(savingPrs.getWithdrawalAmount().ZERO);
                                        }
                                        if (savingPrs.getWithdrawalAmountPlan() != null)
                                            savingPRSSet.setWithdrawalAmountPlan(
                                                    savingPrs.getWithdrawalAmountPlan().toString());
                                        if (savingPrs.getNextWithdrawalAmountPlan() != null)
                                            savingPRSSet.setNextWithdrawalAmountPlan(
                                                    savingPrs.getNextWithdrawalAmountPlan().toString());
                                        if (savingPrs.getIsCloseSavingAccount() != null) {
                                            if (savingPrs.getIsCloseSavingAccount() == true) {
                                                savingPRSSet.setIsCloseSavingAccount("true");
                                            } else {
                                                savingPRSSet.setIsCloseSavingAccount("false");
                                            }
                                        }
                                        if (savingPrs.getHoldBalance() != null) {
                                            savingPRSSet.setHoldBalance(savingPrs.getHoldBalance());
                                        } else {
                                            savingPRSSet.setHoldBalance(savingPrs.getHoldBalance().ZERO);
                                        }
                                        if (savingPrs.getClearBalance() != null) {
                                            savingPRSSet.setClearBalance(savingPrs.getClearBalance());
                                        } else {
                                            savingPRSSet.setClearBalance(savingPrs.getClearBalance().ZERO);
                                        }
                                        if (savingPrs.getDepositAmount() != null) {
                                            savingPRSSet.setDepositAmount(savingPrs.getDepositAmount());
                                        } else {
                                            savingPRSSet.setDepositAmount(savingPrs.getDepositAmount().ZERO);
                                        }
                                        savingPRSListTemp.add(savingPRSSet);
                                    }
                                    customerPRSSet.setSavingList(savingPRSListTemp);
                                    customerPRSListTemp.add(customerPRSSet);
                                }
                                prsSet.setCustomerList(customerPRSListTemp);
                                prsListTemp.add(prsSet);
                            }
                        }
                        // Jika Fetch Previous PRS false
                    } else {
                        System.out.println(new Date());
                        PRS prs = prsService.getPRSBySentra(sentra, new Date());
                        PRSResponseList prsSet = new PRSResponseList();
                        if (prs != null) {
                            prsSet.setPrsId(prs.getPrsId().toString());
                            prsSet.setSentraId(prs.getSentraId().getSentraId().toString());
                            prsSet.setSentraCode(prs.getSentraId().getSentraCode());
                            prsSet.setSentraName(prs.getSentraId().getSentraName());
                            prsSet.setReport(prs.getReport());
                            if (prs.getUpdateDate() != null)
                                prsSet.setUpdatedDate(formatter.format(prs.getUpdateDate()));
                            PRSPhoto photo = prsService.getPrsPhotoByPrs(prs);
                            if (photo != null)
                                prsSet.setHasPrsPhoto(
                                        WebGuiConstant.TERMINAL_PRS_PHOTO_REQUEST.replace("**/{apkVersion:.+}",
                                                "/" + paramApkVersion) + "/" + photo.getPrsPhotoId().toString());
                            if (prs.getPrsDate() != null)
                                prsSet.setPrsDate(formatter.format(prs.getPrsDate()));
                            if (prs.getPrsTime() != null)
                                prsSet.setPrsTime(prs.getPrsTime());
                            prsSet.setStatus(prs.getStatus());
                            prsSet.setBringMoney(prs.getBringMoney());
                            if (prs.getPsCompanion() != null)
                                prsSet.setPsCompanion(prs.getPsCompanion());
                            List<PSCompanionPojo> psCompanionListTemp = new ArrayList<>();
                            List<User> psCompanionList = userService
                                    .loadUserByLocationId(prs.getSentraId().getLocationId());
                            for (User psCompanion : psCompanionList) {
                                PSCompanionPojo psCompanionSet = new PSCompanionPojo();
                                psCompanionSet.setPsId(psCompanion.getUserId().toString());
                                psCompanionSet.setCode(psCompanion.getUsername());
                                psCompanionSet.setPsName(psCompanion.getName());
                                psCompanionListTemp.add(psCompanionSet);
                            }
                            prsSet.setPsIdCompanionList(psCompanionListTemp);
                            List<CustomerPRSListResponse> customerPRSListTemp = new ArrayList<>();
                            List<CustomerPRS> customerPRSList = customerService.getCustomerPRSByPrsId(prs);
                            if (customerPRSList.isEmpty())
                                prsSet.setCustomerList(null);
                            for (CustomerPRS customerPRS : customerPRSList) {
                                CustomerPRSListResponse customerPRSSet = new CustomerPRSListResponse();
                                customerPRSSet.setCustomerId(customerPRS.getCustomerId().getCustomerId().toString());
                                customerPRSSet.setCustomerName(customerPRS.getCustomerId().getCustomerName());
                                customerPRSSet.setCustomerCif(customerPRS.getCustomerId().getCustomerCifNumber());
                                if (customerPRS.getCustomerId().getSwId() != null) {
                                    customerPRSSet.setSwId(customerPRS.getCustomerId().getSwId().toString());
                                }
                                customerPRSSet.setCustomerPrsId(customerPRS.getCustomerPrsId().toString());
                                if (customerPRS.getGroupId() != null) {
                                    customerPRSSet.setGroupId(customerPRS.getGroupId().getGroupId().toString());
                                    customerPRSSet.setGroupName(customerPRS.getGroupId().getGroupName());
                                }
                                if (customerPRS.getIsAttend() != null) {
                                    customerPRSSet.setIsAttend(customerPRS.getIsAttend());
                                }
                                if (customerPRS.getNotAttendReason() != null)
                                    customerPRSSet.setNotAttendReason(customerPRS.getNotAttendReason().toString());
                                List<LoanPRSListResponse> loanPRSListTemp = new ArrayList<>();
                                List<LoanPRS> loanPRSList = loanService.getLoanPRSByPrsId(prs, customerPRS);
                                if (loanPRSList.isEmpty())
                                    customerPRSSet.setLoanList(null);
                                for (LoanPRS loanPRS : loanPRSList) {
                                    LoanPRSListResponse loanPRSSet = new LoanPRSListResponse();

                                    Loan loan = loanService.findById(loanPRS.getLoanId().getLoanId().toString());
                                    if (loan != null) {
                                        if (loan.getStatus().equals(WebGuiConstant.STATUS_APPROVED)
                                                || loan.getStatus().equals(WebGuiConstant.STATUS_ACTIVE)) {
                                            loanPRSSet.setLoanPrsId(loanPRS.getLoanPRSId().toString());
                                            loanPRSSet.setLoanId(loanPRS.getLoanId().getLoanId().toString());
                                            if (loan.getProductId() != null) {
                                                LoanProduct loanProduct = swService
                                                        .findByProductId(loan.getProductId().toString());
                                                if (loanProduct != null) {
                                                    if (loanProduct.getProductCode().toUpperCase().startsWith("NOV")) {
                                                        loanPRSSet.setIsLoanPhone("true");
                                                        loanPRSSet.setProductType("phone");
                                                    } else {
                                                        loanPRSSet.setIsLoanPhone("false");
                                                        loanPRSSet.setProductType("cash");
                                                    }
                                                }
                                            }
                                            loanPRSSet.setAppId(loanPRS.getAppId());
                                            loanPRSSet.setLoanCreatedDate(
                                                    formatDateTime.format(loanPRS.getLoanId().getCreatedDate()));
                                            if (loanPRS.getDisbursementAmount() != null) {
                                                if (!decimalFormat.format(loanPRS.getDisbursementAmount())
                                                        .equals("0")) {
                                                    loanPRSSet.setDisbursementAmount(loanPRS.getDisbursementAmount());
                                                    disbursementPlan = true;
                                                } else {
                                                    loanPRSSet.setDisbursementAmount(
                                                            loanPRS.getDisbursementAmount().ZERO);
                                                    disbursementPlan = false;
                                                }
                                            } else {
                                                loanPRSSet.setDisbursementAmount(loanPRS.getDisbursementAmount().ZERO);
                                            }
                                            loanPRSSet.setCanPendingDisbursement(loanPRS.getCanPendingDisbursement());
                                            if (disbursementPlan) {
                                                DisbursementPhoto disbursementPhoto = loanService
                                                        .getDisbursementPhoto(loanPRS);
                                                if (disbursementPhoto != null)
                                                    loanPRSSet.setHasDisbursementPhoto(
                                                            WebGuiConstant.TERMINAL_GET_DISBURSEMENT_PHOTO_REQUEST
                                                                    .replace("**/{apkVersion:.+}",
                                                                            "/" + paramApkVersion)
                                                                    + "/" + loanPRS.getLoanPRSId().toString());
                                            }
                                            if (loanPRS.getInstallmentAmount() != null) {
                                                if (disbursementPlan) {
                                                    loanPRSSet.setInstallmentAmount(loanPRS.getInstallmentAmount());
                                                } else {
                                                    loanPRSSet.setInstallmentAmount(loanPRS.getInstallmentAmount());
                                                }
                                            } else {
                                                loanPRSSet.setInstallmentAmount(loanPRS.getInstallmentAmount().ZERO);
                                            }
                                            if (loanPRS.getCurrentMargin() != null) {
                                                if (disbursementPlan) {
                                                    loanPRSSet.setCurrentMargin(loanPRS.getCurrentMargin());
                                                } else {
                                                    loanPRSSet.setCurrentMargin(loanPRS.getCurrentMargin());
                                                }
                                            } else {
                                                loanPRSSet.setCurrentMargin(loanPRS.getCurrentMargin().ZERO);
                                            }
                                            loanPRSSet
                                                    .setDisbursementFlag(String.valueOf(loanPRS.getDisbursementFlag()));
                                            if (loanPRS.getUseEmergencyFund() != null)
                                                loanPRSSet.setUseEmergencyFund(loanPRS.getUseEmergencyFund());
                                            if (loanPRS.getMarginAmount() != null) {
                                                loanPRSSet.setMarginAmount(loanPRS.getMarginAmount());
                                            } else {
                                                loanPRSSet.setMarginAmount(loanPRS.getMarginAmount().ZERO);
                                            }
                                            if (loanPRS.getPlafonAmount() != null) {
                                                loanPRSSet.setPlafonAmount(loanPRS.getPlafonAmount());
                                            } else {
                                                loanPRSSet.setPlafonAmount(loanPRS.getPlafonAmount().ZERO);
                                            }
                                            if (loanPRS.getOutstandingAmount() != null) {
                                                if (disbursementPlan) {
                                                    loanPRSSet
                                                            .setOutstandingAmount(loanPRS.getOutstandingAmount().ZERO);
                                                } else {
                                                    loanPRSSet.setOutstandingAmount(loanPRS.getOutstandingAmount());
                                                }
                                            } else {
                                                loanPRSSet.setOutstandingAmount(loanPRS.getOutstandingAmount().ZERO);
                                            }
                                            if (loanPRS.getOutstandingCount() != null) {
                                                loanPRSSet
                                                        .setOutstandingCount(loanPRS.getOutstandingCount().toString());
                                            } else {
                                                loanPRSSet.setOutstandingCount("0");
                                            }
                                            if (loanPRS.getRemainingPrincipal() != null) {
                                                loanPRSSet.setRemainingPrincipal(loanPRS.getRemainingPrincipal());
                                            } else {
                                                loanPRSSet.setRemainingPrincipal(loanPRS.getRemainingPrincipal().ZERO);
                                            }
                                            if (loanPRS.getRemainingOutstandingPrincipal() != null) {
                                                loanPRSSet.setRemainingOutstandingPrincipal(
                                                        loanPRS.getRemainingOutstandingPrincipal());
                                            } else {
                                                loanPRSSet.setRemainingOutstandingPrincipal(
                                                        loanPRS.getRemainingOutstandingPrincipal().ZERO);
                                            }
                                            if (loanPRS.getOutstandingMarginAmount() != null) {
                                                loanPRSSet.setOutstandingMarginAmount(
                                                        loanPRS.getOutstandingMarginAmount());
                                            } else {
                                                loanPRSSet.setOutstandingMarginAmount(
                                                        loanPRS.getOutstandingMarginAmount().ZERO);
                                            }
                                            if (loanPRS.getDueDate() != null)
                                                loanPRSSet.setDueDate(formatter.format(loanPRS.getDueDate()));
                                            if (loanPRS.getOverdueDays() != null)
                                                if (!disbursementPlan)
                                                    loanPRSSet.setOverdueDays(loanPRS.getOverdueDays().toString());
                                            if (loanPRS.getIsEarlyTermination() != null) {
                                                if (loanPRS.getIsEarlyTermination() == true) {
                                                    loanPRSSet.setIsEarlyTermination("true");
                                                } else {
                                                    loanPRSSet.setIsEarlyTermination("false");
                                                }
                                            }
                                            if (loanPRS.getIsEarlyTerminationAdHoc() != null) {
                                                if (loanPRS.getIsEarlyTerminationAdHoc() == true) {
                                                    loanPRSSet.setIsEarlyTerminationAdhoc("true");
                                                } else {
                                                    loanPRSSet.setIsEarlyTerminationAdhoc("false");
                                                }
                                            }
                                            LoanPRS lastLoanPrs = loanService.findLastLoanPrs(loan, new Date());
                                            if (lastLoanPrs != null) {
                                                DiscountMarginApproval discountMarginApproval = loanService
                                                        .findLastDiscountMarginApprovalByCustomer(lastLoanPrs
                                                                .getCustomerId().getCustomerId().getCustomerId());
                                                if (discountMarginApproval != null) {
                                                    if (discountMarginApproval.getStatus() != null) {
                                                        if (discountMarginApproval.getStatus()
                                                                .equals(WebGuiConstant.STATUS_APPROVED)
                                                                || discountMarginApproval.getStatus().equals(
                                                                WebGuiConstant.STATUS_WAITING_FOR_APPROVAL)) {
                                                            loanPRSSet.setIsEarlyTermination("true");
                                                        } else {
                                                            loanPRSSet.setIsEarlyTermination("false");
                                                        }
                                                    }
                                                }
                                            }
                                            if (loanPRS.getEarlyTerminationReason() != null)
                                                loanPRSSet
                                                        .setEarlyTerminationReason(loanPRS.getEarlyTerminationReason());
                                            if (loanPRS.getMarginDiscountDeviationFlag() != null) {
                                                if (loanPRS.getMarginDiscountDeviationFlag() == true) {
                                                    loanPRSSet.setMarginDiscountDeviationFlag("true");
                                                } else {
                                                    loanPRSSet.setMarginDiscountDeviationFlag("false");
                                                }
                                            }
                                            if (loanPRS.getMarginDiscountDeviationAmount() != null) {
                                                loanPRSSet.setMarginDiscountDeviationAmount(
                                                        loanPRS.getMarginDiscountDeviationAmount());
                                            } else {
                                                loanPRSSet.setMarginDiscountDeviationAmount(
                                                        loanPRS.getMarginDiscountDeviationAmount().ZERO);
                                            }
                                            if (loanPRS.getWowIbStatus() != null)
                                                loanPRSSet.setWowIbStatus(loanPRS.getWowIbStatus());
                                            loanPRSSet.setHasDisbursementPlan(disbursementPlan);
                                            if (loanPRS.getInstallmentPaymentAmount() != null) {
                                                if (loanPRSSet.getMarginDiscountDeviationAmount()
                                                        .compareTo(BigDecimal.ZERO) > 0
                                                        && loanPRSSet.getIsEarlyTermination().equals("true")) {
                                                    loanPRSSet.setInstallmentPaymentAmount(
                                                            loanPRS.getInstallmentPaymentAmount().subtract(
                                                                    loanPRSSet.getMarginDiscountDeviationAmount()));
                                                } else {
                                                    loanPRSSet.setInstallmentPaymentAmount(
                                                            loanPRS.getInstallmentPaymentAmount());
                                                }
                                            } else {
                                                loanPRSSet.setInstallmentPaymentAmount(
                                                        loanPRS.getInstallmentPaymentAmount().ZERO);
                                            }
                                            if (loanPRS.getAngsuran() != null)
                                                loanPRSSet.setAngsuran(loanPRS.getAngsuran().toString());
                                            if (loanPRS.getTenor() != null)
                                                loanPRSSet.setTenor(loanPRS.getTenor().toString());
                                            List<DiscountMarginApproval> discountMarginApprovalList = loanService
                                                    .getDiscountMarginApprovalByCustomerId(
                                                            customerPRS.getCustomerId().getCustomerId());
                                            if (discountMarginApprovalList != null
                                                    || !discountMarginApprovalList.isEmpty()) {
                                                for (DiscountMarginApproval discountMarginApproval : discountMarginApprovalList) {
                                                    DiscountMarginLoanMap discountMarginLoanMap = loanService
                                                            .getDiscountMarginLoanMapByLoanPrsIdAndDiscountMarginId(
                                                                    loanPRS.getLoanPRSId(),
                                                                    discountMarginApproval.getDiscountMarginId());
                                                    if (discountMarginLoanMap != null) {
                                                        if (loanPRS.getPrsId().getPrsDate()
                                                                .after(dateExtractor.getDateBeforeDays(-14))) {
                                                            DiscountMarginLoanResponse discountMarginLoanResponse = new DiscountMarginLoanResponse();
                                                            discountMarginLoanResponse
                                                                    .setAmount(discountMarginLoanMap.getAmount());
                                                            discountMarginLoanResponse
                                                                    .setDiscountMarginId(discountMarginLoanMap
                                                                            .getDiscountMarginId().toString());
                                                            discountMarginLoanResponse
                                                                    .setStatus(discountMarginLoanMap.getStatus());
                                                            loanPRSSet.setDiscountMargin(discountMarginLoanResponse);
                                                        }
                                                    }
                                                }
                                            }
                                            loanPRSListTemp.add(loanPRSSet);
                                        }
                                    }
                                }
                                if (disbursementPlan) {
                                    Long swId = customerPRS.getCustomerId().getSwId();
                                    if (swId != null) {
                                        SwIdPhoto idPhoto = swService
                                                .getSwIdPhoto(customerPRS.getCustomerId().getSwId().toString());
                                        if (idPhoto != null)
                                            customerPRSSet
                                                    .setHasIdCardPhoto(WebGuiConstant.TERMINAL_GET_ID_PHOTO_REQUEST
                                                            .replace("**/{apkVersion:.+}", "/" + paramApkVersion) + "/"
                                                            + customerPRS.getCustomerId().getCustomerId().toString());
                                        SwSurveyPhoto surveyPhoto = swService
                                                .getSwSurveyPhoto(customerPRS.getCustomerId().getSwId().toString());
                                        if (surveyPhoto != null)
                                            customerPRSSet.setHasBusinessPlacePhoto(
                                                    WebGuiConstant.TERMINAL_BUSINESS_PLACE_PHOTO_REQUEST
                                                            .replace("**/{apkVersion:.+}", "/" + paramApkVersion) + "/"
                                                            + customerPRS.getCustomerId().getCustomerId().toString());
                                    } else {
                                        customerPRSSet.setHasIdCardPhoto("");
                                        customerPRSSet.setHasBusinessPlacePhoto("");
                                    }
                                }
                                customerPRSSet.setLoanList(loanPRSListTemp);
                                List<SavingPRSListResponse> savingPRSListTemp = new ArrayList<>();
                                List<SavingPRS> savingPRSList = savingService.getSavingByPRSIdAndCustomerId(prs,
                                        customerPRS);
                                if (savingPRSList.isEmpty())
                                    customerPRSSet.setSavingList(null);
                                for (SavingPRS savingPrs : savingPRSList) {
                                    SavingPRSListResponse savingPRSSet = new SavingPRSListResponse();
                                    savingPRSSet.setAccountId(savingPrs.getAccountId().toString());
                                    savingPRSSet.setAccountNumber(savingPrs.getAccountNumber());
                                    if (savingPrs.getWithdrawalAmount() != null) {
                                        savingPRSSet.setWithdrawalAmount(savingPrs.getWithdrawalAmount());
                                    } else {
                                        savingPRSSet.setWithdrawalAmount(savingPrs.getWithdrawalAmount().ZERO);
                                    }
                                    if (savingPrs.getWithdrawalAmountPlan() != null) {
                                        savingPRSSet.setWithdrawalAmountPlan(
                                                savingPrs.getWithdrawalAmountPlan().toString());
                                    } else {
                                        savingPRSSet.setWithdrawalAmountPlan("0");
                                    }
                                    if (savingPrs.getNextWithdrawalAmountPlan() != null)
                                        savingPRSSet.setNextWithdrawalAmountPlan(
                                                savingPrs.getNextWithdrawalAmountPlan().toString());
                                    if (savingPrs.getIsCloseSavingAccount() != null) {
                                        if (savingPrs.getIsCloseSavingAccount() == true) {
                                            savingPRSSet.setIsCloseSavingAccount("true");
                                        } else {
                                            savingPRSSet.setIsCloseSavingAccount("false");
                                        }
                                    }
                                    if (savingPrs.getHoldBalance() != null) {
                                        savingPRSSet.setHoldBalance(savingPrs.getHoldBalance());
                                    } else {
                                        savingPRSSet.setHoldBalance(savingPrs.getHoldBalance().ZERO);
                                    }
                                    if (savingPrs.getClearBalance() != null) {
                                        savingPRSSet.setClearBalance(savingPrs.getClearBalance());
                                    } else {
                                        savingPRSSet.setClearBalance(savingPrs.getClearBalance().ZERO);
                                    }
                                    if (savingPrs.getDepositAmount() != null) {
                                        savingPRSSet.setDepositAmount(savingPrs.getDepositAmount());
                                    } else {
                                        savingPRSSet.setDepositAmount(savingPrs.getDepositAmount().ZERO);
                                    }
                                    savingPRSListTemp.add(savingPRSSet);
                                }
                                customerPRSSet.setSavingList(savingPRSListTemp);
                                customerPRSListTemp.add(customerPRSSet);
                            }
                            prsSet.setCustomerList(customerPRSListTemp);
                            prsListTemp.add(prsSet);
                        }
                    }
                }
                response.setPrsList(prsListTemp);
                response.setSentraHasPrsDataList(sentraHasPrsDataList);
            }
        } catch (Exception e) {
            log.error("syncPrs error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("syncPrs error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_PRS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("syncPrs saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("syncPrs RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SUBMIT_PRS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SubmitPRSResponse doSubmitPRS(@RequestBody final SubmitPRSRequest request,
                                  @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final SubmitPRSResponse response = new SubmitPRSResponse();
        Map<String, List<LoanPRS>> loanMap = new HashMap<>();
        Map<String, List<SavingPRS>> savingMap = new HashMap<>();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("submitPRS Try to processing submit PRS request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("trying to submit PRS ");
                if (request.getStatus().equals(WebGuiConstant.STATUS_DRAFT)) {
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_STATUS);
                    String labelUnknown = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelUnknown);
                    return response;
                } else if (request.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_STATUS);
                    String labelUnknown = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelUnknown);
                    return response;
                } else if (request.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_STATUS);
                    String labelUnknown = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelUnknown);
                    return response;
                }
                // PROSES KE TABLE PRS --> HANYA BISA UPDATE

                /*
                 * //step save 1. save PRS 2. save customer 3. save loan 4. save saving
                 */

                // 1. cari id PRSnya dulu
                if (request.getPrsId() == null) {
                    response.setResponseCode(WebGuiConstant.RC_FAILED);
                    String labelUnknown = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelUnknown);
                    return response;
                }
                PRS prs = prsService.getPRSById(Long.parseLong(request.getPrsId()));
                if (prs == null) {
                    // idprs tidak ditemukan, RC gagal
                    response.setResponseCode(WebGuiConstant.RC_FAILED);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                } else {
                    if (null != request.getPsCompanion()) {
                        // set PsIdCompanion
                        if (!request.getPsCompanion().equals("")) {
                            prs.setPsCompanion(request.getPsCompanion());
                        } else {
                            prs.setPsCompanion("");
                        }
                    }
                    List<LoanPRS> loanPRSForUpdate = new ArrayList<>();
                    List<SavingPRS> savingPRSForUpdate = new ArrayList<>();
                    prs.setLatitude(request.getLatitude());
                    prs.setLongitude(request.getLongitude());
                    if (request.getBringMoney() != null && !"".equals(request.getBringMoney()))
                        prs.setBringMoney(new BigDecimal(request.getBringMoney()));
                    if (request.getActualMoney() != null && !"".equals(request.getActualMoney()))
                        prs.setActualMoney(new BigDecimal(request.getActualMoney()));
                    if (request.getPaymentMoney() != null && !"".equals(request.getPaymentMoney()))
                        prs.setPaymentMoney(new BigDecimal(request.getPaymentMoney()));
                    if (request.getWithdrawalAdhoc() != null && !"".equals(request.getWithdrawalAdhoc()))
                        prs.setWithdrawalAdhoc(new BigDecimal(request.getWithdrawalAdhoc()));
                    // mapping Customer PRS
                    List<CustomerPRS> customerPRSForUpdate = new ArrayList<>();
                    List<CustomerPRSRequest> customerPRSList = request.getCustomerList();
                    List<DiscountMarginReq> discountMarginReqs = request.getDiscountMarginPlan();
                    for (CustomerPRSRequest customerPRSRequest : customerPRSList) {
                        CustomerPRS custPRS = new CustomerPRS();
                        // search customer PRS by customer id
                        custPRS = customerService
                                .getCustomerPRSById(Long.parseLong(customerPRSRequest.getCustomerPrsId()));
                        custPRS.setPrsId(prs);
                        custPRS.setCustomerPrsId(Long.parseLong(customerPRSRequest.getCustomerPrsId()));
                        if (customerPRSRequest.getIsAttend().equals("true")) {
                            custPRS.setIsAttend(true);
                            if (customerPRSRequest.getNotAttendReason() != null) {
                                PRSParameter prsParamAttend = new PRSParameter();
                                if (!customerPRSRequest.getNotAttendReason().equals("")) {
                                    prsParamAttend = parameterService
                                            .findByParameterId(Long.parseLong(customerPRSRequest.getNotAttendReason()));
                                    if (prsParamAttend != null)
                                        custPRS.setNotAttendReason(prsParamAttend.getParameterId());
                                }
                            }
                        } else {
                            custPRS.setIsAttend(false);
                            if (customerPRSRequest.getNotAttendReason() != null) {
                                PRSParameter prsParamAttend = new PRSParameter();
                                if (!customerPRSRequest.getNotAttendReason().equals("")) {
                                    prsParamAttend = parameterService
                                            .findByParameterId(Long.parseLong(customerPRSRequest.getNotAttendReason()));
                                    if (prsParamAttend != null)
                                        custPRS.setNotAttendReason(prsParamAttend.getParameterId());
                                }
                            }
                        }
                        if (customerPRSRequest.getIsEarlyTerminationPlan() != null) {
                            if (customerPRSRequest.getIsEarlyTerminationPlan().equals("true")) {
                                DiscountMarginReq discountMarginReq = customerPRSRequest.getDiscountMarginReq();
                                for (LoanPRSRequest loan : discountMarginReq.getLoans()) {
                                    LoanPRS loanPRS = loanService.getLoanPRSById(Long.parseLong(loan.getLoanPrsId()));
                                    loanPRS.setEarlyTerminationPlan(true);
                                }
                                custPRS.setEarlyTerminationPlan(true);
                                EarlyTerminationPlan earlyTerminationPlan = new EarlyTerminationPlan();
                                earlyTerminationPlan.setCustomerPrsId(custPRS.getCustomerPrsId());
                            } else {
                                custPRS.setEarlyTerminationPlan(false);
                            }
                        }
                        // mapping LOAN
                        List<LoanPRSRequest> loanPRSList = customerPRSRequest.getLoanList();
                        if (loanPRSList != null) {
                            for (LoanPRSRequest loanPRSRequest : loanPRSList) {
                                LoanPRS loanPRS = new LoanPRS();
                                loanPRS = loanService.getLoanPRSById(Long.parseLong(loanPRSRequest.getLoanPrsId()));
                                loanPRS.setPrsId(prs);
                                loanPRS.setCustomerId(custPRS);
                                loanPRS.setLoanPRSId(Long.parseLong(loanPRSRequest.getLoanPrsId()));
                                loanPRS.setAppId(loanPRSRequest.getAppId());
                                loanPRS.setDisbursementFlag(loanPRSRequest.getDisbursementFlag().charAt(0));
                                if (loanPRS.getRemainingPrincipal() == null) {
                                    loanPRS.setRemainingPrincipal(loanPRS.getRemainingPrincipal().ZERO);
                                }
                                if (loanPRS.getInstallmentPaymentAmount() == null) {
                                    loanPRS.setInstallmentPaymentAmount(loanPRS.getInstallmentPaymentAmount().ZERO);
                                }
                                loanPRS.setRemainingPrincipalAfterSubmit(loanPRS.getRemainingPrincipal()
                                        .subtract(loanPRS.getInstallmentPaymentAmount()));
                                loanPRS.setInstallmentPaymentAmount(
                                        new BigDecimal(loanPRSRequest.getInstallmentPaymentAmount()));
                                if (loanPRSRequest.getUseEmergencyFund().equals("true")) {
                                    loanPRS.setUseEmergencyFund(true);
                                }
                                if (loanPRSRequest.getIsEarlyTerminationCanceled() != null) {
                                    if (loanPRSRequest.getIsEarlyTerminationCanceled().equals("true")) {
                                        loanPRS.setIsEarlyTermination(false);
                                    } else {
                                        if (loanPRSRequest.getIsEarlyTermination() != null) {
                                            if (loanPRSRequest.getIsEarlyTermination().equals("true")) {
                                                loanPRS.setIsEarlyTermination(true);
                                                // earlyTerminationReason ambil dari parameter PRS
                                                if (loanPRSRequest.getEarlyTerminationReason() != null) {
                                                    if (!loanPRSRequest.getEarlyTerminationReason().equals("")) {
                                                        loanPRS.setEarlyTerminationReason(
                                                                loanPRSRequest.getEarlyTerminationReason());
                                                    }
                                                }
                                            } else
                                                loanPRS.setIsEarlyTermination(false);
                                        }
                                        if (loanPRSRequest.getIsEarlyTerminationAdHoc() != null) {
                                            if (loanPRSRequest.getIsEarlyTerminationAdHoc().equals("true")) {
                                                loanPRS.setIsEarlyTerminationAdHoc(true);
                                                // earlyTerminationReason ambil dari parameter PRS
                                                if (loanPRSRequest.getEarlyTerminationReason() != null) {
                                                    if (!loanPRSRequest.getEarlyTerminationReason().equals("")) {
                                                        loanPRS.setEarlyTerminationReason(
                                                                loanPRSRequest.getEarlyTerminationReason());
                                                    }
                                                }
                                            } else {
                                                loanPRS.setIsEarlyTerminationAdHoc(false);
                                            }
                                        }
                                    }
                                }
                                if (loanPRSRequest.getMarginDiscountDeviationFlag().equals("true")) {
                                    loanPRS.setMarginDiscountDeviationFlag(true);
                                    loanPRS.setMarginDiscountDeviationPercentage(
                                            new BigDecimal(loanPRSRequest.getMarginDiscountDeviationPercentage()));
                                    loanPRS.setMarginDiscountDeviationAmount(
                                            new BigDecimal(loanPRSRequest.getMarginDiscountDeviationAmount()));
                                    loanPRS.setInstallmentPaymentAmount(loanPRS.getInstallmentPaymentAmount()
                                            .add(loanPRS.getMarginDiscountDeviationAmount()));
                                } else {
                                    loanPRS.setMarginDiscountDeviationFlag(false);
                                }
                                loanPRS.setUpdatedBy(request.getUsername());
                                loanPRS.setUpdatedDate(new Date());
                                if (loanPRSRequest.getWowIbStatus().equals("true"))
                                    loanPRS.setWowIbStatus(true);
                                loanPRSForUpdate.add(loanPRS);
                                if (loanMap.get(customerPRSRequest.getCustomerPrsId()) == null) {
                                    List<LoanPRS> loanList = new ArrayList<LoanPRS>();
                                    loanList.add(loanPRS);
                                    loanMap.put(customerPRSRequest.getCustomerPrsId(), loanList);
                                } else {
                                    List<LoanPRS> loanList = loanMap.get(customerPRSRequest.getCustomerPrsId());
                                    loanList.add(loanPRS);
                                    loanMap.put(customerPRSRequest.getCustomerPrsId(), loanList);
                                }
                            }
                        }
                        // mapping saving
                        List<SavingPRSRequest> savingPRSList = customerPRSRequest.getSavingList();
                        for (SavingPRSRequest savingPRSRequest : savingPRSList) {
                            SavingPRS savingPRS = new SavingPRS();
                            savingPRS = savingService.getSavingPRSById(Long.parseLong(savingPRSRequest.getAccountId()));
                            savingPRS.setPrsId(prs);
                            savingPRS.setCustomerId(custPRS);
                            savingPRS.setAccountId(Long.parseLong(savingPRSRequest.getAccountId()));
                            savingPRS.setAccountNumber(savingPRSRequest.getAccountNumber());
                            savingPRS.setWithdrawalAmount(new BigDecimal(savingPRSRequest.getWithdrawalAmount()));
                            if (savingPRSRequest.getNextWithdrawalAmountPlan() != null) {
                                savingPRS.setNextWithdrawalAmountPlan(
                                        new BigDecimal(savingPRSRequest.getNextWithdrawalAmountPlan()));
                            }
                            if (savingPRSRequest.getIsCloseSavingAccount() != null) {
                                if (savingPRSRequest.getIsCloseSavingAccount().equals("true"))
                                    savingPRS.setIsCloseSavingAccount(true);
                            }
                            savingPRS.setClearBalance(new BigDecimal(savingPRSRequest.getClearBalance()));
                            savingPRS.setDepositAmount(new BigDecimal(savingPRSRequest.getDepositAmount()));
                            savingPRS.setUpdatedBy(request.getUsername());
                            savingPRS.setUpdatedDate(new Date());
                            savingPRSForUpdate.add(savingPRS);
                            if (savingMap.get(customerPRSRequest.getCustomerPrsId()) == null) {
                                List<SavingPRS> savingList = new ArrayList<>();
                                savingList.add(savingPRS);
                                savingMap.put(customerPRSRequest.getCustomerPrsId(), savingList);
                            } else {
                                List<SavingPRS> savingList = savingMap.get(customerPRSRequest.getCustomerPrsId());
                                savingList.add(savingPRS);
                                savingMap.put(customerPRSRequest.getCustomerPrsId(), savingList);
                            }
                        }
                        custPRS.setUpdatedBy(request.getUsername());
                        custPRS.setUpdatedDate(new Date());
                        customerPRSForUpdate.add(custPRS);
                    }
                    // mapping discount Margin
                    if (discountMarginReqs != null) {
                        if (!discountMarginReqs.isEmpty()) {
                            for (DiscountMarginReq discountMarginReq : discountMarginReqs) {
                                DiscountMarginApproval discountMarginApproval = new DiscountMarginApproval();
                                discountMarginApproval.setAlasan(discountMarginReq.getAlasan());
                                discountMarginApproval.setCustomerId(discountMarginReq.getCustomerId());
                                discountMarginApproval.setTotal(discountMarginReq.getTotal());
                                discountMarginApproval.setStatus(WebGuiConstant.STATUS_WAITING_FOR_APPROVAL);
                                discountMarginApproval.setCreatedBy(request.getUsername());
                                loanService.save(discountMarginApproval);
                                for (LoanPRSRequest loan : discountMarginReq.getLoans()) {
                                    DiscountMarginLoanMap discountMarginLoanMap = new DiscountMarginLoanMap();
                                    discountMarginLoanMap
                                            .setDiscountMarginId(discountMarginApproval.getDiscountMarginId());
                                    discountMarginLoanMap.setLoanPrsId(Long.parseLong(loan.getLoanPrsId()));
                                    loanService.save(discountMarginLoanMap);
                                }
                            }
                        }
                    }
                    // 1. save PRS
                    prs.setUpdateBy(request.getUsername());
                    prs.setUpdateDate(new Date());
                    prs.setSentraId(prs.getSentraId());
                    prs.setPrsTime(request.getPrsStartTime());
                    prs.setPrsEndTime(request.getPrsEndTime());
                    prs.setReport(request.getReport());
                    log.info("PRS STATUS : " + prs.getStatus());
                    if (prs.getStatus().equals(WebGuiConstant.STATUS_SUBMIT)) {
                        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        String labelUnknown = getMessage("webservice.rc.label." + WebGuiConstant.RC_PRS_ALREADY_SUBMIT);
                        response.setResponseMessage(labelUnknown);
                        return response;
                    } else if (prs.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        String labelUnknown = getMessage("webservice.rc.label." + WebGuiConstant.RC_ALREADY_APPROVED);
                        response.setResponseMessage(labelUnknown);
                        return response;
                    } else if (prs.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        String labelUnknown = getMessage("webservice.rc.label." + WebGuiConstant.RC_ALREADY_REJECTED);
                        response.setResponseMessage(labelUnknown);
                        return response;
                    }
                    prs.setCreatedBy(request.getUsername());
                    List<CustomerPRS> customerPrsClone = new ArrayList<>(customerPRSForUpdate);
                    ESBSubmitPrsRequest esbRequest = composeESBMessageSubmit(prs, request, customerPrsClone, loanMap, savingMap);
                    ESBSubmitPrsResponse submitPrsResponse = new ESBSubmitPrsResponse();
                    try {
                        if (esbRequest != null) {
                            submitPrsResponse = restClient.submitPrs(esbRequest);
                        } else {
                            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                        }
                        if (submitPrsResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS) || submitPrsResponse.getResponseCode().equals("09")) {
                            // cut off flag to set business date to +1
                            log.info("SENDING SUCCESS");
                            Boolean cutOff = false;
                            if (submitPrsResponse.getResponseCode().equals("09")) {
                                log.info("GET RESPONSE 09");
                                cutOff = true;
                            }
                            // Date configuration for current and current+1
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(new Date());
                            Date currDate = cal.getTime();
                            cal.add(Calendar.DATE, 1);
                            Date currDatePlusOne = cal.getTime();
                            log.debug("[**] allow submit " + allowSubmit);
                            if ("1".equals(allowSubmit)) {
                                prs.setStatus(request.getStatus());
                            }
                            prs.setUpdateDate(new Date());
                            log.info("UPDATING PRS TABLE");
                            prsService.savePRS(prs);
                            log.info("UPDATING SUCCESS");
                            log.info("MAPPING TO MUTATION TRX TABLE");
                            MutationTrxTabel mutationTrxTabel = prsService.findMutationByPrs(prs.getPrsId());
                            if (mutationTrxTabel == null) {
                                mutationTrxTabel = new MutationTrxTabel();
                            }
                            mutationTrxTabel.setCreatedBy(request.getUsername());
                            mutationTrxTabel.setPrsId(prs.getPrsId());
                            mutationTrxTabel.setTrxType("PRS-Masuk");
                            if (cutOff) {
                                mutationTrxTabel.setBusinessDate(currDatePlusOne);
                            } else {
                                mutationTrxTabel.setBusinessDate(currDate);
                            }
                            log.info("INSERTING RECORD");
                            prsService.saveMutation(mutationTrxTabel);
                            log.info("INSERTING SUCCESS");
                            log.info("MAPPING TO REPORT TABLE");
                            for (ESBCustomer cust : esbRequest.getCustomerList()) {
                                for (ESBLoanCustomer loan : cust.getLoanList()) {
                                    if (loan.getDisbursementFlag().equals("1")) {
                                        List<LoanPRS> loanPRS = loanService
                                                .getLoanPRSListByAppIdAndDisbursementAmount(loan.getAppid());
                                        if (loanPRS != null) {
                                            if (!loanPRS.isEmpty()) {
                                                for (LoanPRS loanPRS2 : loanPRS) {
                                                    ReportTrxTable reportTrxLoanTable = new ReportTrxTable();
                                                    log.info("Masuk Report : " + loan.getAppid());
                                                    reportTrxLoanTable.setPrsId(prs.getPrsId());
                                                    reportTrxLoanTable.setAppId(loan.getAppid());
                                                    reportTrxLoanTable.setCreatedBy(request.getUsername());
                                                    reportTrxLoanTable.setApprovedDate(currDate);
                                                    if (cutOff) {
                                                        log.info("Melewati Cut Off");
                                                        log.info("Tanggal Bisnis : " + currDatePlusOne);
                                                        reportTrxLoanTable.setBusinessDate(currDatePlusOne);
                                                    } else
                                                        reportTrxLoanTable.setBusinessDate(currDate);
                                                    reportTrxLoanTable.setTypeTrx("1");
                                                    reportTrxLoanTable.setAmount(loanPRS2.getDisbursementAmount());
                                                    log.info("INSERTING DISBURSEMENT DATA TO REPORT");
                                                    prsService.saveToReport(reportTrxLoanTable);
                                                    log.info("INSERTING SUCCESS");
                                                }
                                            }
                                        }
                                    }
                                    if (loan.getInstallmentPaymentAmount() != null
                                            && (loan.getIsEarlyTermination().equals("false")
                                            || loan.getIsEarlyTermination().equals(""))) {
                                        if (NumberUtils.isNumber(loan.getInstallmentPaymentAmount())) {
                                            if (!loan.getInstallmentPaymentAmount().equals("0")) {
                                                ReportTrxTable reportTrxLoanTable = new ReportTrxTable();
                                                log.info("Masuk Report : " + loan.getAppid());
                                                reportTrxLoanTable.setPrsId(prs.getPrsId());
                                                reportTrxLoanTable.setAppId(loan.getAppid());
                                                reportTrxLoanTable.setCreatedBy(request.getUsername());
                                                reportTrxLoanTable.setApprovedDate(currDate);
                                                if (cutOff) {
                                                    log.info("Melewati Cut Off");
                                                    log.info("Tanggal Bisnis : " + currDatePlusOne);
                                                    reportTrxLoanTable.setBusinessDate(currDatePlusOne);
                                                } else
                                                    reportTrxLoanTable.setBusinessDate(currDate);
                                                reportTrxLoanTable.setTypeTrx("2");
                                                reportTrxLoanTable.setAmount(
                                                        new BigDecimal(loan.getInstallmentPaymentAmount().substring(0,
                                                                loan.getInstallmentPaymentAmount().length() - 2)));
                                                log.info("INSERTING REGULAR TRANSACTION TO REPORT");
                                                prsService.saveToReport(reportTrxLoanTable);
                                                log.info("INSERTING SUCCESS");
                                            }
                                        }
                                    }
                                    if (loan.getIsEarlyTermination().equals("true")) {
                                        if (NumberUtils.isNumber(loan.getInstallmentPaymentAmount())) {
                                            if (!loan.getInstallmentPaymentAmount().equals("0")) {
                                                ReportTrxTable reportTrxLoanTable = new ReportTrxTable();
                                                log.info("Masuk Report : " + loan.getAppid());
                                                reportTrxLoanTable.setPrsId(prs.getPrsId());
                                                reportTrxLoanTable.setAppId(loan.getAppid());
                                                reportTrxLoanTable.setCreatedBy(request.getUsername());
                                                reportTrxLoanTable.setApprovedDate(currDate);
                                                if (cutOff) {
                                                    log.info("Melewati Cut Off");
                                                    log.info("Tanggal Bisnis : " + currDatePlusOne);
                                                    reportTrxLoanTable.setBusinessDate(currDatePlusOne);
                                                } else
                                                    reportTrxLoanTable.setBusinessDate(currDate);
                                                reportTrxLoanTable.setTypeTrx("3");
                                                if (loan.getMarginDiscountDeviationFlag().equals("true")) {
                                                    reportTrxLoanTable.setAmount(new BigDecimal(loan
                                                            .getInstallmentPaymentAmount()
                                                            .substring(0, loan.getInstallmentPaymentAmount().length()
                                                                    - 2)).subtract(new BigDecimal(loan
                                                            .getMarginDiscountDeviationAmount()
                                                            .substring(0, loan
                                                                    .getMarginDiscountDeviationAmount()
                                                                    .length() - 2))));
                                                } else {
                                                    reportTrxLoanTable.setAmount(new BigDecimal(
                                                            loan.getInstallmentPaymentAmount().substring(0,
                                                                    loan.getInstallmentPaymentAmount().length() - 2)));
                                                }
                                                log.info("INSERTING EARLY TERMINATION TO REPORT");
                                                prsService.saveToReport(reportTrxLoanTable);
                                                log.info("INSERTING SUCCESS");
                                            }
                                        }
                                    }
                                }
                                for (ESBSaving saving : cust.getSavingList()) {
                                    if (saving.getWithdrawalAmount() != null) {
                                        if (NumberUtils.isNumber(saving.getWithdrawalAmount())) {
                                            if (!saving.getWithdrawalAmount().equals("0")) {
                                                log.info("ACCOUNT NUMBER : " + saving.getAccountNumber());
                                                log.info("JUMLAH TARIK TABUNGAN : " + saving.getWithdrawalAmount());
                                                ReportTrxTable reportTrxSavingTable = new ReportTrxTable();
                                                reportTrxSavingTable.setPrsId(prs.getPrsId());
                                                reportTrxSavingTable.setAccountNumber(saving.getAccountNumber());
                                                reportTrxSavingTable.setCreatedBy(request.getUsername());
                                                reportTrxSavingTable.setApprovedDate(currDate);
                                                if (cutOff) {
                                                    log.info("Melewati Cut Off");
                                                    log.info("Tanggal Bisnis : " + currDatePlusOne);
                                                    reportTrxSavingTable.setBusinessDate(currDatePlusOne);
                                                } else
                                                    reportTrxSavingTable.setBusinessDate(currDate);
                                                reportTrxSavingTable.setTypeTrx("4");
                                                reportTrxSavingTable.setAmount(
                                                        new BigDecimal(saving.getWithdrawalAmount().substring(0,
                                                                saving.getWithdrawalAmount().length() - 2)));
                                                log.info("INSERTING SAVING WITHDRAWAL TO REPORT");
                                                prsService.saveToReport(reportTrxSavingTable);
                                                log.info("INSERTING SUCCESS");
                                            }
                                        }
                                    }
                                    if (saving.getDepositAmount() != null) {
                                        if (NumberUtils.isNumber(saving.getDepositAmount())) {
                                            if (!saving.getDepositAmount().equals("0")) {
                                                ReportTrxTable reportTrxSavingTable = new ReportTrxTable();
                                                reportTrxSavingTable.setPrsId(prs.getPrsId());
                                                reportTrxSavingTable.setAccountNumber(saving.getAccountNumber());
                                                reportTrxSavingTable.setCreatedBy(request.getUsername());
                                                reportTrxSavingTable.setApprovedDate(currDate);
                                                if (cutOff) {
                                                    log.info("Melewati Cut Off");
                                                    log.info("Tanggal Bisnis : " + currDatePlusOne);
                                                    reportTrxSavingTable.setBusinessDate(currDatePlusOne);
                                                } else
                                                    reportTrxSavingTable.setBusinessDate(currDate);
                                                reportTrxSavingTable.setTypeTrx("5");
                                                reportTrxSavingTable.setAmount(new BigDecimal(saving.getDepositAmount()
                                                        .substring(0, saving.getDepositAmount().length() - 2)));
                                                log.info("INSERTING SAVING DEPOSIT TO REPORT");
                                                prsService.saveToReport(reportTrxSavingTable);
                                                log.info("INSERTING SUCCESS");
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            log.error("SENDING FAILED");
                            response.setResponseCode(submitPrsResponse.getResponseCode());
                            response.setResponseMessage(submitPrsResponse.getResponseMessage());
                            log.info("RESPONSE MESSAGE : " + response.toString());
                            return response;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    PRS prsSave = prsService.savePRSEvent(prs, customerPRSForUpdate, loanPRSForUpdate, savingPRSForUpdate);
                    if (prsSave == null) {
                        response.setResponseCode(WebGuiConstant.RC_FAILED);
                        String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                        response.setResponseMessage(labelFailed);
                        log.error("Inserting PRS Failed");
                    }
                    response.setPrsId(prsSave.getPrsId().toString());
                }
            }
        } catch (Exception e) {
            log.error("submitPRS error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("submitPRS error: " + e.getMessage());
        } finally {
            try {
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_PRS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("submitPRS saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("submitPRS RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SUBMIT_DISBURSEMENT_PHOTO_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    DisbursementPhotoResponse submitDisbursementPhoto(@RequestBody final PhotoRequest request,
                                                      @PathVariable("apkVersion") String apkVersion) {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final DisbursementPhotoResponse response = new DisbursementPhotoResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("submitDisbursementPhoto Try to processing Submit Disbursemet Photo request");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("trying to submit disbursement photo ");
                LoanPRS loanPrs = loanService.getLoanPRSById(Long.parseLong(request.getLoanPrsId()));
                if (loanPrs != null) {
                    DisbursementPhoto photo = loanService.getDisbursementPhoto(loanPrs);
                    if (terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "submitDisbursementPhoto")
                            && photo != null) {
                        label = getMessage("webservice.rc.label." + WebGuiConstant.PHOTO_ALREADY_EXIST);
                        response.setResponseMessage(label);
                    }
                    if (photo == null)
                        photo = new DisbursementPhoto();
                    photo.setLoanPrsId(loanPrs);
                    photo.setDisbursementPhoto(request.getDisbursementPhoto().getBytes());
                    loanService.saveDisbursementPhoto(photo);
                } else {
                    response.setResponseCode(WebGuiConstant.UNKNOWN_LOAN_ID);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                }
            }
            return response;
        } catch (Exception e) {
            log.error("submitDisbursementPhoto error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("submitDisbursementPhoto Error: " + e.getMessage());
            return response;
        } finally {
            try {
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_DISBURSEMENT_PHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("submitDisbursementPhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SUBMIT_PRS_PHOTO_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PRSPhotoResponse submitPrsPhoto(@RequestBody final PrsPhotoRequest request,
                                    @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final PRSPhotoResponse response = new PRSPhotoResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("submitPrsPhoto Try to processing Submit PRS Photo request");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("trying to submit prs photo ");
                if (request.getPrsId() == null) {
                    response.setResponseCode(WebGuiConstant.RC_PRS_NULL);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    return response;
                }
                if ("".equals(request.getPrsId())) {
                    response.setResponseCode(WebGuiConstant.RC_PRS_NULL);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    return response;
                }
                PRS prs = prsService.getPRSById(Long.parseLong(request.getPrsId()));
                if (prs != null) {
                    PRSPhoto prsPhoto = prsService.getPrsPhotoByPrs(prs);
                    if (terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "submitPrsPhoto")
                            && prsPhoto != null) {
                        label = getMessage("webservice.rc.label." + WebGuiConstant.PHOTO_ALREADY_EXIST);
                        response.setResponseMessage(label);
                    }
                    if (prsPhoto == null)
                        prsPhoto = new PRSPhoto();
                    prsPhoto.setPrsId(prs);
                    prsPhoto.setPrsPhoto(request.getPrsPhoto().getBytes());
                    prsService.savePrsPhoto(prsPhoto);
                } else {
                    response.setResponseCode(WebGuiConstant.RC_PRS_NULL);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                }
            }
        } catch (NumberFormatException e) {
            log.error("submitPrsPhoto error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("submitPrsPhoto error: " + e.getMessage());
        } finally {
            try {
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_PRS_PHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("submitPrsPhoto RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("submitPrsPhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @SuppressWarnings("finally")
    @RequestMapping(value = WebGuiConstant.TERMINAL_APPROVAL_PRS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    ApprovalPRSResponse approvalPRS(@RequestBody final ApprovalPRSRequest request,
                                    @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final ApprovalPRSResponse response = new ApprovalPRSResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("prs/approval Try to processing approval PRS request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                log.info("Validating success");
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("trying to approve PRS ");
                log.info("Check User " + request.getUserMs());
                User userMs = userService.findUserByUsername(request.getUserMs());
                if (userMs == null) {
                    log.error("USER NOT FOUND");
                    response.setResponseCode(WebGuiConstant.RC_FAILED);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    log.info("RESPONSE MESSAGE : " + response.toString());
                    return response;
                }
                if (!"2".equals(userMs.getRoleUser())) {
                    log.error("UNAUTHORIZED USER");
                    response.setResponseCode(WebGuiConstant.USER_CANNOT_APPROVE);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    log.info("RESPONSE MESSAGE : " + response.toString());
                    return response;
                }
                log.info("FIND PRS WITH ID " + request.getPrsId());
                PRS prs = prsService.getPRSById(Long.parseLong(request.getPrsId()));
                if (prs == null) {
                    // idprs tidak ditemukan, RC gagal
                    log.error("PRS NOT FOUND");
                    response.setResponseCode(WebGuiConstant.RC_PRS_NULL);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                } else {
                    if (request.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                        log.info("PRS REJECTED");
                        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        String labelReject = getMessage("webservice.rc.label." + WebGuiConstant.RC_ALREADY_REJECTED);
                        response.setResponseMessage(labelReject);
                        prs.setStatus(request.getStatus());
                        prs.setUpdateBy(request.getUserMs());
                        prs.setUpdateDate(new Date());
                        prsService.savePRS(prs);
                        log.info("RESPONSE MESSAGE : " + response.toString());
                        return response;
                    }
                    if (prs.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                        log.error("PRS ALREADY APPROVED");
                        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        String labelFailed = getMessage("webservice.rc.label." + WebGuiConstant.RC_ALREADY_APPROVED);
                        response.setResponseMessage(labelFailed);
                        log.info("RESPONSE MESSAGE : " + response.toString());
                        return response;
                    } else if (prs.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                        log.error("PRS ALREADY REJECTED");
                        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        String labelFailed = getMessage("webservice.rc.label." + WebGuiConstant.RC_ALREADY_REJECTED);
                        response.setResponseMessage(labelFailed);
                        log.info("RESPONSE MESSAGE : " + response.toString());
                        return response;
                    }
                    log.info("MAPPING FINISHED");
                    try {
                        log.info("INCOMING LOGIN APPROVAL MESSAGE");
                        ProsperaLoginRequest prosperaLogin = new ProsperaLoginRequest();
                        prosperaLogin.setUsername(request.getUserMs().trim());
                        try {
                            prosperaLogin
                                    .setPassword(prosperaEncryptionService.encryptField(request.getPassword().trim()));
                        } catch (Exception e1) {
                            log.error("PASSWORD ENCRYPTION ERROR..., " + e1.getMessage());
                        }
                        prosperaLogin.setImei(request.getImei().trim());
                        prosperaLogin.setTransmissionDateAndTime(request.getTransmissionDateAndTime().trim());
                        prosperaLogin.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber().trim());
                        log.info("Try to send login request");
                        if (prosperaLogin.getUsername() != null) {
                            userMs = userService.findUserByUsername(prosperaLogin.getUsername());
                            if (prs.getSentraId() != null) {
                                Sentra sentraPrs = sentraService
                                        .findBySentraId(prs.getSentraId().getSentraId().toString());
                                if (!sentraPrs.getLocationId().equals(userMs.getOfficeCode())) {
                                    log.error("UNAUTHORIZED USER");
                                    response.setResponseCode(WebGuiConstant.USER_CANNOT_APPROVE);
                                    String labelFailed = getMessage(
                                            "webservice.rc.label." + response.getResponseCode());
                                    response.setResponseMessage(labelFailed);
                                    log.info("RESPONSE MESSAGE : " + response.toString());
                                    return response;
                                }
                            } else {
                                log.error("UNKNOWN SENTRA");
                                response.setResponseCode(WebGuiConstant.UNKNOWN_SENTRA_ID);
                                String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(labelFailed);
                                log.info("RESPONSE MESSAGE : " + response.toString());
                                return response;
                            }
                        } else {
                            log.error("USER NOT REGISTERED");
                            response.setResponseCode(WebGuiConstant.UNREGISTERED_USER);
                            String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(labelFailed);
                            log.info("RESPONSE MESSAGE : " + response.toString());
                            return response;
                        }
                        log.info("SEND LOGIN APPROVAL MESSAGE TO ESB");
                        ProsperaLoginResponse prosperaLoginResponse = restClient.login(prosperaLogin);
                        log.info("GET RESPONSE FROM ESB : " + prosperaLoginResponse.getResponseCode());
                        response.setResponseCode(prosperaLoginResponse.getResponseCode());
                        response.setResponseMessage(prosperaLoginResponse.getResponseMessage());
                        if (!prosperaLoginResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                            log.error("SENDING FAILED");
                            response.setResponseCode(prosperaLoginResponse.getResponseCode());
                            response.setResponseMessage(prosperaLoginResponse.getResponseMessage());
                            log.info("RESPONSE MESSAGE : " + response.toString());
                            return response;
                        }
                        // update mutation trx table
                        log.info("[**] allow approve >> " + allowApprove);
                        MutationTrxTabel mt = prsService.findMutationByPrs(prs.getPrsId());
                        if (mt != null) {
                            mt.setApprovedBy(request.getUserMs());
                            if (request.getTotalPRSMoney() != null) {
                                if (!"".equals(request.getTotalPRSMoney())) {
                                    mt.setTotalPRSMoney(new BigDecimal(request.getTotalPRSMoney()));
                                } else {
                                    mt.setTotalPRSMoney(new BigDecimal(request.getTotalPRSMoney()).ZERO);
                                }
                            }
                            mt.setApprovedDate(new Date());
                            log.info("[**] allow approve success ");
                            prs.setStatus(request.getStatus());
                            prs.setUpdateBy(request.getUserMs());
                            prs.setUpdateDate(new Date());
                            prsService.saveMutation(mt);
                            prsService.savePRS(prs);
                        }
                    } catch (KeyManagementException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        } catch (NumberFormatException e) {
            log.error("prs/approval error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("prsApproval error: " + e.getMessage());
        } finally {
            try {
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_APPROVAL_PRS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("prs/approval saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("prs/approval RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }
    }

    @SuppressWarnings("unused")
    private ESBSubmitPrsRequest composeESBMessage(PRS prs, ApprovalPRSRequest request) {
        List<ESBCustomer> esbCustomer = new ArrayList<>();
        List<CustomerPRS> customerPrsList = customerService.getCustomerPRSByPrsId(prs);
        List<LoanPRS> loanPrsList = new ArrayList<>();
        List<SavingPRS> savingPrsList = new ArrayList<>();
        PRSParameter notAttendReason = new PRSParameter();
        ESBSubmitPrsRequest forESB = new ESBSubmitPrsRequest();
        try {
            forESB.setImei(request.getImei());
            forESB.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
            forESB.setTransmissionDateAndTime(request.getTransmissionDateAndTime());
            forESB.setUsername(prs.getCreatedBy());
            forESB.setPrsId(request.getPrsId());
            forESB.setSentraId(prs.getSentraId().getProsperaId().toString());
            forESB.setPrsDate(formatter.format(prs.getPrsDate()));
            forESB.setTokenChallenge(request.getTokenChallenge());
            forESB.setTokenResponse(request.getTokenResponse());
            if (prs.getPsCompanion() != null) {
                forESB.setPsCompanion(prs.getPsCompanion());
            } else {
                forESB.setPsCompanion("");
            }
            if (customerPrsList.isEmpty()) {
                forESB.setCustomerList(null);
            } else {
                for (CustomerPRS customerPrs : customerPrsList) {
                    ESBCustomer customer = new ESBCustomer();
                    customer.setCustomerId(customerPrs.getCustomerId().getProsperaId());
                    if (customerPrs.getIsAttend() != null) {
                        if (customerPrs.getIsAttend() == true) {
                            customer.setIsAttend("true");
                        } else {
                            customer.setIsAttend("false");
                            if (customerPrs.getNotAttendReason() != null) {
                                notAttendReason = parameterService.findByParameterId(customerPrs.getNotAttendReason());
                                if (notAttendReason != null) {
                                    if ("Sakit".equalsIgnoreCase(notAttendReason.getParamValueString())) {
                                        customer.setNotAttendReason(PRSConstant.sakit);
                                    } else if ("Izin".equalsIgnoreCase(notAttendReason.getParamValueString())) {
                                        customer.setNotAttendReason(PRSConstant.izin);
                                    } else if ("Lain-lain".equalsIgnoreCase(notAttendReason.getParamValueString())) {
                                        customer.setNotAttendReason(PRSConstant.lain_lain);
                                    } else {
                                        customer.setNotAttendReason(PRSConstant.alpha);
                                    }
                                } else {
                                    customer.setNotAttendReason(PRSConstant.alpha);
                                }
                            } else {
                                customer.setNotAttendReason(PRSConstant.alpha);
                            }
                        }
                    } else {
                        customer.setIsAttend("");
                    }
                    List<ESBLoanCustomer> esbLoanList = new ArrayList<>();
                    loanPrsList = loanService.getLoanPRSByPrsId(prs, customerPrs);
                    if (loanPrsList.isEmpty()) {
                        customer.setLoanList(null);
                    } else {
                        for (LoanPRS loanPrs : loanPrsList) {
                            ESBLoanCustomer loan = new ESBLoanCustomer();
                            loan.setAppid(loanPrs.getAppId());
                            loan.setDisbursementFlag(String.valueOf(loanPrs.getDisbursementFlag()));
                            if (loanPrs.getInstallmentPaymentAmount() != null) {
                                loan.setInstallmentPaymentAmount(
                                        loanPrs.getInstallmentPaymentAmount().toString().replace(".", ""));
                                if (loan.getInstallmentPaymentAmount().equals("000"))
                                    loan.setInstallmentPaymentAmount("");
                            }
                            if (loanPrs.getIsEarlyTermination() != null) {
                                log.info("APPID : " + loanPrs.getAppId());
                                log.info("Flag pelunasan dipercepat : " + loanPrs.getIsEarlyTermination());
                                if (loanPrs.getIsEarlyTermination()) {
                                    log.info("Lunas Dipercepat");
                                    loan.setIsEarlyTermination("true");
                                    if (loanPrs.getEarlyTerminationReason() != null)
                                        loan.setEarlyTerminationReason(loanPrs.getEarlyTerminationReason());
                                } else {
                                    loan.setIsEarlyTermination("false");
                                }
                            } else {
                                loan.setIsEarlyTermination("");
                            }
                            if (loanPrs.getIsEarlyTerminationAdHoc() != null) {
                                if (loanPrs.getIsEarlyTerminationAdHoc()) {
                                    log.info("Lunas Dipercepat");
                                    loan.setIsEarlyTermination("true");
                                    if (loanPrs.getEarlyTerminationReason() != null)
                                        loan.setEarlyTerminationReason(loanPrs.getEarlyTerminationReason());
                                }
                            } else {
                                loan.setIsEarlyTermination("");
                            }
                            if (loanPrs.getLoanPRSId() != null)
                                loan.setLoanId(loanPrs.getLoanId().getProsperaId());
                            if (loanPrs.getMarginDiscountDeviationFlag() != null) {
                                if (loanPrs.getMarginDiscountDeviationFlag() == true) {
                                    loan.setMarginDiscountDeviationFlag("true");
                                    if (loanPrs.getMarginDiscountDeviationPercentage() != null) {
                                        loan.setMarginDiscountDeviationPercentage(loanPrs
                                                .getMarginDiscountDeviationPercentage().toString().replace(".00", ""));
                                        if (loan.getMarginDiscountDeviationPercentage().equals("000"))
                                            loan.setMarginDiscountDeviationPercentage("");
                                    }
                                    if (loanPrs.getMarginDiscountDeviationAmount() != null) {
                                        loan.setMarginDiscountDeviationAmount(
                                                loanPrs.getMarginDiscountDeviationAmount().toString().replace(".", ""));
                                        if (loan.getMarginDiscountDeviationAmount().equals("000")) {
                                            loan.setMarginDiscountDeviationFlag("false");
                                            loan.setMarginDiscountDeviationAmount("");
                                        }
                                    }
                                } else {
                                    loan.setMarginDiscountDeviationFlag("false");
                                }
                            } else {
                                loan.setMarginDiscountDeviationFlag("");
                            }
                            if (loanPrs.getUseEmergencyFund() != null) {
                                if (loanPrs.getUseEmergencyFund() == true) {
                                    loan.setUseEmergencyFund("true");
                                } else {
                                    loan.setUseEmergencyFund("false");
                                }
                            } else {
                                loan.setUseEmergencyFund("");
                            }
                            esbLoanList.add(loan);
                        }
                    }
                    customer.setLoanList(esbLoanList.toArray(new ESBLoanCustomer[0]));
                    List<ESBSaving> esbSavingList = new ArrayList<>();
                    savingPrsList = savingService.getSavingByPRSIdAndCustomerId(prs, customerPrs);
                    for (SavingPRS savingPrs : savingPrsList) {
                        ESBSaving saving = new ESBSaving();
                        saving.setAccountId(savingPrs.getAccountIdProspera().toString());
                        saving.setAccountNumber(savingPrs.getAccountNumber());
                        if (savingPrs.getClearBalance() != null) {
                            saving.setClearBalance(savingPrs.getClearBalance().toString().replace(".", ""));
                            if (saving.getClearBalance().equals("000"))
                                saving.setClearBalance("");
                        }
                        if (savingPrs.getDepositAmount() != null) {
                            saving.setDepositAmount(savingPrs.getDepositAmount().toString().replace(".", ""));
                            if (saving.getDepositAmount().equals("000"))
                                saving.setDepositAmount("");
                        } else {
                            saving.setDepositAmount("");
                        }
                        if (savingPrs.getIsCloseSavingAccount() != null) {
                            if (savingPrs.getIsCloseSavingAccount() == true) {
                                saving.setIsCloseSavingAccount("true");
                            } else {
                                saving.setIsCloseSavingAccount("false");
                            }
                        } else {
                            saving.setIsCloseSavingAccount("");
                        }
                        if (savingPrs.getNextWithdrawalAmountPlan() != null) {
                            saving.setNextWithdrawalAmountPlan(
                                    savingPrs.getNextWithdrawalAmountPlan().toString().replace(".", ""));
                            if (saving.getNextWithdrawalAmountPlan().equals("000"))
                                saving.setNextWithdrawalAmountPlan("");
                        }
                        if (savingPrs.getWithdrawalAmount() != null) {
                            saving.setWithdrawalAmount(savingPrs.getWithdrawalAmount().toString().replace(".", ""));
                            if (saving.getWithdrawalAmount().equals("000"))
                                saving.setWithdrawalAmount("");
                        }
                        esbSavingList.add(saving);
                    }
                    customer.setSavingList(esbSavingList.toArray(new ESBSaving[0]));
                    esbCustomer.add(customer);
                }
            }
            forESB.setCustomerList(esbCustomer.toArray(new ESBCustomer[0]));
        } catch (Exception e) {
            log.error("composeESBMessage error: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
        return forESB;
    }

    private ESBSubmitPrsRequest composeESBMessageSubmit(PRS prs, SubmitPRSRequest request,
                                                        List<CustomerPRS> customerPrsList, Map<String, List<LoanPRS>> loanMap,
                                                        Map<String, List<SavingPRS>> savingMap) {
        List<ESBCustomer> esbCustomer = new ArrayList<>();
        PRSParameter notAttendReason = new PRSParameter();
        ESBSubmitPrsRequest forESB = new ESBSubmitPrsRequest();
        try {
            forESB.setImei(request.getImei());
            forESB.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
            forESB.setTransmissionDateAndTime(request.getTransmissionDateAndTime());
            forESB.setUsername(prs.getCreatedBy());
            forESB.setPrsId(request.getPrsId());
            forESB.setSentraId(prs.getSentraId().getProsperaId().toString());
            forESB.setPrsDate(formatter.format(prs.getPrsDate()));
            if (prs.getPsCompanion() != null) {
                forESB.setPsCompanion(prs.getPsCompanion());
            } else {
                forESB.setPsCompanion("");
            }
            if (customerPrsList.isEmpty()) {
                forESB.setCustomerList(null);
            } else {
                for (CustomerPRS customerPrs : customerPrsList) {
                    boolean isLoanWith = true;
                    boolean isSavingWith = true;
                    ESBCustomer customer = new ESBCustomer();
                    customer.setCustomerId(customerPrs.getCustomerId().getProsperaId());
                    if (customerPrs.getIsAttend() != null) {
                        if (customerPrs.getIsAttend() == true) {
                            customer.setIsAttend("true");
                        } else {
                            customer.setIsAttend("false");
                            if (customerPrs.getNotAttendReason() != null) {
                                notAttendReason = parameterService
                                        .findByParameterId(customerPrs.getNotAttendReason());
                                if (notAttendReason != null) {
                                    if ("Sakit".equalsIgnoreCase(notAttendReason.getParamValueString())) {
                                        customer.setNotAttendReason(PRSConstant.sakit);
                                    } else if ("Izin".equalsIgnoreCase(notAttendReason.getParamValueString())) {
                                        customer.setNotAttendReason(PRSConstant.izin);
                                    } else if ("Lain-lain"
                                            .equalsIgnoreCase(notAttendReason.getParamValueString())) {
                                        customer.setNotAttendReason(PRSConstant.lain_lain);
                                    } else {
                                        customer.setNotAttendReason(PRSConstant.alpha);
                                    }
                                } else {
                                    customer.setNotAttendReason(PRSConstant.alpha);
                                }
                            } else {
                                customer.setNotAttendReason(PRSConstant.alpha);
                            }
                        }
                    } else {
                        customer.setIsAttend("");
                    }
                    List<ESBLoanCustomer> esbLoanList = new ArrayList<>();
                    List<LoanPRS> loanPrsList = loanMap.get(String.valueOf(customerPrs.getCustomerPrsId()));
                    if (loanPrsList == null) {
                        customer.setLoanList(null);
                    } else {
                        for (LoanPRS loanPrs : loanPrsList) {
                            ESBLoanCustomer loan = new ESBLoanCustomer();
                            loan.setAppid(loanPrs.getAppId());
                            loan.setDisbursementFlag(String.valueOf(loanPrs.getDisbursementFlag()));
                            if (loanPrs.getInstallmentPaymentAmount() != null) {
                                log.info("[PRE] installment payment amount > "
                                        + loanPrs.getInstallmentPaymentAmount().toString());
                                if (loanPrs.getInstallmentPaymentAmount().compareTo(BigDecimal.ZERO) == 0) {
                                    loan.setInstallmentPaymentAmount("");
                                } else {
                                    loan.setInstallmentPaymentAmount(
                                            loanPrs.getInstallmentPaymentAmount().toString());
                                }
                                log.info("[POST] installment payment amount > "
                                        + loan.getInstallmentPaymentAmount());
                            }
                            if (loanPrs.getIsEarlyTermination() != null) {
                                log.info("APPID : " + loanPrs.getAppId());
                                log.info("Flag pelunasan dipercepat : " + loanPrs.getIsEarlyTermination());
                                if (loanPrs.getIsEarlyTermination() == true) {
                                    log.info("Lunas Dipercepat");
                                    loan.setIsEarlyTermination("true");
                                    if (loanPrs.getEarlyTerminationReason() != null)
                                        loan.setEarlyTerminationReason(loanPrs.getEarlyTerminationReason());
                                } else {
                                    loan.setIsEarlyTermination("false");
                                }
                            } else {
                                loan.setIsEarlyTermination("");
                            }
                            if (loanPrs.getIsEarlyTerminationAdHoc() != null) {
                                log.info(
                                        "Flag pelunasan dipercepat adhoc: " + loanPrs.getIsEarlyTerminationAdHoc());
                                if (loanPrs.getIsEarlyTerminationAdHoc() == true) {
                                    log.info("Lunas Dipercepat");
                                    loan.setIsEarlyTermination("true");
                                    if (loanPrs.getEarlyTerminationReason() != null)
                                        loan.setEarlyTerminationReason(loanPrs.getEarlyTerminationReason());
                                }
                            } else {
                                loan.setIsEarlyTermination("");
                            }
                            if (loanPrs.getLoanPRSId() != null)
                                loan.setLoanId(loanPrs.getLoanId().getProsperaId());
                            if (loanPrs.getMarginDiscountDeviationFlag() != null) {
                                if (loanPrs.getMarginDiscountDeviationFlag() == true) {
                                    loan.setMarginDiscountDeviationFlag("true");
                                    if (loanPrs.getMarginDiscountDeviationPercentage() != null) {
                                        if (loanPrs.getMarginDiscountDeviationPercentage()
                                                .compareTo(BigDecimal.ZERO) == 0) {
                                            loan.setMarginDiscountDeviationPercentage("");
                                        } else {
                                            loan.setMarginDiscountDeviationPercentage(
                                                    loanPrs.getMarginDiscountDeviationPercentage().toString()
                                                            .replace(".0", "").concat("00"));
                                        }

                                    }
                                    if (loanPrs.getMarginDiscountDeviationAmount() != null) {
                                        if (loanPrs.getMarginDiscountDeviationAmount()
                                                .compareTo(BigDecimal.ZERO) == 0) {
                                            loan.setMarginDiscountDeviationFlag("false");
                                            loan.setMarginDiscountDeviationAmount("");
                                        } else {
                                            loan.setMarginDiscountDeviationAmount(
                                                    loanPrs.getMarginDiscountDeviationAmount().toString());
                                        }

                                    }
                                } else {
                                    loan.setMarginDiscountDeviationFlag("false");
                                }
                            } else {
                                loan.setMarginDiscountDeviationFlag("");
                            }
                            if (loanPrs.getUseEmergencyFund() != null) {
                                if (loanPrs.getUseEmergencyFund() == true) {
                                    loan.setUseEmergencyFund("true");
                                } else {
                                    loan.setUseEmergencyFund("false");
                                }
                            } else {
                                loan.setUseEmergencyFund("");
                            }
                            log.info("loan.getIsEarlyTermination() " + loan.getAppid() + " : "
                                    + loan.getIsEarlyTermination());
                            if (!(loan.getInstallmentPaymentAmount().equals("")
                                    && loan.getDisbursementFlag().equals("4"))) {
                                log.info("Ready to Send");
                                esbLoanList.add(loan);
                            }
                        }
                    }
                    customer.setLoanList(esbLoanList.toArray(new ESBLoanCustomer[0]));
                    if (customer.getLoanList() != null) {
                        if (customer.getLoanList().length != 0) {
                            ESBLoanCustomer esbLoan = customer.getLoanList()[0];
                            isLoanWith = !(esbLoan.getInstallmentPaymentAmount().equals("")
                                    && esbLoan.getDisbursementFlag().equals("4"));
                        } else {
                            isLoanWith = false;
                        }
                    } else {
                        isLoanWith = false;
                    }
                    log.info("IS LOAN WITH : " + isLoanWith);
                    List<ESBSaving> esbSavingList = new ArrayList<>();
                    List<SavingPRS> savingPrsList = savingMap.get(String.valueOf(customerPrs.getCustomerPrsId()));
                    if (savingPrsList != null) {
                        for (SavingPRS savingPrs : savingPrsList) {
                            ESBSaving saving = new ESBSaving();
                            saving.setAccountId(savingPrs.getAccountIdProspera().toString());
                            saving.setAccountNumber(savingPrs.getAccountNumber());
                            if (savingPrs.getClearBalance() != null) {
                                if (savingPrs.getClearBalance().compareTo(BigDecimal.ZERO) == 0) {
                                    saving.setClearBalance("");
                                } else {
                                    saving.setClearBalance(savingPrs.getClearBalance().toString());
                                }
                            }
                            if (savingPrs.getDepositAmount() != null) {
                                log.info(
                                        "[PRE] saving deposit amount > " + savingPrs.getDepositAmount().toString());
                                if (savingPrs.getDepositAmount().compareTo(BigDecimal.ZERO) == 0) {
                                    saving.setDepositAmount("");
                                } else {
                                    saving.setDepositAmount(savingPrs.getDepositAmount().toString());
                                }
                                log.info("[POST] saving deposit amount > "
                                        + savingPrs.getDepositAmount().toString());
                            } else {
                                saving.setDepositAmount("");
                                log.info("[NULL] saving deposit amount " + saving.getDepositAmount());
                            }
                            if (savingPrs.getIsCloseSavingAccount() != null) {
                                if (savingPrs.getIsCloseSavingAccount() == true) {
                                    saving.setIsCloseSavingAccount("true");
                                } else {
                                    saving.setIsCloseSavingAccount("false");
                                }
                            } else {
                                saving.setIsCloseSavingAccount("");
                            }
                            if (savingPrs.getNextWithdrawalAmountPlan() != null) {
                                if (savingPrs.getWithdrawalAmount().compareTo(BigDecimal.ZERO) == 0) {
                                    saving.setNextWithdrawalAmountPlan("");
                                } else {
                                    saving.setNextWithdrawalAmountPlan(
                                            savingPrs.getNextWithdrawalAmountPlan().toString());
                                }
                            }
                            if (savingPrs.getWithdrawalAmount() != null) {
                                log.info("[PRE] saving withdrawal amount > "
                                        + savingPrs.getWithdrawalAmount().toString());
                                if (savingPrs.getWithdrawalAmount().compareTo(BigDecimal.ZERO) == 0) {
                                    saving.setWithdrawalAmount("");
                                } else {
                                    saving.setWithdrawalAmount(savingPrs.getWithdrawalAmount().toString());
                                }
                                log.info("[POST] saving withdrawal amount > " + saving.getWithdrawalAmount());
                            }
                            esbSavingList.add(saving);
                        }
                        customer.setSavingList(esbSavingList.toArray(new ESBSaving[0]));
                        if (customer.getSavingList() != null) {
                            if (customer.getSavingList().length != 0) {
                                ESBSaving esbsv = customer.getSavingList()[0];
                                isSavingWith = !(esbsv.getWithdrawalAmount().equals("")
                                        && esbsv.getDepositAmount().equals(""));
                            } else {
                                isSavingWith = false;
                            }
                        } else {
                            isSavingWith = false;
                        }
                        log.info("IS SAVING WITH : " + isSavingWith);
                    }
                    if (isLoanWith || isSavingWith) {
                        log.info("ADD CUSTOMER");
                        esbCustomer.add(customer);
                    }
                }
            }
            forESB.setCustomerList(esbCustomer.toArray(new ESBCustomer[0]));
        } catch (Exception e) {
            log.error("composeESBMessageSubmit error: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
        return forESB;
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_CHECK_PRS_STATUS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    CheckStatusPRSResponse checkPrsStatus(@RequestBody final CheckStatusPRSRequest request,
                                          @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final CheckStatusPRSResponse response = new CheckStatusPRSResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("prs/checkStatus Try to processing check PRS Status request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Check PRS Status");
                List<SentraPRSListResponse> sentraPrsList = new ArrayList<>();
                User user = (User) userService.loadUserByUsername(request.getUsername());
                List<Sentra> sentraList = sentraService.getSentraByWismaId(user.getOfficeCode());
                for (Sentra sentra : sentraList) {
                    SentraPRSListResponse sentraPrs = new SentraPRSListResponse();
                    PRS prs = prsService.getPRSBySentraPrevious(sentra, new Date());
                    if (prs != null) {
                        if (prs.getSentraId() != null)
                            sentraPrs.setSentraId(prs.getSentraId().getSentraId().toString());
                        sentraPrs.setStatusPrs(prs.getStatus());
                        if (prs.getUpdateDate() != null) {
                            sentraPrs.setUpdatedDate(formatter.format(prs.getUpdateDate()));
                        } else {
                            sentraPrs.setUpdatedDate("");
                        }
                        sentraPrsList.add(sentraPrs);
                    }
                }
                response.setSentraList(sentraPrsList);
            }
        } catch (UsernameNotFoundException e) {
            log.error("prs/checkStatus error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("prsCheckStatus error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_LOAN_REFRESH);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("prs/checkStatus RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("prs/checkStatus saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_CHECK_SUM_RCTT_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PRSSummaryRCTTListResponse sumRctt(@RequestBody final SyncPRSRequest request,
                                       @PathVariable("apkVersion") String apkVersion) throws IOException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        Date prsDate = new Date();
        try {
            prsDate = formatter.parse(request.getPrsDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        final PRSSummaryRCTTListResponse response = new PRSSummaryRCTTListResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("cekRctt Try to processing Check Summary RCTT By MS : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Validation success, get Summary RCTT");
                User userMS = (User) userService.loadUserByUsername(request.getUsername());
                Calendar c = Calendar.getInstance();
                c.setTime(prsDate);
                // manipulate date
                c.add(Calendar.DATE, 1);
                c.clear(Calendar.HOUR_OF_DAY);
                c.clear(Calendar.AM_PM);
                c.clear(Calendar.MINUTE);
                c.clear(Calendar.SECOND);
                c.clear(Calendar.MILLISECOND);// same with c.add(Calendar.DAY_OF_MONTH, 1);
                // convert calendar to date
                Date currentDatePlusOne = c.getTime();
                List<PRS> prsList = prsService.getSentraByPrsAndPrsDate(userMS.getOfficeCode(), prsDate);
                List<PRSSummaryRCTTResponse> prsResponseList = new ArrayList<>();
                //disbursementFlag tunda
                char disbursementFlag = '2';
                // H+1
                if (prsList.isEmpty()) {
                    List<Sentra> sentraList = sentraService.getSentraByWismaId(userMS.getOfficeCode());
                    for (Sentra sentra : sentraList) {
                        PRSSummaryRCTTResponse prsResponse = new PRSSummaryRCTTResponse();
                        List<Loan> loanList = loanService.getLoanCairBySentra(sentra.getSentraId(), prsDate,
                                currentDatePlusOne);
                        List<LoanPRSListResponse> loanPRSListTemp = new ArrayList<>();
                        List<SavingPRSListResponse> savingPRSListTemp = new ArrayList<>();
                        for (Loan loan : loanList) {
                            if (loan.getPlafond() != null) {
                                if (loan.getPlafond().compareTo(BigDecimal.ZERO) != 0) {
                                    LoanPRSListResponse loanPRSSet = new LoanPRSListResponse();
                                    loanPRSSet.setLoanId(loan.getLoanId().toString());
                                    loanPRSSet.setAppId(loan.getAppId());
                                    loanPRSSet.setDisbursementAmount(loan.getPlafond());
                                    if (loan != null) {
                                        if (loan.getProductId() != null) {
                                            LoanProduct loanProduct = swService
                                                    .findByProductId(loan.getProductId().toString());
                                            if (loanProduct != null) {
                                                if (loanProduct.getProductCode().toUpperCase().startsWith("NOV")) {
                                                    loanPRSSet.setIsLoanPhone("true");
                                                } else {
                                                    loanPRSSet.setIsLoanPhone("false");
                                                }
                                            }
                                        }
                                    }
                                    log.info("loanPRSSet.getAppId() H+1 : " + loanPRSSet.getAppId());
                                    loanPRSListTemp.add(loanPRSSet);
                                }
                            }
                        }
                        Calendar cmin = Calendar.getInstance();
                        cmin.setTime(prsDate);
                        // manipulate date
                        cmin.add(Calendar.DATE, -14);
                        cmin.clear(Calendar.HOUR_OF_DAY);
                        cmin.clear(Calendar.AM_PM);
                        cmin.clear(Calendar.MINUTE);
                        cmin.clear(Calendar.SECOND);
                        cmin.clear(Calendar.MILLISECOND);// same with c.add(Calendar.DAY_OF_MONTH, 1);
                        // convert calendar to date
                        Date prsDateMinTwoWeeks = cmin.getTime();
                        List<LoanPRS> listLoanPrs = loanService.findBySentraAndPrsDateAndDisbursmentFlag(
                                sentra.getSentraId(), prsDateMinTwoWeeks, prsDateMinTwoWeeks, disbursementFlag);
                        for (LoanPRS loanPRS : listLoanPrs) {
                            if (loanPRS.getDisbursementAmount() != null) {
                                if (loanPRS.getDisbursementAmount().compareTo(BigDecimal.ZERO) != 0) {
                                    LoanPRSListResponse loanPRSSet = new LoanPRSListResponse();
                                    loanPRSSet.setLoanId(loanPRS.getLoanId().getLoanId().toString());
                                    loanPRSSet.setAppId(loanPRS.getAppId());
                                    loanPRSSet.setDisbursementAmount(loanPRS.getDisbursementAmount());
                                    Loan loan = loanService.findById(loanPRS.getLoanId().getLoanId().toString());
                                    if (loan != null) {
                                        if (loan.getProductId() != null) {
                                            LoanProduct loanProduct = swService
                                                    .findByProductId(loan.getProductId().toString());
                                            if (loanProduct != null) {
                                                if (loanProduct.getProductCode().toUpperCase().startsWith("NOV")) {
                                                    loanPRSSet.setIsLoanPhone("true");
                                                } else {
                                                    loanPRSSet.setIsLoanPhone("false");
                                                }
                                            }
                                        }
                                    }
                                    log.info("loanPRSSet.getAppId() H+0 : " + loanPRSSet.getAppId());
                                    loanPRSListTemp.add(loanPRSSet);
                                }
                            }
                        }
                        List<SavingPRS> savingRTList = savingService.findSavingRTBySentraId(sentra.getSentraId(),
                                prsDateMinTwoWeeks);
                        for (SavingPRS savingRT : savingRTList) {
                            if (savingRT.getNextWithdrawalAmountPlan() != null) {
                                if (savingRT.getNextWithdrawalAmountPlan().compareTo(BigDecimal.ZERO) != 0) {
                                    SavingPRSListResponse savingPRSSet = new SavingPRSListResponse();
                                    savingPRSSet.setAccountId(savingRT.getAccountId().toString());
                                    savingPRSSet.setAccountNumber(savingRT.getAccountNumber());
                                    savingPRSSet
                                            .setWithdrawalAmountPlan(savingRT.getNextWithdrawalAmountPlan().toString());
                                    savingPRSListTemp.add(savingPRSSet);
                                }
                            }
                        }
                        if (!loanPRSListTemp.isEmpty()) {
                            prsResponse.setLoanList(loanPRSListTemp);
                        }
                        if (!savingPRSListTemp.isEmpty()) {
                            prsResponse.setSavingList(savingPRSListTemp);
                        }
                        if (!savingPRSListTemp.isEmpty() || !loanPRSListTemp.isEmpty()) {
                            if (sentra.getAssignedTo() != null) {
                                User userPS = userService.findUserByUsername(sentra.getAssignedTo());
                                if (userPS != null)
                                    prsResponse.setPsAssign(userPS.getName());
                            }
                        }
                        log.info("prsResponse.getPsAssign() H+1 : " + prsResponse.getPsAssign());
                        if (prsResponse.getLoanList() != null || prsResponse.getSavingList() != null) {
                            prsResponseList.add(prsResponse);
                        }
                    }
                    //H+0
                } else {
                    for (PRS prsMaster : prsList) {
                        PRSSummaryRCTTResponse prsResponse = new PRSSummaryRCTTResponse();
                        List<LoanPRSListResponse> loanPRSListTemp = new ArrayList<>();
                        List<SavingPRSListResponse> savingPRSListTemp = new ArrayList<>();
                        log.info("prsId H+0 : " + prsMaster.getPrsId());
                        if (prsMaster != null) {
                            List<LoanPRS> loanRCList = loanService.findLoanPRSByPrsId(prsMaster);
                            for (LoanPRS loanRC : loanRCList) {
                                if (loanRC.getDisbursementAmount() != null) {
                                    if (loanRC.getDisbursementAmount().compareTo(BigDecimal.ZERO) != 0) {
                                        LoanPRSListResponse loanPRSSet = new LoanPRSListResponse();
                                        loanPRSSet.setLoanId(loanRC.getLoanId().getLoanId().toString());
                                        loanPRSSet.setAppId(loanRC.getAppId());
                                        loanPRSSet.setDisbursementAmount(loanRC.getDisbursementAmount());
                                        Loan loan = loanService.findById(loanRC.getLoanId().getLoanId().toString());
                                        if (loan != null) {
                                            if (loan.getProductId() != null) {
                                                LoanProduct loanProduct = swService
                                                        .findByProductId(loan.getProductId().toString());
                                                if (loanProduct != null) {
                                                    if (loanProduct.getProductCode().toUpperCase().startsWith("NOV")) {
                                                        loanPRSSet.setIsLoanPhone("true");
                                                    } else {
                                                        loanPRSSet.setIsLoanPhone("false");
                                                    }
                                                }
                                            }
                                        }
                                        log.info("loanPRSSet.getAppId() H+0 : " + loanPRSSet.getAppId());
                                        loanPRSListTemp.add(loanPRSSet);
                                    }
                                }
                            }
                            List<SavingPRS> savingRTList = savingService.findSavingPRSByPrsId(prsMaster);
                            for (SavingPRS savingRT : savingRTList) {
                                if (savingRT.getWithdrawalAmountPlan() != null) {
                                    if (savingRT.getWithdrawalAmountPlan().compareTo(BigDecimal.ZERO) != 0) {
                                        SavingPRSListResponse savingPRSSet = new SavingPRSListResponse();
                                        savingPRSSet.setAccountId(savingRT.getAccountId().toString());
                                        savingPRSSet.setAccountNumber(savingRT.getAccountNumber());
                                        savingPRSSet
                                                .setWithdrawalAmountPlan(savingRT.getWithdrawalAmountPlan().toString());
                                        savingPRSListTemp.add(savingPRSSet);
                                    }
                                }
                            }

                            if (!loanPRSListTemp.isEmpty()) {
                                prsResponse.setLoanList(loanPRSListTemp);
                            }
                            if (!savingPRSListTemp.isEmpty()) {
                                prsResponse.setSavingList(savingPRSListTemp);
                            }
                            if (!savingPRSListTemp.isEmpty() || !loanPRSListTemp.isEmpty()) {
                                if (prsMaster.getSentraId().getAssignedTo() != null) {
                                    User userPS = userService
                                            .findUserByUsername(prsMaster.getSentraId().getAssignedTo());
                                    if (userPS != null)
                                        prsResponse.setPsAssign(userPS.getName());
                                }
                            }
                            if (prsResponse.getLoanList() != null || prsResponse.getSavingList() != null) {
                                prsResponseList.add(prsResponse);
                            }
                        }
                    }
                }
                response.setPrsList(prsResponseList);
            }
        } catch (Exception e) {
            log.error("cekRctt error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("cekRctt error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_CHECK_SUM_RCTT_REQUEST);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("cekRctt RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("cekRctt saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_LOGIN_APPROVAL_PATH, method = {RequestMethod.POST})
    public @ResponseBody
    LoginResponse doLogin(@RequestBody LoginRequest request,
                          @PathVariable("apkVersion") String versionNumber)
            throws KeyManagementException, NoSuchAlgorithmException, IOException {

        LoginResponse response = new LoginResponse();
        ProsperaLoginRequest prosperaLogin = new ProsperaLoginRequest();
        prosperaLogin.setUsername(request.getUsername().trim());
        try {
            prosperaLogin.setPassword(prosperaEncryptionService.encryptField(request.getPassword().trim()));
        } catch (Exception e1) {
            log.error("PASSWORD ENCRYPTION ERROR..., " + e1.getMessage());
        }
        prosperaLogin.setImei(request.getImei().trim());
        prosperaLogin.setTransmissionDateAndTime(request.getTransmissionDateAndTime().trim());
        prosperaLogin.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber().trim());
        try {
            log.info("loginApprove INCOMING LOGIN APPROVAL MESSAGE : " + jsonUtils.toJson(request));
            User user = userService.findUserByUsername(request.getUsername());
            if (user != null) {
                if ("1".equals(user.getRoleUser())) {
                    response.setResponseCode(WebGuiConstant.USER_CANNOT_APPROVE);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    return response;
                }
            } else {
                response.setResponseCode(WebGuiConstant.RC_USERNAME_NOTEXISTS);
                String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(labelFailed);
                return response;
            }
            // access url and get the response
            log.info("Try to send login request");
            if (request.getSentraList() != null) {
                for (PRSPrepare sentra : request.getSentraList()) {
                    Sentra sentraForPrs = sentraService.findBySentraId(sentra.getSentraId());
                    if (user != null) {
                        if (sentraForPrs != null) {
                            if (!user.getOfficeCode().equals(sentraForPrs.getLocationId())) {
                                response.setResponseCode(WebGuiConstant.UNREGISTERED_USER);
                                response.setResponseMessage(
                                        getMessage("webservice.rc.label." + response.getResponseCode()));
                                return response;
                            }
                        }
                    } else {
                        response.setResponseCode(WebGuiConstant.RC_USERNAME_NOTEXISTS);
                        response.setResponseMessage(getMessage("webservice.rc.label." + response.getResponseCode()));
                        return response;
                    }
                }
            }
            ProsperaLoginResponse prosperaLoginResponse = restClient.login(prosperaLogin);
            log.info("Get Response from ESB : " + prosperaLoginResponse.getResponseCode());
            if (request.getType() == null) {
                response.setResponseCode(WebGuiConstant.USER_CANNOT_APPROVE);
                response.setResponseMessage(getMessage("webservice.rc.label." + response.getResponseCode()));
                return response;
            }
            if (prosperaLoginResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                if (request.getTotalCollect() != null && !"".equals(request.getTotalCollect())
                        && request.getType().equals("2")) {
                    if (NumberUtils.isNumber(request.getTotalCollect())) {
                        MutationTrxTabel mutationTrxTabel = new MutationTrxTabel();
                        mutationTrxTabel.setApprovedBy(request.getUsername());
                        mutationTrxTabel.setApprovedDate(new Date());
                        if (request.getUserPs() != null)
                            mutationTrxTabel.setCreatedBy(request.getUserPs());
                        mutationTrxTabel.setTotalCollect(new BigDecimal(request.getTotalCollect()));
                        mutationTrxTabel.setTrxType("Penagihan");
                        mutationTrxTabel.setBusinessDate(new Date());
                        // prsService.saveMutation(mutationTrxTabel);
                    }
                } else if (request.getType().equals("1")) {
                    if (request.getSentraList() != null) {
                        for (PRSPrepare sentra : request.getSentraList()) {
                            Sentra sentraForPrs = sentraService.findBySentraId(sentra.getSentraId());
                            if (sentraForPrs != null) {
                                PRS prs = prsService.getPrsBySentraId(sentraForPrs);
                                if (prs != null) {
                                    if (prs.getStatus().equals(WebGuiConstant.STATUS_INPROCESS)) {
                                        response.setResponseCode(prosperaLoginResponse.getResponseCode());
                                        response.setResponseMessage(prosperaLoginResponse.getResponseMessage());
                                        return response;
                                    } else {
                                        if (sentra.getBringMoney() != null && !"".equals(sentra.getBringMoney())) {
                                            prs.setBringMoney(new BigDecimal(sentra.getBringMoney()));
                                            prs.setStatus(WebGuiConstant.STATUS_INPROCESS);
                                        }
                                        prsService.savePRS(prs);
                                    }
                                }
                            }
                        }
                    }
                    log.info("try to create mutation TRXTable ");
                    MutationTrxTabel mutationTrxTabel = new MutationTrxTabel();
                    log.info("Create mutation TRXTable : SUCCESS");
                    mutationTrxTabel.setApprovedBy(request.getUsername());
                    mutationTrxTabel.setApprovedDate(new Date());
                    if (request.getUserPs() != null)
                        mutationTrxTabel.setCreatedBy(request.getUserPs());
                    if (request.getTotalBringMoney() != null && !"".equals(request.getTotalBringMoney())) {
                        if (NumberUtils.isNumber(request.getTotalBringMoney())) {
                            log.info("Set Bring Money to mutation TRXTable ");
                            mutationTrxTabel.setBringMoney(new BigDecimal(request.getTotalBringMoney()));
                        } else {
                            mutationTrxTabel.setBringMoney(BigDecimal.ZERO);
                        }
                    } else {
                        mutationTrxTabel.setBringMoney(BigDecimal.ZERO);
                    }
                    mutationTrxTabel.setTrxType("PRS-Keluar");
                    mutationTrxTabel.setBusinessDate(new Date());
                    log.info("try to save value to mutationTrxTabel");
                    prsService.saveMutation(mutationTrxTabel);
                    log.info("Save value to mutationTrxTabel : SUCCESS");
                }
            }
            response.setResponseCode(prosperaLoginResponse.getResponseCode());
            response.setResponseMessage(prosperaLoginResponse.getResponseMessage());
        } catch (Exception e) {
            log.error("loginApprove error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("loginApprove error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_LOGIN_APPROVAL_PATH);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey("");
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("loginApprove RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("loginApprove saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.DISCOUNT_MARGIN_SUBMIT_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    DiscountMarginApprovalResponse doAdd(@RequestBody String requestString,
                                         @PathVariable("apkVersion") String apkVersion) throws Exception {

        Object obj = new JsonUtils().fromJson(requestString, DiscountMarginReq.class);
        final DiscountMarginReq request = (DiscountMarginReq) obj;
        final DiscountMarginApprovalResponse responseCode = new DiscountMarginApprovalResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        String discountMarginId = "";
        try {
            log.info("discountMarginApproval INCOMING SUBMIT DISCOUNT MARGIN REQUEST : " + requestString);
            // validasi
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.trace("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei()
                        + " ,sessionKey : " + request.getSessionKey());
            } else if (!"REJECTED".equalsIgnoreCase(request.getStatus())
                    && !"DRAFT".equalsIgnoreCase(request.getStatus())
                    && !"APPROVED".equalsIgnoreCase(request.getStatus())
                    && !"WAITING FOR APPROVAL".equalsIgnoreCase(request.getStatus())) {
                responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_STATUS);
            } else {
                if (request.getCustomerId() != null) {
                    Customer customer = customerService.findById(request.getCustomerId().toString());
                    if (customer == null) {
                        responseCode.setResponseCode(WebGuiConstant.UNKNOWN_CUSTOMER_ID);
                        String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(label);
                        return responseCode;
                    }
                }
                DiscountMarginApproval discountMarginApproval = loanService
                        .getDiscontMarginApprovalById(request.getDiscountMarginId());
                if (discountMarginApproval == null) {
                    discountMarginApproval = new DiscountMarginApproval();
                }
                discountMarginApproval.setStatus(request.getStatus());
                discountMarginApproval.setUserBwmp(request.getUserBwmp());
                loanService.save(discountMarginApproval);
                discountMarginId = discountMarginApproval.getDiscountMarginId().toString();
                DiscountMarginOfficerMap discountMarginOfficerMap = new DiscountMarginOfficerMap();
                User user = userService.findUserByUsername(request.getUsername());
                if (request.getStatus().equals(WebGuiConstant.STATUS_WAITING_FOR_APPROVAL)) {
                    discountMarginOfficerMap.setDiscountMarginId(Long.parseLong(discountMarginId));
                    discountMarginOfficerMap.setOfficer(user.getUserId());
                    discountMarginOfficerMap.setStatus(request.getStatus());
                    discountMarginOfficerMap.setUpdatedBy(request.getUsername());
                    loanService.save(discountMarginOfficerMap);
                } else if (request.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                    discountMarginOfficerMap.setDiscountMarginId(Long.parseLong(discountMarginId));
                    discountMarginOfficerMap.setStatus(request.getStatus());
                    discountMarginOfficerMap.setUpdatedBy(request.getUsername());
                    discountMarginOfficerMap.setUpdatedDate(new Date());
                    loanService.save(discountMarginOfficerMap);
                } else if (request.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                    discountMarginOfficerMap.setDiscountMarginId(Long.parseLong(discountMarginId));
                    discountMarginOfficerMap.setStatus(request.getStatus());
                    discountMarginOfficerMap.setUpdatedBy(request.getUsername());
                    discountMarginOfficerMap.setUpdatedDate(new Date());
                    loanService.save(discountMarginOfficerMap);
                }
                List<DiscountMarginApprovalHistoryRequest> discountMarginApprovalHistoryRequestsList = request
                        .getApprovalHistories();
                for (DiscountMarginApprovalHistoryRequest discountMarginApprovalHistoryRequest : discountMarginApprovalHistoryRequestsList) {
                    DiscountMarginApprovalHistory discountMarginApprovalHistory = loanService
                            .findByDiscountMarginIdAndLevel(
                                    Long.parseLong(discountMarginApprovalHistoryRequest.getDiscountMarginId()),
                                    discountMarginApprovalHistoryRequest.getLevel());
                    if (discountMarginApprovalHistory == null) {
                        discountMarginApprovalHistory = new DiscountMarginApprovalHistory();
                    }
                    discountMarginApprovalHistory.setDiscountMarginId(
                            Long.parseLong(discountMarginApprovalHistoryRequest.getDiscountMarginId()));
                    discountMarginApprovalHistory.setLimit(discountMarginApprovalHistoryRequest.getLimit());
                    discountMarginApprovalHistory.setDate(discountMarginApprovalHistoryRequest.getDate());
                    discountMarginApprovalHistory.setLevel(discountMarginApprovalHistoryRequest.getLevel());
                    discountMarginApprovalHistory.setName(discountMarginApprovalHistoryRequest.getName());
                    discountMarginApprovalHistory.setRole(discountMarginApprovalHistoryRequest.getRole());
                    discountMarginApprovalHistory.setStatus(discountMarginApprovalHistoryRequest.getStatus());
                    discountMarginApprovalHistory
                            .setSupervisorId(discountMarginApprovalHistoryRequest.getSupervisorId());
                    discountMarginApprovalHistory.setTanggal(new Date());
                    if (discountMarginApprovalHistoryRequest.getApprovedAmount() != null
                            || !discountMarginApprovalHistoryRequest.getApprovedAmount().equals(""))
                        discountMarginApprovalHistory.setApprovedAmount(
                                new BigDecimal(discountMarginApprovalHistoryRequest.getApprovedAmount()));
                    loanService.save(discountMarginApprovalHistory);
                }
                responseCode.setDiscountMarginId(discountMarginId);
                responseCode.setLocalId(request.getLocalId());
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
            }
        } catch (Exception e) {
            log.error("discountMarginApproval error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("discountMarginApproval error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_LOAN);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save messages masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save messages keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(responseCode).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_LOAN);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        auditLog.setDescription(request.toString() + " | " + responseCode.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                    }
                });
                log.info("discountMarginApproval RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("discountMarginApproval saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

    @RequestMapping(value = WebGuiConstant.DISCOUNT_MARGIN_LIST_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    DiscountMarginListResponse discountMarginList(@RequestBody final DiscountMarginListRequest request,
                                                  @PathVariable("apkVersion") String apkVersion) throws IOException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final DiscountMarginListResponse response = new DiscountMarginListResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("discountMarginList Try to processing Discount Margin Request List : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            //Date configuration for current and current-14
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.clear(Calendar.HOUR_OF_DAY);
            cal.clear(Calendar.HOUR);
            cal.clear(Calendar.AM_PM);
            cal.clear(Calendar.MINUTE);
            cal.clear(Calendar.SECOND);
            cal.clear(Calendar.MILLISECOND);
            Date currDate = cal.getTime();
            cal.add(Calendar.DATE, -14);
            Date currDateMinTwoWeeks = cal.getTime();
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Validation success, get discount margin list");
                User userMs = userService.findUserByUsername(request.getUsername());
                Location loc = areaService.findLocationById(userMs.getOfficeCode());
                List<CustomerListResponse> customerListMap = new ArrayList<>();
                CustomerListResponse customerListResponse = null;
                if ("3".equals(userMs.getRoleUser())) {
                    List<DiscountMarginApprovalAndHistory> approvaAndlHistory = loanService.getDiscountMarginApprovalAndHistory(userMs.getUserId().toString());
                    for (DiscountMarginApprovalAndHistory discountMarginApprovalHistories : approvaAndlHistory) {
                        if (discountMarginApprovalHistories != null) {
                            if (!discountMarginApprovalHistories.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                                Customer customer = customerService.findById(discountMarginApprovalHistories.getCustomerId().toString());
                                CustomerPRS customerPRS = customerService.findTopCustomerPRSbyCustomer(customer);
                                if ((customerPRS.getPrsId().getPrsDate().after(currDateMinTwoWeeks) || customerPRS.getPrsId().getPrsDate().equals(currDateMinTwoWeeks)) && (customerPRS.getPrsId().getPrsDate().before(currDate) || customerPRS.getPrsId().getPrsDate().equals(currDate))) {
                                    customerListResponse = new CustomerListResponse();
                                    Group group = sentraService.getGroupByGroupId(customer.getGroup().getGroupId().toString());
                                    Sentra sentra = sentraService.findBySentraId(group.getSentra().getSentraId().toString());
                                    customerListResponse.setCustomerId(customer.getCustomerId().toString());
                                    customerListResponse.setWismaCode(sentra.getLocationId());
                                    loc = areaService.findLocationById(sentra.getLocationId());
                                    customerListResponse.setWismaId(loc.getLocationId());
                                    customerListResponse.setWismaName(loc.getName());
                                    customerListResponse.setDiscountMarginId(discountMarginApprovalHistories.getDiscountMarginId().toString());
                                    customerListResponse.setRequestedAmount(discountMarginApprovalHistories.getTotal().toString());
                                    customerListResponse.setCifNumber(customer.getCustomerCifNumber());
                                    customerListResponse.setCustomerName(customer.getCustomerName());
                                    customerListResponse.setSentraName(sentra.getSentraName());
                                    customerListResponse.setGroupName(group.getGroupName());
                                    List<DiscountMarginLoanMap> discountMarginLoanMapList = loanService.getDiscountMarginLoanMapByDiscountMarginId(discountMarginApprovalHistories.getDiscountMarginId());
                                    if (discountMarginLoanMapList != null) {
                                        if (!discountMarginLoanMapList.isEmpty()) {
                                            List<LoanListDiscountMarginResponse> loanListDiscountMarginResponsesMap = new ArrayList<>();
                                            for (DiscountMarginLoanMap discountMarginLoanMap : discountMarginLoanMapList) {
                                                LoanPRS loanPRS = loanService.getLoanPRSById(discountMarginLoanMap.getLoanPrsId());
                                                LoanListDiscountMarginResponse loanListDiscountMarginResponse = new LoanListDiscountMarginResponse();
                                                loanListDiscountMarginResponse.setAppid(loanPRS.getAppId());
                                                loanListDiscountMarginResponse.setLoanId(loanPRS.getLoanId().getLoanId().toString());
                                                loanListDiscountMarginResponse.setLoanPrsId(loanPRS.getLoanPRSId().toString());
                                                loanListDiscountMarginResponse.setSisaMargin(loanPRS.getOutstandingMarginAmount());
                                                loanListDiscountMarginResponse.setSisaPokok(loanPRS.getRemainingOutstandingPrincipal());
                                                loanListDiscountMarginResponsesMap.add(loanListDiscountMarginResponse);
                                            }
                                            customerListResponse.setLoanList(loanListDiscountMarginResponsesMap);
                                        }
                                    }
                                    List<DiscountMarginApprovalHistory> discountMarginApprovalHistoriesList = loanService.getDiscountMarginApprovalHistoryByDiscountMarginId(discountMarginApprovalHistories.getDiscountMarginId());
                                    List<DiscountMarginApprovalHistoryResponse> discountMarginApprovalHistoryListMap = new ArrayList<>();
                                    if (discountMarginApprovalHistoriesList != null) {
                                        if (!discountMarginApprovalHistoriesList.isEmpty()) {
                                            for (DiscountMarginApprovalHistory discountMarginApprovalHistory : discountMarginApprovalHistoriesList) {
                                                DiscountMarginApprovalHistoryResponse discountMarginApprovalHistoryMap = new DiscountMarginApprovalHistoryResponse();
                                                discountMarginApprovalHistoryMap.setLevel(discountMarginApprovalHistory.getLevel());
                                                discountMarginApprovalHistoryMap.setDiscountMarginId(discountMarginApprovalHistory.getDiscountMarginId().toString());
                                                discountMarginApprovalHistoryMap.setLimit(discountMarginApprovalHistory.getLimit());
                                                discountMarginApprovalHistoryMap.setName(discountMarginApprovalHistory.getName());
                                                discountMarginApprovalHistoryMap.setRole(discountMarginApprovalHistory.getRole());
                                                discountMarginApprovalHistoryMap.setDate(discountMarginApprovalHistory.getDate());
                                                discountMarginApprovalHistoryMap.setStatus(discountMarginApprovalHistory.getStatus());
                                                discountMarginApprovalHistoryMap.setSupervisorId(discountMarginApprovalHistory.getSupervisorId());
                                                if (discountMarginApprovalHistory.getTanggal() != null) {
                                                    discountMarginApprovalHistoryMap.setTanggal(formatter.format(discountMarginApprovalHistory.getTanggal()));
                                                }
                                                if (discountMarginApprovalHistory.getApprovedAmount() != null) {
                                                    discountMarginApprovalHistoryMap.setApprovedAmount(discountMarginApprovalHistory.getApprovedAmount().toString());
                                                } else {
                                                    discountMarginApprovalHistoryMap.setApprovedAmount(discountMarginApprovalHistory.getApprovedAmount().ZERO.toString());
                                                }
                                                discountMarginApprovalHistoryListMap.add(discountMarginApprovalHistoryMap);
                                            }
                                            customerListResponse.setApprovalHistories(discountMarginApprovalHistoryListMap);
                                        } else {
                                            customerListResponse.setApprovalHistories(new ArrayList<>());
                                        }
                                    } else {
                                        customerListResponse.setApprovalHistories(new ArrayList<>());
                                    }
                                    customerListMap.add(customerListResponse);
                                }
                            }
                        }
                    }
                    response.setCustomerList(customerListMap);
                    return response;
                }
                List<Sentra> sentraList = sentraService.getSentraByWismaId(userMs.getOfficeCode());
                for (Sentra sentra : sentraList) {
                    List<Customer> customerList = customerService.getBySentraId(sentra);
                    for (Customer customer : customerList) {
                        List<DiscountMarginApproval> discountMarginApprovalList = loanService.findByCustomerIdAndStatusNotRejected(customer.getCustomerId());
                        if (discountMarginApprovalList != null || !discountMarginApprovalList.isEmpty()) {
                            for (DiscountMarginApproval discountMarginApproval : discountMarginApprovalList) {
                                if (!discountMarginApproval.getStatus().equals(WebGuiConstant.STATUS_APPROVED) || "1".equals(userMs.getRoleUser())) {
                                    CustomerPRS customerPRS = customerService.findTopCustomerPRSbyCustomer(customer);
                                    if ((customerPRS.getPrsId().getPrsDate().after(currDateMinTwoWeeks) || customerPRS.getPrsId().getPrsDate().equals(currDateMinTwoWeeks)) && (customerPRS.getPrsId().getPrsDate().before(currDate) || customerPRS.getPrsId().getPrsDate().equals(currDate))) {
                                        customerListResponse = new CustomerListResponse();
                                        customerListResponse.setCustomerId(customer.getCustomerId().toString());
                                        customerListResponse.setWismaCode(loc.getLocationCode());
                                        customerListResponse.setWismaId(loc.getLocationId());
                                        customerListResponse.setWismaName(loc.getName());
                                        customerListResponse.setDiscountMarginId(discountMarginApproval.getDiscountMarginId().toString());
                                        customerListResponse.setRequestedAmount(discountMarginApproval.getTotal().toString());
                                        customerListResponse.setCifNumber(customer.getCustomerCifNumber());
                                        customerListResponse.setCustomerName(customer.getCustomerName());
                                        Group group = sentraService.getGroupByGroupId(customer.getGroup().getGroupId().toString());
                                        customerListResponse.setSentraName(sentra.getSentraName());
                                        customerListResponse.setGroupName(group.getGroupName());
                                        List<DiscountMarginLoanMap> discountMarginLoanMapList = loanService.getDiscountMarginLoanMapByDiscountMarginId(discountMarginApproval.getDiscountMarginId());
                                        if (discountMarginLoanMapList != null) {
                                            if (!discountMarginLoanMapList.isEmpty()) {
                                                List<LoanListDiscountMarginResponse> loanListDiscountMarginResponsesMap = new ArrayList<>();
                                                for (DiscountMarginLoanMap discountMarginLoanMap : discountMarginLoanMapList) {
                                                    LoanPRS loanPRS = loanService.getLoanPRSById(discountMarginLoanMap.getLoanPrsId());
                                                    LoanListDiscountMarginResponse loanListDiscountMarginResponse = new LoanListDiscountMarginResponse();
                                                    loanListDiscountMarginResponse.setAppid(loanPRS.getAppId());
                                                    loanListDiscountMarginResponse.setLoanId(loanPRS.getLoanId().getLoanId().toString());
                                                    loanListDiscountMarginResponse.setLoanPrsId(loanPRS.getLoanPRSId().toString());
                                                    loanListDiscountMarginResponse.setSisaMargin(loanPRS.getOutstandingMarginAmount());
                                                    loanListDiscountMarginResponse.setSisaPokok(loanPRS.getRemainingOutstandingPrincipal());
                                                    loanListDiscountMarginResponsesMap.add(loanListDiscountMarginResponse);
                                                }
                                                customerListResponse.setLoanList(loanListDiscountMarginResponsesMap);
                                            }
                                        }
                                        List<DiscountMarginApprovalHistory> discountMarginApprovalHistoriesList = loanService.getDiscountMarginApprovalHistoryByDiscountMarginId(discountMarginApproval.getDiscountMarginId());
                                        List<DiscountMarginApprovalHistoryResponse> discountMarginApprovalHistoryListMap = new ArrayList<>();
                                        if (discountMarginApprovalHistoriesList != null) {
                                            if (!discountMarginApprovalHistoriesList.isEmpty()) {
                                                for (DiscountMarginApprovalHistory discountMarginApprovalHistory : discountMarginApprovalHistoriesList) {
                                                    DiscountMarginApprovalHistoryResponse discountMarginApprovalHistoryMap = new DiscountMarginApprovalHistoryResponse();
                                                    discountMarginApprovalHistoryMap.setLevel(discountMarginApprovalHistory.getLevel());
                                                    discountMarginApprovalHistoryMap.setDiscountMarginId(discountMarginApprovalHistory.getDiscountMarginId().toString());
                                                    discountMarginApprovalHistoryMap.setLimit(discountMarginApprovalHistory.getLimit());
                                                    discountMarginApprovalHistoryMap.setName(discountMarginApprovalHistory.getName());
                                                    discountMarginApprovalHistoryMap.setDate(discountMarginApprovalHistory.getDate());
                                                    discountMarginApprovalHistoryMap.setStatus(discountMarginApprovalHistory.getStatus());
                                                    discountMarginApprovalHistoryMap.setRole(discountMarginApprovalHistory.getRole());
                                                    discountMarginApprovalHistoryMap.setSupervisorId(discountMarginApprovalHistory.getSupervisorId());
                                                    if (discountMarginApprovalHistory.getTanggal() != null) {
                                                        discountMarginApprovalHistoryMap.setTanggal(formatter.format(discountMarginApprovalHistory.getTanggal()));
                                                    }
                                                    if (discountMarginApprovalHistory.getApprovedAmount() != null) {
                                                        discountMarginApprovalHistoryMap.setApprovedAmount(discountMarginApprovalHistory.getApprovedAmount().toString());
                                                    } else {
                                                        discountMarginApprovalHistoryMap.setApprovedAmount(discountMarginApprovalHistory.getApprovedAmount().ZERO.toString());
                                                    }
                                                    discountMarginApprovalHistoryListMap.add(discountMarginApprovalHistoryMap);
                                                }
                                                customerListResponse.setApprovalHistories(discountMarginApprovalHistoryListMap);
                                            } else {
                                                customerListResponse.setApprovalHistories(new ArrayList<>());
                                            }
                                        } else {
                                            customerListResponse.setApprovalHistories(new ArrayList<>());
                                        }
                                        customerListMap.add(customerListResponse);
                                    }
                                }
                            }
                        }
                    }
                }
                response.setCustomerList(customerListMap);
            }
        } catch (Exception e) {
            log.error("discountMarginList error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("discountMarginList error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_LOAN_REFRESH);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("discountMarginList saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("discountMarginList RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }
    }
}
