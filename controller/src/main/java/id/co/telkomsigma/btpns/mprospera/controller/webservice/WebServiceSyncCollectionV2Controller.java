package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingNonPRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.SyncNonPRSRequest;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceSyncCollectionV2Controller")
public class WebServiceSyncCollectionV2Controller extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ParameterService parameterService;

    @Autowired
    private UserService userService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private SavingService savingService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_NON_PRS_V2_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncNonPRSResponse syncNonPrs(@RequestBody final SyncNonPRSRequest request,
                                  @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final SyncNonPRSResponse response = new SyncNonPRSResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("nonPrs/sync/v2 Try to processing sync Collection request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                String paramApkVersion = parameterService.loadParamByParamName(WebGuiConstant.PARAMETER_CURRENT_VERSION,
                        "1.0");
                log.info("Starting Get User Data");
                User user = (User) userService.loadUserByUsername(request.getUsername());
                log.info("Finishing get user data");
                log.info("Starting Get Sentra for Non PRS");
                String page = request.getPage();
                Page<Sentra> sentraForNonPRS = sentraService.getSentraByWismaIdPaging(user.getOfficeCode(), page);
                log.info("Finishing Get Sentra for Non PRS");
                List<NonPRSResponseList> nonPrsListTemp = new ArrayList<>();
                log.info("START MAPPING DATA");
                for (Sentra sentra : sentraForNonPRS) {
                    NonPRSResponseList nonPrsSet = new NonPRSResponseList();
                    nonPrsSet.setSentraId(sentra.getSentraId().toString());
                    nonPrsSet.setSentraCode(sentra.getSentraCode());
                    nonPrsSet.setSentraName(sentra.getSentraName());
                    List<Customer> customerList = customerService.getBySentraId(sentra);
                    List<CustomerNonPRSListResponse> customerForNonPrsListTemp = new ArrayList<>();
                    for (Customer customer : customerList) {
                        if (loanService.isNonPRSCustomer(customer) && savingService.isNonPrsCustomerSaving(customer)) {
                            CustomerNonPRSListResponse customerNonPrs = new CustomerNonPRSListResponse();
                            if (customer.getCustomerId() != null) {
                                customerNonPrs.setCustomerId(customer.getCustomerId().toString());
                                customerNonPrs.setCustomerCif(customer.getCustomerCifNumber());
                                if (customer.getSwId() != null) {
                                    customerNonPrs.setSwId(customer.getSwId().toString());
                                }
                                if (customer.getGroup() != null) {
                                    customerNonPrs.setGroupId(customer.getGroup().getGroupId().toString());
                                    Group customerGroup = sentraService.getGroupByGroupId(customerNonPrs.getGroupId());
                                    customerNonPrs.setGroupName(customerGroup.getGroupName());
                                }
                            } else {
                                customerNonPrs.setCustomerId("");
                            }
                            customerNonPrs.setCustomerName(customer.getCustomerName());
                            List<LoanNonPRS> loanNonPrsList = loanService.getLoanNonPRSByCustomer(customer);
                            List<LoanNonPRSListResponse> loanNonPrsListTemp = new ArrayList<>();
                            for (LoanNonPRS loanNonPrs : loanNonPrsList) {
                                Loan loan = loanService.getByLoanId(loanNonPrs.getLoanId().getLoanId());
                                LoanNonPRSListResponse loanNonPrsSet = new LoanNonPRSListResponse();
                                loanNonPrsSet.setAppId(loan.getAppId());
                                loanNonPrsSet.setLoanCreatedDate(formatDateTime.format(loan.getCreatedDate()));
                                if (loanNonPrs.getCurrentMargin() != null) {
                                    loanNonPrsSet.setCurrentMargin(loanNonPrs.getCurrentMargin());
                                } else {
                                    loanNonPrsSet.setCurrentMargin(loanNonPrs.getCurrentMargin().ZERO);
                                }
                                if (loanNonPrs.getInstallmentAmount() != null) {
                                    loanNonPrsSet.setInstallmentAmount(loanNonPrs.getInstallmentAmount());
                                } else {
                                    loanNonPrsSet.setInstallmentAmount(loanNonPrs.getInstallmentAmount().ZERO);
                                }
                                if (loanNonPrs.getInstallmentPaymentAmount() != null) {
                                    loanNonPrsSet.setInstallmentPaymentAmount(loanNonPrs.getInstallmentPaymentAmount());
                                } else {
                                    loanNonPrsSet
                                            .setInstallmentPaymentAmount(loanNonPrs.getInstallmentPaymentAmount().ZERO);
                                }
                                if (loanNonPrs.getRemainingPrincipal() != null) {
                                    loanNonPrsSet.setRemainingPrincipal(loanNonPrs.getRemainingPrincipal());
                                } else {
                                    loanNonPrsSet.setRemainingPrincipal(loanNonPrs.getRemainingPrincipal().ZERO);
                                }
                                if (loanNonPrs.getMarginAmount() != null) {
                                    loanNonPrsSet.setMarginAmount(loanNonPrs.getMarginAmount());
                                } else {
                                    loanNonPrsSet.setMarginAmount(loanNonPrs.getMarginAmount().ZERO);
                                }
                                loanNonPrsSet.setLoanId(loan.getLoanId().toString());
                                loanNonPrsSet.setLoanNonPrsId(loanNonPrs.getLoanNonPRSId().toString());
                                if (loanNonPrs.getMeetingDate() != null) {
                                    loanNonPrsSet.setMeetingDate(formatter.format(loanNonPrs.getMeetingDate()));
                                } else {
                                    loanNonPrsSet.setMeetingDate("");
                                }
                                if (loanNonPrs.getDueDate() != null) {
                                    loanNonPrsSet.setDueDate(formatter.format(loanNonPrs.getDueDate()));
                                } else {
                                    loanNonPrsSet.setDueDate("");
                                }
                                loanNonPrsSet.setNonPrsPhoto(
                                        WebGuiConstant.TERMINAL_NON_PRS_PHOTO_REQUEST.replace("**/{apkVersion:.+}",
                                                "/" + paramApkVersion) + "/" + loanNonPrs.getLoanNonPRSId());
                                loanNonPrsSet.setOutstandingAmount(loanNonPrs.getOutstandingAmount());
                                if (loanNonPrs.getOutstandingCount() != null)
                                    loanNonPrsSet.setOutstandingCount(loanNonPrs.getOutstandingCount().toString());
                                if (loanNonPrs.getOutstandingReason() != null) {
                                    loanNonPrsSet.setOutstandingReason(
                                            loanNonPrs.getOutstandingReason().getParameterId().toString());
                                } else {
                                    loanNonPrsSet.setOutstandingReason("");
                                }
                                Date curdate = new Date();
                                if (loanNonPrs.getDueDate() != null) {
                                    Long diff = curdate.getTime() - loanNonPrs.getDueDate().getTime();
                                    Long diffDays = diff / (24 * 60 * 60 * 1000);
                                    loanNonPrsSet.setOverdueDays(diffDays.toString());
                                }
                                loanNonPrsSet.setPayCommitment(loanNonPrs.getPayCommitment());
                                if (loanNonPrs.getPeopleMeet() != null) {
                                    loanNonPrsSet.setPeopleMeet(loanNonPrs.getPeopleMeet().getParameterId().toString());
                                } else {
                                    loanNonPrsSet.setPeopleMeet("");
                                }
                                if (loanNonPrs.getCommitmentDate() != null) {
                                    loanNonPrsSet.setCommitmentDate(formatter.format(loanNonPrs.getCommitmentDate()));
                                } else {
                                    loanNonPrsSet.setCommitmentDate("");
                                }
                                if (loanNonPrs.getNextCommitmentDate() != null) {
                                    loanNonPrsSet.setNextCommitmentDate(formatter.format(loanNonPrs.getNextCommitmentDate()));
                                } else {
                                    loanNonPrsSet.setNextCommitmentDate("");
                                }
                                if (loanNonPrs.getUpdatedDate() != null) {
                                    loanNonPrsSet.setUpdatedDate(formatter.format(loanNonPrs.getUpdatedDate()));
                                } else {
                                    loanNonPrsSet.setUpdatedDate("");
                                }
                                if (loanNonPrs.getSubmitDate() != null) {
                                    loanNonPrsSet.setSubmitDate(formatter.format(loanNonPrs.getSubmitDate()));
                                } else {
                                    loanNonPrsSet.setSubmitDate("");
                                }
                                loanNonPrsSet.setCreatedBy(loanNonPrs.getCreatedBy());
                                loanNonPrsSet.setCreatedDate(formatter.format(loanNonPrs.getCreatedDate()));
                                loanNonPrsSet.setUpdatedBy(loanNonPrs.getUpdatedBy());
                                loanNonPrsSet.setInputMoney(loanNonPrs.getInputMoney());
                                loanNonPrsSet.setStatus(loanNonPrs.getStatus());
                                loanNonPrsListTemp.add(loanNonPrsSet);
                            }
                            if (!loanNonPrsListTemp.isEmpty()) {
                                customerNonPrs.setLoanList(loanNonPrsListTemp);
                            }
                            List<SavingNonPRS> savingNonPrsList = savingService.getSavingNonPrsByCustomer(customer);
                            List<SavingNonPRSListResponse> savingNonPrsListTemp = new ArrayList<>();
                            for (SavingNonPRS savingNonPrs : savingNonPrsList) {
                                SavingNonPRSListResponse savingNonPrsSet = new SavingNonPRSListResponse();
                                savingNonPrsSet.setAccountNumber(savingNonPrs.getAccountNumber());
                                savingNonPrsSet.setClearBalance(savingNonPrs.getClearBalance());
                                savingNonPrsSet.setDepositAmount(savingNonPrs.getDepositAmount());
                                savingNonPrsSet.setSavingNonPrsId(savingNonPrs.getSavingNonPrsId().toString());
                                savingNonPrsListTemp.add(savingNonPrsSet);
                            }
                            if (!savingNonPrsListTemp.isEmpty())
                                customerNonPrs.setSavingList(savingNonPrsListTemp);
                            customerForNonPrsListTemp.add(customerNonPrs);
                        }
                    }
                    if (!customerForNonPrsListTemp.isEmpty())
                        nonPrsSet.setCustomerList(customerForNonPrsListTemp);
                    nonPrsListTemp.add(nonPrsSet);
                }
                response.setSentraList(nonPrsListTemp);
                log.info("Total Page of Sentra : " + sentraForNonPRS.getTotalPages());
                if (sentraForNonPRS.getTotalPages() == 0) {
                    response.setTotalPage((String.valueOf(1)));
                } else {
                    response.setTotalPage((String.valueOf(sentraForNonPRS.getTotalPages())));
                }
                log.info("Finishing get non prs");
            }
        } catch (Exception e) {
            log.error("nonPrs/sync/v2 error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("nonPrsSyncV2 error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_NON_PRS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("nonPrs/sync/v2 RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("nonPrs/sync/v2 saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }
}
