package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.PRSParameterRequest;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.service.ParameterService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServicePRSParameter")
public class WebServicePRSParameter extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ParameterService paramService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_EARLY_TERMINATION_REASON_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PRSParameterResponse syncEarlyTerminationReason(
            @RequestBody final PRSParameterRequest request, @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final PRSParameterResponse response = new PRSParameterResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("earlyTerminationReason Try to processing SYNC Early Termination Reason request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                List<ReasonListResponse> paramList = new ArrayList<>();
                log.info("Get Parameter Alasan Pelunasan Dipercepat");
                List<PRSParameter> param = paramService.getPRSParam(WebGuiConstant.EARLY_TERMINATION_REASON_PARAM);
                for (PRSParameter reasonList : param) {
                    ReasonListResponse reason = new ReasonListResponse();
                    reason.setId(reasonList.getParameterId().toString());
                    reason.setDesc(reasonList.getParamValueString());
                    paramList.add(reason);
                }
                response.setReasonList(paramList);
            }
        } catch (Exception e) {
            log.error("earlyTerminationReason error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("earlyTerminationReason error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_EARLY_TERMINATION_REASON);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("earlyTerminationReason RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("earlyTerminationReason saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_DAYA_FILE_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PRSParameterResponse syncDayaFile(@RequestBody final PRSParameterRequest request,
                                      @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final PRSParameterResponse response = new PRSParameterResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("dayaFile Try to processing SYNC DAYA FILE LIST request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Get Parameter File Daya");
                List<PRSParameter> paramList = paramService.getPRSParam(WebGuiConstant.DAYA_FILE_PARAM);
                List<FileDayaList> fileDayaPathResponse = new ArrayList<>();
                for (PRSParameter paramFileDaya : paramList) {
                    FileDayaList fileDaya = new FileDayaList();
                    fileDaya.setFilePath("/sync/param/dayaFile/" + apkVersion + "/" + paramFileDaya.getParamDescription());
                    fileDayaPathResponse.add(fileDaya);
                }
                response.setFilePathList(fileDayaPathResponse);
            }
        } catch (Exception e) {
            log.error("dayaFile error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("dayaFile error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_DAYA_FILE);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("dayaFile RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("dayaFile saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_DAYA_FILE_ID_REQUEST, method = {RequestMethod.POST})
    public void getNonPRSPhoto(final HttpServletRequest request, final HttpServletResponse response,
                               @PathVariable("apkVersion") String apkVersion, @PathVariable("fileName") String fileName,
                               @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                               @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                               @RequestHeader(value = "username", required = true) final String username,
                               @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                               @RequestHeader(value = "imei", required = true) final String imei,
                               @RequestHeader(value = "longitude", required = true) final String longitude,
                               @RequestHeader(value = "latitude", required = true) final String latitude) {

        final PRSParameterRequest requestMapping = new PRSParameterRequest();
        requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
        requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
        requestMapping.setUsername(username);
        requestMapping.setSessionKey(sessionKey);
        requestMapping.setImei(imei);
        requestMapping.setLongitude(longitude);
        requestMapping.setLatitude(latitude);
        final PRSParameterResponse responseCode = new PRSParameterResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("sync/param/dayaFile/fileName Try to processing get file daya: " + jsonUtils.toJson(requestMapping));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
                try {
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Get File Daya");
                PRSParameter fileDaya = paramService.getFileDayaByFileName(fileName);
                if (fileDaya != null) {
                    log.info("SUCCESS GET FILE DAYA FROM DB");
                    responseCode.setFile(fileDaya.getParamValueByte());
                    try {
                        response.setContentType("application/octet-stream");
                        response.setHeader("Content-Disposition",
                                "attachment;filename=" + fileName);
                        response.getOutputStream().write(Base64.decodeBase64(responseCode.getFile()));
                        response.setContentLength(responseCode.getFile().length);
                        response.getOutputStream().flush();
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.RC_FILE_DAYA_NULL);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                    try {
                        response.getWriter().write((new JsonUtils().toJson(responseCode)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            log.error("sync/param/dayaFile/fileName error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("dayaFile error: " + e.getMessage());
            try {
                response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(imei);
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_FILE_DAYA);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(retrievalReferenceNumber.trim());
                        terminalActivity.setSessionKey(sessionKey);
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(username.trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(requestMapping).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(responseCode.getFile()).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("sync/param/dayaFile/fileName saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_PRAYER_PROMISE_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PRSParameterResponse syncPrayerPromise(@RequestBody final PRSParameterRequest request,
                                           @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final PRSParameterResponse response = new PRSParameterResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("prayerPromise Try to processing SYNC Prayer Promise request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Get Parameter Doa dan Janji");
                List<PRSParameter> param = paramService.getPRSParam(WebGuiConstant.PRAYER_PROMISE_PARAM);
                for (PRSParameter paramDoaJanji : param) {
                    if (paramDoaJanji.getParamName().equals("doa")) {
                        response.setPrayer(paramDoaJanji.getParamValueString());
                    } else if (paramDoaJanji.getParamName().equals("janji")) {
                        response.setPromise(paramDoaJanji.getParamValueString());
                    }
                }
            }
        } catch (Exception e) {
            log.error("prayerPromise error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("prayerPromise error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_PRAYER_PROMISE);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("prayerPromise RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("prayerPromise saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_NOT_ATTEND_REASON_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PRSParameterResponse syncNotAttendReason(@RequestBody final PRSParameterRequest request,
                                             @PathVariable("apkVersion") String apkVersion) throws IOException {


        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final PRSParameterResponse response = new PRSParameterResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("notAttendReason Try to processing SYNC Not Attend Reason request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                List<ReasonListResponse> paramList = new ArrayList<>();
                log.info("Get Parameter Alasan Tidak Hadir");
                List<PRSParameter> param = paramService.getPRSParam(WebGuiConstant.NOT_ATTEND_REASON_PARAM);
                for (PRSParameter reasonList : param) {
                    ReasonListResponse reason = new ReasonListResponse();
                    reason.setId(reasonList.getParameterId().toString());
                    reason.setName(reasonList.getParamValueString());
                    paramList.add(reason);
                }
                response.setReasonList(paramList);
            }
        } catch (Exception e) {
            log.error("notAttendReason error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("notAttendReason error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_NOT_ATTEND_REASON);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("notAttendReason RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("notAttendReason saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_PEOPLE_MEET_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PRSParameterResponse syncPeopleMeet(@RequestBody final PRSParameterRequest request,
                                        @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final PRSParameterResponse response = new PRSParameterResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("peopleMeet Try to processing SYNC People Meet request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                List<PeopleMeetResponse> paramList = new ArrayList<>();
                log.info("Get Parameter Orang yang ditemui");
                List<PRSParameter> param = paramService.getPRSParam(WebGuiConstant.PEOPLE_MEET_PARAM);
                for (PRSParameter reasonList : param) {
                    PeopleMeetResponse reason = new PeopleMeetResponse();
                    reason.setId(reasonList.getParameterId().toString());
                    reason.setName(reasonList.getParamValueString());
                    paramList.add(reason);
                }
                response.setPeopleList(paramList);
            }
        } catch (Exception e) {
            log.error("peopleMeet error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("peopleMeet error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_NOT_ATTEND_REASON);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("peopleMeet RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("peopleMeet saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_OUTSTANDING_REASON_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PRSParameterResponse syncOutstandingReason(@RequestBody final PRSParameterRequest request,
                                               @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final PRSParameterResponse response = new PRSParameterResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("outstandingReason Try to processing SYNC Outstanding Reason request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                List<ReasonListResponse> paramList = new ArrayList<>();
                log.info("Get Parameter Alasan Menunggak");
                List<PRSParameter> param = paramService.getPRSParam(WebGuiConstant.OUTSTANDING_REASON_PARAM);
                for (PRSParameter reasonList : param) {
                    ReasonListResponse reason = new ReasonListResponse();
                    reason.setId(reasonList.getParameterId().toString());
                    reason.setName(reasonList.getParamValueString());
                    paramList.add(reason);
                }
                response.setReasonList(paramList);
            }
        } catch (Exception e) {
            log.error("outstandingReason error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("outstandingReason error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_NOT_ATTEND_REASON);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("outstandingReason RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("outstandingReason saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_DENOM_REQUEST, method = { RequestMethod.POST })
    public @ResponseBody PRSParameterResponse syncDenom(@RequestBody final PRSParameterRequest request,
                                                        @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();

        final PRSParameterResponse response = new PRSParameterResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);

        try {
            try {
                log.trace("sync/param/denom Try to processing SYNC Denom request: "+jsonUtils.toJson(request));
            } catch (Exception e) {
            }
            log.trace("Validating Request");

            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);

            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.trace("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                List<DenomListResponse> paramList = new ArrayList<>();
                log.trace("Get Parameter Denom");
                List<PRSParameter> param = paramService.getPRSParam(WebGuiConstant.DENOM_PARAM);
                for (PRSParameter denomList : param) {
                    DenomListResponse denom = new DenomListResponse();
                    denom.setId(denomList.getParameterId().toString());
                    denom.setAmount(denomList.getParamValueString());
                    paramList.add(denom);
                }
                response.setDenomList(paramList);
            }
        } catch (Exception e) {
            log.error("sync/param/denom error: "+ e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("syncDenom error: "+ e.getMessage());
        }finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {

                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());

                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_DENOM);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());

                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);

                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);

                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);

                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("syncDenom RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("syncDenom saveTerminalActivityAndMessageLogs error: "+e.getMessage());
            }

            return response;
        }
    }
}
