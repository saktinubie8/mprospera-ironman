package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.claim.InsuranceClaim;
import id.co.telkomsigma.btpns.mprospera.model.claim.InsuranceClaimPhoto;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.ApproveInsuranceClaimRequest;
import id.co.telkomsigma.btpns.mprospera.request.PhotoRequest;
import id.co.telkomsigma.btpns.mprospera.request.SubmitInsuranceClaimRequest;
import id.co.telkomsigma.btpns.mprospera.request.SubmitPhotoInsuranceClaimRequest;
import id.co.telkomsigma.btpns.mprospera.request.SyncInsuranceClaimRequest;
import id.co.telkomsigma.btpns.mprospera.response.ApproveInsuranceClaimResponse;
import id.co.telkomsigma.btpns.mprospera.response.InsuranceClaimData;
import id.co.telkomsigma.btpns.mprospera.response.SubmitInsuranceClaimResponse;
import id.co.telkomsigma.btpns.mprospera.response.SubmitPhotoInsuranceClaimResponse;
import id.co.telkomsigma.btpns.mprospera.response.SyncInsuranceClaimResponse;
import id.co.telkomsigma.btpns.mprospera.service.InsuranceClaimService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

@Controller
public class WebserviceInsuranceClaimController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private InsuranceClaimService insuranceClaimService;

    @Autowired
    private RESTClient restClient;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.CLAIM_INSURANCE_SUBMIT_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SubmitInsuranceClaimResponse submitInsuranceClaim(@RequestBody final SubmitInsuranceClaimRequest request,
                                                      @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final SubmitInsuranceClaimResponse response = new SubmitInsuranceClaimResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("insuranceSubmit Try to processing submit claim insurance request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else if (!"REJECTED".equalsIgnoreCase(request.getStatus())
                    && !"DRAFT".equalsIgnoreCase(request.getStatus())
                    && !"APPROVED".equalsIgnoreCase(request.getStatus())) {
                response.setResponseCode(WebGuiConstant.RC_UNKNOWN_STATUS);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Execute Submit Data Insurance Claim");
                if (insuranceClaimService.validateSubmitInsuranceClaim(request, response)) {
                    try {
                        if (request.getAction().toUpperCase().equals("INSERT")) {
                            insuranceClaimService.submitInsuranceClaim(request, response);
                            log.info("Insurance ID = " + response.getInsuranceClaimId());
                            response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                            response.setResponseMessage(getMessage("webservice.rc.label." + response.getResponseCode()));
                        } else if (request.getAction().toUpperCase().equals("UPDATE")) {
                            InsuranceClaim insurance = insuranceClaimService.findById(Long.valueOf(request.getInsuranceClaimId()));
                            if (insurance != null && WebGuiConstant.STATUS_APPROVED.equals(insurance.getStatus())) {
                                log.error("STATUS ALREADY APPROVED");
                                response.setResponseCode(WebGuiConstant.RC_ALREADY_APPROVED);
                                label = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(label);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            } else if (insurance != null && WebGuiConstant.STATUS_APPROVED_MS.equals(insurance.getStatus())) {
                                log.error("STATUS ALREADY APPROVED");
                                response.setResponseCode(WebGuiConstant.RC_ALREADY_APPROVED);
                                label = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(label);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            } else if (insurance != null && WebGuiConstant.STATUS_CLOSED.equals(insurance.getStatus())) {
                                log.error("STATUS ALREADY CLOSED");
                                response.setResponseCode(WebGuiConstant.RC_ALREADY_CLOSED);
                                label = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(label);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            } else if (insurance != null && WebGuiConstant.STATUS_REJECTED.equals(insurance.getStatus())) {
                                log.error("STATUS ALREADY REJECTED");
                                response.setResponseCode(WebGuiConstant.RC_ALREADY_REJECTED);
                                label = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(label);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            } else if (insurance != null && WebGuiConstant.STATUS_CANCEL.equals(insurance.getStatus())) {
                                log.error("STATUS ALREADY CANCEL");
                                response.setResponseCode(WebGuiConstant.RC_ALREADY_CANCEL);
                                label = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(label);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            }
                            if (request.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                                insuranceClaimService.rejectInsuranceClaim(request);
                            } else {
                                log.info("START MAPPING DATA");
                                SubmitInsuranceClaimRequest esbReq = new SubmitInsuranceClaimRequest();
                                esbReq.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
                                esbReq.setTransmissionDateAndTime(request.getTransmissionDateAndTime());
                                esbReq.setImei(request.getImei());
                                esbReq.setUsername(request.getUsername());
                                esbReq.setCustomerNumber(request.getCustomerNumber());
                                esbReq.setSpouseFlag(request.getSpouseFlag());
                                esbReq.setCustomerFlag(request.getCustomerFlag());
                                esbReq.setDeathCause(request.getDeathCause());
                                esbReq.setCreatedDate(formatter.format(insurance.getCreatedDate()));
                                esbReq.setCreatedBy(insurance.getCreatedBy());
                                esbReq.setApprovedBy(request.getApprovedBy());
                                esbReq.setApprovedDate(request.getApprovedDate());
                                esbReq.setOldStatus("9");
                                esbReq.setNewStatus("15");
                                log.info("SEND REQUEST TO ESB");
                                SubmitInsuranceClaimResponse esbResponse = restClient.claimInsurance(esbReq);
                                if (esbResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                                    log.info("GET SUCCESS RESPONSE FROM ESB");
                                    insuranceClaimService.approveInsuranceClaim(request);
                                }
                                response.setResponseCode(esbResponse.getResponseCode());
                                response.setResponseMessage(esbResponse.getResponseMessage());
                            }
                        }
                    } catch (Exception e) {
                        response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                        e.printStackTrace();
                        response.setResponseMessage(e.getMessage());
                    }
                } else {
                    response.setResponseMessage(getMessage("webservice.rc.label." + response.getResponseCode()));
                }
            }
        } catch (Exception e) {
            log.error("insuranceSubmit error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("insuranceSubmit error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_INSURANCE_CLAIM);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("insuranceSubmit RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("insuranceSubmit saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.CLAIM_INSURANCE_SYNC_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncInsuranceClaimResponse syncInsuranceClaim(@RequestBody final SyncInsuranceClaimRequest request,
                                                  @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final SyncInsuranceClaimResponse response = new SyncInsuranceClaimResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("insuranceSync Try to processing sync claim insurance request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Execute Submit Data Insurance Claim");
                List<InsuranceClaimData> insuranceClaimData = insuranceClaimService.getInsuranceClaimData(request);
                response.setClaimList(insuranceClaimData);
            }
        } catch (Exception e) {
            log.error("insuranceSync error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("insuranceSync error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_CLAIM_INSURANCE_REQUEST);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);

                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("insuranceSync RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("insuranceSync saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.CLAIM_INSURANCE_PHOTO_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SubmitPhotoInsuranceClaimResponse submitInsuranceClaimPhoto(@RequestBody final SubmitPhotoInsuranceClaimRequest request,
                                                                @PathVariable("apkVersion") String apkVersion) {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final SubmitPhotoInsuranceClaimResponse response = new SubmitPhotoInsuranceClaimResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("insurancePhoto Try to processing submit photo claim insurance request: " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Execute Submit Photo Insurance Claim");
                insuranceClaimService.submitPhotoInsuranceClaim(request);
            }
        } catch (Exception e) {
            log.error("insurancePhoto error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("insurancePhoto error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_PHOTO_INSURANCE_CLAIM);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("insurancePhoto RESPONSE MESSAGE : " + response);
            } catch (Exception e) {
                log.error("insurancePhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.CLAIM_INSURANCE_APPROVE_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    ApproveInsuranceClaimResponse approveInsuranceClaim(@RequestBody final ApproveInsuranceClaimRequest request,
                                                        @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final ApproveInsuranceClaimResponse response = new ApproveInsuranceClaimResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("insuranceApprove Try to processing approve claim insurance request : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Execute Submit Data Insurance Claim");
                if (insuranceClaimService.validateApproveInsuranceClaim(request, response)) {
                    insuranceClaimService.approveInsuranceClaim(request);
                } else {
                    response.setResponseMessage(getMessage("webservice.rc.label." + response.getResponseCode()));
                }
            }
        } catch (Exception e) {
            log.error("insuranceApprove error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("insuranceApprove error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_APPROVE_INSURANCE_CLAIM);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("insuranceApprove RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("insuranceApprove saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.CLAIM_INSURANCE_PHOTO_LINK_REQUEST, method = {RequestMethod.POST})
    public void getIdPhoto(final HttpServletRequest request, final HttpServletResponse response,
                           @PathVariable("apkVersion") String apkVersion, @PathVariable("id") String id,
                           @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                           @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                           @RequestHeader(value = "username", required = true) final String username,
                           @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                           @RequestHeader(value = "imei", required = true) final String imei) throws Exception {

        final PhotoRequest requestMapping = new PhotoRequest();
        requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
        requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
        requestMapping.setUsername(username);
        requestMapping.setSessionKey(sessionKey);
        requestMapping.setImei(imei);
        final SubmitPhotoInsuranceClaimResponse responseCode = new SubmitPhotoInsuranceClaimResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("insurancePhotoLink Try to processing get link photo claim insurance request: " + jsonUtils.toJson(requestMapping));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
                try {
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Get Insurance Claim Photo");
                InsuranceClaimPhoto photo = insuranceClaimService.getInsuranceClaimPhotoById(id);
                if (photo != null) {
                    try {
                        response.setContentType("image/jpg;charset=utf-8");
                        response.getOutputStream().write(Base64.decodeBase64(photo.getPhoto()));
                        response.getOutputStream().flush();
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.ID_PHOTO_NULL);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                    try {
                        response.getWriter().write((new JsonUtils().toJson(responseCode)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            log.error("insurancePhotoLink error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("insurancePhotoLink error: " + e.getMessage());
            try {
                response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(imei);
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_ID_PHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(retrievalReferenceNumber.trim());
                        terminalActivity.setSessionKey(sessionKey);
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(username.trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(requestMapping).getBytes());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(responseCode).getBytes());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("insurancePhotoLink RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("insurancePhotoLink saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
    }
}
