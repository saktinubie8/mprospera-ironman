package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import id.co.telkomsigma.btpns.mprospera.constant.PRSConstant;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.prs.MutationTrxTabel;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.saving.SavingPRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.*;
import id.co.telkomsigma.btpns.mprospera.response.ApprovalPRSResponse;
import id.co.telkomsigma.btpns.mprospera.response.LoginResponse;
import id.co.telkomsigma.btpns.mprospera.response.ProsperaLoginResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceNewApprovalPRSController")
public class WebServiceNewApprovalPRSController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ProsperaEncryptionService prosperaEncryptionService;

    @Autowired
    private UserService userService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private RESTClient restClient;

    @Autowired
    private PRSService prsService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private SavingService savingService;

    @Autowired
    private ParameterService parameterService;

    @Value("${allow.submit:1}")
    private String allowSubmit;

    @Value("${allow.approve:1}")
    private String allowApprove;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_PREPARE_PRS_APPROVAL, method = {RequestMethod.POST})
    public @ResponseBody
    LoginResponse doApprovePreparePRS(@RequestBody LoginRequest request,
                                      @PathVariable("apkVersion") String versionNumber) throws KeyManagementException, NoSuchAlgorithmException, IOException {

        LoginResponse response = new LoginResponse();
        try {
            log.info("preparePRSApproval INCOMING PREPARE PRS MESSAGE : " + jsonUtils.maskingPasswordJson(jsonUtils.toJson(request)));
            ProsperaLoginRequest prosperaLogin = new ProsperaLoginRequest();
            prosperaLogin.setUsername(request.getUserApproval().trim());
            try {
                prosperaLogin.setPassword(prosperaEncryptionService.encryptField(request.getPassword().trim()));
            } catch (Exception e1) {
                log.error("PASSWORD ENCRYPTION ERROR..., " + e1.getMessage());
            }
            prosperaLogin.setImei(request.getImei().trim());
            prosperaLogin.setTransmissionDateAndTime(request.getTransmissionDateAndTime().trim());
            prosperaLogin.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber().trim());
            log.info("User to approve : " + request.getUserApproval());
            User user = userService.findUserByUsername(request.getUserApproval());
            if (user == null) {
                response.setResponseCode(WebGuiConstant.RC_USERNAME_NOTEXISTS);
                String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(labelFailed);
                return response;
            }
            if (request.getUserApproval().equals(request.getUsername())) {
                log.error("USER UNAUTHORIZED");
                response.setResponseCode(WebGuiConstant.USER_CANNOT_APPROVE);
                String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(labelFailed);
                log.info("RESPONSE MESSAGE : " + response.toString());
                return response;
            }
            String districtCode = null;
            // access url and get the response
            log.info("Try to send login request");
            if (request.getSentraList() != null) {
                for (PRSPrepare sentra : request.getSentraList()) {
                    Sentra sentraForPrs = sentraService.findBySentraId(sentra.getSentraId());
                    if (user != null) {
                        if (sentraForPrs != null) {
                            if (!user.getOfficeCode().equals(sentraForPrs.getLocationId())) {
                                response.setResponseCode(WebGuiConstant.UNREGISTERED_USER);
                                response.setResponseMessage(getMessage("webservice.rc.label." + response.getResponseCode()));
                                return response;
                            }
                        }
                    } else {
                        response.setResponseCode(WebGuiConstant.RC_USERNAME_NOTEXISTS);
                        response.setResponseMessage(getMessage("webservice.rc.label." + response.getResponseCode()));
                        return response;
                    }
                }
            }
            ProsperaLoginResponse prosperaLoginResponse = restClient.login(prosperaLogin);
            log.info("Get Response from ESB : " + prosperaLoginResponse.getResponseCode());
            if (request.getType() == null) {
                response.setResponseCode(WebGuiConstant.USER_CANNOT_APPROVE);
                response.setResponseMessage(getMessage("webservice.rc.label." + response.getResponseCode()));
                return response;
            }
            if (request.getType().equals("1")) {
                if (request.getSentraList() != null) {
                    for (PRSPrepare sentra : request.getSentraList()) {
                        Sentra sentraForPrs = sentraService.findBySentraId(sentra.getSentraId());
                        if (sentraForPrs != null) {
                            PRS prs = prsService.getPrsBySentraId(sentraForPrs);
                            if (prs != null) {
                                if (prs.getStatus().equals(WebGuiConstant.STATUS_INPROCESS)) {
                                    response.setResponseCode(prosperaLoginResponse.getResponseCode());
                                    response.setResponseMessage(prosperaLoginResponse.getResponseMessage());
                                    return response;
                                } else {
                                    if (sentra.getBringMoney() != null && !"".equals(sentra.getBringMoney())) {
                                        prs.setBringMoney(new BigDecimal(sentra.getBringMoney()));
                                        prs.setStatus(WebGuiConstant.STATUS_INPROCESS);
                                    }
                                    prsService.savePRS(prs);
                                }
                            }
                        }
                    }
                }
                MutationTrxTabel mutationTrxTabel = new MutationTrxTabel();
                mutationTrxTabel.setApprovedBy(request.getUserApproval());
                mutationTrxTabel.setApprovedDate(new Date());
                if (request.getUserPs() != null)
                    mutationTrxTabel.setCreatedBy(request.getUserPs());
                if (request.getTotalBringMoney() != null && !"".equals(request.getTotalBringMoney())) {
                    if (NumberUtils.isNumber(request.getTotalBringMoney())) {
                        mutationTrxTabel.setBringMoney(new BigDecimal(request.getTotalBringMoney()));
                    } else {
                        mutationTrxTabel.setBringMoney(BigDecimal.ZERO);
                    }
                } else {
                    mutationTrxTabel.setBringMoney(BigDecimal.ZERO);
                }
                mutationTrxTabel.setTrxType("PRS-Keluar");
                mutationTrxTabel.setBusinessDate(new Date());
                prsService.saveMutation(mutationTrxTabel);
            }
            response.setResponseCode(prosperaLoginResponse.getResponseCode());
            response.setResponseMessage(prosperaLoginResponse.getResponseMessage());
        } catch (Exception e) {
            log.error("preparePRSApproval error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("preparePRSApproval error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_PREPARE_PRS_APPROVAL);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey("");
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("preparePRSApproval RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("preparePRSApproval saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_APPROVAL_PRS_V2_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    ApprovalPRSResponse approvalPRS(@RequestBody final ApprovalPRSRequest request,
                                    @PathVariable("apkVersion") String apkVersion) throws IOException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final ApprovalPRSResponse response = new ApprovalPRSResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("prsApprovalV2 Try to processing approval PRS request : " + jsonUtils.maskingPasswordJson(jsonUtils.toJson(request)));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                log.info("Validating success");
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("trying to approve PRS ");
                log.info("Check User " + request.getUserMs());
                User userMs = userService.findUserByUsername(request.getUserMs());
                if (userMs == null) {
                    log.error("USER NOT FOUND");
                    response.setResponseCode(WebGuiConstant.RC_FAILED);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    log.info("RESPONSE MESSAGE : " + response.toString());
                    return response;
                }
                if (request.getUserMs().toLowerCase().trim().equals(request.getUsername().toLowerCase().trim())) {
                    log.error("USER UNAUTHORIZED");
                    response.setResponseCode(WebGuiConstant.USER_CANNOT_APPROVE);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    log.info("RESPONSE MESSAGE : " + response.toString());
                    return response;
                }
                log.info("FIND PRS WITH ID " + request.getPrsId());
                PRS prs = prsService.getPRSById(Long.parseLong(request.getPrsId()));
                if (prs == null) {
                    // idprs tidak ditemukan, RC gagal
                    log.error("PRS NOT FOUND");
                    response.setResponseCode(WebGuiConstant.RC_PRS_NULL);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                } else {
                    if (request.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                        log.info("PRS REJECTED");
                        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        String labelReject = getMessage("webservice.rc.label." + WebGuiConstant.RC_ALREADY_REJECTED);
                        response.setResponseMessage(labelReject);
                        prs.setStatus(request.getStatus());
                        prs.setUpdateBy(request.getUserMs());
                        prs.setUpdateDate(new Date());
                        prsService.savePRS(prs);
                        log.info("RESPONSE MESSAGE : " + response.toString());
                        return response;
                    }
                    if (prs.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                        log.error("PRS ALREADY APPROVED");
                        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        String labelFailed = getMessage("webservice.rc.label." + WebGuiConstant.RC_ALREADY_APPROVED);
                        response.setResponseMessage(labelFailed);
                        log.info("RESPONSE MESSAGE : " + response.toString());
                        return response;
                    } else if (prs.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                        log.error("PRS ALREADY REJECTED");
                        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        String labelFailed = getMessage("webservice.rc.label." + WebGuiConstant.RC_ALREADY_REJECTED);
                        response.setResponseMessage(labelFailed);
                        log.info("RESPONSE MESSAGE : " + response.toString());
                        return response;
                    }
                    log.info("MAPPING FINISHED");
                    try {
                        log.info("INCOMING LOGIN APPROVAL MESSAGE : " + request.toString());
                        ProsperaLoginRequest prosperaLogin = new ProsperaLoginRequest();
                        prosperaLogin.setUsername(request.getUserMs().trim());
                        try {
                            prosperaLogin.setPassword(prosperaEncryptionService.encryptField(request.getPassword().trim()));
                        } catch (Exception e1) {
                            log.error("PASSWORD ENCRYPTION ERROR..., " + e1.getMessage());
                        }
                        prosperaLogin.setImei(request.getImei().trim());
                        prosperaLogin.setTransmissionDateAndTime(request.getTransmissionDateAndTime().trim());
                        prosperaLogin.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber().trim());
                        log.info("Try to send login request");
                        if (prosperaLogin.getUsername() != null) {
                            userMs = userService.findUserByUsername(prosperaLogin.getUsername());
                            if (prs.getSentraId() != null) {
                                log.debug("sentra id : " + prs.getSentraId());
                                Sentra sentraPrs = sentraService.findBySentraId(prs.getSentraId().getSentraId().toString());
                                log.debug("Sentra from DB : " + sentraPrs);
                                if (!sentraPrs.getLocationId().equals(userMs.getOfficeCode())) {
                                    log.error("UNAUTHORIZED USER");
                                    response.setResponseCode(WebGuiConstant.UNREGISTERED_USER);
                                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                                    response.setResponseMessage(labelFailed);
                                    log.info("RESPONSE MESSAGE : " + response.toString());
                                    return response;
                                }
                            } else {
                                log.error("UNKNOWN SENTRA");
                                response.setResponseCode(WebGuiConstant.UNKNOWN_SENTRA_ID);
                                String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(labelFailed);
                                log.info("RESPONSE MESSAGE : " + response.toString());
                                return response;
                            }
                        } else {
                            log.error("USER NOT REGISTERED");
                            response.setResponseCode(WebGuiConstant.UNREGISTERED_USER);
                            String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(labelFailed);
                            log.info("RESPONSE MESSAGE : " + response.toString());
                            return response;
                        }
                        log.info("SEND LOGIN APPROVAL MESSAGE TO ESB");
                        ProsperaLoginResponse prosperaLoginResponse = restClient.login(prosperaLogin);
                        log.info("GET RESPONSE FROM ESB : " + prosperaLoginResponse.getResponseCode());
                        response.setResponseCode(prosperaLoginResponse.getResponseCode());
                        response.setResponseMessage(prosperaLoginResponse.getResponseMessage());
                        if (!prosperaLoginResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                            log.error("SENDING FAILED");
                            response.setResponseCode(prosperaLoginResponse.getResponseCode());
                            response.setResponseMessage(prosperaLoginResponse.getResponseMessage());
                            log.info("RESPONSE MESSAGE : " + response.toString());
                            return response;
                        }
                        // update mutation trx table
                        if ("1".equals(allowApprove)) {
                            MutationTrxTabel mt = prsService.findMutationByPrs(prs.getPrsId());
                            mt.setApprovedBy(request.getUserMs());
                            if (request.getTotalPRSMoney() != null) {
                                if (!"".equals(request.getTotalPRSMoney())) {
                                    mt.setTotalPRSMoney(new BigDecimal(request.getTotalPRSMoney()));
                                } else {
                                    mt.setTotalPRSMoney(new BigDecimal(request.getTotalPRSMoney()).ZERO);
                                }
                            }
                            mt.setApprovedDate(new Date());
                            if ("1".equals(allowApprove)) {
                                prs.setStatus(request.getStatus());
                            }
                            prs.setUpdateBy(request.getUserMs());
                            prs.setUpdateDate(new Date());
                            prsService.saveMutation(mt);
                            prsService.savePRS(prs);
                        }
                    } catch (KeyManagementException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            log.error("prsApprovalV2 error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("prsApprovalV2 error: " + e.getMessage());
        } finally {
            try {
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_APPROVAL_PRS);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save message masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save message keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (JsonMappingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JsonGenerationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("prsApprovalV2 RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("prsApprovalV2 saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @SuppressWarnings("unused")
    private ESBSubmitPrsRequest composeESBMessage(PRS prs, ApprovalPRSRequest request) {
        List<ESBCustomer> esbCustomer = new ArrayList<>();
        List<CustomerPRS> customerPrsList = customerService.getCustomerPRSByPrsId(prs);
        List<LoanPRS> loanPrsList = new ArrayList<>();
        List<SavingPRS> savingPrsList = new ArrayList<>();
        PRSParameter notAttendReason = new PRSParameter();
        ESBSubmitPrsRequest forESB = new ESBSubmitPrsRequest();
        try {
            forESB.setImei(request.getImei());
            forESB.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
            forESB.setTransmissionDateAndTime(request.getTransmissionDateAndTime());
            forESB.setUsername(prs.getCreatedBy());
            forESB.setPrsId(request.getPrsId());
            forESB.setSentraId(prs.getSentraId().getProsperaId().toString());
            forESB.setPrsDate(formatter.format(prs.getPrsDate()));
            forESB.setTokenChallenge(request.getTokenChallenge());
            forESB.setTokenResponse(request.getTokenResponse());
            if (prs.getPsCompanion() != null) {
                forESB.setPsCompanion(prs.getPsCompanion());
            } else {
                forESB.setPsCompanion("");
            }
            if (customerPrsList.isEmpty()) {
                forESB.setCustomerList(null);
            } else {
                for (CustomerPRS customerPrs : customerPrsList) {
                    ESBCustomer customer = new ESBCustomer();
                    customer.setCustomerId(customerPrs.getCustomerId().getProsperaId());
                    if (customerPrs.getIsAttend() != null) {
                        if (customerPrs.getIsAttend() == true) {
                            customer.setIsAttend("true");
                        } else {
                            customer.setIsAttend("false");
                            if (customerPrs.getNotAttendReason() != null) {
                                notAttendReason = parameterService.findByParameterId(customerPrs.getNotAttendReason());
                                if (notAttendReason != null) {
                                    if ("Sakit".equalsIgnoreCase(notAttendReason.getParamValueString())) {
                                        customer.setNotAttendReason(PRSConstant.sakit);
                                    } else if ("Izin".equalsIgnoreCase(notAttendReason.getParamValueString())) {
                                        customer.setNotAttendReason(PRSConstant.izin);
                                    } else if ("Lain-lain".equalsIgnoreCase(notAttendReason.getParamValueString())) {
                                        customer.setNotAttendReason(PRSConstant.lain_lain);
                                    } else {
                                        customer.setNotAttendReason(PRSConstant.alpha);
                                    }
                                } else {
                                    customer.setNotAttendReason(PRSConstant.alpha);
                                }
                            } else {
                                customer.setNotAttendReason(PRSConstant.alpha);
                            }
                        }
                    } else {
                        customer.setIsAttend("");
                    }
                    List<ESBLoanCustomer> esbLoanList = new ArrayList<>();
                    loanPrsList = loanService.getLoanPRSByPrsId(prs, customerPrs);
                    if (loanPrsList.isEmpty()) {
                        customer.setLoanList(null);
                    } else {
                        for (LoanPRS loanPrs : loanPrsList) {
                            ESBLoanCustomer loan = new ESBLoanCustomer();
                            loan.setAppid(loanPrs.getAppId());
                            loan.setDisbursementFlag(String.valueOf(loanPrs.getDisbursementFlag()));
                            if (loanPrs.getInstallmentPaymentAmount() != null) {
                                loan.setInstallmentPaymentAmount(loanPrs.getInstallmentPaymentAmount().toString()
                                        .replace(".", ""));
                                if (loan.getInstallmentPaymentAmount().equals("000"))
                                    loan.setInstallmentPaymentAmount("");
                            }
                            if (loanPrs.getIsEarlyTermination() != null) {
                                if (loanPrs.getIsEarlyTermination()) {
                                    loan.setIsEarlyTermination("true");
                                    if (loanPrs.getEarlyTerminationReason() != null)
                                        loan.setEarlyTerminationReason(loanPrs.getEarlyTerminationReason());
                                } else {
                                    loan.setIsEarlyTermination("false");
                                }
                            } else {
                                loan.setIsEarlyTermination("");
                            }
                            if (loanPrs.getIsEarlyTerminationAdHoc() != null) {
                                if (loanPrs.getIsEarlyTerminationAdHoc()) {
                                    log.debug("Lunas Dipercepat");
                                    loan.setIsEarlyTermination("true");
                                    if (loanPrs.getEarlyTerminationReason() != null)
                                        loan.setEarlyTerminationReason(loanPrs.getEarlyTerminationReason());
                                }
                            } else {
                                loan.setIsEarlyTermination("");
                            }
                            if (loanPrs.getLoanPRSId() != null)
                                loan.setLoanId(loanPrs.getLoanId().getProsperaId());
                            if (loanPrs.getMarginDiscountDeviationFlag() != null) {
                                if (loanPrs.getMarginDiscountDeviationFlag() == true) {
                                    loan.setMarginDiscountDeviationFlag("true");
                                    if (loanPrs.getMarginDiscountDeviationPercentage() != null) {
                                        loan.setMarginDiscountDeviationPercentage(loanPrs
                                                .getMarginDiscountDeviationPercentage().toString()
                                                .replace(".00", ""));
                                        if (loan.getMarginDiscountDeviationPercentage().equals("000"))
                                            loan.setMarginDiscountDeviationPercentage("");
                                    }
                                    if (loanPrs.getMarginDiscountDeviationAmount() != null) {
                                        loan.setMarginDiscountDeviationAmount(loanPrs
                                                .getMarginDiscountDeviationAmount().toString().replace(".", ""));
                                        if (loan.getMarginDiscountDeviationAmount().equals("000")) {
                                            loan.setMarginDiscountDeviationFlag("false");
                                            loan.setMarginDiscountDeviationAmount("");
                                        }
                                    }
                                } else {
                                    loan.setMarginDiscountDeviationFlag("false");
                                }
                            } else {
                                loan.setMarginDiscountDeviationFlag("");
                            }
                            if (loanPrs.getUseEmergencyFund() != null) {
                                if (loanPrs.getUseEmergencyFund() == true) {
                                    loan.setUseEmergencyFund("true");
                                } else {
                                    loan.setUseEmergencyFund("false");
                                }
                            } else {
                                loan.setUseEmergencyFund("");
                            }
                            esbLoanList.add(loan);
                        }
                    }
                    customer.setLoanList(esbLoanList.toArray(new ESBLoanCustomer[0]));
                    List<ESBSaving> esbSavingList = new ArrayList<>();
                    savingPrsList = savingService.getSavingByPRSIdAndCustomerId(prs, customerPrs);
                    for (SavingPRS savingPrs : savingPrsList) {
                        ESBSaving saving = new ESBSaving();
                        saving.setAccountId(savingPrs.getAccountIdProspera().toString());
                        saving.setAccountNumber(savingPrs.getAccountNumber());
                        if (savingPrs.getClearBalance() != null) {
                            saving.setClearBalance(savingPrs.getClearBalance().toString().replace(".", ""));
                            if (saving.getClearBalance().equals("000"))
                                saving.setClearBalance("");
                        }
                        if (savingPrs.getDepositAmount() != null) {
                            saving.setDepositAmount(savingPrs.getDepositAmount().toString().replace(".", ""));
                            if (saving.getDepositAmount().equals("000"))
                                saving.setDepositAmount("");
                        } else {
                            saving.setDepositAmount("");
                        }
                        if (savingPrs.getIsCloseSavingAccount() != null) {
                            if (savingPrs.getIsCloseSavingAccount() == true) {
                                saving.setIsCloseSavingAccount("true");
                            } else {
                                saving.setIsCloseSavingAccount("false");
                            }
                        } else {
                            saving.setIsCloseSavingAccount("");
                        }
                        if (savingPrs.getNextWithdrawalAmountPlan() != null) {
                            saving.setNextWithdrawalAmountPlan(savingPrs.getNextWithdrawalAmountPlan().toString()
                                    .replace(".", ""));
                            if (saving.getNextWithdrawalAmountPlan().equals("000"))
                                saving.setNextWithdrawalAmountPlan("");
                        }
                        if (savingPrs.getWithdrawalAmount() != null) {
                            saving.setWithdrawalAmount(savingPrs.getWithdrawalAmount().toString().replace(".", ""));
                            if (saving.getWithdrawalAmount().equals("000"))
                                saving.setWithdrawalAmount("");
                        }
                        esbSavingList.add(saving);
                    }
                    customer.setSavingList(esbSavingList.toArray(new ESBSaving[0]));
                    esbCustomer.add(customer);
                }
            }
            forESB.setCustomerList(esbCustomer.toArray(new ESBCustomer[0]));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("composeESBMessage error: " + e.getMessage());
            return null;
        }
        return forESB;
    }
}
